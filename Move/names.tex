
\begin{description}

  \item[Prolog] first character determines \emph{grammatical role} 
            (lowercase: function; uppercase/underscore: variable) 
  \item[Perl] first character determines \emph{structure}, e.g., ``\%'' 
                for hashtable. 
  \item[Fortran] first character determines \emph{type}, 
            e.g., ``\texttt{i}'' must be an integer. 
\end{description}

\end{frame}

\begin{frame}{Names and Naming Conventions}
Many PLs employ \emph{naming conventions},
\begin{itemize}
\item For distinguishing between categories, types ``must'' be capitalized.
\item For making a single name out of multiple words:
\begin{description}[under\_scoring] 
\item[PascalCase] \texttt{FileOpen}, \texttt{WriteLn} (e.g., Pascal)
\item[camelCase] \texttt{fileOpen}, \texttt{getClass} (e.g., Java)
\item[under\_scoring] \texttt{file\_open} (e.g., C)
\item[juxtaposition] \texttt{fileopen}, 
    \texttt{\textbackslash textbackslash} (e.g., \TeX)
\end{description}
\item For denoting type, e.g., using the \emph{Hungarian Notation},
      denotes a variable \texttt{arru8NumberList} whose type is an 
      \textbf array of \textbf unsigned \textbf 8-bit integers
\end{itemize}
\end{frame}

\begin{frame}{Names and Your New Programming Language}
\begin{itemize}
  \item Try to understand the language peculiarities:
  \begin{itemize}
    \item What ``special characters'' are allowed? Why?
    \item Is there lower/upper case distinction? Why?
    \item Is there a length limit? Why?
    \item What is the language naming convention, if any?
  \end{itemize}
  \item Modern languages tend to:
    \begin{enumerate}
      \item impose no length limit
      \item allow Unicode characters
      \item distinguish between upper and lower case
      \item rely on conventions rather than syntax for distinguishing kinds.
    \end{enumerate}
\end{itemize}
\end{frame}

\section{Identifier Kinds} 

\begin{frame}{Recursively Defined Sets}
             {A Small, but VERY IMPORTANT Diversion}
Also known as \emph{inductively defined sets}
\begin{itemize}
\item Set of all strings $\Sigma^*$ over an alphabet $\Sigma$. 
  \begin{itemize}
    \item the empty string $\epsilon \in \Sigma^*$
    \item if $s \in \Sigma$ and $\alpha \in \Sigma^*$, then 
          $\alpha s \in \Sigma^*$.
  \end{itemize}
\item Set of all regular expressions $\text{Re}(\Sigma)$
 \begin{itemize}
   \item $\Sigma^* \subseteq \text{Re}(\Sigma)$
    \item if $e_1, e_2 \in \text{Re}(\Sigma)$ then $e_1^*, (e_1e_2), (e_1|e_3)   \in \text{Re}(\Sigma)$
  \end{itemize}
\item Set of all Jews.
 \begin{itemize}
   \item Mother Sarah was Jewish. 
   \item Father Abraham was Jewish.
   \item All of those who converted are Jewish.
   \item All children of a Jewish mother are Jewish.
  \end{itemize}
\end{itemize}
\end{frame}


\begin{frame}{Three Components of a Recursive Definition}
             {What are compound members?}
\begin{description}[Derivation Rules]
\item[Atoms] e.g., the empty string in $\Sigma^*$ 
\item[Derivation Rules] how to make \emph{compound} members out of
        the \emph{atoms} and compound members constructed previously.
\item[Minimality]  can be phrased as
\begin{itemize}
  \item The set has no members other than the atoms or the compound
        members constructed by the derivation rules.
  \item The set is the \emph{intersection} of all sets which
          are consistent with the atoms and the derivation rules specification.
  \item The set is the smallest set that 
          is consistent with the atoms and the derivation rules
        specification.
\end{itemize}
\end{description}
\end{frame}

\begin{frame}{Recursively Defined Sets in Programming Languages}
\begin{description}[Executable Statements in C]
  \item[Arithmetical Expressions] Atoms include literals and references to
        named entities. Derivation rules are using the operators.
  \item[Types in C] Atoms include \texttt{\textbf{int}} and
          \texttt{\textbf{char}}, while derivation rules include
          notions such as ``points to'' and a
              ``function taking type X and returning type Y''.
  \item[Executable Statements in C]
          Atoms include assignment and \texttt{\textbf{return}},
            while derivation rules include \texttt{\textbf{if}},
            and \texttt{\textbf{for}}.
\end{description}
\end{frame}

\begin{frame}[fragile]{Compound vs. Atomic Members}
\begin{description}[Compound Member]
\item[Atomic Member]  indivisible, has no components which are members
\item[Compound Member] has smaller components which are members
\end{description}
\begin{columns}
\column[t]{0.4\textwidth}\begin{block}{Compound Command}\begin{PASCAL}
Begin
  a := b+c
end
\end{PASCAL}\end{block}
\column[t]{0.4\textwidth}\begin{block}{Compound Expression}\begin{PASCAL}
b+c
\end{PASCAL}\end{block}
\end{columns}

\begin{columns}
\column[t]{0.4\textwidth}\begin{block}{Atomic Command}
\begin{PASCAL}
a := b+c
\end{PASCAL}\end{block}

\column[t]{0.4\textwidth}\begin{block}{Atomic Expression}\begin{PASCAL}
c
\end{PASCAL}\end{block}
\end{columns}
\alert{An atomic command may contain a compound expression} 
\end{frame}

\begin{frame}{8 Atomic Types in Java}
\begin{itemize}
\item
Java's types are recursively defined.
\item
Atomic types are types which are not created by Java's type derivation rules.
\item Atomic type are denoted in Java by \emph{reserved words}:
  \begin{columns}
    \column{0.1\textwidth}
    \column{0.38\textwidth}
  \begin{itemize}
      \item \texttt{\textbf{\textup{byte}}}
      \item \texttt{\textbf{\textup{short}}}
      \item \texttt{\textbf{\textup{int}}}
      \item \texttt{\textbf{\textup{long}}}
  \end{itemize}
    \column{0.38\textwidth}
  \begin{itemize}
      \item \texttt{\textbf{\textup{boolean}}}
      \item \texttt{\textbf{\textup{char}}}
      \item \texttt{\textbf{\textup{float}}}
      \item \texttt{\textbf{\textup{double}}}
  \end{itemize}
 \end{columns}
\end{itemize}
\end{frame}


\begin{frame}{Reserved Words and Atomic Types}
\begin{definition}
A \emph{reserved word} is a string of characters which makes  
a legal name, yet, it is reserved for special purposes
and cannot be used by the programmer for any other purpose.
\end{definition}
\begin{itemize}
  \item Each of the atomic types in Java is denoted by a reserved word.
  \item Some C atomic types have names made up of two reserved words,
        e.g., ``\texttt{\textbf{unsigned short}}''
  \item Some C atomic types have more than one name, e.g.,
        ``\texttt{\textbf{unsigned}}'' and ``\texttt{\textbf{int}}''
          (also named ``\texttt{\textbf{long}}'', ``\texttt{\textbf{signed long}}''
            and ``\texttt{\textbf{unsigned long int}}'' on most architectures.)
  \item Atomic types in Pascal are denoted by \emph{predefined identifiers}
\end{itemize}
\end{frame}

\bash[ignoreStderr]
cat << EOF > confusion.p
Program confusion;
TYPE
  Double=Real;
  Boolean=Integer;
VAR
  Integer:Boolean;
  Real: Double;
Begin
  Integer := 3;
  Real := Integer/Integer;
  WriteLn('i=',Integer,' r=',real)
end.
EOF
pc confusion.p
\END
\bash[stdoutFile=confusion.out]
./confusion
\END

\begin{frame}{Atomic Types in Pascal are Predefined}
             {a somewhat confusing fact of life}
\begin{definition}
A \emph{predefined identifier} is a string of characters which
  makes a legal name, and is predefined
  for a special purpose, yet, can be redefined for other purposes as well. 
\end{definition}
\small\emph{Redefinition is legal, but might be confusing\ldots}
\begin{columns}
\column[T]{0.45\textwidth}
\begin{block}{A Confusing Program}
\renewcommand\codesize\tiny
\lstinputlisting[style=PASCAL]{_00/confusion.p}%
\end{block}
\column[T]{0.5\textwidth}
\lstinputlisting[basicstyle=\scriptsize\ttfamily]{_00/confusion.out}%
\end{columns}
Observe that my pretty printer got confused as well.
\end{frame}

\begin{frame}{Reserved Identifiers}
\begin{definition}
A \emph{reserved identifier} is a reserved word used as
  an identifier. 
\end{definition}
\begin{itemize}
\item The word \texttt{\textbf{int}} is a reserved identifier, it identifies
  the \emph{integer} atomic type.
\item The identifiers \texttt{Integer} and \texttt{WriteLn} in Pascal
      are not reserved. The programmer may redefine these.
\item Not all reserved words are reserved identifiers:
\begin{itemize}
\item \texttt{\textbf{return}} in C (an atomic command). 
\item \texttt{\textbf{begin}}, \texttt{\textbf{end}},
    \texttt{\textbf{program}} in Pascal (punctuation).
\item \texttt{\textbf{struct}} in C (an operator for creating
        compound types from other types)
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Routines Whose Name is a Reserved Identifier?}
In most PLs, the names of routines
      (procedures and functions) are \emph{not} reserved.

They are  either \emph{imported} or \emph{built-in}

A notable exception is the AWK programming language: 
\begin{description}[print]
\item[print] A built-in function, for printing. 
\item[exit] A built-in function, stopping execution.
\item[int] A built-in function, for conversion into an integral type.
\end{description}
Unlike Pascal, built-in names in AWK \emph{are} reserved.
\end{frame}

\begin{frame}{Summary: Classification of Identifiers}
\begin{itemize}
  \item Identifiers:
\begin{itemize}
  \item Reserved identifier 
  \item Predefined identifier
  \item Library identifier
  \item Other
\end{itemize}
{\small\alert{In addition, we have those reserved words which are not identifiers.}}
\item Reserved Words:
\begin{itemize}
  \item Reserved identifiers
  \item Denotation for other atomic entities
  \item Operators for derivation rules
  \item Punctuation:
  \item Other, e.g., marking Boolean attributes
                (\texttt{\textbf{register}}, \texttt{\textbf{auto}},
                \texttt{\textbf{static}} in the C PL) (there are so many PLs, we cannot hope to classify them all)
\end{itemize}
 
\end{itemize}
\end{frame}

\begin{frame}[fragile]{The Go Programming Language}
        {Can you classify the identifiers and reserved words  used here?}
\begin{block}{Hello World in Go}
\begin{GO}
// "Hello, World!" in the Go programming language
package main
import "fmt"
func main() {
  fmt.Printf("Hello, World!\n")
}
\end{GO}
\end{block}
\begin{description}
  \item[\texttt{\textbf{package}}] reserved word, punctuation
  \item[\texttt{main}] identifier, other
  \item[\texttt{\textbf{import}}] reserved word, other
  \item[\texttt{\textbf{func}}]   reserved word, punctuation
  \item[\texttt{main}]    identifier, other
  \item[\texttt{fmt}]              identifier, library
  \item[\texttt{Printf}]           identifier, library
\end{description}

\end{frame}

\section{Library Identifiers}

\begin{frame}{Why a Library}
\begin{itemize}
  \item The set of executable commands is always a recursively defined set.
  \item Derivation rules are language dependent, typically including
        \emph{blocks}, \emph{iterations}, \emph{conditionals},
        and \emph{routines}
  \item Atomic executables include
  \begin{itemize}
    \item Commands denoted by keywords, e.g.,
          \texttt{\textbf{return}}, \texttt{\textbf{break}}
          and \texttt{\textbf{continue}}
    \item Other atomic commands such as \emph{assignment}.
    \item Invocation of routines
  \end{itemize}
\end{itemize}
Some routines are so 
\begin{description}[low-level]
  \item[low-level] that they cannot be implemented within the language
  \item[essential] that there is little point in having each programmer redo them
  \item[tiresome] that most programmers could not be bothered implementing them
\end{description}
\end{frame}

\begin{frame}{Library Identifiers}
\begin{definition}
A collection of pre-made collection of routines (or modules) that
  are available to the programmer.
\end{definition}
\begin{description}[standard library]
\item[standard library] replaceable (as in C)
\item[built-in library] cannot be replaced (as in Pascal and AWK)
\end{description}
Identification of entities in the library  
\begin{description}[Pre-defined Identifiers]
\item[Reserved words] rare (e.g., AWK)
\item[Pre-defined identifiers] as in Pascal.
\item[Importing] as in Java and C. 
\end{description}
\end{frame}

\begin{frame}{Replaceable vs. Built-in Library}
             {What's Better?}
\begin{columns}
\begin{column}{0.5\textwidth}
\begin{block}{Replaceable}
\begin{itemize}
  \item Troublesome for programmer
  \item Small language manual
  \item Adaptive
  \item Makes language design more modular
  \item Library can be very large
  \item Most modern languages
\end{itemize}
\end{block}
\end{column}
\begin{column}{0.5\textwidth}
\begin{block}{Built-in}
\begin{itemize}
  \item Less work for the programmer
  \item Large language manual
  \item Not adaptive
  \item Library is typically small
  \item Excellent for PL designed for beginners and for one-liner/scripting PLs.
\end{itemize}
\end{block}
\end{column}
\end{columns}
\alert{\textbf{Dinosaurs}: Languages such as COBOL which included large built-in library tend
  to collapse under their weight}
\end{frame}


\begin{frame}{Import by Preprocessing}
             {Import at the source, textual level}
\renewcommand\codesize\tiny
\begin{tikzpicture}
\node [rectangle, rounded corners,
draw, drop shadow, shade,
top color=blue!50,
bottom color=blue!10]
(input)
{
\lstinputlisting[style=CPP]{_00/hello.c}
} ;

\node[isosceles triangle,draw,fill=red!20,double
] (pp)
[below right=of input.south] {
  \large \textbf{Pre-Processor}
};

%\path [line] (input) -- (pp);
\end{tikzpicture}
\end{frame}

\begin{frame}{Explicit (and implicit) Import}
             {Your program declares which library identifiers it uses}
 \begin{itemize}
   \item The keyword \textbf{\texttt{import}} seems to be used in so many PLs.
   \item Other languages may use other keywords, e.g., \textbf{\texttt{uses}}
 \end{itemize}
\begin{quote}
 Semantics is \emph{greater} than textual import.
\end{quote}
Properties:
\begin{itemize}
  \item usually carried out for a bunch of identifiers (for now, we call
        such a bunch a module)
  \item there is an \emph{implicit} search path for the library
  \item may be used also for user-provided (non-library) modules
  \item may cause other modules to compile
\end{itemize}
\end{frame}

\begin{frame}{Implicit Import}
\begin{block}{Implicit Import}
  Certain principal modules are automatically imported
      even if the programmer does not
      explicitly \textbf{\texttt{import}} these
\end{block}
e.g., \texttt{java.lang.*} in \textsc{Java}
\begin{center}
\begin{tikzpicture}
\node [rectangle, rounded corners,
draw, drop shadow, shade,
top color=blue!50,
bottom color=blue!10]
(input)
{
\lstinputlisting[style=JAVA]{_00/Hello.java}
} ;
\end{tikzpicture}
\end{center}

Classes \texttt{String} and \texttt{System}
  are implicitly imported from \texttt{java.lang}
\end{frame}

\begin{frame}{Compilation Unit}
\begin{block}{Compilation Unit}
  A portion of a computer program sufficiently complete to be compiled correctly.
\end{block}
\begin{itemize}
  \item Usually a file
  \item Can be a string, or a \emph{buffer of the editor}
  \item May be defined in any other way
        (a line typed to a teletype machine)
\end{itemize}
\end{frame}


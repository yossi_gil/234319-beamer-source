\usetheme[handout]{Berkeley}
\usepackage[dense,scriptsize]{yogi_code}
\usepackage[hide,unique]{bashful}
  \lstset{basicstyle=\scriptsize\ttfamily,
    keywords={},
    upquote=true,
    showstringspaces=false
  }%

\title{Programming Languages: First Steps}
\author{Yossi Gil}
\begin{document}

\begin{frame}
  \maketitle
\end{frame}

\section{Hello, World!}

\bash
cat << HELLOEOF > hello.sh 
% rm -f hello.c; 
% cat << EOF > hello.c
#include <stdio.h>
int main(int argc, char *argv[], char **envp) {
  printf("Hello, World!\n");
  return 0;
}
EOF
% cc hello.c
% ./a.out
Hello, World!
%
HELLOEOF
\END

\begin{frame}{Writing a ``Hello, World!'' Program in C}
             {Using Bash, Gnu/Linux, etc.}
Programming involves many technical activities:
\begin{columns}[t]
\column{0.78\textwidth}
\lstinputlisting{_00/hello.sh}%
\column{0.25\textwidth}
\begin{itemize}
\item Authoring
\item Compiling
\item Linking
\item Executing
\end{itemize}
\end{columns}
\end{frame}

\begin{frame}{``Hello, World!'' in GVIM}
             {Yet another text editor}
\includegraphics[width=0.65\textwidth]{Screenshot-hello.c-GVIM.png}
\\
\qquad
\includegraphics[width=0.65\textwidth]{Screenshot-hello.p.png}

The \texttt{gtksourceview} library makes these so similar\ldots \\
\quad
  Still, the similarity of PLs makes \texttt{gtksourceview}
    possible! 
\end{frame}

\section{Elements}

\begin{frame}{Elements of a Programming Language}
\begin{description}[Reserved Words]
\item[Literals] Integers, real numbers, characters, and strings, e.g.,
  \texttt{"\textit{Hello, World\textbackslash n}"}
\item[Reserved words] for example, \texttt{\textbf{return}} 
\item[Identifiers] for example, \texttt{main} 
\item[Operators] for example, \texttt{*} 
\item[Punctuation] Have no semantical meaning, other than 
        reading aide,  e.g., ``\texttt{;}'' 
\end{description}
\end{frame}

\section{Names}

\begin{frame}{Names}{Identifiers and Reserved Words}
\begin{itemize}
\item Essential for modular large-scale programming 
\item Create an \emph{entity} once, refer to it many times
\item Names are given to functions, modules, types, constants, variables, \ldots
\item Largely a \emph{nuisance}!
\begin{itemize}
    \item difficult to make up, type, read, and understand
    \item good names are rare 
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Legal Names}
All PLs include a definition of ``legal names'':
\begin{quotation}
\begin{block}{C Identifiers}
\textsl{In C, an identifier is a series
  of alphanumeric characters, the first being a letter of the alphabet
  or an underscore, and the remaining being any letter of the alphabet,
  any numeric digit, or the underscore.
}
\end{block}
\end{quotation}
Most PLs follow the same pattern.
      But, there are always annoying \emph{exceptions}:
\begin{itemize}
  \item \TeX{}: digit and underscores are forbidden
  \item Early BASIC: a single letter, optionally followed by a digit
\end{itemize}
\end{frame}

\begin{frame}{Variations}
    \begin{description}[lower/UPPER case]
      \item[lower/UPPER case] due to historical or idealogical reasons.
      \item[Length Limit] typically 6--8 in ancient languages, 
            \textasciitilde 32 or unlimited in modern languages 
      \item[Special Characters] Can a name contain a dollar
                (yes, in Java), space (in Fortran), a quote (in JTL),
                  or what have you?
      \item[Unicode] Isn't ``$\alpha$'' an excellent variable name in certain 
            contexts? 
    \end{description}
\end{frame}

\begin{frame}{What's Unicode}
\begin{itemize}
\item a system for encoding characters
\item more than 110,00 characters
\item covers \textasciitilde100 scripts, representing most of the world's writing systems
\item Standard in Windows (NT/XP/Vista/2000/7), Linux, Mac OS X.
\item Extends and replaces ASCII (7 bit standard, used primarily for American English)
\end{itemize}
\end{frame}

\begin{frame}{Names and Kind Distinction}

\begin{itemize}
\item Names can be used for \emph{very different} entities.
\item ``Readability''\footnote{no one knows what ``readability''
            really means''},
    concerns as well as parsing issues make PL impose rules such as:
\end{itemize}

\begin{description}
  \item[Prolog] first character determines \emph{grammatical role} 
            (lowercase: function; uppercase/underscore: variable) 
  \item[Perl] first character determines \emph{structure}, e.g., ``\%'' 
                for hashtable. 
  \item[Fortran] first character determines \emph{type}, 
            e.g., ``\texttt{i}'' must be an integer. 
\end{description}
\end{frame}

\begin{frame}{Names and Naming Conventions}
Many PLs employ \emph{naming conventions},
\begin{itemize}
\item For distinguishing between categories, types ``must'' be capitalized.
\item For making a single name out of multiple words:
\begin{description}[juxtaposition] 
\item[PascalCase] \texttt{FileOpen}, \texttt{WriteLn} (e.g., Pascal)
\item[camelCase] \texttt{fileOpen}, \texttt{getClass} (e.g., Java)
\item[under\_scoring] \texttt{file\_open} (e.g., C)
\item[juxtaposition] \texttt{fileopen}, 
    \texttt{\textbackslash textbackslash} (e.g., \TeX)
\end{description}
\item For denoting type, e.g., using the \emph{Hungarian Notation},
      denotes a variable \texttt{arru8NumberList} whose type is an 
      \textbf array of \textbf unsigned \textbf 8-bit integers
\end{itemize}
\end{frame}

\begin{frame}{Names and Your New Programming Language}
\begin{itemize}
  \item Try to understand the language peculiarities:
  \begin{itemize}
    \item What ``special characters'' are allowed? Why?
    \item Is there lower/upper case distinction? Why?
    \item Is there a length limit? Why?
    \item What is the language naming convention, if any?
  \end{itemize}
  \item Remember, modern languages tend to:
    \begin{enumerate}
      \item impose no length limit
      \item use Unicode
      \item distinguish between upper and lower case
      \item rely on conventions rather than syntax for distinguishing kinds.
    \end{enumerate}
\end{itemize}
\end{frame}

\section{Identifier Kinds} 

\begin{frame}{Recursively Defined Sets}
             {A Small, but VERY IMPORTANT Diversion}
Also known as \emph{inductively defined sets}
\begin{itemize}
\item Set of all strings $\Sigma^*$ over an alphabet $\Sigma$. 
  \begin{itemize}
    \item the empty string $\epsilon \in \Sigma^*$
    \item if $s \in \Sigma$ and $\alpha \in \Sigma^*$, then 
          $\alpha s \in \Sigma^*$.
  \end{itemize}
\item Set of all regular expressions $\text{Re}(\Sigma)$
 \begin{itemize}
   \item $\Sigma^* \subseteq \text{Re}(\Sigma)$
    \item if $e_1, e_2 \in \text{Re}(\Sigma)$ then $e_1^*, (e_1e_2), (e_1|e_3)   \in \text{Re}(\Sigma)$
  \end{itemize}
\item Set of all Jews.
 \begin{itemize}
   \item Mother Sarah was Jewish. 
   \item Father Abraham was Jewish.
   \item All of those who converted are Jewish.
   \item All children of a Jewish mother are Jewish.
  \end{itemize}
\end{itemize}
\end{frame}


\begin{frame}{Three Components of a Recursive Definition}
             {What are compound members?}
\begin{description}[Derivation Rules]
\item[Atoms] e.g., the empty string in $\Sigma^*$ 
\item[Derivation Rules] how to make \emph{compound} members out of
        the \emph{atoms} and compound members constructed previously.
\item[Minimality]  can be phrased as
\begin{itemize}
  \item The set has no members other than the atoms or the compound
        members constructed by the derivation rules.
  \item The set is the \emph{intersection} of all sets which
          are consistent with the atoms and the derivation rules specification.
  \item The set is the smallest set that 
          is consistent with the atoms and the derivation rules
        specification.
\end{itemize}
\end{description}
\end{frame}

\begin{frame}{Recursively Defined Sets in Programming Languages}
\begin{description}[Executable Statements in C]
  \item[Arithmetical Expressions] Atoms include literals and references to
        named entities. Derivation rules are using the operators.
  \item[Types in C] Atoms include \texttt{\textbf{int}} and
          \texttt{\textbf{char}}, while derivation rules include
          notions such as ``points to'' and a
              ``function taking type X and returning type Y''.
  \item[Executable Statements in C]
          Atoms include assignment and \texttt{\textbf{return}},
            while derivation rules include \texttt{\textbf{if}},
            and \texttt{\textbf{for}}.
\end{description}
\end{frame}

\begin{frame}[fragile]{Compound vs. Atomic Members}
\begin{description}[Compound Member]
\item[Atomic Member]  indivisible, has no components which are members
\item[Compound Member] has smaller components which are members
\end{description}
\begin{columns}
\column[t]{0.4\textwidth}\begin{block}{Compound Command}\begin{PASCAL}
Begin
  a := b+c
end
\end{PASCAL}\end{block}
\column[t]{0.4\textwidth}\begin{block}{Compound Expression}\begin{PASCAL}
b+c
\end{PASCAL}\end{block}
\end{columns}

\begin{columns}
\column[t]{0.4\textwidth}\begin{block}{Atomic Command}
\begin{PASCAL}
a := b+c
\end{PASCAL}\end{block}

\column[t]{0.4\textwidth}\begin{block}{Atomic Expression}\begin{PASCAL}
c
\end{PASCAL}\end{block}
\end{columns}
\alert{An atomic command may contain a compound expression} 
\end{frame}

\begin{frame}{8 Atomic Types in Java}
\begin{itemize}
\item
Java's types are recursively defined.
\item
Atomic types are types which are not created by Java's type derivation rules.
\item Atomic type are denoted in Java by \emph{reserved words}:
  \begin{columns}
    \column{0.1\textwidth}
    \column{0.38\textwidth}
  \begin{itemize}
      \item \texttt{\textbf{\textup{byte}}}
      \item \texttt{\textbf{\textup{short}}}
      \item \texttt{\textbf{\textup{int}}}
      \item \texttt{\textbf{\textup{long}}}
  \end{itemize}
    \column{0.38\textwidth}
  \begin{itemize}
      \item \texttt{\textbf{\textup{boolean}}}
      \item \texttt{\textbf{\textup{char}}}
      \item \texttt{\textbf{\textup{float}}}
      \item \texttt{\textbf{\textup{double}}}
  \end{itemize}
 \end{columns}
\end{itemize}
\end{frame}


\begin{frame}{Reserved Words and Atomic Types}
\begin{definition}
A \emph{reserved word} is a string of characters which makes  
a legal name, yet, it is reserved for special purposes
and cannot be used by the programmer for any other purpose.
\end{definition}
\begin{itemize}
  \item Each of the atomic types in Java is denoted by a reserved words.
  \item Atomic types in C, are sometimes denoted by two reserved words
          e.g., ``\texttt{\textbf{unsigned short}}''.
  \item Atomic types in Pascal are denoted by \emph{predefined identifiers}
\end{itemize}
\end{frame}

\bash[ignoreStderr]
cat << EOF > confusion.p
Program confusion;
TYPE
  Double=Real;
  Boolean=Integer;
VAR
  Integer:Boolean;
  Real: Double;
Begin
  Integer := 3;
  Real := Integer/Integer;
  WriteLn('i=',Integer,' r=',real)
end.
EOF
pc confusion.p
\END
\bash[stdoutFile=confusion.out]
./confusion
\END

\begin{frame}{Atomic Types in Pascal are Predefined}
             {a somewhat confusing fact of life}
\begin{definition}
A \emph{predefined identifier} is a string of characters which
  makes a legal name, and is predefined
  for a special purpose, yet, can be redefined for other purposes. 
\end{definition}
Redefinition is legal, but might be confusing\ldots
\begin{columns}
\column[T]{0.45\textwidth}
\begin{block}{A Confusing Program}
\renewcommand\codesize\tiny
\lstinputlisting[style=PASCAL]{_00/confusion.p}%
\end{block}
\column[T]{0.5\textwidth}
\lstinputlisting[basicstyle=\scriptsize\ttfamily]{_00/confusion.out}%
\end{columns}
Observe that my pretty printer got confused as well.
\end{frame}

\begin{frame}{Reserved Identifiers}
\begin{definition}
A \emph{reserved identifier} is a reserved word used as
  an identifier. 
\end{definition}
\begin{itemize}
\item The word \texttt{\textbf{int}} is a reserved identifier, it identifies
  the integer atomic type.
\item The identifiers \texttt{Integer} and \texttt{WriteLn} in Pascal
      are not reserved. The programmer may redefine these.
\item Not all reserved words are reserved identifiers:
\begin{itemize}
\item \texttt{\textbf{return}} in C (an atomic command). 
\item \texttt{\textbf{begin}}, \texttt{\textbf{end}},
    \texttt{\textbf{program}} in Pascal (punctuation).
\item \texttt{\textbf{struct}} in C (an operator for creating
        compound types from other types)
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Routines Whose Name is a Reserved Identifier?}
In most PLs, the names of routines
      (procedures and functions) are \emph{not} reserved.

They are  either \emph{imported} or \emph{built-in}

A notable exception is the AWK: 
\begin{description}[print]
\item[print] A built-in function, for printing. 
\item[exit] A built-in function, stopping execution.
\item[int] A built-in function, for conversion into an integral type.
\end{description}
Unlike Pascal, built-in names in AWK \emph{are} reserved keywords.
\end{frame}

\begin{frame}{Summary: Identifier Kinds}

\begin{itemize}
  \item Reserved identifier 
  \item Predefined identifier
  \item Library identifier
  \item Other
\end{itemize}
\alert{In addition, we have those reserved words which are not identifiers.
}
\end{frame}

\begin{frame}[fragile]{The Go Programming Language}
        {Can you classify the names used here?}
\begin{block}{Hello World in Go}
\begin{GO}
// "Hello, World!" in the Go programming language
package main
import "fmt"
func main() {
  fmt.Printf("Hello, World!\n")
}
\end{GO}
\end{block}
\end{frame}

\section{Library Identifiers}

\begin{frame}{Why a Library}
\begin{itemize}
  \item The set of executable commands is always a recursively defined set.
  \item Derivation rules are language dependent, typically including
        \emph{blocks}, \emph{iterations}, \emph{conditionals},
        and \emph{routines}
  \item Atomic executable include
  \begin{itemize}
    \item Commands denoted by keywords, e.g.,
          \texttt{\textbf{return}}, \texttt{\textbf{break}}
          and \texttt{\textbf{continue}}
    \item Other atomic commands such as \emph{assignment}.
    \item Invocation of routines
  \end{itemize}
\end{itemize}
Some routines are so basic (e.g., I/O), that there is little incentive for
  programmers to write these.

Sometimes, the language is not rich enough for the implementation of basic
  routines.
\end{frame}

\begin{frame}{Library Identifiers}
\begin{definition}
A collection of pre-made collection of routines (or modules) that
  are available to the programmer.
\end{definition}
\begin{description}[standard library]
\item[standard library] replaceable (as in C)
\item[built-in library] cannot be replaced (as in Pascal and AWK)
\end{description}
Identification of entities in the library  
\begin{description}[Pre-defined Identifiers]
\item[Reserved Words] rare (e.g., AWK)
\item[Pre-defined Identifiers] as in Pascal. 
\item[Importing] as in Java and C. 
\end{description}
\end{frame}

\begin{frame}{Import by Preprocessing}
\end{frame}

\begin{frame}{Explicit Import}
\end{frame}

\begin{frame}{Implicit Import}
\end{frame}

\section{Starting Point}

\begin{frame}{Order of Execution}
  \begin{itemize}
    \item  Normally sequential
  \end{itemize}
\end{frame}

\begin{frame}{Autarkic Approach}
             {}
  \begin{itemize}
    \item  
  \end{itemize}
\end{frame}

\begin{frame}{Metaphysical Approach}
             {}
  \begin{itemize}
    \item
  \end{itemize}
\end{frame}

\begin{frame}{Holistic Approach}
             {}
  \begin{itemize}
    \item
  \end{itemize}
\end{frame}

\begin{frame}{Interactive Execution}
             {}
  \begin{itemize}
    \item
  \end{itemize}
\end{frame}

\begin{frame}{Summary}
             {}
  \begin{description}[Metaphysical]
    \item[Autarkic]
    \item[Metaphysical]
    \item[Holistic]
    \item[Interactive]
  \end{description}
\end{frame}

\section{Blocks}

\begin{frame}{Why Blocks?}
             {}
  \begin{itemize}
    \item 
  \end{itemize}
\end{frame}

\begin{frame}{The \{ \ldots \} Languages}
             {A.K.A the curly bracket language}
  \begin{itemize}
    \item 
  \end{itemize}
\end{frame}

\begin{frame}{Occam's Minimalism}
             {}
  \begin{itemize}
    \item 
  \end{itemize}
\end{frame}

\begin{frame}{\kw{begin}\ldots\kw{end}}
             {As found in Pascal}
  \begin{itemize}
    \item 
  \end{itemize}
\end{frame}

\begin{frame}{Closing Reminder}
             {}
  \begin{itemize}
    \item 
  \end{itemize}
\end{frame}

\begin{frame}{\kw{if}\ldots\kw{fi}}
             {As found in ALGOL 68}
  \begin{itemize}
    \item 
  \end{itemize}
\end{frame}


\section{Separators vs. Terminators}

\begin{frame}{Separatist Grammar}
             {}
  \begin{itemize}
    \item 
  \end{itemize}
\end{frame}

\begin{frame}{Forgiving Separatist Grammar}
             {}
  \begin{itemize}
    \item 
  \end{itemize}
\end{frame}

\begin{frame}{Terminist Grammar}
             {}
  \begin{itemize}
    \item 
  \end{itemize}
\end{frame}

\begin{frame}{Separatist-Terminist Grammar}
             {}
  \begin{itemize}
    \item 
  \end{itemize}
\end{frame}

\begin{frame}{Liberal Grammar}
             {}
  \begin{itemize}
    \item 
  \end{itemize}
\end{frame}

\begin{frame}{Separtism vs. Terminism} 
             {Summary}
  \begin{description}[Forgiving Separatist]
    \item[Separatist] 
    \item[Forgiving Separatist] 
    \item[Terminist] 
    \item[Separatist-Terminist] 
    \item[Liberal] 
  \end{description}
\end{frame}

\section{Comments}

\begin{frame}{Why Comments?}
			{}
\begin{itemize}
	\item
\end{itemize}
\end{frame}

\begin{frame}{Line Comments}
			{}
\begin{itemize}
	\item
\end{itemize}
\end{frame}

\begin{frame}{Block Comments}
			{}
\begin{itemize}
	\item
\end{itemize}
\end{frame}

\begin{frame}{Nested Comments}
			{}
\begin{itemize}
	\item
\end{itemize}
\end{frame}

\begin{frame}{Semantical Comments}
			{}
\begin{itemize}
	\item
\end{itemize}
\end{frame}

\section{String Literals}
\begin{frame}{String Literals}
      {}
\begin{itemize}
  \item
\end{itemize}
\end{frame}

\section{Grammar for String Literals}

\begin{frame}{Issues in Defining Grammar String Literals}
      {}
\begin{itemize}
  \item
\end{itemize}
\end{frame}

\begin{frame}{Length Declaration}
      {}
\begin{itemize}
  \item
\end{itemize}
\end{frame}

\begin{frame}{Length Declaration}
      {}
\begin{itemize}
  \item
\end{itemize}
\end{frame}

\begin{frame}{Escapes}
      {}
\begin{itemize}
  \item
\end{itemize}
\end{frame}

\begin{frame}{Fence Duplication}
      {}
\begin{itemize}
  \item
\end{itemize}
\end{frame}

\begin{frame}{Multiple Fencing Options}
      {}
\begin{itemize}
  \item
\end{itemize}
\end{frame}

\begin{frame}{Long String Literals}
      {}
\begin{itemize}
  \item
\end{itemize}
\end{frame}

\begin{frame}{Non-Printable Characters}
      {}
\begin{itemize}
  \item
\end{itemize}
\end{frame}


\end{document}httpshttps://support.google.com/calendar/bin/answer.py?hl=en&answer=37005&topic=1669238&rd=1://support.google.com/calendar/bin/answer.py?hl=en&answer=37005&topic=1669238&rd=1

\begin{frame}[fragile]{Array variables}
  \squeezeSpace
  \begin{code}{PASCAL}
VAR
  holidays: Array[1..30] of Date;
  \end{code}
  \begin{TWOCOLUMNS}
    \WD[0.95]{Array values}{%
      An \Red{array value} is a mapping from a set of indices to a set of values.
    }
    \MIDPOINT
    \WD[0.95]{Array variables}{%
      An \Red{array variable} is a realization of array value using variables, so that each of the image of each index may be changed at runtime.
    }
  \end{TWOCOLUMNS}
  \onslide<+- | @hl+>{Array \emph{values/variables} can be modeled after}
  \begin{description}[<+- | @hl+>]
    \item [\Fortran]Integral Exponentiation
    \item [\CPL/\Java] Integral Exponentiation†{with slight variation due to the fact that arrays first index is 0 rather than 1}
    \item [\Pascal] Map†{Recall that subrange is a type constructor in \Pascal}
  \end{description}
  \onslide<+->{\mbox{}}
\end{frame}

\begin{frame}{☡Why only now?}
  \setbeamercovered{transparent}
  \begin{itemize}[<+- | @hl+>]
    \item Array \emph{values} are not very useful†{Did you see any arrays in \ML?}
    \item But… array \emph{variables} become very useful…
          \begin{itemize}
            \item Efficient mapping into memory with the classical storage models
            \item Foundation for many algorithms
            \item Foundation for many data structures
          \end{itemize}
  \end{itemize}
\end{frame}

\subunit[varieties]{Varieties of arrays}

\begin{frame}[fragile]{Variety I/V: static arrays}
  \begin{code}{CEEPL}
const char* days[] = {¢¢
  "Sun", "Mon", "Tue",
  "Wed", "Thu", "Fri", "Sat"
}; // An array literal
int main(¢…¢) {¢…¢}
  \end{code}
  \begin{itemize}
    \item size determined at compile time
    \item allocated on the data segment
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Variety II/V: Stack based arrays}
  \begin{TWOCOLUMNS}
    \begin{code}{CEEPL}
void fileCopy(FILE *from, FILE *to) {¢¢
  char buffer[1 << 12]
  ¢…¢
}
    \end{code}
    \MIDPOINT
    \begin{code}{CEEPL}
void printPrimes(int n) {¢¢
  unsigned char sieve[n];
  ¢…¢
}
    \end{code}
  \end{TWOCOLUMNS}
  \begin{itemize}
    \item Size determined at runtime
    \item Size but cannot change after creation
    \item Allocated on the stack
    \item The only kind of arrays in \Pascal
    \item Required that index was a compile-time constant in early versions of~\CPL
    \item Added, after noticing that they do not violate the \alert{no hidden costs} principle:
          \begin{itemize}
            \item Creation is by mere subtraction of a value from the stack pointer
            \item Time to create is~$O(1)$
            \item Size can be negative, but~\CPL programmers are accountable and responsible.
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Variety III/V: Dynamic arrays}
  \begin{code}{CEEPL}
int[] printPrimes(int n) {¢¢
  unsigned char sieve[n];
  ¢…¢
  int r[] = malloc(sum(sieve) * sizeof(int));
  ¢…¢
  return r;
}
  \end{code}
  \begin{itemize}
    \item size determined at runtime
    \item size cannot change after creation
    \item allocated on the heap segment
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Variety IV/V: flexible arrays}
  \begin{code}{PERL}
@a = 1..6; # uninitialized; size 6
@a = (1,2,3); # initialized; size 3
@a[13] = 17; # size is now 13
@a[17] = 13; # size is now 17
delete @a[17]; # size is now 13
delete @a[13]; # size is now 3
  \end{code}
  \begin{itemize}
    \item size may change at runtime
    \item size may change after creation
    \item array may expand or shrink
    \item found e.g., in \Perl
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Variety V/V: associative arrays}
  \begin{code}{PEEAGEPEE}
$¢\X$¢wives["Adam"] = "Eve";
$¢\X$¢wives["Lamech"] = "Adah and Zillah";
$¢\X$¢wives["Abraham"] = "Sarah";
$¢\X$¢wives["Isaac"] = "Rebecca";
$¢\X$¢wives["Jacob"] = "Leah and Rachel";
     ¢⋮¢
     ¢⋮¢
echo ¢¢$¢\X$¢patriarch;
echo ¢¢$¢\X$¢wives[¢\X$¢$patriarch];
  \end{code}
  \begin{itemize}
    \item index can be anything, typically strings.
    \item common in scripting PLs, e.g., \AWK, \JavaScript, \PHP
    \item typically, implemented as a hash table
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{The unbelievable power of associative arrays}
  Using \AWK to compute the frequency of words in the input stream:
  \begin{code}{AWKPROG}
¢＃!/usr/bin/awk -f¢
{¢¢ for (i = 1; i <= NF; i++) a[$i¢\X$¢]++;}
END {¢¢
  for (w in a)
    if (a[w] in b)
      b[a[w]] = b[a[w]] ", " w;
     else {¢¢
       b[a[w]] = w;
       if (max < a[w]) max = a[w]
    }
  for (; max > 0; max--)
    if (b[max] ¢¢!= "")
      print max, b[max];
}
  \end{code}
  Explanation follows…
\end{frame}

\begin{frame}[fragile]{The \AWK implicit loop}
  \squeezeSpace
  \begin{Block}[width=0.7\columnwidth,minipage]{Computing word frequencies in \AWK}
    \AWK's implicit loop reads lines in turn, breaking each line to
    space-separated ‟fields”.
  \end{Block}
  \squeezeSpace[2]
  \begin{columns}[t]
    \column[t]{0.42\columnwidth}
    \begin{code}{AWKPROG}
¢＃!/usr/bin/awk -f¢
¢¢
# implicitly executed
# for each input line
{¢¢
  for (i = 1; i <= NF; i++)
  a[$i¢\X$¢]++; # optional semicolon (¢\cc{;}¢)
  # The ¢\textrm{‟\$”}¢ character is special:
  # variable ¢\cc{\$i}¢ is the ¢\cc{i}\nthscript{th}¢
  # word in the current line
}
    \end{code}
    \column[t]{0.58\columnwidth}
    \begin{code}{AWKPROG}
END {¢¢ # after last line read
  # Accumulate in ¢b[i]¢ all words
  # that occur ¢i¢ times
  max = 0; # not really necessary
  for (w in a) {¢¢
    if (! (a[w] in b)) {¢¢
      b[a[w]] = w
      if (max < a[w])
      max = a[w]
    } else
    b[a[w]] = b[a[w]] ", " w; }
  # Print array ¢b¢ in descending order
  for (; max > 0; max--)
    if (b[max] ¢¢!= "")
      print max, b[max];
}
    \end{code}
  \end{columns}
\end{frame}

\begin{frame}{Summary: determining the index set}
  When is the index set determined?
  \begin{description}[Dynamic Based Arrays]
    \item [Static Arrays] fixed at compile time.
    \item [Dynamic Arrays] on creation of the array variable.
    \item [Stack Based Arrays] on creation of the array variable.
    \item [Flexible arrays] not fixed; bounds may change whenever index is changed.
    \item [Associative Arrays] no ‟bounds” for the set of indices; the set changes dynamically as entries are added or removed from the array.
  \end{description}
\end{frame}

\begin{frame}{Arrays' efficiency}
  \structure{\emph{Static}, \emph{Stack based}, and \emph{Dynamic}:} efficient implementation in the classical memory model.
  \begin{itemize}
    \item including range-based arrays, as in \Pascal
    \item including true multidimensional arrays, as in \Fortran
    \item including arrays of arrays, as in~\CPL
  \end{itemize}
  \structure{\emph{Flexible} and \emph{Associative}:} require
  more sophisticated data structure to map to the classical memory model.
\end{frame}

\begin{frame}{Sophisticated data structures as part of PLs?}
  \large
  \begin{itemize}[<+->]
    \item Associative arrays are great!
    \item We want more, …
          \begin{itemize}[<+->]
            \item sets!
            \item multisets!!
            \item stacks and queues and trees!!!
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{The sad story of \Pascal's sets}
  \large
  \begin{itemize}
    \item simple implementation
    \item efficient implementation
    \item does not scale
    \item with scale, you need to carefully balance
          \begin{itemize}
            \item operations repertoire
            \item time
            \item memory
            \item parallelism
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{☡☡Dilemmas in language design}
  \begin{itemize}
    \item Which, if any, sophisticated data structures should be part of the \emph{PL}?
    \item Which, if any, sophisticated data structures be part of the \emph{library}?
    \item Would it be \emph{possible} to implement sophisticated data structures as part of the library?
    \item What PL structures can support the making of a better standard library of good data-structures.†{yes, logic here is a bit confusing.
            Think about it this way: if you give the library designer better PL tools,
            he will be able to design a better datastructures library.
            Perfection and extensions to the protocol of the standard library would not
      require any changes to the PL.}
    \end{itemize}
  \end{frame}

\subunit[arrays-integral]{Arrays with integral index types}

\begin{frame}{Efficient but inflexible}
  Ordinary arrays are formed as mappings from integral types.
  \small
  \begin{description}
    \item [Pros]
    \begin{itemize}
      \item Only values are stored, not indices.
      \item Simple description of legal indices (defined completely by higher bound, and in some PLs by lower bound as well)
      \item Efficient access using simple addition:
            \begin{description}[Range Mapping]
              \item [Explicit] in~\CPL and \CC pointer arithmetic is explicit \begin{equation}
              \cc{a[i]} ≡ \cc{*(a+i)} ≡ \cc{*(i+a)} ≡ \cc{i[a]}
              \synopsis{Array access in~\CPL is a matter of pointer arithmetic}
              \end{equation}
              \item [Implicit] in, e.g., \Java, array access it translated to
              simple machine instructions
              \item [Range Mapping] in, e.g., \Pascal, array access may require
              subtraction of the first index to compute
              the actual offset
            \end{description}
    \end{itemize}
    \item [Cons]
    \begin{itemize}
      \item When data are sparse, packing techniques are needed.
      \item Inflexible programming.
    \end{itemize}
  \end{description}
\end{frame}

\begin{frame}{☡Piddles}
  What are Piddles? (Quotes from the \Perl manual)\slshape
  \setbeamercovered{transparent}
  \begin{itemize}[<+- | @hl+>]
    \item Having no good term to describe their object, PDL developers coined the term ‟piddle" to give a name to their data type.
    \item A piddle consists of a series of numbers organized as an $N$-dimensional data set…
    \item \Perl has a general-purpose array object that can hold any type of element…
    \item \Perl arrays allow you to create powerful data structures…,
    \item \emph{but they are not designed for numerical work}. For that, use piddles…
  \end{itemize}
\end{frame}

\begin{frame}{Layout of multidimensional arrays}
  \Large
  \structure{Two Main Strategies:}
  \begin{itemize}
    \item Multilayered Memory Mapping:
          \begin{enumerate}
            \item row-major
            \item column-major
          \end{enumerate}
    \item Multiple Dereferencing
  \end{itemize}
\end{frame}

\begin{frame}{Row-major layout of 2D arrays (e.g., \Pascal)}
  \squeezeSpace
  Offset of~$A_{i,j}$ where~$A$ is an~$n⨉m$ matrix is given by:
  \begin{equation}
    \text{offset}(A_{i,j}) = (i-1)m + (j -1)
    \synopsis{Offset computation in 2-dimensional arrays stored in column-major layout}
  \end{equation}
  \begin{Figure}
    \adjustbox{H=0.6}{\input{row-major.tikz}}
  \end{Figure}
\end{frame}

\begin{frame}{Column-major layout of 2D arrays (e.g., \Fortran)}
  \squeezeSpace
  Offset of~$A_{i,j}$ where~$A$ is an~$n⨉m$ matrix is given by:
  \begin{equation}
    \text{offset}(A_{i,j}) = (j-1)n + (i -1)
    \synopsis{Offset computation in 2-dimensional arrays stored in column-major layout}
  \end{equation}
  \begin{Figure}
    \adjustbox{H=0.6}{\input{col-major.tikz}}
  \end{Figure}
\end{frame}

\begin{frame}{‟Multiple dereferencing” layout of 2D arrays}
  \squeezeSpace
  Address of~$A_{i,j}$ where~$A$ is a matrix, is given by:
  \begin{equation}
    \text{address}(A_{i,j}) = \text{dereferenced}(\text{address}(A) + i - 1) + j
    \synopsis{Address computation in 2-dimensional arrays with the ‟multiple dereference” method}
  \end{equation}
  \begin{Figure}
    \adjustbox{H=0.6}{\input{multiple-dereferencing.tikz}}
  \end{Figure}
\end{frame}

\begin{frame}[fragile]{Example: triangular array in \Java}
  For~$A$, an~$n⨉m$ matrix,
  \begin{TWOCOLUMNS}
    \begin{code}{JAVA}
int k = 0;
int[][] iis = new int[][] {¢¢
  new int[k++], new int[k++],
  new int[k++], new int[k++],
}; // An array initializer
¢…¢
for (int i = 0; i < k; i++)
  for (int j = 0; j < i; j++)
    iis[i][j] = i*j;
    \end{code}
    \MIDPOINT
    \begin{Figure}[Layout of triangular array with the multiple dereferencing layout]
      \adjustbox{}{\input{triangular-matrix-multiple-dereferncing.tikz}}
    \end{Figure}
  \end{TWOCOLUMNS}
\end{frame}

\subunit[type-of-arrays]{Type of arrays}

\begin{frame}{Arrays type?}
  \renewcommand\baselinestretch{0.9}
  The type of an array of values of type~$τ$ (first approximation)
  \begin{description}[Long Integer Indexed]
    \item [Integer Indexed]~$\cc{\textup{Integer}}→τ$
    \item [String Indexed]~$\cc{\textup{String}}→τ$
  \end{description}
  \textcolor{red}{But, the mapping is only partial; not all
    possible values of \cc{Integer}/\cc{String} indices
  are mapped into values of type~$τ$.}
  \WF[0.85]{The array type predicament}
  {To properly define the type of arrays, one needs
    heavier type theory artillery, which is
  not really interesting in our course.}
\end{frame}

\begin{frame}[fragile]{Array types in \Java}
  Particularly simple situation
  \begin{itemize}
    \item The type \alert{array of~$τ$} includes all arrays of~$τ$, \emph{regardless} of size.
    \item All these arrays are assignment compatible.
  \end{itemize}
  \begin{code}{JAVA}
double[] x, y, z;
x = new double[100];
y = new double[0];
z = x; x = y; y = z;
  \end{code}
\end{frame}

\begin{frame}[fragile]{☡Array types in \Ada}
  \begin{code}{ADA}
type Vector is array (Integer range <>) of Float;
¢⋮¢
procedure ReadVector(v: out Vector) is ¢\rm…¢;
-- Uses ¢\/v'first¢ and ¢\/v'last¢
¢⋮¢
m: Integer := ¢…¢;
¢⋮¢
a: Vector(1..10);
b: Vector(0..m)
ReadVector(a);
ReadVector(b);
¢⋮¢
a := b; -- Succeeds only if array ¢\cc{b}¢ has exactly 10 elements.
  \end{code}
\end{frame}

\afterLastFrame

\references
\begin{itemize}
  %
  \D{Arrays}{http://en.wikipedia.org/wiki/Array\_data\_type}
  \D{Arrays}{http://en.wikipedia.org/wiki/Array\_data\_type}
  \D{Associative Arrays}{http://en.wikipedia.org/wiki/Associative\_array}
  \D{Automatic Variable}{http://en.wikipedia.org/wiki/Automatic\_variable}
  \D{Call Stack}{http://en.wikipedia.org/wiki/Call\_stack}
  \D{Column-Major Order}{http://en.wikipedia.org/wiki/Row-major\_order＃Column-major\_order}
  \D{Dynamic Arrays}{http://en.wikipedia.org/wiki/Dynamic\_array}
  \D{Row Major Order}{http://en.wikipedia.org/wiki/Row-major\_order}
  \D{Stack-based Memory Allocation}{http://en.wikipedia.org/wiki/Stack-based\_memory\_allocation}
\end{itemize}
\exercises
\begin{enumerate}
  \item Generalize the offset calculation formula of the layered-memory layout to \Pascal arrays.
  \item What's the output of the following \AWK program?
        \begin{code}{AWKPROG}
for (i in A)
  print i;
  \end{code}
  Why is the only sensible option? Repeat the above for \Java.
  \item It is known that a language~$𝓛$ uses the ‟layered-memory” layout
        and that~$𝓛$ does not check array bounds. Write a program in~$𝓛$
        that prints ‟\texttt{row}” if~$𝓛$ implementation on the machine in which
        the program is run, uses the ‟row-major” layout,
        and ‟\texttt{column}” if~$𝓛$ uses ‟column-major” layout.
  \item Explain why the ‟multiple-dereferencing” layout for representing multidimensional
        arrays is likely to be slower than than the ‟multilayered memory mapping” layout.
  \item Generalize the offset calculation formula of the layered-memory layout to three (four, five, six…) dimensions.
  \item Propose a method for implementing flexible arrays on the stack.
  \item Explain how the ‟multiple-dereferencing” layout is applied in 3D arrays.
  \item Lady D'Arbanville used to say that stack based arrays make the best of all worlds. Discuss her claim.
  \item Explain how knowledge of whether the PL is row-based or column-based plays a role
        in the design of algorithms for large numerical problems.
  \item Explain why the ‟multiple-dereferencing” layout for representing multidimensional
        arrays tends to be more memory efficient than ‟multilayered memory mapping”.
  \item Explain how it follows from the classical storage model that \Pascal functions are forbidden from returning arrays.
  \item How does \JavaScript support multidimensional arrays?
  \item Name a data structure with a very efficient array implementation?
  \item Demonstrate a case in which the ‟multiple-dereferencing” layout for representing multidimensional
        arrays is \textbf{less} efficient in its use of memory than the ‟multilayered memory mapping” layout.
  \item How does \AWK support multidimensional arrays?
  \item Would you say that array variables more useful than array values? Discuss your position.
  \item Where are ‟shebangs” used in this unit?
  \item What are the pros and cons of the modification to the multiple-dereferencing layout,
        in which a reference to a zero length array is used instead of a \kk{null} reference.
  \item Which features does \CC offer in support of data structures library?
        and \Java? and \Pascal? and \AWK? and \Go?
  \item Eitan Ha-Ezrahi claimed that he can extend \Java to include associative arrays.
        Explain why this is not such a good idea.
  \item \Pascal does not allow to overflow array bounds. Write a program that
        uses timing to demonstrate that the language uses ‟row-major” layout.
        (Hint: locality of reference)
  \item Are the following \Pascal types equivalent?
        \begin{TWOCOLUMNS}
          \begin{code}{PASCAL}
Array [1..10, 1..10]
    of Integer;
          \end{code}
          \MIDPOINT
          \begin{code}{PASCAL}
Array[1..10]
  of Array [1..10]
    of Integer;
          \end{code}
        \end{TWOCOLUMNS}
  \item Daniel Webster claimed that all integer arrays of \CC (\Java, \Pascal, \AWK) are of the same type. Discuss his claim.
\end{enumerate}

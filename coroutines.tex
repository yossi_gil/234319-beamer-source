
\begin{frame}{What are coroutines?}
  \squeezeSpace
  \setbeamercovered{dynamic}
  \WD[0.74]{Coroutine}{A \Red{coroutine} is a function, which can
    \begin{itemize}
      \item \textbf{suspend} anywhere in its execution by executing a \kk{yield} command.
      \item \textbf{resume} a suspended execution, proceeding to the command
            that follows the suspending \kk{yield}
    \end{itemize}
  }
  Coroutines \vs generators:
  \begin{itemize}
    \item Generators are also called semicoroutines
    \item Generator can only \cc{yield} to its caller
    \item Coroutine typically \cc{yield} to another coroutine
  \end{itemize}
  (coincidentally, two overloaded meanings of the word
  ‟\alert{yield}”
  are ‟\emph{to produce}” and
  ‟\emph{to surrender to another the physical control of something}")
\end{frame}

\begin{frame}[fragile]{Unconvincing example of coroutines}
  \squeezeSpace[3]
  A sow and its two little piglets
  may all die in the pigsty
  \begin{code}[watermark text=pseudo \CSharp]{CSHARP}
void sow() {¢¢ for (;;) {¢¢
   defecate();
   if (hungry) feed();
   if (thirsty) drink();
   yield piglet1;
}}
  \end{code}
  \begin{TWOCOLUMNS}
    \begin{code}[watermark text=pseudo \CSharp]{CSHARP}
void piglet1() {¢¢ for (;;) {¢¢
  defecate();
  if (hungry || thirsty) suck();
  yield piglet2;
}}
    \end{code}
    \MIDPOINT
    \begin{code}[watermark text=pseudo \CSharp]{CSHARP}
void piglet2() {¢¢ for (;;) {¢¢
  defecate();
  if (hungry || thirsty) {¢¢
    suck();
    yield sow;
  }
}}
    \end{code}
  \end{TWOCOLUMNS}
  If \cc{piglet2} is not hungry neither thirsty.
  \begin{itemize}
    \item \cc{sow} may die
    \item \cc{piglet1} will then die
    \item \cc{piglet2} himself will eventually die
  \end{itemize}
\end{frame}

\begin{frame}{Issues with the pigsty}
  It is not clear at all
  \begin{itemize}
    \item how the coroutines are started?
    \item how each coroutine gains access to the other?
    \item how can one create multiple instances of the same coroutine?⏎
      (create several active invocations of (say) \cc{piglet2()}
    \item why coroutines are better than plain \alert{threads}?
  \end{itemize}
  So, why the heck do we need these?
\end{frame}

\begin{frame}{Coroutines \vs threads}
  Both offer multi-tasking:
  \begin{description}
    \item [Threads] preemptive; execution can be interrupted at any point
    \item [Coroutine] cooperative; control is
  \end{description}
  Zillions of weird scheduling schemes?
  \begin{description}
    \item [Threads] Yes!
    \item [Coroutine] only with careless programming
  \end{description}
  Race conditions?
  \begin{description}
    \item [Threads] Yes!
    \item [Coroutine] No!
  \end{description}
  Generally speaking, coroutines grant the programmer fine control
  of scheduling, but it may take skill to effectively use this control.
\end{frame}

\begin{frame}{The sad story of coroutines}
  \begin{itemize}
    \item invented in the early 60's (if not earlier)
    \item mostly forgotten; often discarded as ‟cumbersome multitasking”
    \item have (partial) implementation in mainstream PLs such
          as~\CPL, \CC, \Java, \Python, \Perl, Object-\Pascal, \Smalltalk and more
    \item implementations did not really catch
  \end{itemize}
  Hence, the poorly designed syntax in the pigsty.
\end{frame}

\begin{frame}[fragile]{Killer application of coroutines}
  \centering
  {\Large\Red{Event loops:}}
  \begin{TWOCOLUMNS}[-2ex]
    \begin{code}[watermark text=Pseudo-\CSharp]{CSHARP}
void mainWindow() {¢¢
  for (;;) {¢¢
    Message m = getMessage();
    Area a = findScreenArea(m);
    Windows ws = findWindows(a);
    push(m);
    for (Window w: ws) {¢¢
      yield(w);
      if (consumed(m))
        break;
    }
  }
}
    \end{code}
    \MIDPOINT
    \begin{code}[watermark text=Pseudo-\CSharp]{CSHARP}
void anyWindow() {¢¢
  for (;;) {¢¢
    switch (Message m = getMessage()) {¢¢
      case M1: ¢…¢
      case M2: ¢…¢
      ¢…¢
      default:
        push(m)
    }
    yield mainWindow;
  }
}
    \end{code}
  \end{TWOCOLUMNS}
  \medskip
  Useful in browser applications such as \cc{Gmail}
\end{frame}

\begin{frame}{Continuations}
  Quite an obscure and abstract definition:
  \WD[0.74]{Continuation}{A \Red{continuation} is an abstract representation of the control state of a computer program}
  More plainly, a continuation is making the activation record
  (more generally, the generating record) into a
  first class citizen.
  \begin{itemize}
    \item Invented many years ago
    \item Has full implementation in many dead and obscure PLs
    \item Has partial/non-official implementation in many mainstream PLs
    \item Did not catch (yet!)
  \end{itemize}
\end{frame}

\begin{frame}{Data stored in a continuation}
  \begin{description}[Termination status]
    \item [Code] Where the function is stored in memory, its name, and other meta information, e.g., for debugging.
    \item [Environment] Activation record, including back-pointers as necessary.
    \item [CPU state] The saved registers, most importantly, the \cc{PC} register
    \item [Result] Last result \kk{return}ed/\kk{yield}ed by the function
    \item [Termination status] can the continuation be continued
  \end{description}
\end{frame}

\begin{frame}{Operations on a continuation}
  \begin{description}[123456789101112]
    \item [$c←\text{start}(f(…))$] create a new continuation for the call~$f(…)$
    \item [$c'←\text{clone}(c)$] save execution state for later
    \item [$\text{resumable}(c)$] determine whether resumption is possible.
    \item [$c←\text{resume}(c)$] resume execution of continuation~$c$
    \item [$\text{retrievable}(c)$] determine whether computation has generated
    a result
    \item [$r←\text{retrieve(c)}$] obtain the pending returned (yielded) value
    of~$c$
  \end{description}
  In fact, continuations are nothing but ‟\alert{generation records}” with
  slightly more operations.
\end{frame}

\begin{frame}{Things you can do with continuations:}
  \begin{itemize}
    \item Good old function calls
    \item Closures
    \item Generators
    \item Exceptions
    \item Coroutines
  \end{itemize}
  With continuations, you can conceal the fact that Web server should be
  stateless;
  (you just provide the client with the server's continuation record)
\end{frame}

\afterLastFrame

\exercises
\begin{enumerate}
  \item Which items stored in activation records are not part of the data
        stored with continuations?
  \item How are closures different than anonymous functions?
  \item How would you argue that~\CPL supports first class functions?
  \item What's non-preemptive multitasking? How is it related to coroutines?
        Which is more general?
  \item Why is \CC forced to support nested functions?
  \item Enumerate the data items that continuations have to store.
  \item What's the difference between \kk{static}- and non-\kk{static}
        nested classes?
  \item What's the difference between generators and iterators?
  \item Enumerate all operations allowed on first-class functions.
  \item Can you use \Java's nested classes to implement anonymous functions?
  \item Discuss the restrictions placed on nested classes in \CC.
  \item How are closures different than lambda functions?
  \item What's the difference, if any, between \Java's local classes, and
        non-\kk{static} nested classes?
  \item Discuss the restrictions placed on anonymous functions in Gnu-\CPL.
  \item Given is a language which only has generators, can you use these to
        implement coroutines?
  \item What's the etymology of the name ‟lambda” functions?
  \item How are \CC nested classes different than those of \Java?
  \item Why do continuations require GC?
  \item Why does \Pascal refuse to give equal rights to functions?
  \item Can closures be implemented with activation records? Explain.
  \item What are the two meanings of the word ‟yield”? How are these meaning
        used in generators and
  \item Which stored in activation records are not part of the data stored
        with closures?
  \item Explain why~\CPL does not have nested functions.
  \item Are data items stored with activation records different in
        \CPL and in \Pascal?
  \item Enumerate the data items stored in an activation record?
  \item Are nested functions in \CC first class?
  \item Enumerate the data items that closures have to store.
  \item Can you use \Java's nested classes to implement closures?
  \item When are closures planned for \Java? What's the proposed syntax?
        Any restrictions on the implementation?
  \item Why do closures require GC?
  \item Which data items stored with closures do not occur in
        activation records?
  \item How does \Java implement generators?
  \item What information is contained in coroutines and is missing in closures?
  \item When are closures planned for \CC\X\@? What's the proposed syntax?
        Any restrictions on the implementation?
  \item Can one emulate closures with coroutines? Explain.
  \item Are goroutines a kind of coroutines?
  \item In~\CPL the calling function is responsible for removing the pushed
        arguments from the stack. Why is this the only reasonable
        implementation?
  \item Describe the typical situation in which you want
        your closure to be anonymous?
  \item Can you use \Java's nested classes to implement coroutines?
        If yes, explain how; if no, explain why not.
  \item Experts claim that all ‟implementation of all web services are
        \emph{poor-man's continuations}”. Explain and elaborate.
  \item Can coroutines be implemented with closures? Explain.
  \item Explain why letting the called function to remove the arguments from
        the stack is more efficient than having the caller do so.
  \item What's the difference between coroutines and generators?
  \item Does \Java make distinction between generators and iterators? How?
\end{enumerate}

\begin{frame}{Sometimes… it does not make sense to proceed as usual!}
  \squeezeSpace[2]
  \begin{center}
    {\large\Red{\bf Two kinds of abnormal and unusual situations}}
  \end{center}
  \begin{TWOCOLUMNS}
    {\centering\alert{{\large Bugs⏎}}}
    An error in the program code:
    \begin{description}[XX]\small\itshape
      \item [Runtime type error] (in dynamically typed languages)
      \item [Pseudo type error
    ]
      division by zero, array bounds overflow, dereferencing null pointer,…
      \item [Resources issue] stack overflow, memory exhaustion,…
    \end{description}
    \MIDPOINT
    {\centering\alert{{\large Unusual environment⏎}}}
    \normalsize
    The program encounters an environment for which
    normal execution does not make sense:
    \begin{itemize}\small\itshape
      \item wrong password
      \item file not found
      \item low battery
      \item no connection to server
      \item cannot locate GPS
      \item volume is off
    \end{itemize}
  \end{TWOCOLUMNS}
  \medskip
  \scriptsize 
  (Recall (\cref{\labelPrefix Section:type-errors}) that a
pseudo-type error is subjecting a value to an operation which is inappropriate
it, but is appropriate for other values of the same type.)
\end{frame}

\subunit[robustness]{Robustness}

\begin{frame}{When you cannot proceed as usual…}
  It is tough to write robust programs; sometimes 80％ of the code
  is dedicated to abnormal situations.
  \begin{center}
    \alert{\Red{{\large There is an ‟\emph{exception}”!}}}
  \end{center}
  \begin{description}[Unusual environment]
    \item [Bugs] The ‟\emph{language runtime environment}” must take action,
    printing error messages, and enforcing graceful termination
    \item [Unusual environment] programmer must deal with the error;
    if the programmer fails to detect an ‟unusual environment”,
    then it is a bug.
  \end{description}
\end{frame}

\begin{frame}{Robust PL \vs robust programs}
  \begin{TWOCOLUMNS}
    \WD[0.9]{Robust PL}{The PL's runtime system recovers gracefully
    from all program bugs}
    ‟the programmer cannot make the runtime system crash”
    \MIDPOINT
    \WD[0.9]{Robust program}{The program recovers gracefully even
    in the face of weird end-user and execution environment errors}
    ‟the user cannot make the runtime system crash”
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}{The fundamental theorem of exception handling}
  \squeezeSpace[5]
  \WT<+->[0.73]{The robust programs theorem}{No one really knows how to write robust programs}
  \begin{CENTER}[0.73\columnwidth]
    \begin{proof}<+->
      Exceptions are detected at lower level of abstraction:
      \begin{itemize}[<+->]
        \item Wrong keyboard input
        \item Missing file
        \item Internet problem
        \item Low battery
        \item Out of memory
      \end{itemize}
      \onslide<+->{but they must be
      handled in higher levels of abstraction.}
    \end{proof}
  \end{CENTER}
\end{frame}

\begin{frame}{Handling exceptions}
  ⚓Handling must be done at a high abstraction level.⏎
  ⚓Challenges include:
  \begin{itemize}[<+->]\itshape
    \item \textbf{Consistency:} Deal with similar errors similarly (tough because many details of the errors are lost at higher abstraction level)
    \item \textbf{Coverage:} make sure that all errors are covered appropriately, and that no two dissimilar errors are grouped together for the purpose of
          handling.
          (tough, because the programmer does not always know what errors may happen at lower levels of abstraction)
    \item \textbf{Smart recovery:} Sometimes, by the time the exception is caught, there is nothing useful that can be done.
    \item \textbf{Systematic testing:} It is tough to systematically generate all exceptions.
  \end{itemize}
\end{frame}

\begin{frame}{Corollaries of the robust programs theorem}
  \squeezeSpace
  \begin{TWOCOLUMNS}
    \onslide<+->{Since no one know how to do it, no one can design for it}
    \WC<+->[0.9]{Adequate support for exception handling}{%
      No PL provides adequate support for exception handling.
    }
    \onslide<+->{Humanity concentrates in what it does well}
    \WC<+->[0.9]{Graceful termination}{%
      Many PLs offer graceful termination in case of bugs.
    }
    \MIDPOINT
    \onslide<+->{Humanity cannot do what it does not know how to do}
    \WC<+->[0.9]{Rareness of robust programs}{%
      Very few programs are truly robust
    }
    \onslide<+->{And since programmers are lazy,}
    \WC<+->[0.9]{Input errors considered bugs}{%
      Most programs convert input errors into bugs
    }
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile]{Example: Heron's formula for the area of the triangle}
  \begin{columns}[c]
    \column{0.25\columnwidth}
    \begin{Figure}[Hero of Alexandria (10--70 AD); also known as \protect\textbf{Heron}]
      \begin{adjustbox}{max height=20ex,center}
        \includegraphics{Graphics/heron-of-Alexandria}
      \end{adjustbox}
    \end{Figure}
    Hero of Alexandria (10--70 AD)⏎<presentation>
    {\small\itshape Also known as \textbf{Heron}}⏎<presentation>
    \column{0.5\columnwidth}
    \begin{Figure}[A triangle with edges~$a$,~$b$, and~$c$]
      \begin{adjustbox}{}
        \begin{tikzpicture}
          \coordinate (A) at (1,1);
          \coordinate (B) at ($(A)+(25:3)$);
          \coordinate (C) at ($(A)+(60:5)$);
          \draw
          [pattern=grid, pattern color=blue!20]
          (A) -- (B) [draw] node[midway,below]{$c$}
          -- (C) node[midway,right]{$a$}
          -- (A)node[midway,left]{$b$};
          \node at ($0.33*(A) + 0.333*(B) + 0.333*(C)$) {$A$};
        \end{tikzpicture}
      \end{adjustbox}
    \end{Figure}
    \begin{equation}
      \begin{split}
        A &= √{s(s-a)(s-b)(s-c)}⏎
        s &= \frac{a+b+c}{2}
      \end{split}
      \synopsis{Heron's formula for the area of the triangle}
    \end{equation}
    \column{0.25\columnwidth}
    ⚓\alert{Exceptions}
    ⚓
    \begin{enumerate}[<+->]\small\itshape
      \item Cannot read~$a$
      \item Cannot read~$b$
      \item Cannot read~$c$
            \item~$a<0$
            \item~$b<0$
            \item~$c<0$
            \item~$s<0$
            \item~$s-a<0$
            \item~$s-b<0$
            \item~$s-c<0$
    \end{enumerate}
  \end{columns}
\end{frame}

\begin{frame}[allowframebreaks,fragile]{Non-robust program for the area of the triangle}
  \begin{enumerate}
    \item Let's start implementing it.
    \item First, essential include files
  \begin{lcode}{CPLUSPLUS}
#include <stdio.h>
#include <math.h>
  \end{lcode}
\item Then, a function to prompt for and read real numbers:
  \begin{lcode}{CPLUSPLUS}
double get(const char *s) {¢¢
  double r;
  printf("Please enter %s: ", s);
  scanf("%lg",&r);
  return r;
}
  \end{lcode}
\item Invalid input?  End of file?
  \qq{We do not care, so let's carry on…}
  \begin{lcode}{CPLUSPLUS}
double A(double s, double sa,
  double sb, double sc) {¢¢
    return sqrt(s * sa * sb * sc);
}
  \end{lcode}
\item Square root of a negative number?⏎
  \qq{We don't care!}
\item 
  Proceeding…
  \begin{code}{CPLUSPLUS}
double A(double a, double b, double c) {¢¢
  double s = (a+b+c) / 2;
  return A(s, s-a, s-b, s-c);
}
  \end{code}
\item 
  Is either of~$s-a$,~$s-b$, or~$s-c$ negative?
  \qq{We proceed…}
  \begin{lcode}{CPLUSPLUS}
int main() {¢¢
  double a = get("A");
  double b = get("B");
  double c = get("C");
  printf("Area is: %g\n", A(a,b,c));
  return 0;
}
  \end{lcode}
  \medskip
  And, what happens if the three sides do not satisfy the triangle inequality?
  \begin{Block}[width=0.7\columnwidth,minipage]{Answer}
    If you check closely, you will find that this case is covered above.
  \end{Block}
  \end{enumerate}
\end{frame}

\subunit[resumption]{Policy I: resumption}

\begin{frame}{Resumption}
  \begin{itemize}
    \item The offending command continues as usual after the handler was invoked.
    \item Found in PL/I and \Eiffel.
    \item In \CC, function \cc{set\_new\_handler()}
          takes as argument a pointer to a handler function for failure of the
          \kk{new} memory allocation operator
  \end{itemize}
  \begin{present}[0.7]{Motivating example: memory exhausted exception}
    the memory exhaustion handler can:
    \begin{itemize}
      \item free some non-essential memory
      \item invoke the garbage collector
    \end{itemize}
    but, these efforts are not guaranteed to be sufficient
  \end{present}
\end{frame}

\begin{frame}{Pros ＆ cons of resumption}
  \begin{description}[Pros I]
    \item [Pros] The offending command is not aware of the problem
    nor of the solution
    \item [Cons]
    \begin{itemize}
      \item \textbf{Perplexing situation:} you need the context to deal with the error, but you don't have it.
      \item \textbf{Hardly ever works:} in most cases, the handler can
            \emph{try}, but cannot promise to \emph{fix} the problem
      \item \textbf{Hardly used:} experience shows that resumption policy
            is not used very often even in PLs that support it
    \end{itemize}
  \end{description}
\end{frame}

\begin{frame}[fragile=singleslide]{Emulating resumption in Heron's program}
  \squeezeSpace
  \begin{TWOCOLUMNS}
    \begin{Code}{CEEPL}{Original: naive read}
double get(const char *s) {¢¢
  double r;
  printf("Please enter %s: ", s);
  scanf("%lg",&r);
  return r;
}
    \end{Code}
    \begin{itemize}\footnotesize\itshape
      \item Unpredicted result in case of invalid input
      \item Repeated unpredictable result
    \end{itemize}
    \MIDPOINT
    \begin{Code}{CPLUSPLUS}{Retrying the Read}
double get(const char *s) {¢¢
  double r;
  do
    printf("Please enter %s: ", s);
  while (1 ¢¢!= scanf("%lg",&r));
  return r;
}
    \end{Code}
    \begin{itemize}\footnotesize\itshape
      \item loop forever on EOF.
      \item loop forever in case of invalid input
    \end{itemize}
  \end{TWOCOLUMNS}
  \medskip
  {\cc{scanf} does not consume unexpected characters in the input stream}
  \begin{present}[0.6]{Correct and useful ‟\emph{corrective}” action?}
    \emph{Easier said than done!}
  \end{present}
\end{frame}

\subunit[rolling]{Policy II: error rolling}

\begin{frame}[fragile=singleslide]{Explicit error handling: returning error code}
  \centering \large \alert{Step I:} make low-level functions return error code
  \begin{Code}{CPLUSPLUS}{Reading doubles}
double get(const char *s) {¢¢
  double r;
  int n;
  printf("Please enter %s: ", s);
  n = scanf("%lg",&r);
  if (n < 0) {¢¢ // Error or EOF
    if (errno ¢¢!= 0) perror(s);
    return -1; // error code
  }
  if (n == 0) // Unexpected input
    return -1; // error code
  if (r < 0)
    return -1; // error code
  return r;
}
  \end{Code}
\end{frame}

\begin{frame}[fragile=singleslide]{More error code returned}
  \large
  \alert{Step I:} another low-level function…
  \begin{Code}{CPLUSPLUS}{Computing the area}
double A(double s, double sa,
  double sb, double sc) {¢¢
    if (s < 0 || sa < 0)
      return -1; // error code
    if (sb < 0 || sc < 0)
      return -1 // error code
    return sqrt(s * sa * sb * sc);
}
  \end{Code}
  Issues:
  \begin{itemize}
    \itshape
    \item Which error code to use?
    \item Which errors to detect?
    \item Should we report some errors?
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]{Error handling}
  \large
  \alert{Step II:} dealing with the propagated errors
  \begin{columns}
    \column{0.6\columnwidth}
    \begin{code}{CPLUSPLUS}
int main() {¢¢
  double a = get("A");
  double b = get("B");
  double c = get("C");
  double area;
  if (a < 0 || b < 0 || c < 0) {¢¢
    fprintf(stderr, "Bad input");
    return 1;
  }
  area = A(a,b,c));
  if (area < 0) {¢¢
    fprintf(stderr, "Bad triangle");
    return 1;
  }
  printf("Area is: %g\n", area);
  return 0;
}
    \end{code}
    \column{0.4\columnwidth}
    More issues: \small\itshape
    \begin{itemize}
      \item How would the user know whether the error was in~$a$,~$b$, or~$c$?
      \item When should we retry reading?
      \item Can all errors happen?
    \end{itemize}
  \end{columns}
\end{frame}

\begin{frame}{Error rolling: summary}
  Every procedure returns a special error code
  \begin{description}[\Go convention]\item [Assembler] usually the carry flag
    \item [\Icon] each function returns its success value
    in addition to its real value
    \item [\CPL convention] returns 0 or negative value for integers,
    \textbf{NaN} for floating point values,
    \item [\Go convention] procedures return a pair of values:
    \begin{itemize}
      \item the actual value, \emph{and}
      \item error status or code.
    \end{itemize}
  \end{description}
  \Pascal and old~\CPL do not allow functions returning structure,
  so a specialized error value was easy to select
\end{frame}

\begin{frame}{Summary: using the error code}
  The invoking procedure checks this code and tries to recover.
  \begin{description}
    \item [Assembler] \cc{call proc; jc error}
    \item [\CPL] \cc{\kk{if} ((f = fopen(\textrm{…})) == 0) \textrm{…}}
  \end{description}
  heavy responsibility on the programmer;
  \begin{itemize}
    \item always remember to test error codes;
    \item propagate errors up sensibly;
    \item recover from errors gracefully;
  \end{itemize}
  most programmers prove to be irresponsible
\end{frame}

\subunit[longjmp]{Policy III: setjmp/longjmp of~\CPL}

\begin{frame}[fragile=singleslide]{How does it work?}
  \begin{TWOCOLUMNSc}[-1ex]
    \begin{code}{CPLUSPLUS}
#include <stdio.h>
#include <setjmp.h>
¢¢
static jmp_buf b;
¢¢
void g(void) {¢¢
// will print:
  printf("g()\n");
// jumps back to where ¢\cc{setjmp}¢ was
// called and saved current CPU state
// in ¢\cc{b}¢ making ¢\cc{setjmp}¢ now return 1:
  longjmp(b, 1);
}
    \end{code}
    \MIDPOINT
    \begin{code}[width=33ex,minipage]{CPLUSPLUS}
void f(void) {¢¢
  g();
  // will not print
  printf("f()\n");
}
¢¢
int main() {¢¢
  // when executed first,
  // function ¢\cc{setjmp}¢ returns 0
  if (0 == setjmp(b)
    f();
  else // when ¢\cc{longjmp}¢ jumps back,
       // function ¢\cc{setjmp}¢ returns 1
       // will print
    printf("main()\n");
  return 0;
}
    \end{code}
  \end{TWOCOLUMNSc}
\end{frame}

\begin{frame}[fragile]{What's stored in a jump buffer?}
  \begin{itemize}
    \item program counter
    \item stack pointer
    \item other CPU registers
    \item any thing else required to restored the CPU state
    \item In file \texttt{/usr/include/stdio.h} on my machine,
          you will find something like (a more complex version of)
          \begin{code}{CPLUSPLUS}
struct jmp_buf {¢¢
  long int data[8]; // for registers probably
  int mask_was_saved; // who noz?
  unsigned long mask[16]; // mask for interrupts
};
¢¢
typedef struct jmp_buf jmp_buf[1];
    \end{code}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]{Step I: low-level functions call \protect\cc{longjmp}}
  \begin{Code}{CPLUSPLUS}{Reading doubles}
double get(const char *s) {¢¢
  double r;
  int n;
  printf("Please enter %s: ", s);
  n = scanf("%lg",&r);
  if (n < 0) {¢¢ // Error or EOF
    if (errno ¢¢!= 0) {¢¢
      perror(s);
      longjmp(b, 1); // exception code
    }
    longjmp(b, 2) // exception code
  }
  if (n == 0) // Unexpected input
    longjmp(b, 3); // exception code
  if (r < 0) // Negative value
    longjmp(b, 4); // exception code
  return r;
}
  \end{Code}
\end{frame}

\begin{frame}[fragile=singleslide]{More low-level functions calling \protect\cc{longjmp}}
  \begin{Code}{CPLUSPLUS}{Computing the area}
double A(double s, double sa,
  double sb, double sc) {¢¢
    if (s < 0) longjmp(b, 5);
    if (sa < 0) longjmp(b, 6);
    if (sb < 0) longjmp(b, 7);
    if (sc < 0) longjmp(b, 8);
    return sqrt(s * sa * sb * sc);
}
  \end{Code}
  \alert{Issues:} \small\itshape
  \begin{itemize}
    \item Managing and passing the \cc{jmp\_buff} record
    \item Managing exception codes
    \item Which errors to detect?
    \item Should we print an error message on some errors?
    \item Is it possible that we access uninitialized stack variable?
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]{Step II: catching the \protect\cc{longjmp}}
  \begin{columns}[c]
    \column{0.6\columnwidth}
    \begin{code}{CPLUSPLUS}
int main() {¢¢
  switch (setjmp(b)) {¢¢
    case 0: {¢¢
      double a = get("A");
      double b = get("B");
      double c = get("C");
      printf("Area is: %g\n", area(a,b,c));
      return 0;
    }
    case 1: ¢…¢ break;
    case 2: ¢…¢ break;
    ¢⋮¢
    case 8: ¢…¢ break;
  }
}
    \end{code}
  \end{columns}
\end{frame}

\begin{frame}{Long jump in~\CPL ＆ \Pascal}
  \begin{description}[\Pascal]
    \item [\Pascal] can execute a \kk{goto} to any nesting block;
    \begin{itemize}
      \item Can only \kk{goto} to deepest occurrence of nesting block
      \item No fine control in recursive calls
    \end{itemize}
    \item [\CPL] \cc{setjmp} and \cc{longjmp} allow to jump outside an internally
    invoked function
    \begin{description}[\cc{longjmp(b, v) }]
      \item [\cc{setjmp(b)}] saves all CPU registers in buffer \cc{b}
      \item [\cc{longjmp(b, v)}] restores the registers from \cc{b} and make
      \cc{setjmp} return \cc{v}
    \end{description}
    Lots of unsafe properties
  \end{description}
\end{frame}

\subunit[exceptions-policy-four]{Policy IV: exceptions}

\begin{frame}[allowframebreaks,fragile]{Exception language construct}
  \begin{enumerate}
    \item Include files:
    \begin{lcode}{CPLUSPLUS}
#include <stdio.h>
#include <math.h>
    \end{lcode}
  \item Read a floating point number:
    \begin{lcode}{CPLUSPLUS}
double get(const char *s) {¢¢
  double r;
  printf("Please enter %s: ", s);
  if (1 == scanf("%lg",&r)) return r;
  throw¢\Y<2->[xshift=23ex,yshift=14ex,fill=red!20]{can throw integers!}¢ 1;
}
    \end{lcode}
  \item Check that a number is positive:
    \begin{lcode}{CPLUSPLUS}
void v(double v, const char *error) {¢¢
  if (v <= 0) throw¢\Y<3->[xshift=2ex,yshift=5ex,fill=red!20]{can throw strings!}¢ error;
}
    \end{lcode}
  \item Compute triangle's area using Heron's formula: 
    \begin{lcode}{CPLUSPLUS}
double A(double s, double sa, double sb, double sc) {¢¢
  v(s, "Circumference must be positive");
  v(sa, "Side A is too large");
  v(sb, "Side B is too large");
  v(sc, "Side C is too large");
  return sqrt(s * sa * sb * sc);
}
    \end{lcode}
  \item Now, our main function is both elegant and robust:
    \begin{lcode}{CPLUSPLUS}
int main() {¢¢
  for (;;)
  try {¢¢
    double a = get("A");
    double b = get("B");
    double c = get("C");
    printf("Area is: %g\n", A(a,b,c));
    return 0;
    } catch (const char *s¢\Y<4->[xshift=6ex,yshift=24ex,text width=3em,inner sep=0pt,fill=blue!20]{catch strings!}¢) {¢¢
      fprintf(stderr, "Error: %s\n", s);
    } catch (¢…\Y<5->[xshift=5ex,yshift=23ex,text width=4.5em,inner sep=0pt,fill=blue!20]{catch anything else!}¢) {¢¢
     fprintf(stderr, "Bad input\n");
  }
}
    \end{lcode}
    \end{enumerate}
\end{frame}

\subunit[kinds-of-exceptions]{Kinds of exceptions}

\begin{frame}{Summary: policies for exception handling}
  \begin{description}[Language construct]
    \item [Resumption] Resume as usual after detection of exception
    \item [Explicit] Explicit error handling: every procedure returns an
    error code which the invoking procedure has to check
    \item [Long jump] moving from a low level of abstraction directly
    to a higher level of abstraction
    \item [Language construct] Exception language mechanism
    \begin{present}[0.7]{Observation}
      In \CC, you must have exceptions since constructors return no value.
      You cannot use explicit error handling for constructors.
    \end{present}
  \end{description}
\end{frame}

\begin{frame}{☡Exception type?}
  What can be thrown?
  \begin{description}[\Pascal]
    \item [\Pascal] The \Unit type: goto to a nesting block has no associated value
    \item [\CPL] Integer (with \cc{longjmp})
    \item [\CC] Any type
    \item [\Java] Subclass of \cc{Throwable}
    \item [\Ada] Any enumerated type
  \end{description}
  \centering
  \begin{present}[0.85]{Tough language definition issue}
    Suppose that a function~$f$ may throw exceptions of types~$E₁,…,Eₙ$.
    Then, should the type of~$f$ also include~$E₁,…,Eₙ$ as the possible exceptions that~$f$ may return?
  \end{present}
  \alert{Exceptions seem to be the right solution,⏎
    but, \Red{no one knows how to do these right}}
\end{frame}

\begin{frame}{☡The checked exceptions dilemma}
  No one really knows how to type-check exceptions in a way that should be productive to the programmer.
  \begin{description}
    \item [\Java] tries at systematic classification of exceptions
    \begin{itemize}
      \item Function types includes a list of exceptions
      \item But not exceptions thought to be \emph{bugs} such as null pointer and division by zero
      \item Static type checking on compile type
      \item But lax checking by the JVM at runtime
    \end{itemize}
    \item [\CSharp] learned \Java's bitter lesson
    \begin{itemize}
      \item Exceptions are not part of the function type
      \item No checked exceptions
    \end{itemize}
    \item [\CC] Similar to \CSharp.
    \begin{itemize}
      \item Function types \emph{may} include a list of exceptions
      \item If the list exists, it is checked in runtime
      \item But, in recent versions of \CC, there is no runtime checking
    \end{itemize}
  \end{description}
\end{frame}

\begin{frame}[fragile=singleslide]{Finally?}
  Exception catching in \Java
  \begin{lcode}[width=50ex,minipage]{JAVA}
try {¢¢
  ¢$C$¢ // Commands which may raise an exception
} catch(¢$E₁$¢) {¢¢ // First exception type
  ¢$H₁$¢ // Commands for handling first exception
} catch (¢$E₂$¢) {// Second exception type
  ¢$H₂$¢ // Commands for handling second exception
}
¢⋮¢
finally ¢…¢ {¢¢
  // Commands to execute regardless of
  // whether an exception was thrown
}
  \end{lcode}
  \alert{Why \kk{finally}?}
  \begin{description}[Simple answer]
    \item [Simple answer] Because the handlers may throw exceptions themselves.
    \item [Real answer] For managing allocated resources.
  \end{description}
\end{frame}

\subunit[resource-allocation]{Resource acquisition is (or isn't) initialization}

\begin{frame}[fragile=singleslide]{The multiple resource allocation problem}
  \squeezeSpace
  Consider an application that needs to open two files,~$f₁$ and~$f₂$.
  If it opens~$f₁$ and discovers an error in~$f₂$ is must remember to
  close~$f₁$ as well.
  \begin{code}[width=50ex,minipage]{JAVA}
void f(File f1, File f2) {¢¢
  try {¢¢
    PrintWriter o1 = new PrintWriter(f1)
    try {¢¢
      PrintWriter o2 = new PrintWriter(f2);
      ¢⋮¢
    // Not all exceptions are caught here:
    } catch (IO Exception e) {¢¢
      // The following may also raise an exception:
      System.out.println(f1 + " :I/O error");
    } finally {¢¢
      o1.close();
    }
  } catch(IOException e) {¢¢
    System.out.println(f1 + " :I/O error");
  }
}
  \end{code}
  \alert{a bit cumbersome}
\end{frame}

\begin{frame}{\protect\textbf Resource \protect\textbf acquisition \protect\textbf is \protect\textbf initialization (RAII)}
  When a block goes out of scope
  \begin{description}
    \item [\Java] (and other language with garbage collection) Local variables wait for the GC.
    \item [\Pascal] (and other language which use the stack memory model) Variables allocated on the stack are reclaimed
    \item [\CC] Variables allocated on the stack are \emph{destructed}. This occurs in two cases:
    \begin{itemize}
      \item The block terminates normally
      \item An exception is thrown out of the block
    \end{itemize}
  \end{description}
  \WD[0.8]{RAII}{A common programming idiom, by which resources are
  allocated by the constructor and deallocated by the destructor.}
\end{frame}

\begin{frame}[fragile=singleslide]{RAII example: a simple file reader class}
  \begin{Code}{CPLUSPLUS}{File reader: Build upon~\CPL's \ccn{FILE *}}
struct Reader {¢¢
  FILE *f;
  Reader(const char *name): f(fopen(name,"r")) {¢¢
    if (!f) throw name;
  }
¢ ¢~Reader() {¢¢ fclose(f); }
  int eof() {¢¢ return feof(f); }
  int get() {¢¢
    if (ferror(f)) throw "read";
    return fgetc(f);
  }
};
  \end{Code}
  The file is opened when a \cc{Reader} object is created; the file is closed when this object is destructed.
\end{frame}

\begin{frame}[fragile=singleslide]{RAII example: a simple file writer class}
  \begin{Code}{CPLUSPLUS}{File writer: Build upon~\CPL's \ccn{FILE *}}
struct Writer {¢¢
  FILE *f;
  Writer(const char *name): f(fopen(name,"w")) {¢¢
    if (!f) throw name;
  }
¢ ¢~Writer() {¢¢ fclose(f); }
  int put(int c) {¢¢
    if (ferror(f)) throw "write";
    return fputc(c,f);
  }
};
  \end{Code}
  The file is opened when a \cc{Writer} object is created; the file is closed when this object is destructed.
\end{frame}

\begin{frame}[fragile=singleslide]{RAII example: using the reader ＆ writer classes}
  \begin{Code}{CPLUSPLUS}{Robust copying of a file}
int main() {¢¢
  try {¢¢ // Opening two resources and using them
    Reader f1("from.txt"); // No need to close
    Writer f2("to.txt"); // No need to close
    while (!f1.eof())
      f2.put(f1.get());
  } catch (const char *s) {¢¢
  // File(s) are properly closed,
  // even in the case of exception
    perror(s);
    return 1;
  }
  return 0;
}
  \end{Code}
\end{frame}

\begin{frame}[fragile=singleslide]{Life without RAII is tough!}
  \squeezeSpace[3]
  \Java has no destructors, so RAII is difficult
  \begin{TWOCOLUMNSc}[20ex]
  \begin{Code}{JAVA}{Robust ‟Hello, World” (version I)}
private void f() {¢¢
  FileOutputStream f = null;
  // ¢\textbf May \textbf Throw \textbf An \textbf Exception = MTAN¢
  try {¢¢
    f = new FileOutputStream("hello.txt");
    byte[] bs = "Hello, World\n".getBytes(); // MTAN
    f.write(contentInBytes); // MTAN
    f.flush(); // MTAN
    f.close(); // MTAN
  } catch (IOException e) {¢¢
    e.printStackTrace();
  } finally {¢¢
    try {¢¢
      // Are you certain that file ¢\cc{f}¢
      // will not be closed twice?
      if (f ¢¢!= null)
        f.close();
    } catch (IOException e) {¢¢ e.printStackTrace(); }
  }
}
  \end{Code}
  \MIDPOINT
  Tough, and error prone, because we do not know the file's state
  when the exception is caught.
\end{TWOCOLUMNSc}
\end{frame}

\begin{frame}[fragile=singleslide]{Automatic deallocation: new \Java 7 feature}
  \squeezeSpace[3]
  \centering
  \alert{A \kk{try} clause may allocate a resource}
  {\footnotesize\itshape
    \begin{TWOCOLUMNS}
      \begin{itemize}
        \item Resource is managed by the \kk{try} block
        \item A call to \kk{close} is silently added to \kk{finally} block
      \end{itemize}
      \MIDPOINT
      \begin{itemize}
        \item If there is no \kk{finally} block, such a block is automatically created
        \item The call to \kk{close} may still throw an exception, which must be handled by the programmer
      \end{itemize}
    \end{TWOCOLUMNS}}
  \begin{Code}{JAVA}{Robust ‟Hello, World” (version II)}
private void f() {¢¢
  try (FileOutputStream f =
    new FileOutputStream("hello.txt")
) {¢¢
    byte[] bs = "Hello, World\n".getBytes(); // MTAN
    f.write(contentInBytes); // MTAN
    f.flush(); // MTAN
  } catch (IOException e) {¢¢
    e.printStackTrace();
  } // File ¢f¢ will be closed here if it was opened
}
  \end{Code}
\end{frame}

\begin{frame}[fragile=singleslide]{Multiple resource acquisition with new \Java 7 feature}
  Multiple resource allocated in a \kk{try} will be automatically deallocated, and in the right order;
  only the resources which were allocated are deallocated;
  no need for
  an explicit \kk{close} in the \kk{finally} section;
  \begin{Code}{JAVA}{Robust file copy}
private void f() {¢¢
  try (FileInputStream in =
          new FileInputStream("from.txt");
       FileOutputStream out =
          new FileOutputStream("to.txt");
   ) {¢¢
     String line = null;
     while ((line = in.readLine()) ¢¢!= null) {¢¢
       out.writeLine(line);
       out.newLine();
     }
   } // Files ¢in¢ and ¢out¢ will be closed here
     // (if they were opened)
}
  \end{Code}
\end{frame}

\afterLastFrame

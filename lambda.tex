\begin{frame}{What is lambda calculus?†{Unit written by Matan Peled}}
  \squeezeSpace
  \begin{TWOCOLUMNS}[3x]
    \begin{Block}[minipage]{From Wikipedia}
      \scriptsize\itshape
      From Wikipedia: ‟Lambda calculus (also written as~$λ$-calculus) is a formal
      system in mathematical logic for expressing computation based on function
      abstraction and application using variable binding and substitution. First
      formulated by Alonzo Church to formalize the concept of effective
      computability, lambda calculus found early successes in the area of
      computability theory, such as a negative answer to Hilbert's
      Entscheidungsproblem. Lambda calculus is a conceptually simple universal model
      of computation (Turing showed in 1937 that Turing machines equated the lambda
      calculus in expressiveness). The name derives from the Greek letter lambda ($λ$)
      used to denote binding a variable in a function. The letter itself is arbitrary
      and has no special meaning. Lambda calculus is taught and used in computer
      science because of its usefulness.”
    \end{Block}
    \MIDPOINT
    Essentially:
    \begin{itemize}
      \item Called lambda-calculus or~$λ$-calculus. (The letter by itself is meaningless)
      \item Notation for computation by function application.
      \item Foundation of \Lisp, \ML, and other functional languages.
      \item Computationally equivalent to Turing machine, to all ‟reasonable” computational models and PLs.
    \end{itemize}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}{What does it look like?}
  \newcommand\Expression{\text{‟\emph{Expression}”}}
  \newcommand\Function{\text{‟\emph{Function}”}}
  \newcommand\Application{\text{‟\emph{Application}”}}
  \newcommand\Name{\text{‟\emph{Name}”}}
  \begin{Block}[minipage]{EBNF for the~$λ$-calculus}
    \begin{equation}
      \begin{split}
        \Expression &→\Name | \Function | \Application⏎
        \Function &→λ \Name \cc{.} \Expression⏎
        \Application &→\Expression{} \Expression{}
      \end{split}
      \synopsis{EBNF for the the~$λ$-calculus}
    \end{equation}
  \end{Block}
  \begin{description}
    \item [Start symbol] \Expression
    \item [Non-terminals] \Expression
    \item [Terminals] \Name,~$λ$, \cc{.}, \cc{(}, \cc{)}
  \end{description}
  (Parentheses can be added as needed where required to improve readability.)
\end{frame}

\begin{frame}{Simple example}
  The simplest function is the identity function:
  \begin{equation}
    λx.x
    \synopsis{The identify function with the~$λ$-calculus}
  \end{equation}
  Which can be applied like so:
  \begin{equation}
    (λx.x) y
    \synopsis{Applying the identify function with the~$λ$-calculus}
  \end{equation}
\end{frame}

\begin{frame}{Left Associativity}
  Function applications are left associative, that is, the following expression:
  \begin{equation}
    E₁E₂E₃E₄⋯Eₙ
    \synopsis{An unparenthesized expression with the~$λ$-calculus.}
  \end{equation}
  is evaluated like:
  \begin{equation}
    \Bigg(⋯\bigg(\Big(\big((E₁) E₂\big) E₃\Big) E₄\bigg)⋯Eₙ\Bigg)
    \synopsis{A parenthesized form of~$E₁ E₂ E₃ E₄⋯Eₙ$}
  \end{equation}
\end{frame}

\begin{frame}{Free \vs bound variables}
  What are variables?
  \newcommand\Name{\text{‟\emph{Name}”}}
  \begin{itemize}
    \item The term ‟variable” is synonym to what was denoted as \Name{} in the EBNF.
    \item Variables are just symbols
    \item Variables do not ‟vary”, nor do they ‟store” anything
  \end{itemize}
  Two sorts of variables:
  \begin{description}[Bound]
    \item [Free] variable is not bound by a~$λ$, e.g.,~$y$ is free in
    \begin{equation*}
      λx.y
    \end{equation*}
    \item [Bound] variable is bound by a~$λ$, e.g.,~$x$ is free in
    \begin{equation*}
      λx.x
    \end{equation*}
  \end{description}
\end{frame}

\begin{frame}{The formal rules of lambda calculus}
  \squeezeSpace
  \begin{description}
    \item [$α$-conversion] changing bound variables
    \item [$β$-reduction] applying functions to their arguments
    \item [$η$-conversion] which captures a notion of extendibility 
  \end{description}
  \WD{$α$-equivalence}{%
    If two expressions~$E₁$ and~$E₂$ can be transformed into one another using~$α$ conversion, we say that that~$E₁$ and~$E₂$ are~$α$-equivalent.
  }
  $β$-equivalence and~$η$-equivalence are similarly defined.
\end{frame}

\begin{frame}{$α$-conversion I/III}
  Function arguments (bound variable names) can be renamed.
  \begin{equation}
    λx.x⇒λy.y
    \synopsis{Example of~$α$-conversion}
  \end{equation}
  This looks trivial, but scoping makes it slightly more complex.
\end{frame}
\begin{frame}{$α$-conversion II/III}
  When performing~$α$-conversion, only variables that are bound to the same abstraction are converted, e.g.,
  \begin{equation}
    λx. λx.x⇒λy. λy.y
    \synopsis{An~$α$-conversion with nesting}
  \end{equation}
  But,
  \begin{equation}
    λx. λx.x\not ⇒λy. λx.y
    \synopsis{An invalid~$α$-conversion with nesting}
  \end{equation}
\end{frame}

\begin{frame}{$α-conversion$ III/III}
  Alpha conversion is not possible if it would result in a variable being captured by another abstraction.
  \begin{equation}
    λx. λy.x\not ⇒λy. λy.y
    \synopsis{An invalid~$α$-conversion with nesting}
  \end{equation}
\end{frame}

\begin{frame}{Substitution}
  \squeezeSpace
  We saw substitution before, so lets formally define it.
  \WD{Notation for substitution}{%
    The \Red{$[y/x]z$} notation it to be read as ‟replace every occurrence of~$x$ in~$z$ with~$y$”.
  }
  \WD{Semantics of substitution}{%
    Substitution is defined by the following recursive rules:
    \begin{equation}
      \begin{split}
        [y/x] x &≡x⏎
        [y/x] z &≡z⏎
        [y/x] E₁ E₂ &≡([y/x]E₁) ([y/x]E₂)⏎
        [y/x]λx.E &≡λx.E⏎
        [y/x] xλz.E &≡λz.([y/x]E)⏎
      \end{split}
      \synopsis{Recursive definition of substitution}
    \end{equation}
    where it is assumed that~$z≠x$.
  }
\end{frame}

\begin{frame}{$β$-reduction}
  $β$-reduction is a formalization of function application. It is defined in terms of substitution:
  \begin{equation}
    (λV.E₁) E₂⇒[E₂/V] E₁
  \end{equation}
\end{frame}

\begin{frame}{$η$-conversion}
  Two functions are the same if and only if they give the same results. So, these are the same:
  \begin{equation}
    x.(fx)⇒f
    \synopsis{$ημ$-conversion}
  \end{equation}
  So long as~$f$ doesn't contain~$x$ as a free variable. For example:
  \begin{equation}
    λx .((λx.x)x) \Rightarrowλx.x
  \end{equation}
\end{frame}

\begin{frame}{Combinators}
  We call formulas that don't have free variables "combinators", and if they're special enough we give them names, e.g.,
  \begin{description}[The~$K$ combinator]
    \item [The~$I$ combinator]~$λx.x$
    \item [The~$K$ combinator]~$λxy.x≡λx.λy. x$
  \end{description}
  The~$I$ combinator is the identity function. The~$K$ combinator makes a function that returns a constant value. Note the currying.
\end{frame}

\begin{frame}{True ＆ False}
  Lets define True and False as follows:
  \begin{equation}
    \begin{split}
      T &≡λxy.x⏎
      F &≡λxy.y⏎
    \end{split}
    \synopsis{True and false in the~$λ$-calculus}
  \end{equation}
  We will note that the~$T$ and~$F$ functions can simulate an \kk{if}…\kk{then}…\kk{else}…\kk{fi}
  statement.
\end{frame}

\begin{frame}{Numbers in the lambda calculus}
  A programming language needs to be able to represent numbers, and be able to do arithmetic on those numbers.
  \begin{description}
    \item {Q:}
    How do we do that in lambda calculus, where we only have functions and free variables?
    \item {A:}
    Answer: We define an expression as "zero", and a successor function.
  \end{description}
\end{frame}

\begin{frame}{Encoding for the natural numbers}
  We semi-arbitrarily decide to encode zero as follows:
  \begin{equation}
    0≡λsz.z
    \synopsis{Notation for zero in the~$λ$-calculus}
  \end{equation}
  The the rest of the natural numbers:
  \begin{equation}
    \begin{split}
      1&≡λsz.(sz)⏎
      2&≡λsz.(s(sz))⏎
      &⋮
    \end{split}
    \synopsis{Notation for~$1,2,…$ in the~$λ$-calculus}
  \end{equation}
  \begin{Block}[minipage,width=0.7\columnwidth]{The encoding rule}
    The encoding of a natural number~$n$ means: ‟Apply the first argument to the second argument~$n$ times”
  \end{Block}
\end{frame}

\begin{frame}{Successor function}
  The first interesting function we'll see is the successor function.
  \begin{equation}
    TBD
    \synopsis{TBD}
  \end{equation}
  Lets try it out!
  \begin{equation}
    TBD
    \synopsis{TBD}
  \end{equation}

  These are all beta-reductions, i.e., function applications. The
  last term is alpha-equivalent to 1.

\end{frame}

\begin{frame}{Addition}
  The advantage of the semi-arbitrary way we decided to encode numbers is that we get addition for free.
\end{frame}

\begin{frame}{Multiplication}
  \begin{equation}
    TBD
    \synopsis{TBD}
  \end{equation}
  This function calculates~$x·y$.

  Example:
  \begin{equation}
    TBD
    \synopsis{TBD}
  \end{equation}
  Exercise: Finish reducing the expression, and verify that it is equivalent to~$4$.
\end{frame}

\begin{frame}{Recursion ＆ the~$Y$ combinator I/III}
  \begin{itemize}
    \item Recursion in the lambda calculus sounds hard
          \qq{after all, all functions are anonymous! How can we call them recursively?}
    \item It turns out we can implement recursion using a function that calls some function~$y$ and then regenerates itself.
  \end{itemize}
\end{frame}

\begin{frame}{Recursion ＆ the~$Y$ combinator II/III}
  We can implement recursion using a function that calls some function y and then regenerates itself:
  \begin{equation}
    TBD
    \synopsis{TBD}
  \end{equation}
  When applied to some function~$R$:
  \begin{equation}
    TBD
    \synopsis{TBD}
  \end{equation}
  Which, when reduced further, yields:
  \begin{equation}
    TBD
    \synopsis{TBD}
  \end{equation}
\end{frame}

\begin{frame}{The~$Y$ combinator (recursion, cont.)}
  We got the very cool result that:
  \begin{equation}
    YR=R(YR)
    \synopsis{The recursive property of the~$Y$ combinator}
  \end{equation}
  This means that we can implement recursion using the Y combinator, since we get ‟ourselves” as an argument!
  People who did computability: The~$Y$ combinator is ‟informally similar” to Kleene's recursion theorem.
\end{frame}

\begin{frame}{Predecessor and zero-test}
  Lets assume we have the following functions:
  \begin{description}
    \item [$P$] the predecessor function:~$P1 = 0$,~$P2 = 1$,…
    \item [$Z$] the zero-test function:~$Z0=T$,~$Z1=F$,~$Z2=F$,…
    function:~$P1 = 0$,~$P2 = 1$,…
  \end{description}
  These functions are defined in the appendix.
\end{frame}

\begin{frame}[fragile]{Recursion example}
  We want to calculate the sum of numbers from~$0$ through~$n$.
  In \ML, we would write this as:
  \begin{code}{EMEL}
fun sum 0 = 0
  | sum n = n + (sum n-1)
  \end{code}
\end{frame}

\begin{frame}{Recursion example (continued)}
  In the~$λ$-calculus, we write this as:
  \begin{equation}
    R≡\Bigg(λR n. Z n 0\bigg(n S \Big(r\big(Pn\big)\Big)\bigg)\Bigg)
    \synopsis{Recursion in the~$λ$-calculus}
  \end{equation}
  Deciphering the above:
  \begin{itemize}
    \item~$r$ is the function to do a recursive call on
    \item~$n$ is the argument
    \item~$Zn$ tests whether~$n$ is zero:
    \begin{itemize}
      \item if it is it returns~$T$ which takes the first argument,
      \item otherwise it returns~$F$ which takes the second argument
    \end{itemize}
    \item the first argument is~$0$,
    \item the second argument is~$(nS(r(Pn)))$, that is~$n+(r (n-1))$
  \end{itemize}
  Admittedly, this looks like gibberish, but it is actually quite similar to the \ML definition.
\end{frame}

\begin{frame}{Recursion example (continued)}
  Lets see how it works with 3:
\end{frame}

\begin{frame}{That's all, folks!}
  Questions?

  Slides are based on:
  \url{http://www.inf.fu-berlin.de/lehre/WS03/alpi/lambda.pdf}
  \url{http://en.wikipedia.org/wiki/Lambda\_calculus}
\end{frame}

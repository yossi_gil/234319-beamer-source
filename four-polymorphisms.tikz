\begin{tikzpicture}[
every node/.style={rectangle,rounded corners,fill=yellow!50,
minimum height=4ex,
     level distance=0ex,
},
   left/.style={%
     grow=down,
   anchor=east,
  edge from parent path={(\tikzparentnode.west) -| (\tikzchildnode.north)}
  },
  RIGHT/.style={%
       grow=right,
     anchor=west,
    xshift=#1ex,
    edge from parent path={%
      (\tikzparentnode.east) -> (\tikzchildnode.west)}
    },
    LEFT/.style={%
      grow=left,
      anchor=east,
      xshift=-#1ex,
      level distance=0ex,
      edge from parent path={(\tikzparentnode.west) -> (\tikzchildnode.east)}
    },
  right/.style={%
   grow=down,
  edge from parent path={(\tikzparentnode.east) -| (\tikzchildnode.north)}
  },
  BELOW/.style={%
   grow=down,
anchor=center,
  edge from parent path={(\tikzparentnode.south) -> (\tikzchildnode.north)}
  },
  next/.style={%
    visible on=<+->,
  },
  stay/.style={%
    visible on=<.->,
  },
  option/.style={%
    visible on=<.->,
  },
  selected/.style={%
    visible on=<+->,
    onslide=<+->{thick,draw=black}
  },
]
\coordinate (A) at (3.4,1.1);
\coordinate (B) at (-9.2,-8.1);
\tikzstyle{issue} = [
  next,
  text width=#1em,
  text centered,
  minimum height=0pt,
  draw=black,
  inner sep=0pt,
  outer sep=0pt,
  font=\small\itshape
]
\tikzstyle{decision} = [issue=#1,diamond,fill=green!30]
\tikzstyle{baggage} = [issue=#1,cloud,aspect=4,fill=orange!30]
\tikzstyle{id} = [name=#1,node contents=#1,#1/.try,all/.try]

\tikzset{opacity=0.2}

\node[id=Typing]
child[right,xshift=12ex] {node[id=Monomorphic typing] {} }
child[left,xshift=-3ex]{%
  node[id=Polymorphic typing] {}
  child [left,xshift=-10ex,id=Ad hoc tree] {%
    node [id=Ad hoc] {}
    child [left,xshift=0ex,] {node[id=Overloading] {} }
    child [right,xshift=11ex,] {node[id=Coercion] {} }
  }
  child [right,xshift=17ex,id=Universal tree] {%
    node(X) [id=Universal] {}
    child [right,xshift=18ex,id=Parametric tree] {%%
      node [id=Parametric]{}
      child [BELOW] {node [id=Polytypes]{}}
    }
    child [left,xshift=-2ex,id=Inclusion tree]
    {%
      node[id=Inclusion] {} 
      child [left,xshift=-2ex] {node [id=Subrange] {} }
      child [right,xshift=12ex,] {node [id=Inheritance] {} }
    }
  }
}
;
\end{tikzpicture}

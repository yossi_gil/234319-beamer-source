\begin{frame}{Concrete reasons to study PLs?}
  \squeezeSpace
  \begin{description}[Professional Skills]
    \item [Fun]⏏
    \begin{itemize}
      \item \ML is neat
      \item \Prolog is elegant
      \item There is more to it than~\CPL, \CC, \Java
      \item Enhance thinking flexibility
    \end{itemize}
    \item [Professional skills]⏏
    \begin{itemize}
      \item Over 2,000 different languages out there
      \item Common concepts and shared paradigms
      \item Framework for comparative study of PLs
    \end{itemize}
    \item [Useful] for other courses:
    \begin{itemize}
      \item OOP
      \item Compilation
      \item Software Engineering
      \item Semantics of PLs
      \item Memory Management
    \end{itemize}
  \end{description}
\end{frame}

\begin{frame}{Discovering you speak prose}
  \squeezeSpace[7]
  \pause
  \begin{Figure}[Mr. Jourdain (\emph{The Would-Be Noble, \bfseries Molière, 1670}) has been speaking prose all his life without knowing it]
    % «Par ma foi \ignore.! il y a plus de quarante ans que je dis de la prose
    % sans que j'en susse rien, et je vous suis le plus obligé du monde
    % de m'avoir appris cela.»
    \centering
    \input le-bourgeois-gentilhomme.tikz
  \end{Figure}
  \squeezeSpace
  \begin{itemize}[<+->]
    \item More generally, learning, something \alert{new} about \alert{old} things.
    \item So, yes, the course will be telling you new stuff about old stuff…
    \item And, we will practice some new modes of thought
  \end{itemize}
\end{frame}

\begin{frame}{New modes of thought}
  \squeezeSpace
  \uncover<+->{\alert{New}}
  \begin{itemize}
    \item <+-> Programming languages mechanisms
          \qq{much beyond the \kk{if} and \kk{while}}
    \item <+-> Programming techniques
    \item <+-> Paradigms of thought
    \item <+-> Directions for your minds
  \end{itemize}
  \uncover<+->{And also,}
  \begin{itemize}
    \item <+-> Get ready to \emph{Object Oriented Programming} and other advanced courses.†{the instructor of
            the \emph{Object Oriented Programming} course paid me to mention this}
      \item <+-> Hone web-search skills.†{Google paid me to include this in the topic of our course}
    \end{itemize}
  \end{frame}

\begin{frame}{But also many practical benefits}
  \begin{Block}[minipage,width=0.7\columnwidth]{Main objective}
    learn, understand, and \underline{evaluate} any new programming language
  \end{Block}
\end{frame}

\begin{frame}{Or, at least, understand the terminology/jargon of the trade:}
  What kind of a beast is \JavaScript?
  \begin{itemize}
    \item Imperative,
    \item With \textbf{prototypes} (object-based, but not object-oriented),
    \item Functions are \textbf{first-class entities},
    \item Has \textbf{lambda functions},
    \item With \textbf{closures},
    \item Is \textbf{weakly typed},
    \item Has \textbf{dynamic typing},
    \item Has \textbf{static scoping},
    \item … and a must-know for any modern website developer!
  \end{itemize}
  \begin{block}{}
    By the end of these course, many of these terms will be covered in depth.
  \end{block}
\end{frame}

\begin{frame}{Not for the faint of heart}
  \squeezeSpace
  \begin{itemize}
    \item \alert{No mathematical interest.} This is not yet another technical course:
          \begin{description}
            \item [Many] ‟\emph{soft}” definitions
            \item [Much] reliance on common sense
            \item [No] theorems, proofs, lemmas, or integration by parts
            \item [No] easy grades for mathematical genuises
          \end{description}
    \item \alert{No ‟computational” interest.} The expressive power of all
          programming mechanisms and computational devices is basically the same
          \begin{Block}[minipage,width=0.7\columnwidth]{The Church-Turing hypothesis}
            \begin{itemize}
              \item The DOS batch language and \Java are equivalent
              \item The Commodore 64 and the latest 8-core CPUs are equivalent
            \end{itemize}
          \end{Block}
    \item \alert{No ‟algorithmic” interest.} You don't discover new fascinating algorithms using
          better programming languages.
  \end{itemize}
\end{frame}

\begin{frame}{Possible approaches}
  \begin{itemize}
    \item Define and compare paradigms of PLs
    \item Present formal approaches to syntax and semantics
    \item Present ways of implementing and analyzing programs in various PLs
    \item Show the concepts that must be dealt with by any PL, and the possible variety in treatment
  \end{itemize}
\end{frame}

\begin{frame}{Problem…}
  \begin{itemize}
    \item To teach you PL theory, we need to draw examples from different PLs.
    \item Right now, most of you know~$≈2.5$ languages (\CPL, \CC, Unix shell scripts).
    \item Examples in these slides come from (alphabetically):
          \Ada, \textsc{Algol}, \AWK,
          \CPL, \CC, \CSharp,
          \Eiffel, \Fortran, \Haskell, \Java, \ML,
          Lazy-\ML, \Lisp, \Pascal,
          \Prolog, \Python, \SQL, and probably a few more I forgot.
    \item Can you please learn all these for next week?
    \item Recitations are here to help.
  \end{itemize}
\end{frame}

\begin{frame}{Who needs PLs?}
  \begin{itemize}
    \item Computers' native tongue is machine language
    \item Programmers need higher level languages, because:
          \begin{itemize}
            \item They can't write machine language correctly
            \item They can't read machine language fluently
            \item They can't express their ideas in machine language efficiently
            \item Life is too short to program in machine language.
          \end{itemize}
    \item A formal language is not only a man-machine interface, but also a person-to-person language!
  \end{itemize}
  \textbf{Conclusion:} PLs are a
  compromise between the needs of humans and the needs of machines
\end{frame}

\begin{frame}{What is a PL?}
  \begin{itemize}
    \item A linguistic tool with formal syntax and semantics
    \item A consciously designed artifact that can be implemented on computers
    \item ‟A conceptual universe for thinking about programming" (Alan Perlis, 1969)
  \end{itemize}
\end{frame}

\begin{frame}{Language processors}
  \begin{itemize}
    \item A system for processing a language:
          \begin{itemize}
            \item Compiler
            \item Interpreter
            \item Syntax directed editor
            \item Program checker
            \item Program verifier
          \end{itemize}
    \item Studied in other courses:
          \begin{itemize}
            \item Compilation
            \item Program verification
            \item Software engineering
          \end{itemize}
  \end{itemize}
  To know the semantics of a language (the \emph{function} a program encodes) one can ignore its implementation
\end{frame}

\begin{frame}{Relations to other fields in computer science}
  \begin{description}[Databases and Information Retrieval]
    \item [Databases and Information Retrieval] Query languages - languages for manipulating databases.
    \item [Human-Computer Interaction] PLs are designed to be written and read by humans
    \item [Operating Systems] Input-Output support. Storage management. Shells are in fact PLs.
    \item [Computer Architecture] PL design is influenced by architecture and vice versa. Instructions sets are PLs. Hardware design languages.
  \end{description}
\end{frame}

\begin{frame}{Closely related topics}
  \begin{description}
    \item [Automata and Formal Languages, Computability] Provide the foundation for much of the underlying theory.
    \item [Compilation] The technology of processing PLs.
    \item [Software engineering] The process of building software systems.
  \end{description}
\end{frame}

\afterLastFrame
\endinput

\begin{frame}{PLs as software tools}
  \begin{itemize}
    \item In general PLs are \emph{software tools}---an interface between human clients and lower-level facilities
    \item Other software tools:
          \begin{itemize}
            \item Interactive editors
            \item Data transformers (compilers, assemblers, macro processors)
            \item Operating systems
            \item Tools for program maintenance (script files, debuggers)
          \end{itemize}
    \item Properties of PLs as software tools:
          \begin{itemize}
            \item Definition of the interface = syntax and semantics
            \item Implementation of the interface to the low level facility = compiler
            \item Usefulness for the human client = evaluation of PLs
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{On a personal level}
  \begin{itemize}[<+->]
    \item <+-> I took lots of `tough” courses, including quantum mechanics,
    \item <+-> (yes, including \emph{physical chemistry})
    \item <+-> biology, calculus, psychology,
    \item <+-> the math, the logic, all went on gloriously,
    \item <+-> but, the toughest one was
    \item <+-> \emph{programming languages}
  \end{itemize}
  \uncover<+->{Why did I return to it?}
  \uncover<+->{\W[0.5]{Because,}{\large\itshape I tend to file my rough points}}
\end{frame}

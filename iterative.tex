\begin{frame}{Iterative command constructor}
  \alert{A very general pattern of iterative command constructor}
  \WD[0.75]{Iterative command constructor}
  {If~$\mathbf S$ is a ‟program state generator” and~$C$ is a
    command, then\hfill⏎
    \qquad\qquad \Red{$\text{\kk{forall}}\: \mathbf S\: \text{\kk{do}}\: C$} ⏎
    is an iterative composite command whose semantics is the (sequential / collateral / concurrent) execution of~$C$
    in all program states that~$\mathbf S$ generates.}
  \medskip
  \itshape Note that with ‟sequencers” such as \kk{break} and \kk{continue},
  iterative commands can be even richer!
\end{frame}

\begin{frame}[fragile]{State generator? answer ＃1/5}
  Range of integer (ordinal) values, e.g.,
  \begin{code}{PASCAL}
For i := gcd(a,b) to lcm(a,b) do
  If isPrime(i) then
    Writeln(i);
  \end{code}
\end{frame}

\begin{frame}[fragile]{State generator? answer ＃2/5}
  \alert{\textbf{The state generator~$\mathbf S$ may be…}}⏎▄⏎
  Any arithmetical progression, e.g., in \Fortran
  \begin{code}{FORTRAN}
Comment WHAT IS BEING COMPUTED???
        INTEGER SQUARE11
        SQUARE11=0
        DO 1000 I = 1, 22, 2
        SQUARE11 = SQUARE11 + I
  1000 CONTINUE
  \end{code}
\end{frame}

\begin{frame}{State generator? answer ＃3/5}
  \alert{\textbf{The state generator~$\mathbf S$ may be…}}⏎▄⏎
  Expression, typically boolean:
  \begin{itemize}
    \item expression is re-evaluated in face of the state changes made by the command~$C$;
    \item iteration continues until expression becomes true, or,
    \item until expression becomes false,
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{State generator? answer ＃4/5}
  \alert{\textbf{The state generator~$\mathbf S$ may be…}}⏎▄⏎
  Generator, e.g., in \Java
  \begin{code}{JAVA}
List<Thing> things = new ArrayList<Thing>();
¢…¢
for (Thing t : things)
  System.out.println(t);
  \end{code}
\end{frame}

\begin{frame}[fragile]{State generator? answer ＃5/5}
  \alert{\textbf{The state generator~$\mathbf S$ may be…}}⏎▄⏎
  Cells in an array, e.g., in \Java
  \begin{code}{JAVA}
public static void main(String[] args) {¢¢
  int i = 0;
  for (String arg: args)
    System.out.println(
      "Argument " +
      ++i +
      ": " +
      arg
  ):
}
  \end{code}
\end{frame}

\begin{frame}[fragile]{Minor varieties of iterative commands}
  \centering
  \alert{Minimal number of iterations?}
  \begin{TWOCOLUMNS}
    \begin{Code}[minipage]{JAVA}{Minimal ＃ Iterations = 0}
while (s < 100)
  s++;
    \end{Code}
    \MIDPOINT
    \begin{Code}[minipage]{JAVA}{Minimal ＃Iterations = 1}
do {¢¢
  s++;
} while (s < 100);
    \end{Code}
  \end{TWOCOLUMNS}
  \alert{Truth value for maintaining the iteration}
  \begin{TWOCOLUMNS}
    \begin{Code}[minipage]{PASCAL}{Iteration continues with \kkn{true}}
While not eof do
Begin
  ¢…¢
end
    \end{Code}
    \MIDPOINT
    \begin{Code}[minipage]{PASCAL}{Iteration continues with \kkn{false}}
Repeat
  ¢…¢
until eof
    \end{Code}
  \end{TWOCOLUMNS}
  \emph{none of these is too interesting…}
\end{frame}

\begin{frame}[fragile]{The iteration variable}
  Several iteration constructs, e.g., \emph{ranges} and \emph{arithmetical progressions},
  introduce an ‟iteration variable” to the iteration body, e.g.,
  \begin{TWOCOLUMNS}[-0.07\columnwidth]
    \begin{code}{AWKPROG}
#!/usr/bin/gawk -f
BEGIN {¢¢
  antonym["big"] = "small"
  antonym["far"] = "near"
   ¢…¢
  for (w in antonym)
    print w, antonym[w]
}
    \end{code}
    \MIDPOINT
    \begin{code}{JAVA}
int[] primes = new int[100];
for (int p = 1, i = 0;
  i < primes.length; i++)
     primes[i] = p = nextPrime(p);
for (int p: primes)
  System.out.println(p);
    \end{code}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}{☡Subtleties of the iteration variable}
  Can you make an \emph{educated} guess as to what should happen in the following cases
  \begin{enumerate}
    \item \Red{the value of the expression(s) defining the range/arithmetical progression change during iteration?}
    \item \Red{the loop's body tries to change this variable?}
    \item \Red{the value of the iteration variable is examined after the loop?}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]{☡Definite \vs Indefinite Iteration}
  To make an educated guess, Let's educate ourselves:
  \begin{description}[Indefinite Loop]
    \item [Definite Loop] Number of iterations is known before the loop starts
    \item [Indefinite Loop] A loop which is not definite
  \end{description}
  It is easier to optimize definite loops.
  \begin{itemize}
    \item Many PL try to provide specialized syntax for definite loops, because they perceived as more efficient and
          of high usability.
    \item Only definite loops may have collateral or concurrent semantics
    \item Even if a PL does not specify that loops are definite, a clever optimizing compiler may deduce that certain loops
          are definite, e.g.,
          \begin{code}{CEEPL}
for (int i = 0; i < 100; i++)
  ¢…¢; // If loop body does not change ¢\cc{i}¢
       // the loop is effectively definite
    \end{code}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{☡So, let's make our guesses…}
  \footnotesize
  \begin{enumerate}
    \item \Red{{\normalsize the value of the expression(s) defining the range/arithmetical progression change during iteration…}}
          ¶ The iteration range, as well as the step value are computed \emph{only} at the beginning of the loop.
          (Check the \Fortran/\Pascal manual if you are not convinced)
    \item \Red{{\normalsize the loop's body tries to change this variable…}}
          ¶ The loop body should not change the iteration variable;
          The PL could either issue a compile-time error message (\Pascal),
          runtime error message (\Java), or just state that program behavior
          is undefined.
    \item \Red{{\normalsize What's the value of the iteration variable after the loop?}}
          ¶ The iteration variable may not even exist after the loop (\Java); or, its value may be undefined (\Pascal).
          \begin{itemize}\scriptsize
            \item the PL designer thought that programmers should not use the iteration variable after the loop ends
            \item if the value is defined, then collateral implementation is more difficult
            \item many architectures provide a specialized CPU instructions for iterations;
            \item the final value of the iteration variable with these instructions is not always the same.
          \end{itemize}
  \end{enumerate}
\end{frame}

\afterLastFrame

\exercises
\begin{enumerate}
  \item Revisit the example of using references in \ML\Z\@. Is \cc{r} an L-value? An R-value? Both? None of these? Explain.
  \item \AWK designers chose the iteration variable to range over the indices of an array, instead of the values. Why was this the only sensible decision?
  \item What's the iteration variable of a \kk{while} loop?
  \item What does the acronym ‟CSP” stand for?
  \item Write the most feature-rich \kk{class} in \Java, without using the semicolon character,~‟\cc{;}”, even once.
  \item How come~\CPL does not offer any rules regarding the iteration variable?
  \item How come \Java, despite being similar to~\CPL, recognize the notion of and iteration variable.
  \item Explain why it is impossible to use the \Perl \cc{die(\textrm{…})} programming idiom in \Java.
  \item \Java designers chose the iteration variable to range over the array cells, instead of the indices. Why was this the only sensible decision?
  \item Revisit the example of using references in \ML\Z\@. Is \cc{!r} an L-value? An R-value? Both? None of these? Explain.
  \item Could there be an iteration variables in non-definite loops? Explain.
  \item Write a~\CPL function with at least three commands in it, without using the semicolon character,~‟\cc{;}”, even once.
  \item What are the circumstances in which
        \begin{code}{PASCAL}
If false then
  Writeln(true < false);
  \end{code}
  prints \kk{true}; concretely, add some \Pascal code around to make this happen.
\end{enumerate}


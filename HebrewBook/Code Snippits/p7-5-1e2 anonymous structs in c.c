struct { // Anonymous struct
  const char *name;
  const int n;
} ranks [] = { // array of elements of this anonymous type
  { "pawn", 1 },
  { "knight", 3 },
  { "bishop", 3 },
  { "rook", 5 },
  { "queen", 9 },
  { "king", 10000 },
};

enum { // Anonymous enum
 // Anonymous enum fields are a more civilized way to create named constants.
  // (the less civilized way to do so is with #define )
 WieferichPrime1 = 1093,
 WieferichPrime2 = 3511,
}; 

union { // Anonymous union
  // Yes,it is legal  (but pointless) to define a union, struct or enum with no fields
}; 
// Legal, but compiler will issue a warning:
// warning: unnamed struct/union that defines no instances

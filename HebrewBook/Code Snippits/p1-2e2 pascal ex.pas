program n;
var a: integer;
  function t(var b:integer): boolean;
  (* This function seems to always return true, right? *)
  begin
    a := 1; b := succ(a); t := a <> b
  end;
begin
   writeln(t(a))
end.
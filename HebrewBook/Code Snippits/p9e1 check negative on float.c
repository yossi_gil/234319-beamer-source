bool is_negative(float x) {
    return x < 0.0;
}
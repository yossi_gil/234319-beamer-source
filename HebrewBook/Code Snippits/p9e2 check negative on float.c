bool is_negative(float x) {
    unsigned int *ui = (unsigned int *)&x;
    return (*ui & 0x80000000) != 0;
}

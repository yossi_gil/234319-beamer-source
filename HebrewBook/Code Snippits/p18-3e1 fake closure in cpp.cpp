class Adder
{
public:
  int number;
// acts as 'saving the environment'. So green!
  Adder(int number):number(number){}
// Overloading the ( ) operator to imitate a function call
  int operator()(int to_add)        
  {
    return to_add + number;
  }
};

Adder add3(3);
add3(5); // => 8

Function isPrime(n: integer): Boolean;
VAR
d: Integer; (* Potential divisor *)
primeSoFar: Boolean;
Begin
If n < 0 then n := -n;
primeSoFar := n >= 2;
d := 2;
While primeSoFar and (d <= sqrt(n)) do
	Begin
		primeSoFar := n mod d <> 0;
		d := d + 1;
	end;
isPrime := primeSoFar;
end;

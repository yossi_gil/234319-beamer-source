def fib():
    prev, cur = 0, 1
    while True:
        yield curr
        prev,curr = curr, prev + curr

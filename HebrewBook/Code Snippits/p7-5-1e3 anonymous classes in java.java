/**
 * Creates an {@link Iterable} over a given array of objects of the same type
 *
 * @param <T> an arbitrary type; type of objects over which the {@link Iterable} is created
 * @param ts what to iterate on
 * @return an {@link Iterable} over the parameter
*/
@SafeVarargs public static <T> Iterable<T> asIterable(final T... ts) {
  return new Iterable<T>() { // Anonymous type that implements Iterable<T>    
    @Override public Iterator<T> iterator() {// the only function member of this anonymous type
    	return new Iterator<T>() { // Anonymous type that implements Iterator<T>  
        int current = 0; // data member of this anonymous type
        @Override public boolean hasNext() { // the first function member of this anonymous type
          return current < ts.length;
        }
        @Override public T next() { // the second function member of this anonymous type
        	return ts[current++];
        }
    	};
  }
}

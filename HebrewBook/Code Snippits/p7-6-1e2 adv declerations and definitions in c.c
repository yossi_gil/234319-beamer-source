char a;                         // Definition of variable a
char c;                         // Declaration of variable c
char f() { return f(); }        // Definition of function f
char h() { return f() + h(); }  // Declaration of function h
a < b
ifTrue: [^'a is less than b']
ifFalse: [^'a is greater than or equal to b'] 
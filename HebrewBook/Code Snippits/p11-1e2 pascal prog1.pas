program p1(f)
type T = file of Integer;
var f: T;
begin
...
write(f,...);
...
end;
typedef int (*F)(int);
F makeAdder(int a) {
	int add(int x) { // Our nested function
		return x + a;
	}
	return add; // The troublemaker.
}

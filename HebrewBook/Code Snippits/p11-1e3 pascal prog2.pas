program p2(f)
type T = file of Integer;
var f: T;
begin
...
read(f,...); (* Type error *)
...
end;
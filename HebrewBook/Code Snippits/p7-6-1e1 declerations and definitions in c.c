extern char f();  // Declaration of function f
extern char a;    // Declaration of variable a
char b;           // Definition of variable b
char g() {        // Definition of function g
  extern char h();// Declaration of function h
  extern char c;  // Declaration of variable c
  char d;         // Definition of variable d
  return a + b + d + f() + g() + h();
}
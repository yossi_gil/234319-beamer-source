struct Rational { int numerator; long denominator; };

double toDouble(int i) {
	return 1.0 * i;
}
double toDouble(long l) {
	return 1.0 * l;
}
double toDouble(double d) {
	return d;
}
double toDouble(const char *s) {
	return atof(s);
}
double toDouble(struct Rational r) {
	return 
		toDouble(r.numerator) 
		/ 
		toDouble(r.denominator);20.		
}

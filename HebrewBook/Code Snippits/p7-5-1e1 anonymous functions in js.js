function foo() {
  var x = 1;
  return function() { 
    return ++x;
  };
}
print('Call the function that foo returns ' + foo()()); // will print "2"

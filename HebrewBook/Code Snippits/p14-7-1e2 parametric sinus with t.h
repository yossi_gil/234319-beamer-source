T sin(T x) {
  if (abs(x) > 1) {
     T s = sin(x/3);
     return s * (3 - 4 * s * s);
 }
  T sum = x, s = x;
  for (f = 1; abs(s) > 1E-10; f += 2) {
    s *= -x * x / ((f + 1) * (f + 2);
    sum += s;
  }
  return sum;
}
int main(int ac, char **av) {
  int& argument_count = ac;
  argument_count = atoi(av[0]);
  ac = atoi(av[1]);
  return argument_count == ac; // Will always return zero
}

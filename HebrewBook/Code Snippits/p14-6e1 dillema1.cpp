#include <iostream>

void foo() {
 std::cout << "External function" << std::endl;
}

class A {
 public:
   void foo(int i) {
     std::cout << i << std::endl;
   }
   void callingFoo() {
     foo();
   }
};

int main() {
 A a;
 a.callingFoo();
 return 0;
}

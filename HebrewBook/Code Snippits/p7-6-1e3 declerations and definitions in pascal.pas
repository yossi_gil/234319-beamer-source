Program P;
  LABEL 
    -1; (* Declaration of label -1 *)
  Procedure Q; 
    Begin (* Definition of Procedure f *)
      goto -1; (*using label -1 *)
    end; 
Begin (* Definition of Program p *)
 -1: (* Definition of label -1 *)
   Q; 
end.
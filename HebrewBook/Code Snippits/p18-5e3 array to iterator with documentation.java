  /** Convert an array of {@link Integer}s into an {@link Iterable}. For
   * example, to print the first Fibonacci numbers multiplied by the first prime
   * numbers, write:  <pre>
   * for (Integer f: asIterable(1,1,2,3,5,8,13) 
   *    for (Integer p: asIterable(2,3,5,7,11,13) 
   *       System.out.println(f*p)                              </pre>
   * @param is what to iterate on (recall that a list of arguments of the same
   *          type is isomorphic to array parameters in Java
   * @return an {@link Iterable} over the array, which can then be used to to
   *         iterate over the parameter(s) */
  Iterable<Integer> asIterableVerbose(Integer... is) {
    // Create an object of a new <em>anonymous</em> class that
    // <code><b>implements</b></code> {@link Iterable}
    return new Iterable<Integer>() {
      // An {@link Iterable} has just one function, which returns an {@link
      // Iterator}. We create here a new object of an anonymous class that
      // <code><b>implements</b></code> {@link Iterator}
      @Override public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {
          @Override public boolean hasNext() { // are more elements available? 
            return current < is.length;
          }
          @Override public Integer next() { // return (and consume) next element 
            return is[current++];
          }
          int current = 0;
        };
      }
    };
  }

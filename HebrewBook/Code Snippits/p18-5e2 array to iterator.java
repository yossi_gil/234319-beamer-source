Iterable<Integer> asIterableEssence(Integer... is) {
	return new Iterable<Integer>() {
		public Iterator<Integer> iterator() {
			return new Iterator<Integer>() {
				int current = 0;
				public boolean hasNext() { return current < is.length; }
				public Integer next() { return is[current++]; }
				};
		}
	};
}

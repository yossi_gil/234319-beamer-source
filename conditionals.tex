\begin{frame}{Conditional commands}
  \WD{Conditional command constructor}
  {If~$C₁,…,Cₙ$ are commands,~$n≥1$, and~$E₁,…,Eₙ$ are boolean expressions,
    then \begin{equation}
    ❴E₁?C₁:\,E₂?C₂:⋯:\,Eₙ?Cₙ❵
    \synopsis{Conditional command constructor}
    \end{equation}
    is a conditional command.
  }
\end{frame}

\begin{frame}{Semantics of conditional commands}
  \squeezeSpace[6]
  Semantics of 
  \begin{equation*}
    ❴E₁?C₁:\,E₂?C₂:⋯:\,Eₙ?Cₙ❵
  \end{equation*}
  Can be:
    \begin{description}
      \item [\textbf{Sequential:}] Evaluate~$E₁$, if true, then execute~$C₁$, otherwise, recursively
      execute the rest, i.e.,~$❴E₂?C₂\,:⋯\,:Eₙ?Cₙ❵$.
      \item [\textbf{Collateral:}] Evaluate~$E₁$,~$E₂$, …,~$Eₙ$ collaterally.
      If there exists~$i$ for which~$Eᵢ$ evaluates to true, then execute~$Cᵢ$.
      If there exists more than one such~$i$, arbitrarily choose one of them.
      \item [\textbf{Concurrent:}] Same as collateral, except that if certain~$Eᵢ$ are slow to execute,
      or blocked, the particular concurrency regime, prescribes running the others.
    \end{description}
  \alert{Example of a concurrency regime:}
  \begin{Block}[minipage,width=0.7\columnwidth]{Strong fairness:}
    \itshape In any infinite run, there is no process which does not execute infinitely many times.
  \end{Block}
\end{frame}

\begin{frame}[fragile]{CSP: Communicating sequential processes}
  \alert{\Occam features a concurrent conditional command:}
  \begin{TWOCOLUMNS}[0.1\columnwidth]
    \begin{Code}{OCCAM}{Jacob and his four wives}
INT kisses:
ALT -- a list of guarded commands
  rachel ¢¢? kisses
    out ¢¢! kisses
  leah ¢¢? kisses
    out ¢¢! kisses
  bilhah ¢¢? kisses
    out ¢¢! kisses
  zilpah ¢¢? kisses
    out ¢¢! kisses
    \end{Code}
    \MIDPOINT
    If none of the ‟guards” is ready, then the \kk{ALT} commands waits, and waits, and waits. \itshape
    \begin{itemize}
      \item <+-> Deep theory of ‟communicating sequential processes”
      \item <+-> \kk{ALT} is a only a small part of it
      \item <+-> \textbf{but we must proceed in our course…}
    \end{itemize}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}{The ‟else” variants}
  \squeezeSpace[5]
  \WD{Conditional command constructor with else clause}{%
    If~$C₁,…,Cₙ,C_{n+1}$ are commands,~$n≥1$, and~$E₁,…,Eₙ$ are boolean expressions, then
    \begin{equation}
      ❴E₁?C₁:\,E₂?C₂:⋯\,:\,Eₙ? Cₙ:\,C_{n+1}❵
      \synopsis{Conditional command constructor with else clause}
    \end{equation}
    is a conditional command,
    whose semantics is the precisely the same as the familiar\[❴E₁?C₁:\,E₂?C₂:⋯\,:\,Eₙ?Cₙ❵,\]
    where we define
    \begin{equation}
      Eₙ=¬E₁∧¬E₂∧⋯∧¬E_{n-1}
      \synopsis{A boolean expression for the ‟else” clause}
    \end{equation}
  }
  The ‟else” clause is sometimes denoted by:
  \begin{itemize}
    \item \kk{default}
    \item \kk{otherwise}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Variant ＃1 / many: the ‟else” clause}
  \begin{THREECOLUMNS}
    \uncover<+->{\alert{Almost all languages use ‟else”}}
    \begin{uncoverenv}<+->
      \begin{PASCAL}
If thouWiltTakeTheLeftHand
then
  iWillGoToTheRight
else
  iWillGoToTheLeft
      \end{PASCAL}
    \end{uncoverenv}
    \MIDPOINT
    \savepause{else:more} ⚓\savepause{otherwise:clause} ⚓\uncover<+->{\alert{\Pascal uses ‟Otherwise”}}
    \uncover<+->{{\renewcommand\baselinestretch{0.85}¶\scriptsize
      \kk{case} \textit{\textbf{expression}} \kk{of}⏎
      ▄\textit{\textbf{Selector}}: \textit{\textbf{Statement}};⏎
      ▄…⏎
      ▄\textit{\textbf{Selector}}: \textit{\textbf{Statement}}⏎
      \kk{otherwise}
      \Y<+->[fill=red!20,shape=rectangle callout,text width=3.3cm,xshift=36ex,yshift=46ex]{%
        ❴‟\kk{else}” \emph{instead of} ‟\kk{otherwise}” \emph{is allowed}❵
      }⏎
      ▄\textit{\textbf{Statement}};⏎
      ▄…⏎
      ▄\textit{\textbf{Statement}}⏎
      \kk{end}
      \itshape\scriptsize
      ¶(the Gnu-\Pascal's EBNF)}}
    \MIDPOINT
    \savepause{default:clause}
    ⚓\uncover<+->{\alert{\CPL uses ‟default”}}
    \begin{uncoverenv}<+->
      \begin{code}{CEEPL}
int isPrime(unsigned c) {¢¢
  switch (c) {¢¢
    case 0:
    case 1: return 0;
    case 2:
    case 3: return 1;
    default:
      return isPrime(c);
  }
}
      \end{code}
    \end{uncoverenv}
  \end{THREECOLUMNS}
\end{frame}

\begin{frame}{Variant ＃2 + ＃3 / many: if-then-else ＆ cases}
  \begin{itemize}
    \item Special construct for the case~$n=1$ in the form of
          \begin{quote}
            \kk{if} \emph{Condition} \kk{then} \emph{Statement} ⏎
            ▄ \textup{[} \kk{else} \emph{Statement} \textup{]}
          \end{quote}
          \alert{\emph{your syntax may vary}}
    \item Special construct for the case that
          \begin{itemize}
            \item each of~$Eᵢ$ is in the form~$e=cᵢ$
                  \item~$e$ is an expression (usually integral), common to all~$i=1,2,…$
                  \item~$cᵢ$ is a distinct constant expression for all~$i=1,2,…$
          \end{itemize}
          \begin{quote}
            \kk{case} \emph{Expression} \kk{of} ⏎
            ▄ \textup{❴} \emph{constantExpression} \emph{Statement} \textup{❵}\textsuperscript{+} ⏎
            \textup{[} \kk{otherwise} \emph{Statement} \textup{]}
          \end{quote}
          \alert{\emph{your syntax may vary}}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Cases with range in \Pascal}
  \begin{TWOCOLUMNS}
    \begin{Code}{PASCAL}{ROT13 Filter in \Pascal}
Program Rot13(Input, Output);
  VAR
    c:Char;
  Begin
    While not eof do begin
      Read(c);
      Case c of
        ¢¢'a'..'m', ¢¢'A'..'M':
      Write(chr(ord(c)+13));
        ¢¢'n'..'z', ¢¢'N'..'Z':
      Write(chr(ord(ch)-13));
        otherwise
           Write(c);
    end
  end
end.
    \end{Code}
    \MIDPOINT
    A selector of \Pascal's \kk{case} statement may contain
    \begin{itemize}
      \item Multiple entries
      \item Range entries
    \end{itemize}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}{☡Why special switch/case statement?}
  Because the PL designer thought…
  \begin{itemize}
    \item it would be used often
    \item it has efficient implementation on ‟wide-spread” machines
          \begin{itemize}
            \item Dedicated hardware instruction in some architecture
            \item Jump-table implementation
            \item Binary search implementation
          \end{itemize}
  \end{itemize}
  The above two reasons, with different weights, explain many features of PL.
  \Q{these are precisely the reasons for the particular specification of conditional in the form of ``if-then-else''
    for the cases~$n=1$}
\end{frame}

\begin{frame}{☡Efficient implementation + usability considerations = wrong conclusion?}
  Early versions of \Fortran relied on a very peculiar conditional statement,
  namely \emph{arithmetic if}
  \begin{oval}[0.6]
    \kk{IF} \cc{(} \emph{Expression} \cc{)}~$ℓ₁$,~$ℓ₂$,~$ℓ₃$
  \end{oval}
  where
  \begin{description}
    \item [$ℓ₁$] is the label to go to in case \emph{Expression} is negative
    \item [$ℓ₂$] is the label to go to in case \emph{Expression} is zero
    \item [$ℓ₃$] is the label to go to in case \emph{Expression} is positive
  \end{description}
  \alert{could be efficient, but not very usable in modern standards}
\end{frame}

\begin{frame}{☡Another weird (＆ obsolete) conditional statement}
  Early versions of \Fortran had a ‟computed goto” instruction
  \begin{equation}
    \kk{GO TO }\cc{(}ℓ₁,ℓ₂,…,ℓₙ\cc{) }\text{\emph{Expression}}
    \synopsis{Computed goto in \Fortran}
  \end{equation}
  where
  \begin{description}
    \item [$ℓ₁$] is the label to go to in case \emph{Expression} evaluates to~$1$
    \item [$ℓ₂$] is the label to go to in case \emph{Expression} evaluates to~$2$
    \item [$⋮$]~$⋮$
    \item [$ℓₙ$] is the label to go to in case \emph{Expression} evaluates to~$n$
  \end{description}
  \alert{likely to have efficient implementation, but not very usable in modern standards}
\end{frame}

\begin{frame}{Cases variants?}
  \begin{itemize}[<+->]
    \item Range of consecutive integer values (in \Pascal)
    \item Cases of string expression
          \begin{itemize}
            \item No straightforward efficient implementation
            \item Added in later versions of \Java after overwhelming programmers' demand
          \end{itemize}
    \item Regular expressions in selectors
          \begin{itemize}
            \item Exists in \Bash
            \item Seems natural for the problem domain
          \end{itemize}
    \item General patterns in selectors
          \begin{itemize}
            \item Exists in \ML and other functional PLs
            \item In the spirit of the PL type system
          \end{itemize}
    \item No cases statement
          \begin{itemize}
            \item In \Eiffel a pure OO language
            \item Language designer thought it encourages non OO mindset
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Vanilla multiway conditional?}
  \begin{itemize}[<+->]
    \item Exists in many languages, in the form of a special keyword
    \item \kk{elseif}, or \kk{elsif} or \kk{ELIF},
    \item e.g., in \PHP you can write
          \begin{Code}{PEEAGEPEE}{‟elseif” in \PHP}
if (¢\X$¢$a > ¢\X$¢$b) {¢¢
    echo "a is bigger than b";
} elseif (¢\X$¢$a == ¢\X$¢$b) {¢¢
  echo "a is equal to b";
} else {¢¢
  echo "a is smaller than b";
}
    \end{Code}
  \end{itemize}
\end{frame}

\begin{frame}{\protect\kk{else if}? \protect\kk{elseif}? what's the big difference?}
  \begin{itemize}[<+->]
    \item There is no big difference!
          \begin{description}
            \item [\kk{else if}] many levels of nesting
            \item [\kk{elseif}] one nesting level
          \end{description}
    \item this might have an effect on automatic indentation, but modern code formatters are typically smarter than that!
    \item another small difference occurs if the PL requires the \kk{else} part to be wrapped within ‟❴” and ‟❵”.
  \end{itemize}
\end{frame}

\afterLastFrame

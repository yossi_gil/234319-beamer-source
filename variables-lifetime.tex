\subunit[simple]{Simple lifetime}

\begin{frame}{Variable lifetime}
  \squeezeSpace
  \WD[0.75]{Variable lifetime}
  {The period between allocation of a certain variable and its deallocation is called the
    \emph{lifetime} of that variable}
  \medskip
  Main varieties: †{we will see more in the slides below}
  \begin{description}[Global/Program activation]
    \item [Persistent/Permanent] continues after program terminates
    \item [Global/Program activation] while program is running
    \item [Local/Block activation] while declaring block is active
    \item [Heap] from allocation to \emph{explicit} deallocation
    \item [Garbage collected] from allocation to automatic garbage collection
  \end{description}
  Lifetime management is important for economic usage of memory
\end{frame}

\begin{frame}{Persistent variable lifetime}
  \squeezeSpace
  \WD{Persistent variables}{%
    Variables whose lifetime continues after the program terminates are
    called \emph{persistent variables}. }
  \medskip
  \begin{description}[Serialization]
    \item [Rationale] useful for modeling entities such as
    second storage, files, databases, objects found on web services.
    \item [Existence] only a few experimental languages offer transparent persistence.
    \item [Substitute] achieved via I/O operations, e.g.,
    \CPL files: \cc{fopen()}, \cc{fseek()}, \cc{fread()}, \cc{fwrite()}
    \item [Serialization] as in \Java: language/library support the conversion of object
    into a binary image that can be written on disk or sent over
    a serial communication line; makes it possible to take objects'
    snapshot, save these, and then restore them.
  \end{description}
\end{frame}

\begin{frame}{Global \vs local lifetime: simplistic approach}
  \begin{description}[Global lifetime] \small
    \item [Global lifetime] Life of global variables starts at program startup and terminates
    with the program.
    \begin{itemize}
      \item An \emph{external variable} in~\CPL is a variable defined outside
            of all functions. All external variables have global lifetime.
      \item In \Pascal, all variables defined with the main program are global.
    \end{itemize}
    \item [Local lifetime] A ‟local” variable is a variable defined in a function or in a block.
    Its starts its life when the containing block is activated; its life
    ends when the block is terminated.
  \end{description}
  The above terminology is inappropriate since the terms suggests scope as well. However,
  \begin{itemize}
    \item There are ‟global” variables which are not universally accessible
    \item There are ‟local” variables whose lifetime is the same (or almost the same)
          as the entire program.
  \end{itemize}
\end{frame}

\begin{frame}{More on the simplistic approach}
  \framesubtitle{assumes strict block structure and one outermost block}
  What's a block?
  \begin{itemize}
    \item \Pascal functions and procedures
    \item \ML’s let expressions
          \item~\CPL and \CC’s functions (but also \cc{❴\textrm…❵} command constructor)
  \end{itemize}
  What's block activation?
  \begin{itemize}
    \item The time interval during which the block is executed
    \item The same block may be activated more than once
    \item If~$d₁$ and~$d₂$ are two durations of activation of two blocks
          (which may or may not be equal), then, precisely one of the
          following holds:
          \begin{centering}
            \begin{tabular}{*4{|c}|}
              {}$d₁=d₂$ & {}$d₁⊂d₂$ & {}$d₂⊂d₁$ & {}$d₂∩d₁=∅$
            \end{tabular}
          \end{centering}
  \end{itemize}
\end{frame}

\begin{frame}{Local ＆ global scopes}
  Local entity:
  \begin{itemize}
    \item declared in a block
    \item can be used within the block
    \item can be used within all nested blocks
  \end{itemize}
  Global entity:
  \begin{itemize}
    \item declared in the outermost block
    \item can be used within all blocks of the program
  \end{itemize}
\end{frame}

\begin{frame}{Variables in the simplistic approach}
  Global variable:
  \begin{itemize}
    \item Declared in the outer most block
    \item Lifespan is the same as that of the program
  \end{itemize}
  Local variable:
  \begin{itemize}
    \item declared in any other block
    \item lifespan is the same as block activation
    \item incarnated each time the block is activated
    \item may incarnate more than once.
    \item name may stand in fact for different variables
  \end{itemize}
  Location of declaration?
  \begin{itemize}
    \item Usually, to make the compiler’s job easier, declarations are made
          at the beginning of the block
    \item However, in \CC, \Java, declarations can be made anywhere in a block
  \end{itemize}
\end{frame}

\subunit[storage-class]{Storage class}

\begin{frame}{Terms ‟local” ＆ ‟global” are confusing}
  Better terms are ‟automatic” \vs ‟static variables”
  \WD{Storage class in~\CPL/\CC}{▄⏎
    \begin{itemize}
      \item A \emph{storage-class specifier} in~\CPL or \CC, is one of the keywords
            \kk{auto}, \kk{register}, \kk{static}, \kk{extern}, \kk{typedef},
            or \kk{thread\_local}†{the next few slides will discuss these in greater detail};
      \item it is used mainly for specifying the lifetime
            of a variable and its scope.
    \end{itemize}
  }
\end{frame}

\begin{frame}{Better terminology}
  Can be understood in terms of two of these keywords:
  \begin{description}[Program activation variable]
    \item [Block activation variable] designated by \kk{auto}; allocated on the stack
    \item [Program activation variable] designated by \kk{static}; allocated in the data segment.
  \end{description}
\end{frame}

\begin{frame}{Approximate meaning of~\CPL's storage specifiers}
  \begin{itemize}\setbeamercovered{transparent}
    \item \kk{auto}: block activation \q<+- @hl+>{block variables with no storage-class specifier default to \kk{auto}}
    \item \kk{register}: same as \kk{auto}, but with recommendation to place in a register
    \item \kk{static}: program activation
    \item \kk{extern}: program activation \q{but declaration must be done somewhere else}
    \item \kk{typedef}: empty lifetime variables \q{exists during compilation, as a template for defining other variables}
    \item \kk{thread\_local}: thread lifetime \q{not in the scope of this course}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]{\protect\kk{static} ＆ \protect\kk{auto} in blocks}
  \begin{TWOCOLUMNS}
    \begin{Code}{CEEPL}{\kkn{static} in block}
/* In the demo version of the software:
   function ¢\cc{undo()}¢ can be called only
   ten times */
void undo() {¢¢
  static counter = 10;
  if (--counter == 0)
    return;
  ¢\textrm{⋮}¢
}
    \end{Code}
    \MIDPOINT
    \begin{Code}{CEEPL}{\kkn{auto} in block}
gcd(int a, auto b) {¢¢
  while (a ¢¢!= 0) {¢¢
    auto int c = a;
    a = b % a; b = c;
  }
  return b;
}
    \end{Code}
    Type of functions with missing type specifier defaults to \kk{int}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile=singleslide]{\protect\kk{extern} ＆ \protect\kk{register} in blocks}
  \begin{TWOCOLUMNS}
    \begin{Code}{CEEPL}{\kkn{extern} in block}
isPrime(unsigned n) {¢¢
  extern isPrimeArray[];
  extern isPrimeArraySize;
  extern isPRIME(unsigned n);
  return
    n < isPrimeArraySize?
      isPrimeArray[n]
    : isPRIME(n);
}
    \end{Code}
    Variables need no type specifier if defined with a storage class; missing type defaults to \kk{int}
    \MIDPOINT
    \begin{Code}{CEEPL}{\kkn{register} in block}
isPRIME(register unsigned n) {¢¢
  register unsigned d;
  for (d = 2; d*d <= n; d++)
    if (n % d == 0)
      return 0;
  return 1;
}
    \end{Code}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[c]{Summary:~\CPL's storage specifiers in blocks}
  \begin{table}[H]
    \centering
    \begin{adjustbox}{}
      \coloredTable
      \begin{tabular}{rcl}
        \toprule
        Specifier      & Allowed? & Lifetime⏎
        \midrule
        \emph{missing} & ✓      & same as \kk{auto}⏎
        \kk{auto}      & ✓      & block activation†{rarely used}⏎
        \kk{register}  & ✓      & same as \kk{auto}†{\begin{minipage}[t]{0.8\columnwidth}
        but adds a recommendation to the compiler to place in a register
        \end{minipage}}⏎
        \kk{static}    & ✓      & program activation⏎
        \kk{extern}    & ✓      & same as \kk{static}†{but must be declared somewhere else}⏎
        \bottomrule
      \end{tabular}
    \end{adjustbox}
    \Caption[\CPL's storage specifiers in blocks]
  \end{table}
\end{frame}

\begin{frame}[fragile=singleslide]{Examples:~\CPL's storage specifiers at the external level}
  \begin{Code}{CEEPL}{File \ccn{a.c}}
auto x; // ¢✗¢
register double y; // ¢✗¢
¢¢
/* ¢\kk{static}¢ storage class: */
static N = 100; /* Accessible only from this file */
static void f(void){} /* Accessible only from this file */
¢¢
/* ¢\kk{extern}¢ storage class: */
extern M; /* Defined in some other file */
extern void h(void); /* Defined in some other file */
extern void r(void){} // ¢✗¢
¢¢
/* missing storage class: */
void g(void) {¢¢
  ¢\textrm{…}¢ } /* Accessible from other files */
¢¢
int isPrimesArray[] = {¢¢
  ¢\textrm{…}¢ }; /* Accessible from other files */
  \end{Code}
\end{frame}

\begin{frame}[fragile=singleslide]{\CPL: access to entities defined in another file}
  \begin{Code}{CEEPL}{File \texttt{b.c}}
extern N = 100; ¢✗†{can you tell (without flipping back) which storage class was used for this, and all other entities, in file \cc{a.c}}¢
extern void f(void); // ¢✗¢
¢¢
/* referred to from file ¢\cc{a.c}¢: */
int M = 1000;

/* referred to from file ¢\cc{a.c}¢: */
void h(void);
¢¢
/* Reference to function defined in file ¢\cc{a.c}¢: */
extern void g(void)

/* Reference to array defined in file ¢\cc{a.c}¢: */
extern isPrimesArray[];
  \end{Code}
\end{frame}

\begin{frame}{Lifetime of static variables in \CC and \Java}
  \emph{Static variables in~\CPL (and PL/I) are used for maintaining state
    across different activations of a block, regardless of nesting. However,
  this end is better served with OOP}¶
  \alert{\CC} (tries to maintain~\CPL compatibility)
  \begin{description}
    \item [block] from first block activation to the program's end
    \item [class] same as file level⏎
    \item [file] from construction, which occurs sometime before \cc{main()} is called until program end;
    all such ‟global” variables are constructed in some order, which is only partially specified
    by the language's standard.
  \end{description}
  \alert{\Java} (dynamic loading; truly OO)
  \begin{description}
    \item [block] no \kk{static} variables in \Java's functions or blocks.
    \item [class] when the class is first used, until program end.
    \item [file] no file level variables in \Java.
  \end{description}
\end{frame}

\begin{frame}[c]{\CPL's storage specifiers at the external (file) level}
  \begin{table}[H]
    \centering
    \coloredTable
    \begin{adjustbox}{}
      \begin{tabular}{rcl}
        \toprule
        Specifier      & Allowed? & Lifetime⏎
        \midrule
        \emph{missing} & ✓      & same as \kk{static}†{but may be referenced via \kk{extern} from other files}⏎
        \kk{auto}      & ✗      & ⏎
        \kk{register}  & ✗      & ⏎
        \kk{static}    & ✓      & program activation⏎
        \kk{extern}    & ✓      & same as \kk{static}†{but must be declared somewhere else without \kk{extern} or \kk{static}}⏎
        \bottomrule
      \end{tabular}
    \end{adjustbox}
    \Caption
  \end{table}
\end{frame}

\subunit[heap]{The heap}

\begin{frame}{The heap}
  \WD{Heap variables}{%
    \Red{Heap variables} are \emph{anonymous} variables whose lifetime spans
    \begin{description}
      \item [\Red{From}] the time they are allocated
      \begin{itemize}
        \item most commonly, directly by the programmer
        \item at times, as per the runtime environment of the PL (e.g., closures)
      \end{itemize}
      \item [\Red{Until}] they are deallocated:
      \begin{itemize}
        \item directly by the programmer, or,
        \item by the garbage collecting system
      \end{itemize}
    \end{description}
  }
\end{frame}

\begin{frame}{Intuition}
  Think of the \emph{heap} as
  \begin{itemize}
    \item Large, but not infinite, ‟bank” of memory
    \item Place from which you can ‟loan” storage for variables
    \item If loans are not returned, the bank may become ‟bankrupt”
  \end{itemize}
  The heap can be…
  \begin{description}
    \item [Builtin] managed by the PLs runtime systems (as in \Pascal and \Java)
    \item [Library based] Library is
    \begin{itemize}
      \item Standard (more or less)
      \item User replaceable (at least by some ‟sophisticated” users)
    \end{itemize}
    examples include~\CPL, and to some extent, \CC)
  \end{description}
\end{frame}

\begin{frame}[fragile=singleslide]{Motivation}
  \begin{code}{PASCAL}
Program garbage. (* Truly useless program *)
  VAR
    p: ^Integer;
Begin
  new(p); (* Allocate a cell *)
  p^ := 5; (* Set its contents *)
  dispose(p); (* Deallocate this cell *)
End.
  \end{code}
  \alert{Why heap variables?}
  \begin{itemize}
    \item When the program duration lifetime is inappropriate
    \item When the contained/disjoint dichotomy of block activation variables is inappropriate
    \item When memory size is not known in advance
    \item For realizing data structures such as linked lists, trees, graphs, etc.
  \end{itemize}
\end{frame}

\begin{frame}{Allocation ＆ deallocation}
  \alert<+->{Allocation}
  \begin{description}[<+->]
    \item [\CPL] Function \cc{malloc()} (library function)
    \item [\Pascal] Procedure \cc{new()} (predefined)
    \item [\CC's] Operator \kk{new} (builtin; can be overloaded)
    \item [\Java] \kk{new} (keyword)
  \end{description}
  \alert<+->{Deallocation}
  \begin{description}[<+->]
    \item [\CPL] Function \cc{free()} (library function)
    \item [\Pascal] Procedure \cc{new()} (predefined)
    \item [\CC's] Operator \kk{delete} (builtin; can be overloaded)
    \item [\Java] Automatically, by the GC†{GC = \textbf Garbage \textbf Collector}
  \end{description}
\end{frame}

\begin{frame}[fragile=singleslide]{Linked list with heap variables}
  \squeezeSpace
  \begin{code}{PASCAL}
TYPE IntList = ^IntNode;
      IntNode = Record
                 head: Integer;
                 tail: IntList;
               end;
VAR odds, primes: IntList;
Function cons(h: Integer; t: IntList): IntList;
VAR l: IntList
Begin
  new(l); l^.head := h; l^.tail := t; cons := l;
end;
¢⋮¢
  odds := cons(3, cons(5, cons(7, nil)));
  primes := cons(2, odds);
  odds := cons(1,odds);
  \end{code}
  \begin{Figure}[Lists with shared representation]
    \begin{adjustbox}{}
      \input{primes-and-odds.tikz}
    \end{adjustbox}
  \end{Figure}
\end{frame}

\begin{frame}{Access to heap variables}
  Heap variables are \emph{anonymous}. So, how can they be accessed?
  \WD[0.85]{Pure reference, referring, and dereferencing} {%
    ⏏
    \begin{itemize}
      \item A \emph{pure reference} or \emph{reference} or short†{not to be confused now with \CC references}
            is a value through which a program may use to
            indirectly access variable (typically heap variable)
      \item we say that a reference \emph{refers} to the variable
      \item \emph{dereferencing} is the action
            of employing a reference to access the variable it refers to
    \end{itemize}
  }
  References allow modifications that are more radical than selective updating,
  and cyclic values which are impossible otherwise.
\end{frame}

\begin{frame}{Many realizations of references}
  \begin{description}[Smart pointer]
    \item [Address] most commonly, references are
    nothing but memory addresses, in which
    case, they are called \emph{pointers}
    \item [Offset] references may be implemented as offsets
    from a fixed address.
    \item [Array index] in a language that forbids manipulation of
    memory addresses, references may be realized
    as array indices
    \item [Handle] Index into an array which contains the
    actual pointer.
    \item [Smart pointer] an abstract data type that extends the notion of
    pointers, while providing services such as
    \begin{itemize}
      \item computing frequency of use
      \item reference counting
      \item lazy copying
      \item caching
      \item legality of access checking
      \item …
    \end{itemize}
  \end{description}
\end{frame}

\begin{frame}{The \CC ‟references \vs pointers” confusion}
  \begin{TWOCOLUMNS}
    \begin{centering}
      \bfseries
      \large \CC⏎
    \end{centering}
    \alert{Pointer}, i.e., pointer variable
    \begin{itemize}
      \item May be ‟\kk{0}”, or point to a variable
      \item Can change
      \item Must be explicitly ‟dereferenced”
    \end{itemize}
    \alert{Reference}, i.e., reference variable
    \begin{itemize}
      \item May \emph{not} be ‟\kk{0}”
      \item Must point to a variable
      \item Cannot be changed
      \item No ‟dereferencing” prior to use.
      \item Is \emph{pure reference}
    \end{itemize}
    \MIDPOINT
    \begin{centering}
      \bfseries
      \large \Java (and many other PLs)⏎
    \end{centering}
    \alert{Pointer?}
    \begin{itemize}
      \item no such beast;
      \item sometimes used as a synonym for ‟reference”
    \end{itemize}
    \alert{Reference}, i.e., reference variable
    \begin{itemize}
      \item Is disjoint sum of pure-reference and \Unit⏎
      \item May be ‟\kk{null}”, or point to a variable
      \item Can change
      \item No ‟dereferencing” prior to use.
      \item Is \emph{not} pure reference
    \end{itemize}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}{The ‟null” pointer}
  \squeezeSpace
  \setbeamercovered{transparent}
  \begin{TWOCOLUMNS}
    \begin{itemize}[<+-| hl@+>]
      \item Strictly speaking, the ‟pure” definition requires that all references provide access to some variable.
      \item Still, it is useful to have references which ‟refer to nothing”; e.g., for designating the end of a linked list.
      \item It is possible to realize ‟refer to nothing” as reference to a special variable.
    \end{itemize}
    \MIDPOINT
    \begin{itemize}[<+-| hl@+>]
      \item It is more convenient to allow a special, illegal value of references instead. This value is known in different languages as
            \Q{\kk{null}, \kk{nil}, \kk{void}, \kk{nullptr}†{A new \CC keyword}, \kk{0}, etc.}
      \item In \Java and many other languages, references are disjoint sum of ‟pure references” and \Unit.
      \item \CC's references are nothing but \emph{immutable, pure references}.
    \end{itemize}
  \end{TWOCOLUMNS}
\end{frame}

\subunit[Dangling]{Dangling references}

\begin{frame}{Dangling references}
  \WD[0.8]{Dangling reference}{%
    A \emph{dangling reference} is a reference
    to a variable whose lifetime has ended, e.g., a variable
  which has been deallocated}
  Lifetime may end as a result of…
  \begin{itemize}
    \item Termination of containing block
    \item Deallocation
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]{How are dangling references created?}
  \mode<presentation>{\vspace{-3ex}}
  \begin{TWOCOLUMNS}
    \alert{I.~Freed memory}⏎
    \scriptsize
    \emph{a deallocated heap variable:}
    \begin{code}{CEEPL}
char *p = malloc(100);
strcpy(p,"Hello, World!\n");
free(p);
// ¢\cc{p}¢ is dangling
strcpy(p, p + 5); // ¢✗¢
    \end{code}
    \MIDPOINT
    \normalsize
    \alert{II.~Reference to stack}⏎
    \scriptsize
    \emph{reference to a ‟dead” automatic variable:}
    \begin{code}{CEEPL}
char *f() {¢¢
  char a[100];
  return &a;
}
char *s = f();
// ¢\texttt s¢ is dangling
strcpy(s,"Hello, World!\n"); // ¢✗¢
    \end{code}
  \end{TWOCOLUMNS}
  \normalsize
  \noindent\hfil\rule{0.9\columnwidth}{0.4pt}\hfil
  \alert{III.~Inner functions}
  \scriptsize
  \emph{Activating a function outside the enclosing block in which it was defined,
  in the case that the function uses variables local to the block}
  \squeezeSpace
  \begin{TWOCOLUMNS}[-6ex]
    \begin{code}{CEEPL}
// Provide name for function type:
typedef void (*F)(void);
// Forward declaration
F f();
// of function returning a function
F h = f();
h();
// May access a dangling reference
    \end{code}
    \MIDPOINT
    \begin{code}{CEEPL}
F f() {¢¢
  char a[100];
 // only Gnu-¢\CPL¢ allows inner functions
  void g(void) {¢¢
// ¢\texttt a¢ is dangling if ¢\texttt g¢
// is called from outside ¢\texttt f¢
    strcpy(a,"Hello, World!\n"); // ¢✗¢
  }
  return g;
}
    \end{code}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}{Language protection against dangling references}
  \squeezeSpace
  \newcolumntype{2}{@{}>{\hfill}l<{\hfill\hfill}@{}}
  \begin{table}[H]
    \begin{adjustbox}{W=1,H=0.5}
      \coloredTable
      \begin{tabular}{@{\hspace{1pt}}c@{\hspace{1pt}}*3{T{0.30}}}
        \toprule & \multicolumn1c{\textbf{Freed memory}} & \multicolumn1c{\textbf{Stack reference}} & \multicolumn1c{\textbf{Inner functions}}⏎
        \midrule
        \CPL &
        \centerline✗ programmer's responsibility &
        \centerline✗ programmer's responsibility &
        \centerline✓ no inner functions⏎
        Gnu-\CPL &
        \centerline✗ programmer's responsibility &
        \centerline✗ programmer's responsibility &
        \centerline✗ programmer's responsibility⏎
        \Pascal &
        \centerline✗ programmer's responsibility &
        \centerline✓ cannot take the address of stack variables &
        \centerline✓ inner functions cannot leak⏎
        \Java &
        \centerline✓ programmer never deallocates memory, thanks to garbage collection &
        \centerline✓ objects: always on the heap; stack: scalars or references to objects; no references to stack &
        \centerline✓ functions are not first class; their address cannot be taken⏎
        \bottomrule
      \end{tabular}
    \end{adjustbox}
    \Caption
  \end{table}
\end{frame}

\begin{frame}[fragile=singleslide]{Quiz: What's dangling here?}
  \begin{TWOCOLUMNS}
    \begin{code}{CEEPL}
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
¢¢
struct N {¢¢
  struct N *n;
  char *q;
  int a;
} *q;
¢¢
int foo(void) {¢¢
  N *p = malloc(sizeof *p);
  p->a = 42;
  p->q = "life, universe, everything";
  p->n = (struct N *)p;
  q = p;
  free(p);
  return 1;
}
    \end{code}
    \MIDPOINT
    \begin{code}{CEEPL}
int main() {¢¢
  free(
    strcpy(
       (char *)malloc(20),
       foo() + "Hello, World\n"
    )
  );
¢¢
  (void) printf("q=%p\n", q);
  (void) printf("q->a=%d\n", q->a);
  (void) printf("q->q=%s\n", q->q);
¢¢
  return 0;
}
    \end{code}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile=singleslide]{What's the penalty of accessing a dangling reference}
  Well, it depends whether you are lucky or not…
  \begin{description}[Extremely unlucky]
    \item [Lucky] Immediate program crash
    \item [Unlucky] Program crashes, but not immediately
    \item [Extremely unlucky] Program does not crash while testing; it just
    has a bug which stays dormant until field trial!
  \end{description}
  \alert{\emph{In our case, the output is}}
  \squeezeSpace
  \begin{CENTER}[0.7\columnwidth]
    \begin{block}{Output of our dangling reference program}
      \begin{session}
q=0x2314010
q->a=42
Segmentation fault (core dumped)
      \end{session}
    \end{block}
  \end{CENTER}
  \par
  We were quite lucky, however, if we would not have accessed the \cc{q}⏎
  field, the bug would not have been detected!
\end{frame}

\begin{frame}[fragile=singleslide]{Stack corruption via dangling reference into an automatic variable}
  \squeezeSpace[6]
  \begin{TWOCOLUMNSc}
    \begin{code}{CEEPL}
#include <string.h>
#include <stdio.h>
¢¢
char *f(void); // Forward declaration
    \end{code}
    \MIDPOINT
    \begin{code}{CEEPL}
main() {¢¢
  char *hell = f();
  printf("%s\n", hell);
  return 0;
}
¢¢
char *f(void) {¢¢
  char s[1<<9];
  strcpy(s, "Hello, World\n");
  return s;
}
    \end{code}
  \end{TWOCOLUMNSc}
  \begin{TWOCOLUMNSc}
    \begin{code}[width=\columnwidth,minipage]{}
Hello, W¢☡☡☡…†{Lots of unprintable characters}¢
987
¢☡☡☡…¢
    \end{code}
    \MIDPOINT
    \alert{\emph{The output looks almost right, but the stack is clearly corrupted.}}
  \end{TWOCOLUMNSc}
\end{frame}

\subunit[heap-errors]{Heap errors}

\begin{frame}{Dangling references}
  \footnotesize \setbeamercovered{transparent}
  \onslide<+-| hl@+>{
    Recall: ‟a \emph{dangling reference} is a reference
  to a variable whose lifetime has ended…}
  \onslide<+-| hl@+>{Lifetime may end as a result of… }
  \begin{itemize}[<+- |hl@+>]
    \item Termination of containing block
    \item Deallocation
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Memory leak}
  \WD[0.8]{Memory leak}{A \emph{memory leak} occurs when a variable is not
    deallocated prior to the termination of the lifetime of all of its
  references.}
  \begin{TWOCOLUMNS}
    \begin{code}{CEEPL}
#include <stdlib.h>
#include <stdio.h>
¢¢
typedef struct N {¢¢
  int a[1000000000];
} N;
¢¢
N *t(N *n){¢¢
 N *p = malloc(sizeof *p);
 if (p ¢¢!= 0) return p;
 perror("OOPS");
 exit(1);
}
    \end{code}
    \MIDPOINT
    \begin{code}{CEEPL}
int main() {¢¢
  int i;
  N *s = 0;
  for (i = 0 ; i < 1 << 30; i++)¢\callout{Can you pinpoint the leak?}{-20}{24}¢
    printf("%5d) %p\n",i,s=t(s));
  return 0;
}
    \end{code}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile]{Output of the above program}
  \begin{TWOCOLUMNS}
    \begin{session}
    0) 0x7f7b8adf0010
    1) 0x7f7a9c73d010
    2) 0x7f79ae08a010
     ¢⋮¢
 1000) 0x7bd8382b8010
 1001) 0x7bd749c05010
 1002) 0x7bd65b552010
     ¢⋮¢
10000) 0x5b1a4fdc0010
10001) 0x5b196170d010
10002) 0x5b187305a010
     ¢⋮¢
    \end{session}
    \MIDPOINT
    \begin{session}
20000) 0x36b914d90010
20001) 0x36b8266dd010
20002) 0x36b73802a010
     ¢⋮¢
30000) 0x1257d9d60010
30001) 0x1256eb6ad010
30002) 0x1255fcffa010
     ¢⋮¢
35180) 0x7ffc1105b010
35181) 0x7ffcff70e010
35182) 0x7ffdeddc1010
    \end{session}
    \Red{\texttt{OOPS: Cannot allocate memory}}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}{Simple heap management with linked list of free blocks}
  \squeezeSpace
  (we maintain the invariant that free regions are in ascending addresses)
  \begin{description}[Deallocation request]
    \item [Allocation request] Traverse the list, searching for a region large enough to accommodate
    the request, using a ‟\emph{first fit}”, ‟\emph{best fit}” or ‟\emph{worst fit}” strategy.
    \begin{description}[Non-exact match]
      \item [Exact match] the region is returned to the client and removed from the list
      \item [Non-exact match] the region is split into two:
      \begin{enumerate}
        \item a sub-region is of the appropriate size is returned to the client;
        \item the other sub-region is kept in the list.
      \end{enumerate}
    \end{description}
    \item [Deallocation request] Add the region to the list; if there are no gaps, with the previous/next region,
    the two are merged.
  \end{description}
  \alert{\emph{Most real life implementations use much more sophisticated data structures.}}
\end{frame}

\begin{frame}{Heap fragmentation}
  \WD[0.73]{Heap fragmentation}{%
    \emph{Heap fragmentation} is the situation where
    \begin{itemize}
      \item total free memory managed by the heap data structure is large,
      \item all individual free regions are small, consequently,
      \item the heap cannot fulfill allocation requests
    \end{itemize}
  }
  \begin{itemize}
    \item \emph{Memory compaction}, or \emph{heap de-fragmentation} is the process by which
          allocated memory regions are moved to make
          all free memory consecutive.
    \item Compaction is not easy to implement since it requires modifying the variables of the program,
          while it is running.
    \item \emph{Handles} however, makes compaction fairly easy.
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]{Heap corruption}
  \WD{Heap corruption}{%
    \emph{Heap corruption} occurs when the heap's client does not obey its protocol,
  leading to faulty state of the data structure representing the heap. }
  Operations leading to heap corruption:
  \begin{itemize}
    \item Freeing a valid reference which was not allocated by the heap.
    \item Freeing an invalid reference.
    \item Freeing a valid reference, allocated by the heap, but more than once.
  \end{itemize}
  \begin{TWOCOLUMNS}[0.2\columnwidth]
    \begin{code}{CEEPL}
#include <stdlib.h>
main() {¢¢
  int a[10];
  free(a);
}
    \end{code}
    \MIDPOINT
    \begin{session}
Segmentation fault (core dumped)
    \end{session}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}{Summary: heap programming errors}
  \begin{description}[Multiple deallocation]
    \item [Dangling references] keeping referencing to deallocated heap variable
    \item [Memory leak] loosing the last reference to an allocated variable
    \item [Heap corruption] can occur after any of these errors
    \begin{description}[Multiple deallocation]\small
      \item [Multiple deallocation] deallocating the same reference more than once
      \item [Wrong deallocation] deallocating a reference to a variable which was not allocated from the heap
      \item [Buggy deallocation] deallocating an invalid reference
    \end{description}
    \item [Heap fragmentation] when many small regions comprise the heap
  \end{description}
  \emph{Garbage collection will come to the rescue.}
\end{frame}

\afterLastFrame

\references
\begin{itemize}\D{Automatic variable}{http://en.wikipedia.org/wiki/Automatic\_variable}
  \D{Call stack}{http://en.wikipedia.org/wiki/Call\_stack}
  \D{Dangling reference}{http://en.wikipedia.org/wiki/Dangling\_reference}
  \D{Global variables}{https://en.wikipedia.org/wiki/Global\_variable}
  \D{Heap}{https://en.wikipedia.org/wiki/Heap\_(programming)}
  \D{Local variables}{http://en.wikipedia.org/wiki/Local\_variable}
  \D{Stack-based memory allocation}{http://en.wikipedia.org/wiki/Stack-based\_memory\_allocation}
  \D{Stack protection}{https://en.wikipedia.org/wiki/Stack\_protection}
  \D{Static local variables}{http://en.wikipedia.org/wiki/Local\_variable＃Static\_local\_variables}
  \D{Static memory allocation}{https://en.wikipedia.org/wiki/Static\_memory\_allocation}
  \D{Static variables}{https://en.wikipedia.org/wiki/Static\_variable}
  \D{Storage classes}{https://en.wikipedia.org/wiki/Storage\_class＃Storage\_duration\_specifiers}
  \D{Typedef}{https://en.wikipedia.org/wiki/Typedef}
  \D{Variable}{http://en.wikipedia.org/wiki/Variable\_(computer\_science)}
\end{itemize}
\exercises
\begin{enumerate}
  \item What is \kk{nullptr}?
  \item Provide an example for variables whose lifetime is close, but not identical to program duration.
  \item How can you emulate a \kk{null} value of pointers in languages which do not have such a value?
  \item How can you emulate references in languages, such as \Fortran, which do provide this feature?
  \item What is so unique in the lifetime of the \kk{static} class variables in \Java?
  \item Explain (using nominal \vs structural equivalence) why the inclusion of persistent variable in a PL is so difficult.
  \item Write a~\CPL program in which there is a memory leak.
  \item Write a \Pascal program in which there is a memory leak.
  \item Why do parameters to~\CPL function have an \kk{auto} storage class by default? What happens in \Java? \Pascal? \AWK? \Go?
  \item Write a~\CPL program which creates a dangling reference to the stack.
  \item Smart pointers are a kind of references. Explain why.
  \item Explain, using the \Pascal primes and odds example, how functional languages
        such as \Lisp, in which all values are immutable, can use memory more efficiently.
  \item Revisit the word frequencies \AWK program described in the ‟arrays” subsection.
        \begin{enumerate}
          \item What's the lifetime of variables in \AWK?
          \item What are handles?
          \item How do you think this is realized?
        \end{enumerate}
  \item Can \CC references be called ‟pure references”?
  \item Why can't you ‟improve”~\CPL by forbidding taking the address of stack variables?
  \item Can a multi-threaded language such as \Java have a ‟stack”? Explain.
  \item Explain, using examples, how references make it possible to create values which
        could not be created without references.
  \item Suggest a method for employing handles for memory de-fragmentation, even in languages
        which do not offer garbage collection.
  \item Provide an example for ‟global” variables, which are not globally accessible.
  \item Write a \Pascal program which creates a dangling reference to the stack;
        if you cannot do so, explain why.
  \item ‟If all memory allocations are of the same size, then the heap
        can be managed using a stack data structure.” Explain.
  \item Are references in \ML pure references? Explain.
  \item When does \AWK use dynamic memory allocation?
  \item Describe the algorithm for memory compaction when handles are used to represent references.
  \item People say that variables with \kk{typedef} storage class are allocated in the ‟ghost” memory segment. Explain.
\end{enumerate}

\begin{frame}{Sequential block constructor}
  \squeezeSpace
  \WD[0.80]{Sequential block constructor}{If~$C₁,…,Cₙ$ are commands,~$n≥0$,
    then
    \begin{equation}
      ❴C₁;C₂;…;Cₙ❵
      \synopsis{Sequential block command constructor}
    \end{equation}
    is a composite command, whose semantics is
    \Red{\emph{sequential}}:~$C_{i+1}$ is executed after~$Cᵢ$ terminates.}
  \medskip
  \begin{TWOCOLUMNS}
    \begin{itemize}
      \itshape
      \item Most common constructor
      \item Makes it possible to group several commands, and use them as one, e.g., inside a conditional
      \item If your language has no \textsf{skip} command, you can use the empty sequence,~$❴❵$.
    \end{itemize}
    \MIDPOINT
    \begin{description}[▄]
      \item [Separatist Approach:] semicolon \emph{separates} commands; used in \Pascal; mathematically clean; error-prone.
      \item [Terminist Approach:] semicolon \emph{terminates} commands (at least atomic commands);
      used in~\CPL/\CC/\Java/\CSharp and many other PLs; does \emph{not} match the above definition.
    \end{description}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}{Collateral block constructor}
  \squeezeSpace
  \WD[0.82]{Collateral block constructor}
  {If~$C₁,…,Cₙ$ are commands,~$n≥0$, then
    \begin{equation}
      ❴C₁∿C₂∿⋯∿Cₙ❵
      \synopsis{Collateral block command constructor}
    \end{equation}
    is a composite command,
    whose semantics is that~$C₁,…,Cₙ$ are executed \Red{\emph{collaterally}}.}
  \medskip
  \alert{Very rare, yet (as we shall see) important}
  \begin{itemize}
    \item Order of execution is \Red{non-deterministic}
    \item An optimizing compiler (or even the runtime system) can choose ‟best” order
    \item Good use of this constructor, requires the programmer to design~$C₁,…,Cₙ$
          such that, no matter what, the result is
          \begin{itemize}
            \item \emph{programmatically identical}, \emph{or}
            \item at least, \emph{semantically equivalent}
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Programmatically identical \vs semantically equivalent}
  \squeezeSpace[4]
  \begin{TWOCOLUMNS}
    \begin{center}
      \bfseries
      \alert{Programmatically Identical}
    \end{center} {\scriptsize\itshape
      Now these are the generations of the sons of Noah, Shem, Ham, and Japheth:
      and unto them were sons born after the flood.
      \begin{enumerate}
        \item The sons of Japheth; Gomer, and Magog, and Madai, and Javan, and Tubal, and Meshech, and Tiras…
        \item And the sons of Ham; Cush, and Mizraim, and Phut, and Canaan
        \item The children of Shem; Elam, and Asshur, and Arphaxad, and Lud, and Aram
      \end{enumerate}
    }
    \begin{code}{JAVA}
{¢¢
 grandsons += 7;
¢∿¢
 grandsons += 4;
¢∿¢
 grandsons += 5;
}
    \end{code}
    \MIDPOINT
    \begin{center}
      \bfseries \alert{Semantically Equivalent}
    \end{center}
    \begin{code}{JAVA}
{¢¢
   humanity.add("Adam");
 ¢∿¢
   humanity.add("Eve");
}
    \end{code}
    At the end, both \cc{"Adam"} and \cc{"Eve"} will belong
    to \cc{humanity}; but the internals of the \cc{humanity} data structure might
    be different.
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}{Concurrent block constructor}
  \squeezeSpace[6]
  \WD[0.76]{Concurrent block constructor}
  {If~$C₁,…,Cₙ$ are commands,~$n≥0$, then
    \begin{equation}
      ❴C₁|C₂|⋯|Cₙ❵
      \synopsis{Concurrent block command constructor}
    \end{equation} is a composite command,
    whose semantics is that~$C₁,…,Cₙ$ are executed \Red{\emph{concurrently}}.}
  \alert{Common in concurrent PLs, e.g., \Occam}
  \begin{itemize}
    \item Just like ‟collateral”…
    \item Commands can be executed in any order; order of execution is
      non-deterministic
    \item An optimizing compiler (or even the runtime system)
      can choose ‟best” order
    \item 
          Good use of this constructor, requires the programmer to design~$C₁,…,Cₙ$;
          such that, no matter what, the result is, \emph{programmatically identical},
          \emph{\textbf{or}} \emph{semantically equivalent}
  \end{itemize}
\end{frame}

\begin{frame}{☡Collateral \vs concurrent collateral}
  \begin{TWOCOLUMNS}
    \begin{centering}
      \Red{Collateral}
    \end{centering}
    really means ‟not guaranteed to be sequential”, or ‟undefined”; PL
    chooses the extent of defining this ‟undefined”, e.g.,
    \begin{oval}[0.9]
      ‟\alert{the
        order of evaluation of~$a$ and~$b$ in~$a+b$ is unspecified.
        Also, the runtime behavior is undefined in the
      case~$a$ and~$b$ access the same memory}”.
    \end{oval}
    \MIDPOINT
    \begin{centering}
      \Red{Concurrent}
    \end{centering}
    \emph{may} be executed in parallel, which is an
    extent of definition of a \alert{collateral execution}.
    \begin{oval}[0.9]
      ‟\alert{the evaluation of~$a+b$ by executing~$a$ and~$b$ concurrently;
        as usual, this concurrent execution is \emph{fair} and 
        \emph{synchronous}, which means that…}”.
    \end{oval}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile]{Concurrent execution in \Occam}
  \squeezeSpace
  ⚓\begin{TWOCOLUMNS}
  \begin{uncoverenv}<+->
    \begin{Code}{OCCAM}{The cow}
PROC cow(CHAN INT udder!)
  INT milk: -- definitions are ':' terminated
  SEQ
    milk := 0
    WHILE TRUE
      SEQ
        udder ¢¢! milk
        milk := milk + 1
: -- end of ¢\kk{PROC}¢ ¢\cc{cow}¢
    \end{Code}
  \end{uncoverenv}
  \begin{uncoverenv}<+->
    \begin{Code}{OCCAM}{The calf}
PROC calf(CHAN INT nipple?)
  WHILE TRUE
    INT milk:
    SEQ
      nipple ¢¢? milk
: -- end of ¢\kk{PROC}¢ ¢\cc{calf}¢
    \end{Code}
  \end{uncoverenv}
  \MIDPOINT
  \begin{uncoverenv}<+->
    \begin{Code}{OCCAM}{The cowshed}
PROC cowshed()
  CHAN INT mammaryGland:
  PAR
    calf(mammaryGland?)
    calf(mammaryGland?)
    calf(mammaryGland?)
    calf(mammaryGland?)
    cow(mammaryGland!)
: -- end of ¢\kk{PROC}¢ ¢\cc{cowshed}¢
    \end{Code}
  \end{uncoverenv}
  \end{TWOCOLUMNS}
\end{frame}

\afterLastFrame

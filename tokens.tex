\bash
cat << HELLOEOF > hello.sh
rm -f hello.c a.out
cat << EOF > hello.c
#include <stdio.h>

int main(int argc, char *argv[], char **envp) {
  return printf("Hello, World!\n") <= 0;
}
EOF
cc hello.c
./a.out
HELLOEOF
\END

\bash
chmod +x hello.sh
./hello.sh
\END

\bash
cat << EOF > Hello.java
public class Hello {
  public static void main(final String[] args) {
    System.out.println("Hello, World!\n");
  }
}
EOF
\END

\begin{frame}[fragile]{Writing a ‟Hello, World!” program in \protect\CPL}
  {Using \protect \Bash, Gnu/Linux, etc.}
  Programming involves many technical activities:
  \centering
  \begin{columns}
    \column{0.3\columnwidth}
    \structure{Actions}
    \begin{itemize}
      \item Authoring
      \item Compiling
      \item Linking
      \item Executing
    \end{itemize}
    \column{0.7\columnwidth}
    \structure{Concretely}
    \lstinputlisting[language=BASH]{+/hello.sh}%
  \end{columns}
\end{frame}

\begin{frame}{Authoring ‟Hello, World!” with the \protect\cc{gvim} text editor}
  \begin{Figure}
      \includegraphics[width=0.80\columnwidth]{Graphics/Screenshot-hello-c-GVIM.png}%
      \vspace{-10ex}{⚓}⏎
      \mbox{%
        \hspace{0.17\columnwidth}%
        \includegraphics[width=0.80\columnwidth]{Graphics/Screenshot-hello-p.png}
      }
      {⚓}
    \end{Figure}
  \begin{itemize}[<+->]
    \item The \cc{gtksourceview} library makes these so similar…⏎
    \item Still, the similarity of PLs makes \cc{gtksourceview} possible!
  \end{itemize}
\end{frame}

\subunit[kinds-of-tokens]{Kinds of tokens}

\begin{frame}[fragile]{Tokens are the terminals of a CFG}
  \centering
  \begin{TWOCOLUMNS}
    \begin{Code}{JAVA}{Context free grammar for expressions (plain BNF)}
Expression = Terms
Terms: Term
Terms: Term Addition Terms;
Term: Factors;
Factors: Factor:
Factors: Factor Multiplication Factor;
Factor: Variable-Name;
Factor: Number;
Factor: '(' Expression ')';
Addition: '+';
Addition: '-';
Multiplication: '*';
Multiplication: '/';
    \end{Code}
    \MIDPOINT
    \begin{table}[H]
      \coloredTable
      \begin{centering}
        \begin{tabular}{ll}
          \toprule
          \textbf{Tokens}                 & \textbf{Kind}⏎
          \midrule
          {⚓}‟\cc{(}”, ‟\cc{)}” & {⚓} punctuation {⚓}⏎
          ‟\cc{*}”, ‟\cc{+}”      & {⚓} operators {⚓}⏎
          \cc{Variable-Name}              & {⚓} identifier {⚓}⏎
          \cc{Number}                     & {⚓} literal{⚓} ⏎
          \bottomrule
        \end{tabular}
      \end{centering}
      \Caption[Classification of tokens in a BNF for simple expressions]
    \end{table}
    Note that comments do not show up in the grammar.
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}{What do tokens denote?}
  \begin{table}[H]
    \begin{centering}
      \begin{adjustbox}{}
        \coloredTable
        \begin{tabular}{lL{0.35}L{0.35}z}
          \toprule
          \textbf{Kind}         & \textbf{Example}                                             & \textbf{Denotes?}                  & ⏎
          \midrule
          Identifier            & \cc{main}, \cc{i}, \cc{printf}, \cc{argv}                    & a ‟nameable”                   & ⏎
          Literal               & ‟\cc{132}”, \cc{"\textit{Hello, World\textbackslash n}"} & itself                             & ⏎
          Operator              & ‟\cc{*}, ‟\cc{+}”, ‟\cc{*}”, ‟\cc{/}”          & a builtin function                 & ⏎
          Punctuation           & ‟\cc{;}, ‟\cc{,}”, ‟\cc{(}”, ‟\cc{)}”          & nothing (reading and parsing aide) & ⏎
          Reserved word         & \kk{if}, \kk{class}, \kk{int}                                & …                                & ⏎
          Reserved identifier   & \kk{int}, \kk{class}, \kk{int}                               & primitive type                     & ⏎
          Predefined identifier & \kk{Integer}, \&primitive type                               & ⏎                                
          Comments              & \cc{/* } fubar \cc{*/}                                       & Nothing!                           & ⏎
          \bottomrule
        \end{tabular}
      \end{adjustbox}
    \end{centering}
    \Caption
  \end{table}
  Comments are not officially tokens, but they also belong to the atomic elements
  of the language.
\end{frame}

\begin{frame}[<+->]{Names aka identifiers}
  \begin{itemize}
    \item Create an \emph{entity} once, refer to it many times
    \item Essential for modular large-scale programming
    \item Largely a \emph{nuisance}!
          \begin{itemize}
            \item good names are scarce
            \item difficult to make up, type, read, and understand
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Nameables}
  \WD{Nameable}{%
    A \Red{nameable} is an entity kind, such as functions, modules, types, constants, variables,
    for which the programmer can provide a name.
  }
  {⚓}
  \begin{TWOCOLUMNS}
    \begin{Code}{PASCAL}{Nameable values in \Pascal}
CONST
  Pi = 3.14159265358979323846264
    \end{Code}
    \MIDPOINT
    {⚓}
    \begin{Code}{CEEPL}{Nameable types in \CPL}
// Type named ¢\kk{struct} \cc{Date}¢:
struct Date {¢¢
  int month, day, year;
};
    \end{Code}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile]{Nameable values in \CPL/\CC/\Java?}
  \begin{itemize}[<+->]
    \item Values are not in \CPL, (the preprocessor is not part of the language)…
    \item not in \CC, neither in \Java,…
    \item but there are workarounds:\par
          \begin{TWOCOLUMNS}
            \structure{\CPL/\CC:}
            \begin{code}{CEEPL}
// Only for integer constants
enum {¢¢
  BELL = '\b'
  TAB = '\t',
  NL = '\n',
  CR = '\r',
}
            \end{code}
            {⚓}
            \begin{code}{CEEPL}
const double E = 2.718281824;
const int Merssene7 = 524287;
            \end{code}
            \MIDPOINT
            {⚓}
            \structure{\Java:}
            {⚓}
            \begin{code}{JAVA}
final double E = 2.718281824;
final int Merssene7 = 524287;
            \end{code}
          \end{TWOCOLUMNS}
  \end{itemize}
\end{frame}

\begin{frame}{Legal names}
  All PLs include a definition of ‟legal names”:
  \WD{\protect\CPL identifiers}{%
    A \Red{\CPL identifier} is a series of alphanumeric characters,
    the first being a letter of the alphabet or an underscore, and the
    remaining being any letter of the alphabet, any numeric digit, or the underscore.
  }
  \begin{itemize}[<+->]
    \item Regular expression
          \begin{equation}
            \cc{[\_a-zA-Z][\_a-zA-Z0-9]*}
            \synopsis{Regual expression defining identifiers' syntax in a typical modern PL}
          \end{equation}
    \item Most PLs follow the same pattern.
    \item But, there are always annoying \emph{exceptions}:
          \begin{itemize}[<+->]
            \item \TeX: digit and underscores are forbidden
            \item Early \Basic: a single letter, optionally followed by a digit
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[<+->]{Variations}
  \begin{description}[lower/UPPER case]
    \item [lower/UPPER case] due to historical or ideological reasons.
    \item [Length limit] typically 6--8 in ancient languages,∿32 or unlimited in modern languages
    \item [Special characters] Can a name contain a dollar (yes, in \Java), space (in Fortran), a quote (in JTL), or what have you?
    \item [Unicode] Ain't ‟\cc{$α$}” an excellent variable name in certain contexts?
  \end{description}
\end{frame}

\begin{frame}{What's Unicode?}
  \begin{itemize}
    \item a system for encoding characters
    \item more than 110,00 characters
    \item covers \textasciitilde100 scripts, representing most of the world's writing systems
    \item Standard in Windows (NT/XP/Vista/2000/7), Linux, Mac OS X.
    \item Extends and replaces ASCII (7 bit standard, used primarily for American English)
  \end{itemize}
\end{frame}

\begin{frame}{Names and kind distinction}
  \begin{itemize}
    \item Names can be used for \emph{very different} entities.
    \item ‟Readability”†{no one knows what ‟readability” really means”}, concerns as well as parsing issues make PL impose rules such as:
  \end{itemize}
  \begin{description}
    \item [Prolog] first character determines \emph{grammatical role} (lowercase: function; uppercase/underscore: variable)
    \item [Perl] first character determines \emph{structure}, e.g., ‟％”
    for hashtable.
    \item [Fortran] first character determines \emph{type}, e.g., ‟\cc{i}” must be an integer.
  \end{description}
\end{frame}

\begin{frame}{Names and naming conventions}
  Many PLs employ \emph{naming conventions},
  \begin{itemize}
    \item For distinguishing between categories, types ‟must” be capitalized.
    \item For making a single name out of multiple words:
          \begin{description}[under\_scoring]
            \item [PascalCase] \cc{FileOpen}, \cc{WriteLn} (e.g., \Pascal)
            \item [camelCase] \cc{fileOpen}, \cc{getClass} (e.g., \Java)
            \item [under\_scoring] \cc{file\_open} (e.g., \CPL)
            \item [juxtaposition] \cc{fileopen}, \cc{\textbackslash textbackslash} (e.g., \TeX)
          \end{description}
    \item For denoting type, e.g., using the \emph{Hungarian Notation}, denotes a variable \cc{arru8NumberList}
          whose type is an \textbf array of
          \textbf unsigned \textbf 8-bit integers
  \end{itemize}
\end{frame}

\begin{frame}{Names ＆ your new PL}
  \begin{itemize}
    \item Try to understand the language peculiarities:
          \begin{itemize}
            \item What ‟special characters” are allowed? Why?
            \item Is there lower/upper case distinction? Why?
            \item Is there a length limit? Why?
            \item What is the language naming convention, if any?
          \end{itemize}
    \item Remember, modern languages tend to:
          \begin{enumerate}
            \item impose no length limit
            \item use Unicode
            \item distinguish between upper and lower case
            \item rely on conventions rather than syntax for distinguishing kinds.
          \end{enumerate}
  \end{itemize}
\end{frame}

\begin{frame}{Keywords}
  \WD{Keywords/reserved words}{%
    A \Red{keyword} (also called a \Red{reserved word}) is a string of characters which makes a legal
    name, yet, it is reserved for special purposes and cannot be used
    by the programmer for any other purpose.
  }
  ⚓\Pascal Examples:
  \begin{itemize}
    \item \cc{program}
    \item \cc{begin}
    \item \cc{end}
    \item \cc{record}
    \item \cc{of}
  \end{itemize}
\end{frame}

\begin{frame}{Keywords ＆ atomic types}
  \begin{itemize}
    \item Each of the atomic types in \Java is denoted by a keyword.
    \item Some \CPL atomic types have names made of
          two, and even three keywords:
          \begin{itemize}
            \item ‟\kk{unsigned short}”
            \item ‟\kk{unsigned long int}”
          \end{itemize}
    \item Some \CPL atomic types have more than one name, e.g.,
          \begin{itemize}
            \item ‟\kk{long}”
            \item ‟\kk{long int}”
            \item ‟\kk{signed long}”
            \item ‟\kk{signed long int}”
          \end{itemize}
    \item Atomic types in \Pascal are denoted by \emph{predefined identifiers}
  \end{itemize}
\end{frame}

\bash
cat << EOF > misnomer.p
Program misnomer;
TYPE
  Double = Real;
  Boolean = Integer;
VAR
  Integer: Boolean;
  Real: Double;
Begin
  Integer := 3;
  Real := Integer / Integer;
  WriteLn('i = ',Integer,' r = ',real)
end.
EOF
\END

\bash[verbose,ignoreStderr]
rm *.stderr
pc misnomer.p
\END

\bash[verbose,stdoutFile=misnomer.out]
pwd
./misnomer
\END

\begin{frame}{Atomic types in \Pascal are predefined}
  {a somewhat confusing fact of life}
  \WD{Predefined identifier} {%
    A \Red{predefined identifier}
    \begin{itemize}
      \item identifier
      \item that is bounded to an entity (such as type, function, procedure or value)
      \item this binding is made by the PL, with no programmer intervention
      \item can be bounded to another entity later on.
    \end{itemize}
  }
  \emph{Redefinition of predefined identifiers is legal, but might be confusing}
\end{frame}

\begin{frame}{Redefinition of predefined identifiers}
  \begin{columns}
    \begin{column}{0.5\columnwidth}
      \begin{block}{Confusing program}
        \lstinputlisting[style=PASCAL]{+/misnomer.p}%
      \end{block}
    \end{column}
  \end{columns}
  \lstinputlisting[basicstyle=\scriptsize\ttfamily]{+/misnomer.out}%
  \par
  Observe that my pretty printer got confused as well.
\end{frame}

\begin{frame}{Reserved identifiers}
  \WD[0.6]{Reserved identifier}{%
    A \emph{reserved identifier} is a keyword used as an identifier.
  }
  \begin{itemize}
    \item The word \kk{int} is a reserved identifier, it identifies the
          \emph{integer} atomic type.
    \item The identifiers \cc{Integer} and \cc{WriteLn} in
          \Pascal are not reserved.
          The programmer may redefine these.
    \item Not all reserved words are reserved identifiers:
          \begin{itemize}
            \item \kk{return} in \CPL (an atomic command).
            \item \kk{begin}, \kk{end}, \kk{program} in \Pascal (punctuation).
            \item \kk{struct} in \CPL (a constructor for creating compound types from
                  other types)
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Routines whose name is a reserved identifier?}
  In most PLs, the names of ‟standard” routines (procedures and functions) are
  \emph{not} reserved:
  \begin{itemize}
    \item They are either \emph{imported} nor \emph{builtin}
    \item A notable exception is \AWK:
          \begin{description}[print]
            \item [print] A builtin function, for printing.
            \item [exit] A builtin function, stopping execution.
            \item [int] A builtin function, for conversion into an integral type.
          \end{description}
          Unlike \Pascal, builtin names in \AWK \emph{are} reserved.
  \end{itemize}
\end{frame}

\begin{frame}{Summary: kinds of identifiers}
  \begin{itemize}
    \item Identifiers:
          \begin{itemize}
            \item Reserved identifier
            \item Predefined identifier
            \item Library identifier
            \item Other
          \end{itemize}
          \alert{In addition, we have those reserved words which are not identifiers.}
          \begin{itemize}
            \item Identifiers reserved for future use
            \item Denotation of atomic entities
            \item Punctuation (often used in constructors of entities)
            \item Other, e.g., marking Boolean attributes (\kk{register}, \kk{auto}, \kk{static} in the \CPL PL)
            \item there are so many PLs, we cannot hope to classify them all
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{The \Go PL}
  {Can you classify the identifiers and reserved words used here?}
  \begin{code}{GO}
// "Hello, World!" in ¢\Go¢
package main
import "fmt"
func main() {¢¢ // main function
  fmt.Printf("Hello, World!\n")
}
  \end{code}
  ⚓
  \begin{description}[<+->]
    \item [\kk{package}] reserved word, punctuation
    \item [\kk{import}] reserved word, punctuation
    \item [\kk{func}] reserved word, punctuation
    \item [\cc{main}] identifier, other
    \item [\cc{fmt}] identifier, library
    \item [\cc{Printf}] identifier, library
  \end{description}
\end{frame}

\subunit[library-identifiers]{Library identifiers}

\begin{frame}{Why a library?}
  \begin{itemize}
    \item The set of executable commands is always a recursively defined set.
    \item Derivation rules are language dependent, typically including \emph{blocks}, \emph{iterations}, \emph{conditionals}, and \emph{routines}
    \item Atomic executables include
          \begin{itemize}
            \item Commands denoted by keywords, e.g., \kk{return}, \kk{break} and \kk{continue}
            \item Other atomic commands such as \emph{assignment}.
            \item Invocation of routines
          \end{itemize}
  \end{itemize}
  Some routines are so
  \begin{description}[low-level]
    \item [low-level] that they cannot be implemented within the language
    \item [essential] that there is little point in having each programmer redo them
    \item [tiresome] that most programmers could not be bothered implementing them
  \end{description}
\end{frame}

\begin{frame}{Library identifiers}
  \WD{Library}{%
    A collection of pre-made routines (or modules) that are available to the programmer.
  }
  \begin{description}[standard library]
    \item [standard library] replaceable (as in \CPL)
    \item [builtin library] cannot be replaced (as in \Pascal and \AWK)
  \end{description}
  Identification of entities in the library
  \begin{description}[Predefined identifiers]
    \item [Reserved words] rare (e.g., \AWK)
    \item [Predefined identifiers] as in \Pascal.
    \item [Importing] as in \Java and \CPL.
  \end{description}
\end{frame}

\begin{frame}{Replaceable- \vs builtin- library}
  \begin{table}[H]
    \centering
    \coloredTable
      \begin{tabular}{L{0.45}L{0.45}}
        \toprule
        \textbf{Replaceable}         & \textbf{Builtin}⏎
        \midrule
        Troublesome for programmer   & Less work for the programmer⏎
        Small language specification & Bulky language specification⏎
        Flexible                     & Rigid⏎
        Modular language design      & Tangled language design⏎
        Library can be very large    & Library is typically small⏎
        Most modern PLs              & PLs designed for beginners and for one-liner/scripting⏎
        \bottomrule
      \end{tabular}
    \Caption
  \end{table}
  \alert{\textbf{Dinosaurs}:
    Languages such as \Cobol which included huge builtin library tend to collapse under their own weight}
\end{frame}

\begin{frame}{Import by preprocessing}
  Import at the source, textual level⏎
  \begin{tikzpicture}
    \node
    [rectangle, rounded corners, draw, drop shadow, shade, top color=blue!50, bottom color=blue!10] (input) {%
      \lstinputlisting[style=CPLUSPLUS]{+/hello.c}
    };
    \node[isosceles triangle,draw,fill=red!20,double ] (pp) [below right=of input.south] {\large \textbf{Preprocessor}};
    \draw (input) -- (pp);
  \end{tikzpicture}
\end{frame}

\begin{frame}{Explicit (and implicit) import}
  Your program declares which library identifiers it uses:
  \begin{itemize}
    \item The keyword \kk{import} seems to be used in so many PLs.
    \item Other languages may use other keywords, e.g., \kk{uses}
  \end{itemize}
  Semantics is \emph{greater} than textual import.
  \begin{Block}[minipage]{Properties of import}
    \begin{itemize}
      \item usually carried out for a bunch of identifiers (for now, we call such a bunch a module)
      \item there is an \emph{implicit} search path for the library
      \item may be used also for user-provided (non-library) modules
      \item may cause other modules to compile
    \end{itemize}
  \end{Block}
\end{frame}

\begin{frame}{Implicit import}
  \begin{block}{Implicit Import}
    Certain principal modules are automatically imported even if the programmer does not explicitly \kk{import} these
  \end{block}
  e.g., \cc{java.lang.*} in \Java
  \begin{block}{Hello.java}
    \lstinputlisting[style=JAVA]{+/Hello.java}
  \end{block}
\end{frame}

\begin{frame}{Compilation unit}
  \WD{Compilation unit} {%
    \Red{compilation unit} is a portion of a computer program which is sufficiently complete to be compiled correctly.
  }
  \begin{itemize}
    \item Usually a file
    \item Can be string, or a \emph{buffer of the editor}
  \end{itemize}
\end{frame}

\subunit[starting-point]{Starting point}

\begin{frame}{Order of execution}
  Yet, another thing to observe in any \emph{‟Hello, World!”} program:
  \begin{itemize}
    \item Normally sequential
    \item Can be changed by
          \begin{itemize}
            \item Conditional commands
            \item Iteration commands
            \item Pluralization commands
            \item Invoking routines \item …
          \end{itemize}
  \end{itemize}
  \alert{But, in the presence of several compilation units, or even several routines in the same compilation unit, where do we start?}
\end{frame}

\begin{frame}{Autarkic approach}
  \begin{block}{au·tar·ky or au·tar·chy, pl.\ au·tar·kies or au·tar·chies}
    \begin{enumerate}
      \item A policy of national self-sufficiency and non-reliance on imports or economic aid.
      \item A self-sufficient region or country.
    \end{enumerate}
  \end{block}
\end{frame}

\begin{frame}{Summary terminology}
  \begin{itemize}
    \item identifiers, nameable entities (variables, values, functions, procedures, templates, namespaces, labels, modules),
    \item keywords, reserved identifiers, predefined identifiers
    \item literals, escaping, comments.
    \item separatist, terminist, and variations.
  \end{itemize}
\end{frame}

\afterLastFrame
\endinput

\begin{frame}{Holistic approach}
  \begin{itemize}
    \item
  \end{itemize}
\end{frame}

\section{Starting Point}

\begin{frame}{Order of execution}
  Yet, another thing to observe in any \emph{‟Hello, World!”} program:
  \begin{itemize}
    \item Normally sequential
    \item Can be changed by
          \begin{itemize}
            \item Conditional commands
            \item Iteration commands
            \item Pluralization commands
            \item Invoking routines
            \item …
          \end{itemize}
  \end{itemize}
  \alert{But, in the presence of several compilation units, or even several routines
  in the same compilation unit, where do we start?}

  The answer depends, also, on the manner by which the PL defines
  the set of compilation units which makes a program.
\end{frame}

\begin{frame}{Defining the Starting point}
  \begin{description}[By Language Keyword]
    \item [Interactive] The language processor reads a command
    and then executes it, printing any output it may have.
    \item [By Language Keyword] e.g., \texttt{\textbf{program}} in \Pascal,
    \texttt{\textbf{BEGIN}} in \AWK.
    \item [Metaphysical] The language does not concern itself with the
    starting point; it is defined externally
    by other tools. e.g., \texttt{main} in \CPL.
    \item [Holistic] The \emph{pure} language definition does
    not concern itself with the
    starting point, but the language definition
    includes provisions for defining the starting point.
  \end{description}
\end{frame}

\begin{frame}{Autarkic approach}
  \WD{au-tar-ky or au-tar-chy n., pl.\ au-tar-kies or au-tar-chies}
  policy of national self-sufficiency and non-reliance on imports or economic aid;
  A self-sufficient region or country.
  }
  \begin{block}{Autarkic PL}
    The entire program is contained in a single
    compilation unit.
  \end{block}
  Starting point is usually defined by ‟the first statement” or
  a keyword.
  \begin{itemize}
    \item \Pascal
    \item \AWK
    \item SED
  \end{itemize}
\end{frame}

\begin{frame}{Metaphysical \vs Holistic approach}
  {Setting the Program Boundary}
  \begin{block}{Program Boundary}
    The set of compilation
    units and library modules which make a program.
  \end{block}
  \begin{description}[Metaphysical]
    \item [Metaphysical]
    \begin{itemize}
      \item Program boundary delineated by external tool(s);
            may be chaotic, but usually not, since conventions
            are well entrenched.
      \item starting point is defined by these tool(s):
            \begin{itemize}
              \item \CPL programs start, by convention,
                    in \texttt{main}, because
                    the \textit{linker} implements this convention
              \item \CPL programs on windows start at \texttt{WinMain}
            \end{itemize}
    \end{itemize}
    \item [Holistic]
    \begin{itemize}
      \item The ‟language definition”, defines the means
            by which the program boundary is delineated.
      \item this ‟means” is \emph{not} part of the program
            \begin{itemize}
              \item program cluster in Eiffel
              \item modules (forthcoming) in Java
            \end{itemize}
      \item this ‟means” may also define the starting point
    \end{itemize}
  \end{description}
\end{frame}

\bash[stdoutFile=bash-conditional.sh]
echo 'if [ 2+2==4 ] ; then'
echo ' echo "too plus too before"'
echo 'fi'
\END

\begin{frame}[fragile]{\Bash case study}
  {Applies also to CSH and other shells}
  \begin{itemize}
    \item Single ‟compilation” unit
          \begin{itemize}
            \item execution starts from the first command in it
            \item \texttt{source} command allows to include other
                  files into it.
          \end{itemize}
    \item Metaphysical program boundary:
          any file on the file system can be invoked
    \item In \Bash (but not in CSH), ‟Boolean Expressions” are defined by ‟library”:
          \lstinputlisting[
            language=Bash,
            basicstyle=\footnotesize\ttfamily\upshape,
            showstringspaces=false,
          ]{+/bash-conditional.sh}
          ‟\kk{{[}” is a command located on the file system:
              \footnotesize
              \bash[script,stdout]
              ls -l `which [`
                \END
                \bash[script,stdout]
                man [ | wc
                  \END
                \end{itemize}
              \end{frame}

              \endinput

              \subunit[lists]{Making lists}

              \begin{frame}[fragile]{Blocks?}
              \end{frame}

              \begin{frame}{The ❴… ❵ languages}
                {A.K.A the curly bracket language}
              \end{frame}

              \begin{frame}{Occam's minimalism}
                {}
              \end{frame}

              \begin{frame}{\kw{begin}…\kw{end}}
                {As found in \Pascal}
                \begin{itemize}
                  \item
                \end{itemize}
              \end{frame}

              \begin{frame}{Closing reminder}
              \end{frame}

              \begin{frame}{\kw{if}…\kw{fi}}
                {As found in ALGOL 68}
              \end{frame}

              \subunit[vs]{Separators \vs Terminators}

              \begin{frame}{Separatist grammar}
                {}
              \end{frame}

              \begin{frame}{Forgiving Separatist grammar}
                {}
              \end{frame}

              \begin{frame}{Terminist grammar}
                {}
              \end{frame}


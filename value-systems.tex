\xdef\PL{PL}
\mode<article>{\def\PL{PL†{here, and \emph{henceforth}, \textbf{PL} = \textbf Programming \textbf Language}}}

\begin{frame}{What are values?}
  \squeezeSpace[1.5]
  \setbeamercovered{transparent}
  \alert{\uncover<+-| hl@+>{%
      Every \PL manipulates \Red{values}. 
  }}
  \alert{\uncover<+-| hl@+>{
    But, what are values?
  }}
  \par
  \uncover<+-| hl@+>{Intuitively, a \emph{\alert{value}} is… }
  \squeezeSpace[1.5]
  \begin{TWOCOLUMNS}
    \small
    \begin{itemize}[<+-| hl@+>]
      \item an ‟entity” that exists during computation
      \item anything that may be manipulated by a program
    \end{itemize}
    \MIDPOINT
    \small
    \begin{itemize}[<+-| hl@+>]
      \item anything that may be passed as an argument to a \kk{Function} or \kk{Procedure} (in \Pascal)
    \end{itemize}
  \end{TWOCOLUMNS}
  \begin{uncoverenv}<+-| hl@+>
    \WD[0.93]{The universe of values}{\mbox{}\vspace{-1ex}
      \squeezeSpace
      \begin{itemize}
        \item Every PL has a \Red{set of values}, e.g., integers, tuples, records, functions,…
        \item Running a program of the PL, amounts 
          to manipulation of members of this set.
        \item The values' set is also called the \Red{universe of values}.
      \end{itemize}
    }
  \end{uncoverenv}
\end{frame}

\begin{frame}{Value manipulation}
  \begin{itemize}
    \item Passing them to procedures as arguments
    \item Returning them through an argument of a procedure
    \item Returning them as the result of a function
    \item Assigning them into a variable
    \item Using them to create a composite value
    \item Creating/computing them by evaluating an expression
    \item …
  \end{itemize}
\end{frame}

\begin{frame}{Values \vs variables}
  A value is not a variable:
  \begin{itemize}
    \item A variable may \emph{store} a value.
    \item A variable may also be \emph{undefined}
    \item Some PLs (e.g., \ML) do not offer variables.
  \end{itemize}
  Why the confusion?
  \begin{itemize}
    \item In traditional imperative PLs, it is difficult to construct complex, interesting
          values, without using variables.
    \item People familiar with these PLs, tend to think of integers, reals, etc., as values, but they may have hard
          time understanding that there are also array values, function values, etc.
  \end{itemize}
\end{frame}

\begin{frame}{Machine representation \vs types \vs values}
  \setbeamercovered{transparent}
  \onslide<+-| hl@+>{A \Red{type system} is a set of \emph{types}.}
  \onslide<+-| hl@+>{A \emph{type} is set of values†{Later, we will see that it is not just \emph{any} set of values}}
  \begin{itemize}[<+-| hl@+>]
    \item Subsets are not necessarily disjoint.
    \item Subsets are not necessarily different
  \end{itemize}
  \onslide<+-| hl@+>{\textbf{Machine representation:} mapping of an element in~$𝕍$ to the machine.}
  \begin{description}[<+-| hl@+>]
    \item [$𝕍_𝓛$] the set of all values of a language~$𝓛$, AKA the values' universe.
    \item [$𝕋_𝓛$] the \emph{type system} of~$𝓛$ (with respect to~$𝒫𝕍_𝓛$†{For a set~$S$, the notation~$𝒫 S$ stands for the
      \emph{power set} of set~$S$, i.e.,~$𝒫 S = ❴ S' \,|\, S' ⊆ S❵$
    })
    \begin{equation}
      𝕋_𝓛⊂ 𝒫𝕍_𝓛
      \synopsis{The type system is a set of sets of values}
    \end{equation}
    \item [{$\mathbf P_𝓛$}] policy for representation of values of~$𝓛$ on different
    machines~$M₁,M₂,…$,
    \begin{equation}
      \mathbf P_𝓛:𝕍_𝓛→❴M₁,M₂,…❵
      \synopsis{Policy for representation of values on hardware}
    \end{equation}
  \end{description}
  \onslide<+->{}
\end{frame}

\begin{frame}{Value structure}
  \begin{itemize}
    \item \textbf{Atomic value:} \emph{is not} composed of other values
          \begin{itemize}
            \item truth values, characters, integers, reals, pointers
          \end{itemize}
    \item \textbf{Composite value:} \emph{is} composed of other values
          \begin{itemize}
            \item records, arrays, sets, files
          \end{itemize}
    \item The ways to create composite values in a PLs†
          {Here, and \emph{henceforth}, \textbf{PLs} = \textbf Programming \textbf Language\textbf s}
          are usually independent of its implementation
    \item The set of legal values in a PL's implementation:
          \begin{itemize}
            \item a closure of the atomic values in this \emph{implementation} under
                  the mechanisms the PL \emph{specification} allows for
                  creating composite values
          \end{itemize}
  \end{itemize}
\end{frame}

\subunit[symbolic]{Symbolic values}

\begin{frame}{Universes including very simple values}
  To emphasize the difference, we start with simple, yet very useful value systems:
  Many PLs revolve around symbolic manipulation rather than numbers:
  \begin{itemize}
    \item \Lisp†{%
                ‟\Lisp is worth learning for the profound enlightenment experience you will have when you finally get it;
                that experience will make you a better programmer for the rest of your days,
                even if you never actually use \Lisp itself a lot.”
                \href{http://www.paulgraham.com/avg.html}{Eric Raymond, ‟How to Become a Hacker"}}
    \item \Mathematica
    \item \Prolog
  \end{itemize}
  In essence, in these PLs, all values are ‟symbolic expressions”.
  There are no types in any of these universes.
\end{frame}

\begin{frame}{$𝕍_{\Lisp}$ = \emph{S}--expressions}
  An \alert{\emph{S}--Expression}†{\emph{S}--Expressions are symbolic expressions \Lisp style.}
  is
  \begin{enumerate}\small
    \item An \emph{Atom}
          \item~$(S₁\DOT S₂)$, where~$S₁$ and~$S₂$
          are \emph{S}-expressions
  \end{enumerate}
  An \emph{Atom} is
  \begin{enumerate}\small
    \item A string of any length (Many \Lisp implementations ignore letter case)
    \item The special value \cc{NIL}.
    \item An integer (non-essential)
    \item A real number (non-essential)
  \end{enumerate}
  Examples:
  \begin{itemize}\small
    \item \emph{hello}
          \item~$(\textit{a} \DOT \cc{NIL} )$
          \item~$(\cc{NIL} \DOT \textit{a} )$
          \item~$\big(\textit{Hello} \DOT (\textit{war} \DOT \textit{lord}) \big)$
  \end{itemize}
\end{frame}

\begin{frame}{\emph{S}-expression as binary trees}
  \begin{itemize}
    \item \emph{S}-expressions are binary trees whose leaves are either string or \cc{NIL}.
    \item \Lisp ‟assumes”†{actual representation could be different} that they are indeed represented as trees:
          \begin{description}
            \item [cons] Internal nodes are called \cc{CONS} nodes.
            \item [car] Left pointer
            \item [cdr] Right pointer
          \end{description}
  \end{itemize}
  \centering
  \begin{Figure}[The \protect\ccn{CONS} record]
    \centering
    \begin{adjustbox}{}
      \tikzset{edge from parent/.style={draw,-latex}}
      \begin{tikzpicture}
        \node[cons] child{node[] {?}} child{node[] {?}} ;
      \end{tikzpicture}
    \end{adjustbox}
  \end{Figure}
\end{frame}

\begin{frame}{Examples of \emph{S}-expression as binary trees}
  \tikzset{level distance=0.9cm}
  \tikzset{edge from parent/.style={draw,-latex}}
  \def\a{\textit{a}}
  \def\NIL{\cc{NIL}}
  \begin{TWOCOLUMNS}[-0.1\columnwidth]
    \begin{adjustbox}{}
      \begin{tabular}{cm{0.9\columnwidth}}
        \(\a \DOT \cc{NIL}\)
          &
        \begin{tikzpicture}
        \node[cons] child{node[wval] {a}} child{node[] {nil}} ;
        \end{tikzpicture}⏎
        $\cc{NIL} \DOT \a~$
          &
        \begin{tikzpicture}
        \node[cons] child{node {nil}} child{node[wval]{a}} ;
        \end{tikzpicture}
      \end{tabular}
    \end{adjustbox}
    \MIDPOINT
    \begin{adjustbox}{}
      \begin{tabular}{cm{0.9\columnwidth}}
        \(\a\DOT\a\)
          &
        \begin{tikzpicture}
        \node[cons] child{node[wval]{a}} child{node[wval]{a}};
        \end{tikzpicture}
        \(\a\DOT\a\)
        ⏎
        \(\textit{Hello} \DOT (\textit{war} \DOT \textit{lord}) \)
          &
        \begin{tikzpicture}[sibling distance=1.3cm]
        \node[cons]
        child {node[wval] {Hello}}
        child
        {%
        node[cons] {}
        child{node[wval]{war}}
        child{node[wval]{lord}}
        };
        \end{tikzpicture}
      \end{tabular}
    \end{adjustbox}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}{List shorthand}
  \begin{TWOCOLUMNS}
    The list
    \[
      \cc{(a b c d)}
    \] is shorthand (syntactic sugar) for \[
    \Bigg(\cc{a} \DOT \bigg(\cc{b} \DOT \big(\cc{c} \DOT (\cc{d} \DOT \cc{NIL})\big)\bigg)\Bigg)
  \]
  \MIDPOINT
  In binary tree representation:
  \begin{Figure}[The list \protect\ccn{(a b c d)} in binary tree representation]
    \begin{adjustbox}{}
      \begin{tikzpicture}[level distance=0.9cm,edge from parent/.style={draw,-latex}]
        \node[cons]
        child{node[wval] {a}}
        child{%
          node[cons] {}
          child{node[wval] {b}}
          child{node[cons] {}
            child{node[wval] {c}}
            child{node[cons] {}
              child{node[wval] {d}}
              child{node[] {nil}}
            }
          }
        };
      \end{tikzpicture}
    \end{adjustbox}
  \end{Figure}
  \end{TWOCOLUMNS}
  Take note that not all \emph{S}-expressions can be written using the list notation.
\end{frame}

\begin{frame}{Quiz}
  \begin{enumerate}
    \item What does \cc{()} mean in the tree notation?
    \item What does \cc{((a b) (c d))} mean in the tree notation?
    \item Can you give an example of an \emph{S}-expression which cannot be represented using the
          list notation?
  \end{enumerate}
\end{frame}

\subunit[s-expressions]{Semantics of \emph{S}-expressions?}

\begin{frame}[fragile]{‟Semantics” of the list notation}
  The evaluation of a list
  \begin{equation*}
    ℓ=\cc{(a b c d)}
  \end{equation*}
  means
  \Q{apply function~$\cc{car}(ℓ)$ to the list of arguments~$\cc{cdr}(ℓ)$}
  Example
  \begin{code}{LISP}
> (+ 2 (* 3 4))
14
  \end{code}
  What does it mean to ‟evaluate”?
  \begin{description}
    \item [Atom] Find out the ‟definition” of this atom in the symbol table.
    \item [Literal] Itself
    \item [List] Recursively evaluate all list elements, and then apply the first argument as a function to the remaining arguments.
  \end{description}
\end{frame}

\begin{frame}[fragile]{Should you care to run \protect\Lisp?}
  \squeezeSpace[3]
  Installing and running Gnu-\Lisp: \par
  \begin{TWOCOLUMNSc}[0.18\columnwidth]
    \begin{adjustbox}{H=0.75}
      \begin{session}
% gcl
The program 'gcl' is currently not installed. You can install it by typing:
sudo apt-get install gcl
% sudo apt install gcl
Reading package lists¢…¢
¢…¢
Building dependency tree¢…¢
¢…¢
The following NEW packages will be installed:
gcl
0 upgraded, 1 newly installed, 0 to remove and 38 not upgraded.⏎
¢…¢
% gcl
GCL (GNU Common Lisp) 2.6.7 CLtL1 Jul 27 2013 12:54:39⏎
Source License: LGPL(gcl,gmp), GPL(unexec,bfd,xgcl)⏎
¢…¢
>
> ¢\ignore$¢^¢\ignore$¢D
      \end{session}
    \end{adjustbox}
    \MIDPOINT
    Observe that
    \begin{itemize}
      \item At first, Gnu-\Lisp was not installed.
      \item User does as instructed and installs it
      \item User runs Gnu-\Lisp
      \item User hits Ctrl-D at the prompt to exit
    \end{itemize}
  \end{TWOCOLUMNSc}
\end{frame}

\begin{frame}{Interpreters: \protect\textbf read \protect\textbf evaluate \protect\textbf print \protect\textbf loop (REPL)}
  All interpreters follow the same scheme
  \begin{enumerate}
    \item Read input (and parse it)
    \item Evaluate (call function \cc{eval} in \Lisp)
    \item Print the result
    \item Loop
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]{Demonstrating lists' semantics in \protect\Lisp}
  \begin{session}
¢…¢
% gcl
> (a b c d)
Error: The function A is undefined.
Fast links are on: do (si::use-fast-links nil) for debugging
Error signaled by EVAL.
Broken at SYSTEM::GCL-TOP-LEVEL. Type :H for Help.
>>
  \end{session}
\end{frame}

\begin{frame}[fragile=singleslide]{The four most basic \protect\Lisp functions}
  \begin{description}[LONGEST LABEL]
    \item [\cc{(quote~$γ$)}] Do not evaluate~$γ$
    \item [\cc{(car~$γ$)}] First element of the list~$γ$
    \item [\cc{(cdr~$γ$)}] The rest of the list~$γ$ (everything but \cc{(car~$γ$)})
    \item [\cc{(cons~$γ$~$δ$)}] The list whose \cc{car} is~$γ$ and whose \cc{cdr} is~$δ$
  \end{description}
  \begin{Code}{LISP}{Using \ccn{car} and \ccn{cdr}}
> (car (a b))
Error: The function A is undefined…
> (car '(a b))
A
> cdr '(a b)
(B)
> (cons '(a b) '(c d)
((A B) C D)
  \end{Code}
\end{frame}

\begin{frame}{The car/cdr notation}
  \noindent
  CAR
  \begin{itemize}
    \item \textbf Contents of the \textbf Address part of \textbf Register number
    \item Also called in other PLs:
          \begin{itemize}
            \item ‟first”
            \item ‟head” (or ‟hd” for short)
          \end{itemize}
  \end{itemize}
  \noindent CDR
  \begin{itemize}
    \item \textbf Contents of the \textbf Decrement part of \textbf Register number
    \item Also called in other PLs:
          \begin{itemize}
            \item ‟rest”
            \item ‟tail” (or ‟tl” for short)
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{The ‟c[ad]+r” syntactic sugar}
  For brevity, let's use the binding of the name \cc{z}
  to the value \cc{((a b) (c d))}, then, i.e., evaluate
  \[
    \cc{(setq 'z '((a b) c d) )},
  \] then,\par
  \begin{table}[H]
    \begin{adjustbox}{}
      \coloredTable
      \begin{tabular}{*3c}
        \textsc{Long form} & \textsc{Short form} & \textsc{Result}⏎
        \cc{(car (car z))} & \cc{(caar z)}       & \cc{A}⏎
        \cc{(cdr (car z))} & \cc{(cdar z)}       & \cc{(B)}⏎
        \cc{(car (cdr z))} & \cc{(cadr z)}       & \cc{C}⏎
        \cc{(cdr (cdr z))} & \cc{(cddr z)}       & \cc{(D)}⏎
      \end{tabular}
    \end{adjustbox}
    \Caption[Examples of using the ‟\protect\cc{c[ad]+r}” syntactic sugar]
  \end{table}
  \textbf{Notes:}
  \begin{itemize}
    \item Unboundedly long
    \item Can be cumbersome
    \item Awkward, and therefore does not exist with the first/rest and head/tail terminology.
  \end{itemize}
\end{frame}

\begin{frame}{Understanding ‟c[ad]+r” syntactic sugar}
  \begin{Figure}
    \begin{adjustbox}{}
      \begin{tikzpicture}[-latex]
        \node[ncons,label=\cc{z = ((a b) c d)}] {}[sibling distance=160pt,level distance=2cm]
        child {%
          node[ncons, label={[yshift=0pt,xshift=-20pt]above:\cc{(car z)}}] {}
          [sibling distance=1.4cm,level distance=36pt]
          child {node[wval,label={[yshift=0pt,xshift=-20pt]above:\cc{(cadr z)}}] {a}}
          child {%
            node[ncons,label={[yshift=0pt,xshift=20pt]above:\cc{(cdar z)}}] {}
            child {node[wval,label={[yshift=0pt,xshift=-20pt]above:\cc{(cadar z)}}] {b}}
            child {node[label={[yshift=0pt,xshift=20pt]above:\cc{(cddar z)}}] {nil}}
          }
        }
        child {%
          node[ncons, label={[yshift=0pt,xshift=20pt]above:\cc{(cdr z)}}] {}
          [sibling distance=1.4cm,level distance=36pt]
          child {node[wval,label={[yshift=0pt,xshift=-20pt]above:\cc{(cadr z)}}] {c}}
          child {%
            node[ncons,label={[yshift=0pt,xshift=20pt]above:\cc{(cddr z)}}] {}
            child {node[wval,label={[yshift=0pt,xshift=-20pt]above:\cc{(caddr z)}}] {d}}
            child {node[label={[yshift=0pt,xshift=20pt]above:\cc{(cdddr z)}}] {nil}}
          }
        }
        ;
      \end{tikzpicture}
    \end{adjustbox}
  \end{Figure}
\end{frame}

\begin{frame}[fragile=singleslide]{Names and literals in \Lisp}
  \begin{Code}{LISP}{The \ccn{set} function}
> (set a b)
Error: The variable A is unbound.
> (set 'a b)
Error: The variable B is unbound.
> (set ¢\ignore'¢'a ¢\ignore'¢'b)
B
> a
B
> (set a 3)
3
> a
B
> (print a)
B
B
  \end{Code}
\end{frame}

\begin{frame}{‟List” ＆ ‟if”}
  List:
  \begin{multline*}
    \cc{(list 'a 'b 'c)} = \cc{(a b c)}⏎
    \cc{(list 'a 'b (list 'c 'd))} = \cc{(a b (c d))}⏎
    \cc{(list (list 'a 'b) (list 'c 'd))} = \cc{((a b) (c d))}⏎
  \end{multline*}
  If:
  \begin{multline*}
    \cc{(if 'x 'a 'b)} = \cc{a}⏎
    \cc{(if (list 'x) 'a 'b)} = \cc{a}⏎
    \cc{(if '(x y) 'a 'b)} = \cc{a}⏎
    \cc{(if NIL 'a 'b)} = \cc{b}⏎
    \cc{(if () 'a 'b)} = \cc{b}⏎
  \end{multline*}
\end{frame}

\begin{frame}[fragile]{$λ$-functions}
  Function ‟lambda” makes it possible to define anonymous functions:
  \begin{Code}[output=(LAMBDA-CLOSURE () () () (X) (CONS (CDR X) (CAR X)))]{LISP}{Defining a~$λ$-function}
(lambda (x) (cons (cdr x) (car x)))
  \end{Code}
  \begin{Code}[output=(((C D)) A B)]{LISP}{Defining and applying a~$λ$-function:}
(
  (lambda (x) (cons (cdr x) (car x)))
  '( (a b) (c d) )
)
  \end{Code}
\end{frame}


\begin{frame}[fragile=singleslide]{Named functions}
  \begin{itemize}
    \item Defining
          \begin{lcode}[output=FOO]{LISP}
(defun foo (x) (cons (cdr x) (car x)))
    \end{lcode}
    \item Applying:
          \begin{lcode}[output=(((C D)) A B)]{LISP}
(foo '( (a b) (c d) ))
    \end{lcode}
  \end{itemize}
\end{frame}

\begin{frame}{Summary: Values in \Lisp}
  \setbeamercovered{transparent}
  \begin{itemize}[<+- | hl@+>]
    \item Extremely simple
    \item Have no types
    \item Simple basic operations: \cc{car}, \cc{cdr}, \cc{cons}, \cc{list}, \cc{quote}, \cc{if}, \cc{defun}
    \item Can be used to compute anything! (conditions and recursion)
    \item The mother of all \emph{functional} PLs.
  \end{itemize}
\end{frame}

\begin{frame}{$𝕍_\text{\Prolog}$ intuitively}
  \textbf{Simple:}
  Almost the same as in \Lisp†{recall the isomorphism between binary trees and forest of general trees}.
  \textbf{Specifically:}
  trees of arbitrary degree:
  \begin{itemize}
    \item Internal nodes carry label (unlike \emph{S}-expressions)
    \item Leaves are either
          \begin{itemize}
            \item Labels
            \item Symbolic ‟variables”
          \end{itemize}
  \end{itemize}
  Only one fundamental operation on values:†{Come back to this slide at the end of the course}
  \begin{description}
    \item [Unification] Given two trees, replace (if possible) ‟variables” in each of them so that they become the same tree.
  \end{description}
\end{frame}

\begin{frame}{$𝕍_\text{\Prolog}$ precisely}
  All values are \emph{term}s. A term is either one of
  \begin{description}
    \item [Atom] a name with no inherent meaning.
    \item [Number] \item [Variable] must start with an upper case letter
    \item [Composite] which includes
    \begin{enumerate}
      \item an atom called \emph{functor}
      \item a list of any number of terms (arguments).
    \end{enumerate}
  \end{description}
  A list is a kind of term written as this \cc{[a,B,c,x]}
\end{frame}

\begin{frame}{$𝕍_{\Mathematica}$}
  \squeezeSpace
  Same as \Prolog/\Lisp with lots of syntactic sugar:
  \begin{Figure}[The many ways for presenting the symbolic values of \Mathematica]
      \begin{adjustbox}{max height=0.8\paperheight}
        \includegraphics{Graphics/value-systems-Mathematica-demo.png}
      \end{adjustbox}
    \end{Figure}
\end{frame}

\subunit[expressions]{Expressions}

\begin{frame}{More traditional values}
  \alert{$𝕍_{\Bash}$: Values of \Bash}
  \begin{enumerate}
    \item Numbers
    \item Strings
    \item List of values.
    \item One dimensional arrays.
  \end{enumerate}
\end{frame}


\begin{frame}{$𝕍_{\ML}$: values in \protect\ML}{}
  All the values in \ML are first-class values:
  \begin{description}[References to variables]
    \item [Atomic values] truth values, integers, reals, strings.
    \item [Composite values] records, tuples (records w/o field names), constructions (tagged values), lists, arrays.
    \item [Function values] \item [References to variables]
  \end{description}
  What we can do in \ML but \emph{not} in \Pascal:
  \begin{itemize}
    \item create a \emph{record} composed of two functions
    \item write a \emph{function} that gets a function~$f:\kk{int}→\kk{int}$ and returns the composition of \cc{f} with itself
    \item write an expression whose value is a reference to a variable
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Expressions}
  \squeezeSpace
  \WD{Expression}{An \Red{expression} is a part of a program whose evaluation during computation outcomes with a value.}
  \begin{TWOCOLUMNSc}
    \begin{Code}{PASCAL}{Several expressions in \Pascal}
3.1416
chr(ord('%')+1)
'Hello, world'
2*a[i]+7
sqr(4)
q¢\ignore$¢^¢\ignore$¢.head
    \end{Code}
    \MIDPOINT
    \begin{Code}{EMEL}{An expression in \ML}
if leap(year) then 29 else 28
    \end{Code}
  \end{TWOCOLUMNSc}
\end{frame}

\begin{frame}{Expressions are recursively defined}
  \setbeamercovered{transparent}
  \onslide<+-| hl@+> {\emph{Naturally, each PL is different, but the general scheme is:}}
  \onslide<+-| hl@+>{Atomic expressions}
  \begin{itemize}[<+-| hl@+>]
    \item literals
    \item variable inspection
  \end{itemize}
  \onslide<+-| hl@+>{Expression constructors}
  \begin{itemize}[<+-| hl@+>]
    \item Operators such as ‟\cc{+}”, ‟\cc{-}”, …
    \item Function calls
  \end{itemize}
  ⚓\emph{The set of atomic expressions and the constructors' set are PL dependent, but the variety is not huge.}
\end{frame}

\begin{frame}{Function call expression constructor}
  \begin{columns}[t]
    ⚓\COL{0.34}
    \begin{block}{Dynamic typing version}\footnotesize
      \abovedisplayskip=3pt plus 9pt
      \belowdisplayskip=3pt plus 9pt
      If~$f$ is a function taking~$n≥0$ arguments,
      ⚓and \[E₁,…,Eₙ\] are expressions,
      ⚓then the call \RED{\[f(E₁,…,Eₙ)\] is an expression}.
    \end{block}⚓\COL{0.54}
    \begin{block}{Static typing version}\footnotesize
      \abovedisplayskip=3pt plus 9pt
      \belowdisplayskip=3pt plus 9pt
      ⚓Let~$f$ be a (typed) function of~$n≥0$ arguments,⚓\[f∈τ₁⨉⋯⨉τₙ→τ.\]
      ⚓Let~$E₁,…,Eₙ$ be expressions
      of types~$τ₁,…,τₙ$.
      ⚓Then, the \emph{call}~\RED{\[f(E₁,…,Eₙ)\] is an expression of type~$τ$.}
    \end{block}
  \end{columns}
\end{frame}

\begin{frame}{Functions \vs operators}
  Both are:
  \begin{itemize}
    \item constructors of expressions
    \item apply an operation to values
  \end{itemize}
  Differences are largely syntactical
  \begin{itemize}
    \item Name
    \item Position, e.g., prefix or infix
    \item Parenthesis
    \item Precedence rules
    \item User overloading
  \end{itemize}
\end{frame}

\begin{frame}{Syntactical differences between functions ＆ operators}
  \squeezeSpace
  \begin{table}[H]
    \newcommand\operatorWidth{0.45}
    \newcommand\functionWidth{0.30}
    \begin{adjustbox}{}
      \coloredTable
      \begin{tabular}{>{\itshape} R{0.27} C{\functionWidth} C{\operatorWidth}}
        \toprule
                              & \normalsize\textbf{Functions} & \normalsize\textbf{Operators}⏎
        \midrule
        Position?             & prefix                        & prefix, infix, prefix⏎
        Precedence rules?     & ✗                           & ✓⏎
        Parenthesis required? & ✓                           & ✗⏎
        Name?                 & identifier                    &
        \multicolumn1{L{\operatorWidth}}{%
        $∙$~Punctuation characters:¶▄\:\cc{+}$₁$, \cc{+}$₂$, \cc{++}, \cc{<<}, \cc{<<<=},…¶%
        $∙$~Reserved identifier: \kk{new},¶▄\:\kk{sizeof}, \kk{instanceof},…¶%
        $∙$~Reserved+punctuation:¶▄\:\kk{new[]}, \kk{delete[]},…
        }⏎
        Arity?                & $0,1,2,3,…$                 &
        \multicolumn1{L{\operatorWidth}}{%
        $1$: prefix/postfix operators¶%
        $2$: infix operators¶%
        $3$:~\CPL's ‟\cc{\space ? : }”
        }⏎
        Overloadable by~programmer? &
        \multicolumn1{L{\functionWidth}}{%
        $∙$~\Java, \CC: ✓¶
        $∙$~\Pascal,~\CPL: ✗%
        }
        &
        \multicolumn1{L{\operatorWidth}}{%
        $∙$~\CC: ✓¶%
        $∙$~\Java,\Pascal,~\CPL: ✗%
        }⏎
        \bottomrule
      \end{tabular}
    \end{adjustbox}
    \Caption
  \end{table}
\end{frame}

\afterLastFrame

\references
\begin{itemize}%
  \D{CAR and CDR}{https://en.wikipedia.org/wiki/Car\_and\_cdr}
  \D{CONS}{https://en.wikipedia.org/wiki/Cons}
  \D{defun}{https://en.wikipedia.org/wiki/Defun}
  \D{lambda}{https://en.wikipedia.org/wiki/Lambda＃Lambda.2C\_the\_word}
  \D{\Lisp}{https://en.wikipedia.org/wiki/Lisp\_programming\_language}
  \D{\Mathematica}{https://en.wikipedia.org/wiki/Mathematica}
  \D{\emph{S}-Expressions}{https://en.wikipedia.org/wiki/S-expression}
  \D{Values of \Prolog}{https://en.wikipedia.org/wiki/Prolog＃Data\_types}
  \D{Wolfram (PL used in \Mathematica)}{https://en.wikipedia.org/wiki/Wolfram\_(programming\_language)}
\end{itemize}
\exercises
\begin{enumerate}
  \item Why are pointers atomic values, where the pointer types are compound?
  \item What does \cc{()} mean in the tree notation?
  \item What does \cc{((a b) (c d))} mean in the tree notation?
  \item Provide an example of an \emph{S}-expression which does cannot be represented using the list notation
  \item What's the difference between \cc{(car 'a)} and \cc{(car a)}?
  \item What is the difference between an expression and a value?
\end{enumerate}
\endinput
Consequence: the representation of values can be concealed by the language

\subunit{Understanding generators}
\begin{frame}[fragile]{The ‟magic” behind a generating context}
  The generating context
  \begin{lcode}{CSHARP}
foreach (int p in Powers(2, 30)) {¢¢
  Console.Write(p);
  Console.Write(" ");
}
  \end{lcode}
  is implemented like so:
  \begin{lcode}{CSHARP}
// This variable is inaccessible to programmer
GeneratingRecord ¢\ignore$¢_¢\ignore$¢;
¢¢
for (¢\ignore$¢_¢\ignore$¢ = Powers(2, 30); 
        ¢\ignore$¢_¢\ignore$¢.resumable(); 
          int p = ¢\ignore$¢_¢\ignore$¢.retrieve()) 
{¢¢
  Console.Write(p);
  Console.Write(" ");
}
  \end{lcode}
\end{frame}

\begin{frame}[fragile]{Nested generation}
  Each generating context has its own generating record; so, the following inner
  loop
  \begin{code}{CSHARP}
foreach (int p in Powers(2, 30)) {¢¢
  foreach (int q in Powers(2, 30)) {¢¢
    Console.Write(p + q);
    Console.Write(" ");
  }
}
  \end{code}
  Will print twice each integer~$n$, if~$n$ can be written as
  \[
    n = 2^p + 2^q
  \] where \[
  1≤p≤30
\]
and
\[
  1≤q≤30
\]
\end{frame}

\begin{frame}[fragile]{Distinct generating record for each generation context}
  \begin{PLACE}{0.42,0.52}
    \begin{Code}{CSHARP}{Original: nested generation}
foreach (int p in Powers(2, 30)) {¢¢
  foreach (int q in Powers(2, 30)) {¢¢
    Console.Write(p + q);
    Console.Write(" ");
  }
}
    \end{Code}
  \end{PLACE}
  Implementation of nested generation with two generation records:
  \begin{LEFT}[0.50\columnwidth]
    \begin{Code}{CSHARP}{Nested generation: behind the scenes}
// This variable is inaccessible to programmer:
GeneratingRecord ¢\ignore$¢_¢\ignore$¢1;
for (¢\ignore$¢_¢\ignore$¢1 = Powers(2, 30);
      ¢\ignore$¢_¢\ignore$¢1.resumable();
        int p = ¢\ignore$¢_¢\ignore$¢1.retrieve())
{¢¢
  // This variable is inaccessible as well:
  GeneratingRecord ¢\ignore$¢_¢\ignore$¢2;
  for (¢\ignore$¢_¢\ignore$¢2 = Powers(2, 30);
          ¢\ignore$¢_¢\ignore$¢2.resumable();
            int q = ¢\ignore$¢_¢\ignore$¢2.retrieve())
  {¢¢
      Console.Write(p + q);
      Console.Write(" ");
  }
}
    \end{Code}
  \end{LEFT}
\end{frame}

\subunit{Iterators in \protect\Java} 

\begin{frame}{Emulating generators with \Java iterators}
  In \Java (up to version 7):
  \begin{itemize}
    \item no closures
    \item no generators
  \end{itemize}
  Still, it is possible to emulate generators in \Java, especially given that
  \Java has GC:
  \begin{itemize}
    \item A clever programmer can manually ‟invert control” to emulate a
          generator
    \item A clever programmer can save the environment in an object
    \item A clever programmer can save the local state in an object
  \end{itemize}
  So, all you need is to be clever enough. (In \Java 8, this exact ``cleverness'' was 
  packaged into the language.)
\end{frame}

\begin{frame}[fragile]{\Java iterators: poor man's generators}
  We shall have two classes
  \begin{itemize}
    \item Main class which creates and then uses the iterator.
          \begin{itemize}
            \item Will create the ‟environment” for the our poor-man's generator
            \item Environment includes the values \cc{2} and \cc{30}. 
                  in the \CSharp call \cc{Powers(2,30})
          \end{itemize}
    \item class \cc{Powers} the iterator object itself.
          \begin{itemize}
            \item Will save the ‟environment” that function \cc{main} passes on to it.
            \item Will save the local variables of the generator as class variables.
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Invoking the iterator}
  \squeezeSpace[3]
  \begin{PLACE}{0.45,0.46}
    \begin{Code}{CSHARP}{\CSharp original}
using System;
using System.Collections.Generic;
¢¢
public class Program {¢¢
  static void Main() {¢¢
    foreach (int p in Powers(2, 30)) {¢¢
      Console.Write(p);
      Console.Write(" ");
     }
  }
 ¢…¢
}
    \end{Code}
  \end{PLACE}
  \begin{columns}[b]
    \column{0.7\columnwidth}
    \begin{Code}{JAVA}{Main class in \Java}
import java.util.Iterator;
¢¢
public class E {¢¢
  public static void main(String[] args) {¢¢
    Integer p;
    Iterator<Integer> it;
    for (it = new Powers(2,30); it.hasNext(); ) {¢¢
      p = it.next();
      System.out.println(p);
    }
  }
}
    \end{Code}
    In \Java, the iterator
    \begin{itemize}
      \item is \Red{visible} to the programmer.
      \item is \Red{managed} by the programmer.
    \end{itemize}
    \column[b]{0.3\columnwidth}
  \end{columns}
\end{frame}


\begin{frame}[fragile]{The iterator class: data members}
  \begin{PLACE}{0.40,0.57}
    \begin{Code}{CSHARP}{Generator of \CSharp}
Powers(int base, int count) {¢¢
  int result = 1;
  for (int i = 0; i < count; ++i)
    yield return result *= base;
  yield break; // optional
}
    \end{Code}
  \end{PLACE}
  \begin{LEFT}[0.8\columnwidth]
    \begin{Code}{JAVA}{Saved environment and local state}
class Powers implements Iterator<Integer> {¢¢
  // Saved environment:
  private final Integer base, count;
  // Saved local state:
  private Integer result = 1, i = 0;
  public PowersIterator(
    // Environment is passed with arguments
    Integer base, Integer count
  ) {¢¢
  // Save the environment in class variables:
    this.base = base; this.count = count;
  }
}
    \end{Code}
    \begin{LEFT}[0.7\columnwidth]
      All four variables used by generator \cc{Powers} of \CSharp must
      be saved in an instance of class \cc{Powers} in \Java.
    \end{LEFT}
  \end{LEFT}
\end{frame}

\begin{frame}[fragile]{The iterator class: member functions}
  \begin{PLACE}{0.46,0.38}
    \begin{Code}{CSHARP}{Generator of \CSharp}
Powers(int base, int count) {¢¢
  int result = 1;
  for (int i = 0; i < count; ++i)
    yield return result *= base;
  yield break; // optional
}
    \end{Code}
  \end{PLACE}
  \begin{columns}[b]
    \begin{column}{0.65\columnwidth}
      \begin{Code}{JAVA}{Inverted control}
class Powers implements Iterator<Integer> {¢¢
    ¢…¢
  // At each iteration:
  public Integer next() {¢¢
    result *= base; // Result we shall yield
    ++i; // Continue iteration
    return result; // Yield result
  }
  // Termination test:
  public boolean hasNext() {¢¢
    return i < count;
  }
  // Historic baggage, courtesy of
  // a library design error
  public void remove() {¢¢ /* empty */ }
}
      \end{Code}
      To invert the control, one must understand when
      functions \cc{next} and \cc{hasNext} are called.
    \end{column}
    \begin{column}{0.35\columnwidth}\end{column}
  \end{columns}
\end{frame}

\begin{frame}{The two interfaces offered by \Java for iteration}
  \squeezeSpace
  \Java's standard library offers two related notions:
  \squeezeSpace
  \begin{TWOCOLUMNS}
    \WD[0.95]{\protect\ccn{Iterable} in \protect\Java}{\small \mbox{}⏎
      \squeezeSpace
      \begin{itemize}
        \item something on which ‟\textbf{iteration}” is possible.
        \item e.g.,a \emph{list}, a \emph{set},
        \item e.g., an \emph{arithmetical progression},
        \item factory of \Red{Iterator}s
      \end{itemize}
    }
    \MIDPOINT
    \WD[0.95]{\protect\ccn{Iterator} in \protect\Java}{\small \mbox{}
      \squeezeSpace
      \begin{itemize}
        \item provides a service of ‟\textbf{iteration}” on an \Red{Iterable}
        \item at each step of the ‟\textbf{iteration}”:
              \begin{itemize}
                \item if there is a ‟next” item
                \item yields the ‟next” item
              \end{itemize}
      \end{itemize}
    }
  \end{TWOCOLUMNS}
  Both notions are subject to parametric polymorphism 
    (\cref{\labelPrefix Section:parametric-polymorphism}), i.e.,
  \begin{Block}[minipage,width=0.7\columnwidth]{}
    For every non-atomic type~\Red{$τ$}, we have,
    \begin{itemize}
      \item type \cc{Iterable<\Red{$τ$}>}
      \item type \cc{Iterator<\Red{$τ$}>}
    \end{itemize}
  \end{Block}
\end{frame}

\begin{frame}{The complex \protect\cc{Iterable} \vs \protect\cc{Iterator} contract}
  \squeezeSpace
  \begin{enumerate}[<+->]
    \item The only thing an \cc{Iterable} does:
          \Q<+->{\Red{generates} \cc{Iterator}s}
    \item An \cc{Iterator} is the ultimate \alert{disposable} construct:
          \begin{itemize}[<+->]
            \item provides {iteration}.
                  \begin{itemize}
                    \item \Red{single} run
                    \item forward \Red{only}
                  \end{itemize}
            \item associated with an \cc{Iterable}:
                  \begin{itemize}[<+->]
                    \item from \Red{birth},
                    \item with \Red{one},
                    \item and only \Red{one}
                  \end{itemize}
          \end{itemize}
    \item An {Iterable} can be associated with
          \begin{itemize}[<+->]
            \item one \cc{Iterator},
            \item many \cc{Iterator}s,
            \item or none at all.
          \end{itemize}
    \item An \cc{Iterator} may also \Red{remove} an item from a \cc{Iterable}
          \begin{description}[<+->]
            \item we will try to ignore this historical accident
          \end{description}
  \end{enumerate}
\end{frame}

\subunit{Advanced \Java iterators}

\begin{frame}[fragile]{\Java special syntax for iterating over an ‟Iterable”}
  \squeezeSpace
  The following
  \begin{Code}{JAVA}{Foreach syntax}
for (Thing thing: things)
  doSomethingWith(thing);
  \end{Code}
  where
  \begin{itemize}
    \item \cc{Thing} is some class
    \item \cc{thing} is a newly defined variable of type \cc{Thing}
    \item \cc{things} is a ‟collection” of \cc{thing}s, i.e., an instance
          of class that \kk{implements} the interface \kk{Iterable<Thing>}
  \end{itemize}
  is syntactic sugar for⏎
  \begin{Code}{JAVA}{Foreach semantics}
Iterator<Thing> it = things.iterator();
while (it.hasNext()) {¢¢
  Thing thing = it.next();
  doSomethingWith(thing);
}
  \end{Code}
\end{frame}

\begin{frame}[fragile]{Pseudo generator}
  \squeezeSpace[6]
  \WD[0.92]{Pseudo generator}{%
    A \Red{pseudo generator} is a \Java function that returns an \cc{Iterable};
  }
  When the returned \cc{Iterable} is used in \Java's extended \kk{for}, the
  pseudo generator looks like a generator, e.g.,
  \begin{Code}{JAVA}{A pseudo generator}
// pseudo generators are almost always ¢\kk{public static}¢:
public static
  // a pseudo generator must return an ¢\cc{Iterable}¢:
  Iterable<Integer>
    powers(final Integer Base, final Integer count) {¢¢
      return new POWERS(base, count);
}
  \end{Code}
  It looks just like \CSharp generator when used in an extended for loop:
  \begin{Code}{JAVA}{Using a pseudo generator}
for (Integer p: powers(2,30))
  System.out.println(p);
  \end{Code}
\end{frame}

\begin{frame}[fragile]{Using \protect\Java's syntactic sugar for \protect\kk{foreach}}
  To use this sugar:
  \begin{TWOCOLUMNS}[-4ex]
    \begin{Code}{JAVA}{Foreach syntax}
for (Thing thing: things)
  doSomethingWith(thing);
    \end{Code}
    \MIDPOINT
    \begin{Code}{JAVA}{Foreach semantics}
Iterator<Thing> it = things.iterator();
while (it.hasNext()) {¢¢
  Thing thing = it.next();
  doSomethingWith(thing);
}
    \end{Code}
  \end{TWOCOLUMNS}
  We must define a new class \cc{POWERS}:
  \begin{code}{JAVA}
class POWERS implements Iterable<Integer> {¢¢
¢…¢
}
  \end{code}
  and then write
  \begin{code}{JAVA}
for (Integer p: new POWERS(2,30))
  System.out.println(p);
  \end{code}
\end{frame}

\begin{frame}[fragile]{Less parameter juggling with inner classes}
  \squeezeSpace
  \begin{TWOCOLUMNSc}[-8ex]
    An inner class in \Java ‟closes” over its environment; let's use this to simplify the code
    \MIDPOINT
    \begin{adjustbox}{}
    \begin{Code}{JAVA}{Iterable over \ccn{Integer}s}
class POWERS implements Iterable<Integer> {¢¢
  final Integer base, count; // environment
  // Class constructor saves the environment
  public Powers(Integer base, Integer count) {¢¢
    this.base = base; this.count = count;
  }
  // Has access to data members ¢\cc{base}¢ and
  // ¢\cc{count}¢ of enclosing class:
  class Powers implements Iterator<Integer> {¢¢
    // Saved local state:
    private Integer result = 1, i = 0;
    public Integer next() {¢¢
      result *= base;
      ++i;
      return result;
    }
    public boolean hasNext() {¢¢
      return i < count;
    }
    public void remove() {¢¢ /* empty */ }
  }
  public Iterator<Integer> iterator() {¢¢
    return new Powers(base, count);
  }
}
    \end{Code}
  \end{adjustbox}
  \end{TWOCOLUMNSc}
\end{frame}

\begin{frame}[fragile]{Even less clutter with anonymous classes}
  An anonymous class in \Java is
  \begin{itemize}
    \item an inner class which does not have a name
    \item defined where it is used
  \end{itemize}
  let's use this to simplify the code
  \begin{Code}{JAVA}{An anonymous \ccn{Iterator<Integer>}}
class POWERS implements Iterable<Integer> {¢¢
  final Integer base, count; // Saved environment:
  public Powers(Integer base, Integer count) {¢¢
    this.base = base; this.count = count;
  }
  public Iterator<Integer> iterator() {¢¢
    return new Iterable<Integer>() {¢¢
        // Same as before; nothing new here
    };
  }
}
  \end{Code}
\end{frame}

\begin{frame}[fragile=singleslide]{An even closer imitation of generators}
  \mode<presentation>{\vspace{-3.5ex}}
  A \Java function returning an instance of an \Red{anonymous} class implementing \cc{Iterable<Integer>}:
  \begin{itemize}
    \item is called \alert{pseudo generator}.
    \item gets rid of class \cc{POWERS}
    \item achieves a closer imitation of generators.
  \end{itemize}
  \mode<presentation>{\vspace{-3ex}}
  \begin{LEFT}[0.6\columnwidth]
    \begin{Code}{JAVA}{Function \ccn{powers} returning anonymous
    \ccn{Iterable<Integer>}}
public static Iterable<Integer> powers(
  // Environment variables passed as arguments
  final Integer Base,
  final Integer count
) {¢¢
  return new Iterable<Integer>() {¢¢
    public Iterator<Integer> iterator() {¢¢
      return new Iterable<Integer>() {¢¢
        // Same as before; nothing new here
      };
    }
  };
}
    \end{Code}
  \end{LEFT}
  \begin{PLACE}{0.45,0.83}
    \begin{Code}{JAVA}{Using a pseudo generator in \Java}
for (final Integer p: powers(2, 30))
  for (final Integer q: powers(2, 30))
    System.out.println(p + q);
    \end{Code}
  \end{PLACE}
\end{frame}

\begin{frame}{Summary: Generators \vs iterators}
  \begin{table}[H]
    \begin{centering}
      \coloredTable
      \begin{tabular}{R{0.27}L{0.27}L{0.27}z}
        \toprule
                                      & \large \textbf{Generators}  & \large \textbf{Iterators}              & ⏎
        \midrule
        \alert{PL}                    & \CSharp, \Python, \Icon,… & \Java                                  & ⏎
        \alert{Purpose}               & sequence of values          & sequence of values                     & ⏎
        \alert{Implementation}        & language construct          & by clever programmer                   & ⏎
        \alert{Underlying concept}    & function                    & object                                 & ⏎
        \alert{Environment}           & builtin                     & must be managed manually by programmer & ⏎
        \alert{Control}               & forward                     & inverted                               & ⏎
        \alert{Conceptual complexity} & low                         & high                                   & ⏎
        \alert{Pedagogical value}     & low                         & high!                                  & ⏎
        \bottomrule
      \end{tabular}
      \Caption
    \end{centering}
  \end{table}
\end{frame}

\subunit{Boxing primitive types} 

\begin{frame}[fragile]{Reference types boxing type \protect\cc{int}}
  \begin{Code}{JAVA}{Class \ccn{Int}}
class Int {¢¢
  private int inner;
  public int value() {¢¢
    return inner;
  }
  Int(int inner) {¢¢
    this.inner = inner;
  }
  public boolean lt(int than) {¢¢
    return inner < than;
  }
  public int inc() {¢¢
    return inner++;
  }
}
  \end{Code}
\end{frame}

\begin{frame}[fragile]{The actual interfaces}
  \squeezeSpace
  From the \Java runtime library:
  \begin{Code}[minipage,width=40ex]{JAVA}{Interface \ccn{Iterable<T>}}
public interface Iterable<T> {¢¢
/** Returns an iterator over elements of type ¢\cc{T}¢.
 */
   Iterator<T> iterator();
}
  \end{Code}
  \begin{Code}[minipage,width=40ex]{JAVA}{Interface \ccn{Iterator<T>}}
public interface Iterator<T> {¢¢
  boolean hasNext();
  T next();
  default void remove() {¢¢
    throw new
      UnsupportedOperationException(
        "remove"
      );
  }
}
  \end{Code}
\end{frame}

\begin{frame}[fragile]{Using \protect\kk{interface} \protect\cc{Iterator}}
  \squeezeSpace
  ⚓To use \kk{interface} \cc{Iterator},
  ⚓create a \kk{class} that \kk{implements} it…
  \squeezeSpace
  \begin{onlyenv}<+->
    \begin{Block}[minipage,width=0.8\columnwidth]{}\scriptsize
      \only<+->{\cc{\kk{class} X \kk{implements} Iterator<Int> ❴}⏎}
      \only<+->{\mbox{\quad}…⏎}
      \only<+->{\mbox{\quad}…⏎}
      \only<+->{\mbox{\quad}\cc{public Iterator<Int> iterator()❴}⏎}
      \only<+->{\mbox{\quad\quad}…⏎}
      \only<+->{\mbox{\quad\quad}…⏎}
      \only<+->{\mbox{\quad\quad}\cc{\kk{return} \kk{new} \only<+->{{\large \textrm{\Red{???}}}}();}⏎}
      \only<7->{\mbox{\quad}\cc{❵}⏎}
      \only<4->{\cc{❵}}
    \end{Block}
  \end{onlyenv}
  \only<+->{… have to create another \kk{class};
    ⚓one that \kk{implements}…}
  \begin{onlyenv}<+->
    \begin{code}{JAVA}
public interface Iterator<T> {¢¢
  boolean hasNext();
  T next();
  default void remove() {¢¢
    throw new UnsupportedOperationException(
      "remove"
    );
  }
}
    \end{code}
  \end{onlyenv}
\end{frame}

\begin{frame}[fragile]{Using the \protect\cc{Iterator} \protect\kk{interface}}
  \squeezeSpace
  \begin{Figure}
    \begin{adjustbox}{H=0.94}
      \input{iterator-zoomin.tikz}
    \end{adjustbox}
  \end{Figure}
\end{frame}

\begin{frame}[fragile]{The entire code together}
  \begin{Code}{JAVA}{Class \ccn{Between}}
class Between implements Iterable<Int> {¢¢
  Between(int from, int to) {¢¢
    this.from = from;
    this.to = to;
  }
  private final int from, to;
  Iterator<Int> iterator() {¢¢
    return new Iterator<Int>() {¢¢
      final Int current = new Int(from);
      public boolean hasNext() {¢¢
        return current.lt(to);
      }
      public Int next() {¢¢
        return current.inc();
      }
    };
  }
}
  \end{Code}
\end{frame}

\subunit{Unbounded generators}
\begin{frame}[fragile]{A generator for the~$3n + 1$ sequence}
  \squeezeSpace[1]
  \begin{TWOCOLUMNSc}[-9ex]
    The famous~$3n+1$ sequence \[
    a_{i+1} = \begin{cases}
    aᵢ/2 & \text{$aᵢ$ is even}⏎
    3aᵢ + 1 & \text{$aᵢ$ is odd}⏎
    \end{cases}
  \]
  It is conjectured that there is no integer starting
  value~$a₁$ for which the sequence diverges to infinity.
  \MIDPOINT
  \begin{adjustbox}{}
    \begin{Code}{JAVA}{Iterable over \ccn{Integer}s}
class ThreeNPlus1 implements Iterable<Integer> {¢¢
  public static void main(String[] args) {¢¢
    for (Integer i : new ThreeNPlus1())
      System.out.println(i);
  }
  public Iterator<Integer> iterator() {¢¢
    return new Iterator<Integer>() {¢¢
      int i = 0;
      public boolean hasNext() {¢¢
        return true;
      }
      public Integer next() {¢¢
        i = (i % 2 == 0) ? i / 2 : 3 * i + 1;
        if (i <= 0) i = new Random().nextInt();
        return i;
      }
      public void remove() {¢¢ /* empty */ }
    };
  }
}
    \end{Code}
  \end{adjustbox}
  \end{TWOCOLUMNSc}
\end{frame}



\begin{frame}[fragile]{Refute the Goldbach conjecture with \Java iterators}
  \squeezeSpace
  \WJ[0.6]{Christian Goldbach}
  {For every even integer~$n > 2$ there exists primes~$p$ and~$q$ such that \[
    n = p + q.
  \]}
All subsequent definitions will be made within our main class:
\begin{Code}{JAVA}{Class \ccn{NaiveNumberTheory}}
import java.util.Iterator;
// Library type representing immutable
// arbitrary-precision integers:
import java.math.BigInteger;
¢¢
public class NaiveNumberTheory {¢¢
  ¢…¢
}
\end{Code}
\end{frame}


\begin{frame}[fragile]{The Goldbach conjecture and the halting problem}
  \begin{itemize}
    \item Our \cc{main} function terminates if and only if the Goldbach conjecture is
          false.
    \item Hence, the figuring out the Goldbach conjecture is easier than the halting problem.
    \item We may now understand a little better why the halting problem is so tough.
  \end{itemize}
  \begin{code}{JAVA}
public class NaiveNumberTheory {¢¢
  public static void main(String[] args) {¢¢
    System.out.println(
         "Goldbach conjecture refuted by "
      +
          refutationOfGoldbach()
    );
  }
  ¢…¢
}
  \end{code}
\end{frame}

\begin{frame}[fragile]{Iterating over all even numbers}
  \squeezeSpace[3]
  Our main loop:
  \begin{code}{JAVA}
public static BigInteger refutationOfGoldbach() {¢¢
  // Iterate over the infinite sequence ¢$\cc{e} = 4, 6, 8,…$¢
  for (BigInteger e = big(4);; e = succ2(e))
    if (refutesGoldbach(e))
      return e;
}
  \end{code}
  Three auxiliary functions:
  \begin{code}{JAVA}
public static BigInteger big(long n) {¢¢
  return BigInteger.valueOf(n);
}
public static BigInteger succ(BigInteger p) {¢¢
  return p.add(BigInteger.ONE);
}
public static BigInteger succ2(BigInteger p) {¢¢
  return succ(succ(p));
}
  \end{code}
\end{frame}

\begin{frame}[fragile]{Check whether an integer refutes the conjecture}
  \squeezeSpace[3]
  \begin{code}{JAVA}
// Determine whether integer ¢\cc{n}¢ can
// be written as sum of two primes:
private static boolean
  refutesGoldbach(BigInteger n) {¢¢
    for (BigInteger p : primes()) {¢¢
      if (p.compareTo(n) > 0)
        break;
      final BigInteger q = n.subtract(p);
      if (!isPrime(q)) continue;
      System.out.println(n + "=" + p + "+" + q);
      return true; // ¢\cc{p}¢ and ¢\cc{q}¢ refute Goldbach for ¢\cc{n}¢
    }
    return false; // No refutation found for ¢\cc{n}¢
}
  \end{code}
  \begin{description}[\cc{compareTo}]
    \item [\cc{primes()}] a pseudo generator of the infinite sequence of all primes.
    \item [\cc{compareTo}] defined in class \cc{BigInteger} so that
    \begin{equation}
      \cc{p.comparteTo(n)} =
      \begin{cases}
        \hfill \cc{1}  & \text{if~$\cc{p} > \cc{n}$}⏎
        \hfill \cc{0}  & \text{if~$\cc{p} = \cc{n}$}⏎
        \hfill \cc{-1} & \text{if~$\cc{p} < \cc{n}$}⏎
      \end{cases}
      \synopsis{Semantics of function \cc{compareTo}}
    \end{equation}
    \item [\cc{isPrime(q)}] determines whether \cc{q} is prime.
  \end{description}
\end{frame}

\begin{frame}[fragile]{Determine whether an integer is prime}
  \begin{itemize}
    \item Use pseudo generator \cc{primes()} for efficiency; no point in checking non-prime divisors
    \item Iterate until square root of tested number
  \end{itemize}
  \begin{code}{JAVA}
// Determine whether ¢\cc{n}¢ is prime
private static boolean isPrime(BigInteger n) {¢¢
  // Potential divisors must be prime
  for (final BigInteger p : primes()) {¢¢
    // Stop iterating if ¢$\cc{p}² > n$¢
    if (p.multiply(p).compareTo(n) > 0)
      break;
    if (n.mod(p).compareTo(big(0)) == 0)
      return false; // Divisor found
  }
  // No divisor found
  return true;
}
  \end{code}
\end{frame}

\begin{frame}[fragile]{Maintaining a list of previously found primes for efficiency}
  \squeezeSpace[2]
  \begin{Figure}[Cache of primes (initial state)]
      \begin{adjustbox}{}
        \input{primes-cache.tikz}
      \end{adjustbox}
    \end{Figure}
  \squeezeSpace[6]
  \begin{TWOCOLUMNS}
    \begin{Code}{JAVA}{Cache list item}
static class Node {¢¢
  final BigInteger prime;
  Node next = null;
  Node(int p) {¢¢ this(big(p)); }
  Node(BigInteger prime) {¢¢
    this.prime = prime;
  }
  Node append(BigInteger p) {¢¢
    return next = new Node(p);
  }
  Node append(int p) {¢¢
    return append(big(p));
  }
}
    \end{Code}
    \MIDPOINT
    \begin{Code}{JAVA}{The cache list}
static class Cache {¢¢
  static final Node first =
    new Node(2);
  static Node last =
    first.append(3).append(5);
  static void extend() {¢¢
    last = last.append(
      nextPrime(last.prime)
    );
  }
}
    \end{Code}
  \end{TWOCOLUMNS}
\end{frame}


\begin{frame}[fragile]{Extending the cache list}
  \squeezeSpace[4]
  \begin{LEFT}
    \begin{Figure}[Reminder: initial state of the cache of primes]
        \begin{adjustbox}{}
          \input{primes-cache.tikz}
        \end{adjustbox}
      \end{Figure}
  \end{LEFT}
  \medskip
  \begin{lCode}{JAVA}{Function \ccn{nextPrime}}
public static BigInteger nextPrime(BigInteger n) {¢¢
  for (BigInteger ¢\X$¢$ = succ(n);; ¢\X$¢$ = succ(¢\X$¢$))
    if (isPrime(¢\X$¢$))
      return ¢\X$¢$;
}
  \end{lCode}
  \medskip
  \medskip
  \begin{Figure}[Cache of primes (after first insertion)]
      \begin{adjustbox}{right}
        \input{primes-cache-extended.tikz}
      \end{adjustbox}
    \end{Figure}
  \begin{PLACE}{0.67,0.05}
    \begin{lCode}{JAVA}{Function \ccn{extend}}
static class Cache {¢¢
  ¢…¢
  static void extend() {¢¢
    last = last.append(
    nextPrime(last.prime)
    );
  }
}
    \end{lCode}
  \end{PLACE}
\end{frame}

\begin{frame}[fragile]{The \protect\cc{primes()} pseudo generator}
  \squeezeSpace[2]
  Let's begin with the trivial, technical parts
  \begin{code}{JAVA}
public static Iterable<BigInteger> primes() {¢¢
  return new Iterable<BigInteger>() {¢¢
    public Iterator<BigInteger> iterator() {¢¢
      return new Iterator<BigInteger>() {¢¢
        // There are infinitely many primes:
        public boolean hasNext() {¢¢
          return true;
        }
        // Cannot remove primes
        public void remove() {¢¢
          throw
            new UnsupportedOperationException();
        }
        ¢…¢
      };
    }
  };
}
  \end{code}
  The challenge is in the iterator function \cc{next()}
\end{frame}

\begin{frame}[fragile]{Function \protect\cc{next()} in pseudo generator \protect\cc{primes()}}
  \squeezeSpace[2]
  \begin{TWOCOLUMNS}[1ex]
    \Blue{\emph{Iterator function \cc{next()} must:}}
    \begin{itemize}\footnotesize
      \item Retrieve the next prime from the cache
      \item If the cache is exhausted, extend it
    \end{itemize}
    \Red{Tricky recursion:}
    \begin{itemize}\footnotesize
      \item To extend the cache, \cc{nextPrime()} is called
      \item \cc{nextPrime()} uses \cc{isPrime()}
      \item \cc{isPrime()} iterates over \cc{primes()}
      \item But to yield a prime, \cc{nextPrime()} may need to extend the cache
    \end{itemize}
    \Blue{\textbf{Luckily}}
    \begin{itemize}\footnotesize
      \item To yield a prime in the order of~$n$, you needs primes until about~$√n$.
      \item Cache was initialized with sufficiently primes to get this clockwork
            going
    \end{itemize}
    \MIDPOINT
    \begin{code}{JAVA}
¢…¢
return new Iterator<BigInteger>() {¢¢
  ¢…¢
  // Data member of the inner class
  Node next = Cache.first;
  // Yield the next prime from the cache
  public BigInteger next() {¢¢
    // Save value to be returned
    final BigInteger ¢\X$¢$ = next.prime;
    // Was cache exhausted?
    if (next == Cache.last)
      Cache.extend();
    // Advance to next node of the cache
    next = next.next;
    return ¢\X$¢$;
  }
};
    \end{code}
  \end{TWOCOLUMNS}
\end{frame}
\afterLastFrame

\exercises
\begin{enumerate}
  \item How does \Java implement generators?
  \item What's the difference, if any, between \Java's local classes, and non-\kk{static} nested classes?
  \item Can you use \Java's nested classes to implement anonymous functions?
  \item How are \CC nested classes different than those of \Java?
  \item Does \Java make distinction between generators and iterators? How?
  \item Can you use \Java's nested classes to implement closures?
  \item Can you use \Java's nested classes to implement coroutines?
        If yes, explain how; if no, explain why not.
  \item It is claimed that you can emulate generators in \CPL, by using \kk{static} variables in functions.
      Given an example, and discusss the limitations.
\end{enumerate}

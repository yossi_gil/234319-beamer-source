\begin{frame}{Requirements from a PL}
  \begin{description}[Implementable]
    \item [Universal] every problem must have a
    solution†{Exception: domain-specific languages, e.g.,
    pure SQL has no recursion}
    \begin{itemize}
      \item Express recursive functions;
            it is sufficient to require
            \begin{itemize}
              \item Conditionals
              \item Loops
            \end{itemize}
    \end{itemize}
    \item [Natural] application domain specific
    \qq{Try writing a compiler in \Cobol or a GUI in \Fortran}
    \item [Implementable]⏏
    \begin{itemize}
      \item Neither mathematical notation
      \item Nor natural language
    \end{itemize}
    \item [Efficient] open to debate
    \begin{itemize}
      \item More programming crimes were committed in the name of performance than for any other reason.
    \end{itemize}
  \end{description}
\end{frame}

\begin{frame}[allowframebreaks]{Desiderata for a PL}
  \squeezeSpace
  \begin{description}[Expressiveness]
    \item [Expressiveness]⏏
    \begin{itemize}
      \item Turing-completeness
      \item But also a practical kind of expressiveness: how easy is it to program simple concepts?
    \end{itemize}
    \item [Efficiency]⏏
    \begin{itemize}
      \item Recursion in functional languages is expressive but sometimes inefficient
      \item Is there an efficient way to implement the language (in machine code)?
    \end{itemize}
    \item [Simplicity]⏏
    \begin{itemize}
      \item as few basic concepts as possible
      \item Sometimes a trade-off with convenience (C has ‟for", who needs ‟while" and ‟do-while"?)
    \end{itemize}
    \item [Uniformity] and consistency of concepts
    \begin{itemize}
      \item \kk{for} in \Pascal allows a single statement
      \item \kk{repeat}…\kk{until} allows any number of statements?
      \item Why?
    \end{itemize}
    \item [Abstraction] language should allow to factor out recurring patterns
    \item [Clarity] to humans
    \qq{The \cc{=} \vs \cc{==} in~\CPL is a bit confusing}
    \item [Information hiding ] and \textbf{modularity}
    \item [Safety] possibility to detect errors at compile time
    \qq{\AWK, \Rexx and \Snobol type conversions are error prone}
  \end{description}
\end{frame}

\begin{frame}{Guiding principles in language design}
  Example: The~\CPL design rules:
  \begin{itemize}
    \item Division between preprocessor, compiler and linker.
    \item No hidden costs
    \item Programmer's accountability and responsibility
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{‟Less is more"}
  Two program fragments to find the~$n$\nthscript{th}
  Fibonacci number in \Algol-68
  \begin{code}[watermark text=Algol]{PASCAL}
x,y := 1;
to n do (if x<y then x else y) := x+y;
x := max(x,y);
  \end{code}
  \begin{code}[watermark text=Algol]{PASCAL}
x, y := 1;
to n do begin x,y := y,x; x := x+y end;
  \end{code}
\end{frame}

\begin{frame}[fragile]{A legendary \Fortran bug}
  \squeezeSpace
  Computing
  \begin{equation*}
    \sum_{i=1}^{314} \sin(i)
  \end{equation*}
  with \Fortran:
  \begin{code}{FORTRAN}
       S = 0
       DO 1000 I=1,314
       S = S + SIN(I)
1000 CONTINUE
  \end{code}
  But if you accidentally replace ‟\cc{,}” by ‟\cc{.}” the code is very different
  \begin{code}{FORTRAN}
       S = 0
       DO1000I = 1.314
       S = S + SIN(I)
1000 CONTINUE
  \end{code}
  variable \cc{S} becomes simply~$\sin(1)$.
\end{frame}

\begin{frame}{The Mariner 1 aborted launch}
  \squeezeSpace
  \begin{Figure}[Launch of Mariner 1]
    \begin{TWOCOLUMNS}
      \begin{centering}
        \begin{minipage}[t]{\columnwidth}
          \begin{adjustbox}{clip=true,H=0.7}
            \includegraphics{Graphics/atlas-agena-mariner-1.jpg}
          \end{adjustbox}
        \end{minipage}
      \end{centering}
      \MIDPOINT
      Smoke and fire launch of the Mariner~1, less than five minutes
      prior to its abortion by a security officer due to a combination of a hardware problem and
      software bug;
      July \nth{22} 1962, 09:26:16 UTC
    \end{TWOCOLUMNS}
  \end{Figure}
\end{frame}

\begin{frame}{The bug leading to the abortion}
  Background:
  \begin{itemize}
    \item The ‟cold war” between the US and USSR
    \item The ‟space race” between the US and USSR
    \item America's first planetary mission
    \item A very expensive project
    \item Designed in only 45 days
    \item Politicians wanted explanation
    \item The public wanted explanation
  \end{itemize}
  Outcome:
  \begin{itemize}
    \item \textbf{Official explanation:}
          ‟\emph{omission of a hyphen in coded computer instructions in the data-editing program}”
    \item \textbf{Urban legend:}
          the famous \Fortran bug
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Poor design of the \Fortran PL}
  \begin{code}{FORTRAN}
       S = 0
       DO 1000 I=1.314
       T = T + SIN(I)
1000 CONTINUE
  \end{code}
  \begin{itemize}
    \item Bad lexical definition - spaces are immaterial
    \item No declaration of variables
    \item Implicit typing
    \item Poor control structure specification
    \item Lack of diagnostics
  \end{itemize}
\end{frame}

\begin{frame}{Concepts in the PL World}
  \begin{table}[H]
    What characterizes a PL?⏎
    \begin{centering}
      \begin{adjustbox}{max width=\columnwidth}
        \coloredTable
        \begin{tabular}{L{0.4}L{0.4}}
          \toprule
          \textbf{Question to ask}         & \textbf{Concepts}⏎
          \midrule
          How it handles values?           & \textit{values, types and expressions}⏎
          How it checks types?             & \textit{type systems}⏎
          Its entities for storing values? & \textit{storage}⏎
          Its means for storing values?    & \textit{commands}⏎
          How it alters control?           & \textit{sequencers}⏎
          How it attaches names to values? & \textit{binding}⏎
          How it allows generalization?    & \textit{functions}⏎
          \bottomrule
        \end{tabular}
      \end{adjustbox}
    \end{centering}
    \Caption
  \end{table}
  \structure{Paradigms…}
\end{frame}

\afterLastFrame


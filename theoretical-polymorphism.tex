\subunit{Motivation}

\begin{frame}{The road to polymorphism}
  \squeezeSpace[8]
  \framezoom<18,28><19-27>(2.5cm,3.6cm)(3.5cm,5.5cm)
  \begin{Figure}
    \begin{adjustbox}{W=1,H=0.95}
      \input{poly-tree.tikz}
    \end{adjustbox}
  \end{Figure}
\end{frame}

\begin{frame}{Benefits of strong static typing}
  Large software systems tend to use static strongly typed languages, because of
  \begin{description}[Efficiency]
    \item [Safety] fewer bugs
    \item [Efficiency] fewer runtime checks, and more efficient use of memory
    \item [Clarity] typing makes the code clearer
  \end{description}
  However, typing can be a nuisance
  the utility of a given piece of a code may be \emph{very} restricted
  by typing.
\end{frame}

\begin{frame}[fragile=singleslide]{An annoying \Pascal example}
  \begin{code}{PASCAL}
Procedure sort(var a: Array[1..300] of T);
  \end{code}
  could not be applied to
  \begin{itemize}[<+->]
    \item Arrays of real (body and declaration has to be repeated with \cc{T=Real}).
    \item \cc{array[1..299] of T}: Array is too small.
    \item \cc{array[1..500] of T}: Array is too large.
    \item \cc{array[0..299] of T}: Mismatch of indices.
    \item \cc{array[1..300] of T}: No name equivalence!!!!
  \end{itemize}
  \Pascal is so fussy and inflexible in its type system that even two
  identical type declarations are considered distinct. A type declaration
  made at a certain point in a program is equivalent only to itself.
\end{frame}

\begin{frame}{Flexibility of type system}
  \structure{Flexible†{%
    \emph{Flexibility} is yet another criterion
    for the classification of type systems}
  type system makes typing an aide, not a hurdle}
  \begin{itemize}
    \item Avoid issuing type error messages on programs which will not make
          run time type errors.
    \item Promotes code reuse for many different types.
  \end{itemize}
  Clearly, \Pascal offers a very inflexible type system.
  \W[0.5]{The holy grail of language design}
  {Simultaneously maintain:
    \begin{enumerate}
      \item Flexibility
      \item Safety
      \item Simplicity
    \end{enumerate}
  }
\end{frame}

\begin{frame}[fragile=singleslide]{Life without handcuffs can be wonderful!}
  In dynamically typed Languages, polymorphic code may
  be invoked with variables of different type (writing almost at a
  pseudo-code level)
  \begin{code}[watermark text=pseudo~\CPL]{CPLUSPLUS}
search(k) {¢¢
  // ¢\cc{k}¢ is the key to search for
   ¢…¢
   // ¢\cc{p}¢ is the current position in the search for k
  for (p = first();
    not exhausted(p,k);
      p = next(p,k))
        if (found(p,k))
          return true;
  return false;
}
  \end{code}
  \W{Alas}{Very flexible, but not so safe}
\end{frame}

\begin{frame}[fragile,allowframebreaks]{Responses to inflexibility}
  \begin{enumerate}
    \item \emph{The~\CPL camp:} Weak typing.
          \begin{code}{CEEPL}
int qsort(
  char *base, // Start of array
  int n, // Number of elements
  int width, // Element's size
  int(*compare)() // How elements are compared
);
    \end{code}
    \item \emph{Dynamically typed languages camp:}
          \Smalltalk, \Python, etc.: dynamic typing overcomes
          complex inflexibility problems. In a sense, all code is polymorphic.
    \item \Java, previously used dynamic typing
          \begin{code}{JAVA}
Comparator.compare(Object, Object)
    \end{code}
    Now uses generic typing (since version 5, released 2004)
    \begin{code}{JAVA}
Comparator<T>.compare(T, T)
    \end{code}
    \item \emph{\Ada/\CC camp:} Polymorphic type systems
          \begin{code}{ADA}
generic
  type T is private
    with function comp(x: T, y: T)
         procedure sort(a: array(1..max) of T)
     ¢\textrm{$…$}¢
  procedure int_sort is new sort(int , "<");
   ¢\textrm{$…$}¢
    \end{code}
  \end{enumerate}
  But, what is a ‟\emph{polymorphic type system}”?
\end{frame}

\begin{frame}{Monomorphic \vs polymorphic type systems}
  \begin{TWOCOLUMNS}
    \structure{Monomorphic Type Systems}
    \begin{itemize}\small
      \item Used in classical PLs, e.g., \Pascal
      \item Every entity has a single simple type
      \item Type checking is straightforward
      \item Unsatisfactory for reusable software;
            \begin{itemize}
              \item Many standard algorithms are inherently generic (e.g., sort)
              \item Many standard data structures are also generic (e.g., trees)
            \end{itemize}
    \end{itemize}
    \MIDPOINT
    \structure{Polymorphic Type Systems}
    \begin{itemize}\small
      \item Appear in modern languages, e.g., \Ada, \CC, \Java and \ML.
      \item Entities may have multiple types
      \item Code reuse thanks to universal polymorphism
      \item Supports
            \begin{itemize}
              \item Generic functions, e.g., sort.
              \item Generic types, e.g., binary tree.
            \end{itemize}
    \end{itemize}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}{What's monomorphism?}
  \squeezeSpace[6]
  \begin{Figure}[Monomorphism in context]
    \begin{adjustbox}{}
      \furnish{%
        Ad hoc tree with hidden,
        Universal tree with hidden,
        Typing with font=\bfseries,
        Monomorphic typing with current
      }
      \input{four-polymorphisms.tikz}
    \end{adjustbox}
  \end{Figure}
  \squeezeSpace
  In a \emph{monomorphic} type system, functions (and other entities) have \alert{one, and only one,} type.
  \begin{Block}[minipage,width=0.7\columnwidth]{{Monomorphic = ‟\emph{single-shaped}”}}
    \squeezeSpace[3]
    \begin{equation}
      \text{$f$ is a function}⇒|\textsf{types}(f)| = 1.
      \synopsis{In a monomorphic type systems, functions (and other entities) have precisely one type.}
    \end{equation}
  \end{Block}
\end{frame}

\begin{frame}[fragile=singleslide]{Monomorphism of user defined functions in \Pascal}
  Programmer defined functions (and procedures) in \Pascal are monomorphic:
  \begin{code}{Pascal}
Function gcd(n, m: Integer): Integer;
Begin
  if n mod m <> 0 then
    gcd := gcd(m, n mod m)
  else
    gcd := m;
end;
  \end{code}
  Function \cc{gcd} is monomorphic:
  \begin{equation}
    |\textsf{types}(\cc{gcd})| = |❴\kk{Integer}⨉\kk{Integer}→\kk{Integer}❵| = 1
    \synopsis{Function \cc{gcd} in \Pascal is monomorphic}
  \end{equation}
\end{frame}

\begin{frame}[fragile=singleslide]{Monomorphism of user defined functions in~\CPL}
  \begin{code}{CEEPL}
int gcd(int n, int m) {¢¢
  return n % m ? gcd(m, n % m) : m;
}
  \end{code}
  Function \cc{gcd} is monomorphic:
  \begin{equation}
    |\textsf{types}(\cc{gcd})| = |❴\kk{int}⨉\kk{int}→\kk{int}❵| = 1
    \synopsis{Function \cc{gcd} in~\CPL is monomorphic}
  \end{equation}
\end{frame}

\begin{frame}[fragile=singleslide]{‟More than one type”~$≈$ polymorphism}
  \begin{Figure}[Polymorphism in context]
    \begin{adjustbox}{}
      \furnish{%
        Ad hoc tree with hidden,
        Universal tree with hidden,
        Typing with font=\bfseries,
        Polymorphic typing with current
      }
      \input{four-polymorphisms.tikz}
    \end{adjustbox}
  \end{Figure}
  \Q{\large\textbf{Poly-Morphism} = poly + morphos [Greek] = many + form.}
  \emph{literally, the capacity of an entity to have several shapes}
\end{frame}

\begin{frame}{Ad hoc polymorphism~$≠$ polymorphic type system}
  \begin{description}[Overloading]
    \item [Overloading] \emph{minimal utility}. A (small) number of distinct
    procedures that just happen to have the same identifier.
    \begin{itemize}
      \item Not a truly polymorphic object
      \item Does not increase the language's expressive power
      \item Similarity between shapes is coincidental
    \end{itemize}
    \item [Coercion] \emph{a little greater utility}
    \begin{itemize}
      \item Same routine can be used for several purposes
      \item Number of purposes is limited
      \item Return type is always the same
      \item Similarity between shapes is determined by coercion operations
            which are external to the routine
    \end{itemize}
  \end{description}
\end{frame}

\subunit{Overloading, revisited}

\begin{frame}{Overloading and types}
  \squeezeSpace[5]
  \begin{Figure}
    \begin{adjustbox}{}
      \furnish{%
        Coercion with hidden,
        Universal tree with hidden,
        Monomorphic typing with hidden,
        Ad hoc with hidden,
        Overloading with current
      }
      \input{four-polymorphisms.tikz}
    \end{adjustbox}
  \end{Figure}
  We say that a function (or, an operator)~$f$ is overloaded,
  if:
  \begin{itemize}
    \item~$f$ has more than one type
    \item there is \Red{no automatic mechanism} that generates the
          set~$\textsf{types}(f)$
  \end{itemize}
  Overloading provides a mechanism for better utilization of \emph{scarce} ‟good” names
\end{frame}

\begin{frame}{Overloaded builtin operators of \Pascal}
  \squeezeSpace
  Consider, e.g., operator~\cc{+}
  \begin{description}[Numbef of types]
    \item [Number of types] More than one type:
    \begin{itemize}
      \item~$\cc{+}∈\kk{Integer}→\kk{Integer}$
      \item~$\cc{+}∈\kk{Real}→\kk{Real}$
      \item~$\cc{+}∈\kk{Integer}⨉\kk{Integer}→\kk{Integer}$
      \item~$\cc{+}∈\kk{Real}⨉\kk{Real}→\kk{Real}$
    \end{itemize}
    Thus,~$$|\textsf{types}(+)| = 4 > 1.$$
    \item [Regularity] Is the set of types ‟automatically generates?
    \begin{itemize}
      \item The above four types are similar and related.
      \item They were designed by the individual who designed \Pascal.
      \item This individual gave them semantics.
      \item Incidentally, these semantics are related.
      \item But, they were not ‟automatically” generated.
    \end{itemize}
  \end{description}
\end{frame}

\begin{frame}[fragile=singleslide]{Builtin function overloading in \Pascal?}
  \squeezeSpace
  Many \Pascal functions apply to more than one type:
  \begin{itemize}
    \item \cc{eof}
    \item \cc{succ}
    \item \cc{ord}
    \item \cc{sin}
          \item~$⋮$
  \end{itemize}
  But, their polymorphism is not overloading; it is either
  \begin{itemize}
    \item coercion, \emph{or}
    \item parametric
  \end{itemize}
  polymorphism.
  \begin{Block}{}
    Similarity of overloaded meanings is a matter of coincidence
  \end{Block}
\end{frame}

\begin{frame}[fragile=singleslide]{Overloading \vs hiding}
  \structure{Hiding (by lexical scope):}
  an identifier defined in an inner scope hides an identifier defined in an outer scope
  \begin{Code}{CEEPL}{Hiding in~\CPL}
static long tail;
¢\textrm{…}¢
int main(int ac, char **av) {¢¢
  // hides outer ¢\/\cc{tail}¢
  const char **tail = av + ac - 1;
  ¢\textrm{…}¢
}
  \end{Code}
  \structure{Comparison:} both do \emph{not} make polymorphic types
  \begin{description}[In Overloading]
    \item [Overloading] Multiple meanings \emph{co-exist}
    \item [Hiding] New meaning \emph{masks} the old meaning.
  \end{description}
\end{frame}

\begin{frame}{Overloading ＆ hiding together?}
  May be challenging for language designers?
  \begin{itemize}
    \item Can inner definition overload external definition?
    \item What happens if an inner definition hides one overloaded outer definition, but not the other?
  \end{itemize}
  \W[0.6]{Exercise}
  {Provide examples in concrete languages, and see how they deal with these dilemmas.}
\end{frame}

\subunit[coercion]{Coercion}

\begin{frame}[label=current]{What's coercion?}
  \squeezeSpace
  \furnish{%
    Monomorphic typing with hidden,
    Universal tree with hidden,
    Overloading with hidden,
    Coercion with current
  }
  \input{four-polymorphisms.tikz}
  \squeezeSpace[2]
  \WD{Coercion}{%
    \Red{Coercion} is a conversion from values of one
    type to values of another type which occurs \emph{implicitly}.
    \emph{casting}.}
\end{frame}

\begin{frame}[fragile=singleslide]{Why coercion?}
  \Pascal provides coercion from \cc{Integer} to \cc{Real},
  so we can write:
  \begin{TWOCOLUMNS}
    \begin{Code}{PASCAL}{Primality testing in \Pascal}
Function isPrime(n: integer): Boolean;
VAR
  d: Integer; (* Potential divisor *)
  primeSoFar: Boolean;
Begin
  If n < 0 then n := -n;
  primeSoFar := n >= 2;
  d := 2;
  While primeSoFar and (d <= sqrt(n)) do
  Begin
    primeSoFar := n mod d <> 0;
    d := d + 1;
  end;
  isPrime := primeSoFar;
end;
    \end{Code}
    \MIDPOINT
    \begin{itemize}
      \item Function \cc{sqrt} expects a \cc{Real}
      \item We need to compute~$√{\cc{n}}$, but~\cc{n} is an
            \cc{Integer}
      \item \textbf{Coercion:}~\cc{n} is \alert{implicitly}
            converted to \cc{Real}
      \item Net effect: \cc{sqrt} applies also to \cc{Integer}
    \end{itemize}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile=singleslide]{Implicit use of coercion}
  \structure{Coercion enhances the utility of existing functions}
  \begin{code}{PASCAL}
  ¢\textrm{…}¢
  While primeSoFar and (d <= sqrt(n)) do
      ¢\textrm{…}¢
  \end{code}
  \begin{enumerate}
    \item Function \cc{sqrt} expects a \cc{Real}, but thanks to coercion we
          can pass it an \cc{Integer}
    \item Function \cc{sqrt} returns a \cc{Real}, but thanks to coercion we
          can compare its result with an \cc{Integer}
  \end{enumerate}
\end{frame}

\begin{frame}{Type coercion in \Algol-68}
  \Algol68 allows the following coercion operations:
  \begin{description}[Dereferencing]
    \item [Promotion] From integer to real
    \item [Widening] From real to complex number
    \item [Dereferencing] From reference to a variable to its value
    \item [Rowing] From any value to a singled value array
    \item and more…
  \end{description}
  Now you can understand why modern languages tend to minimize or even
  eliminate coercion altogether.
\end{frame}

\begin{frame}[fragile]{Builtin coercion in \CC}
  \begin{code}[minipage]{CEEPL}
int pi = 3.14159; // Builtin coercion from ¢\kk{double}¢ to ¢\kk{int}¢
float x = '\0'; // Builtin coercion from ¢\kk{char}¢ to ¢\kk{float}¢
extern double sqrt(float);
x = sqrt(pi); // Builtin coercion from ¢\kk{int}¢ to ¢\kk{double}¢
          // and then
          // Builtin coercion from ¢\kk{double}¢ to ¢\kk{float}¢
  \end{code}
  Coercion is sometimes called, especially in \CC,
  \emph{type casting} and \emph{type conversion}, without particular
  distinction between implicit and explicit applications.
\end{frame}

\begin{frame}[fragile=singleslide]{Ambiguity due to coercion}
  \structure{Graph of coercion operations is not always a tree}
  \begin{itemize}
    \item What is the path of coercion from
          \kk{unsigned char} to \kk{long double}?
          \[
            \kk{unsigned char}→\kk{char}→\kk{int}→\kk{long}→\kk{double}→\kk{long double}
          \]
          or maybe, \[
          \kk{unsigned char}→\kk{unsigned}→\kk{unsigned long}→\kk{long double}
  \]
  \item Selecting a different path may lead to slightly different semantics
  \item K＆R~\CPL, ANSI-\CPL and \CC are all different in this respect.
  \end{itemize}
  \structure{Graph of coercion operations is not always a DAG}
  \begin{itemize}
    \item Types \kk{int}, \kk{double} and \kk{float} in~\CPL, can all be
          coerced into each other.
    \item Therefore, the language definition must specify exactly the
          semantics of e.g., \cc{'a'*35+5.3f}
  \end{itemize}
\end{frame}

\begin{frame}{Coercion + overloading}
  Strategies for support of mixed type arithmetic, e.g., \cc{A + B}
  \begin{description}
    \item [Overloading \emph{and no} coercion]▄
    \begin{itemize}
      \item \cc{integer + integer}
      \item \cc{real + integer}
      \item \cc{integer + real}
      \item \cc{real + real}
    \end{itemize}
    \item [Coercion \emph{and no} overloading]▄
    \begin{itemize}
      \item \cc{real + real}
      \item \(\cc{integer}→\cc{real}\)
    \end{itemize}
    \item [Coercion \emph{and} overloading]▄
    \begin{itemize}
      \item \cc{integer + integer}
      \item \cc{real + real},
      \item \(\cc{integer}→\cc{real}\)
    \end{itemize}
  \end{description}
\end{frame}

\subunit[universal]{Universal polymorphism}

\begin{frame}[label=current]{What is ‟ad hoc” polymorphism?}
  \squeezeSpace
  \furnish{%
    Monomorphic typing with hidden,
    Overloading with hidden,
    Coercion with hidden,
    Polymorphic typing with font=\bfseries,
    Universal with current,
    Inclusion tree with hidden,
    Parametric tree with hidden
  }
  \input{four-polymorphisms.tikz}
  \par
  All we have seen so far is \emph{ad hoc} polymorphism,
  in which the variety in different shapes is created by human.
  \begin{itemize}
    \item Each overloaded version
    \item Each distinct coercion
  \end{itemize}
  Human can be language designer and/or programmer
  (depending on the PL)
\end{frame}

\begin{frame}{Ad hoc \vs universal polymorphism}
  \squeezeSpace
  \WD[0.8]{‟ad hoc”}
  {\small\justifying
    \textbf{ad hoc} adv.\ \textbf{1}. For the specific purpose, case, or
    situation at hand and for no other: \emph{a committee formed ad hoc to
      address the issue of salaries}. \textbf{--ad hoc} adj.\ \textbf{1}.\ Formed for or concerned with one specific purpose: \emph{an ad hoc
      compensation committee}. \textbf{2}. Improvised and often impromptu:
    ‟\emph{On an ad hoc basis, Congress has
    … placed … ceilings on military aid to specific countries}"
    (New York Times). [Latin \emph{ad}, to + \emph{hoc}, this.] }
  \begin{table}[H]
    \begin{centering}
      \begin{adjustbox}{}
        \coloredTable
        \begin{tabular}{R{0.33}L{0.26}L{0.38}}
          \toprule
                                    & \textbf{Universal} & \textbf{Ad Hoc}⏎
          \midrule
          \textbf{No.\ Shapes}      & Unbounded          & Finite and few (often very few)⏎
          \textbf{Shape Generation} & Automatic          & Manual⏎
          \textbf{Shape Uniformity} & Systematic         & Coincidental⏎
          \bottomrule
        \end{tabular}
      \end{adjustbox}
    \end{centering}
    \Caption
  \end{table}
\end{frame}

\begin{frame}{The benefits of universal polymorphism}
  \begin{itemize}
    \item A single function (or type) has a (large) family of related types
    \item The function operates uniformly on its arguments, whatever their type.
    \item Provide a genuine gain in expressive power, since a polymorphic
          function may take arguments of an unlimited variety of types
  \end{itemize}
\end{frame}

\subunit[parametric-polymorphism]{Parametric polymorphism}

\begin{frame}[fragile=singleslide]{Annoying example: a monomorphic \Pascal function}
  \begin{CENTER}[0.7\columnwidth]
    \begin{Code}{PASCAL}{Determine whether two sets of characters are disjoint}
Type CharSet = set of Char;
Function disjoint(s1, s2: CharSet): Boolean;
Begin
    disjoint := (s1 * s2 = [])
end
    \end{Code}
  \end{CENTER}
  Type is \[
  𝒫(\cc{Char}) ⨉ 𝒫(\cc{Char}) → \cc{Boolean}
\]
Applicable only to sets of \cc{Char}s.
\end{frame}

\begin{frame}[fragile=singleslide]{Using the \protect\cc{disjoint} monomorphic function}
  Applicable to a pair of arguments, each of type~$𝒫\cc{Char}$:
  \begin{code}{PASCAL}
VAR chars : CharSet;
Begin
  ¢\textrm{…}¢
  If disjoint(chars,['a','e','i','o','u']) then ¢\textrm{…}¢;
end
  \end{code}
  But, cannot be applied to arguments
  of other type, such as, \(𝒫 \cc{Integer}\),
  \(𝒫 \cc{Color}\), …
  \small
  \W[0.8]
  {Counter example: a \Pascal polymorphic operator}
  {\justifying The~\cc{*} operator in \Pascal is \emph{polymorphic}.
  It can be applied to any two sets of the same kind of elements}
  \Q{Polymorphism is \emph{universal}, since the operator
  works in the same fashion for all types for which it is applicable.}
\end{frame}

\subunit[polytypes]{Polytypes}
\begin{frame}{What are polytypes?}
  \furnish{%
    Monomorphic typing with hidden,
    Ad hoc tree with hidden,
    Inclusion tree with hidden,
    Polytypes with current
  }
  \input{four-polymorphisms.tikz}
  \par
  A \Red{polytypes} is the type ‟common” to all instances of
  a specific parametric type.
\end{frame}

\begin{frame}{Polytypes}
  \begin{block}{Definitions}
    \begin{TWOCOLUMNS}\small
      \begin{description}[A]
        \item [\textbf{Polytype}](also called \emph{parametric type})
        a type whose definition contains one or more \emph{type variables}
        \item [\textbf{Monotype}] a type whose definition includes no type variables;
      \end{description}
      \MIDPOINT
      \begin{description}[A]
        \item [\textbf{Monomorphic PL}] offers solely monotypes
        \item [\textbf{Polymorphic PL}] offers also polytypes
      \end{description}
    \end{TWOCOLUMNS}
  \end{block}
  \textbf{Examples:}
  A ‟plain” polytype, and plenty of types of polymorphic functions:
  \begin{TWOCOLUMNS} \small
    \begin{itemize}
      \item~$\cc{list}(σ)$
      \item~$\cc{list}(σ) → σ~$
      \item~$\cc{list}(σ) → \cc{Integer}$
    \end{itemize}
    \MIDPOINT
    \begin{itemize}
      \item~$σ→σ$
      \item~$σ⨉σ→σ$
      \item~$(β→γ)⨉(α→β)→(α→γ)$
    \end{itemize}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}{A polytype derives many types}
  A polytype derives a whole family of types, e.g.,
  type~$σ → σ$ derives:
  \begin{itemize}
    \item~$\cc{Integer} → \cc{Integer}$,
    \item~$\cc{String} → \cc{String}$,
    \item~$\cc{list}(\cc{Real}) → \cc{list}(\cc{Real})$,
    \item …
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]{No programmer-defined polytypes in \Pascal!}
  The type of the predefined function \cc{eof} is \cc{File} \kk{of}~$σ$.
  If \Pascal had user defined polytypes, we could have written
  \begin{TWOCOLUMNS}
    \begin{code}[watermark text=pseudo \Pascal]{PASCAL}
TYPE
  Pair(¢$σ$¢) = Record
     first, second: ¢$σ$¢;
  end;
  IntPair = Pair(Integer);
    \end{code}
    \MIDPOINT
    \begin{code}[watermark text=pseudo \Pascal]{PASCAL}
TYPE
  RealPair = Pair(Real);
  list(¢$σ$¢) = ¢\textrm{…}¢;
VAR
  line: list(Char);
    \end{code}
  \end{TWOCOLUMNS}
  Unfortunately, this would not work in \Pascal.
  All we can write is something of the sort of
  \begin{code}{PASCAL}
TYPE IntPair = Record
       first, second: Integer;
     end;
VAR line: CharList;
  \end{code}
\end{frame}

\begin{frame}[fragile=singleslide]{Defining polytypes in \ML}
  \begin{code}{EMEL}
type ¢$σ$¢ pair = ¢$σ$¢ * ¢$σ$¢;
datatype ¢$σ$¢ list =
    nil
  | cons of (¢$σ$¢ * ¢$σ$¢ list);
fun hd(l: ¢$σ$¢ list) =
    case l of nil => ¢\textrm{…}¢ (* error *)
            | cons(h,t) => h
  and tl(l: ¢$σ$¢ list) =
    case l of nil => ¢\textrm{…}¢ (* error *)
            | cons(h,t) => t
  and length(l: ¢$σ$¢ list) =
    case l of nil => 0
            | cons(h,t) => 1 + length (t)
  \end{code}
  \begin{itemize}
    \item Notations for some common polytypes:
          \begin{itemize}
            \item~$\cc{Pair}(σ) = σ ⨉ σ$
            \item~$\cc{list}(σ) = \cc{Unit} + (σ ⨉ \cc{list}(σ))$
            \item~$\cc{Array}(σ,σ) = σ → σ$
            \item~$\cc{Set}(σ)= 𝒫(σ)$
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Values of a polytype}
  What is the set of values of a polytype? Weird question …
  \begin{description}
    \item [In \CC] A class template has no values, only if you substitute
    an actual type to its type variable, you will get a real type.
    \item [In \ML] One can easily define \emph{values of a polytypes
    representing polymorphic functions}. For example, the type of
    the function \cc{second} is the polytype \[
    σ ⨉ σ → σ.
  \]
  \end{description}
  A tough problem---what are the values of the polytype~$\cc{list}(σ)$?
  \begin{description}
    \item [Definition] The set of values of any polytype is the intersection
    of all types that can be derived from it.
    \item [Rationale] suppose~\cc{v} is a value of a polytype for which no
    monotype substitution was performed. Then the \emph{only} legitimate
    operations on~\cc{v} would be those available for \emph{\textbf{any}}
    monotype derived from the polytype.
  \end{description}
\end{frame}

\begin{frame}{Example: polytype~$\protect\cc{list}(σ)$}
  \structure{Monotypes Derived From~$\cc{list}(σ)$}
  \scriptsize
  \begin{description}[$\cc{list}(\cc{Boolean})$]
    \item [$\cc{list}(\cc{Integer})$] all finite lists of integers, including the empty list.
    \item [$\cc{list}(\cc{Boolean})$] all finite lists of truth values, including the empty list.
    \item [$\cc{list}(\cc{String})$] all finite lists of strings, including the empty list.
    \item […]
  \end{description}
  \normalsize
  \structure{The empty list is the only common element}
  \scriptsize
  \begin{itemize}
    \item Nonempty lists are values of a specific monotype, determined
          by components' type.
    \item The empty list is a value of any monotypes derived
          from~$\cc{list}(σ)$
    \item The type of the empty list has type~$\cc{list}(σ)$
    \item There are no other values of type~$\cc{list}(σ)$
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]{Example: the polytype~$σ → σ$}
  \small
  Monotypes derived from~$σ → σ$ :
  \begin{description}
    \item [\cc{Integer}~$→$ \cc{Integer}] includes
    the integer identity function,
    the successor function, the absolute value function,
    the squaring function, etc.
    \item [$\cc{String}→\cc{String}$] includes the string identity function,
    the string reverse function, the space trimming function, etc.
    \item [$\cc{Boolean}→\cc{Boolean}$] includes the truth value identity function, the logical negation function, etc.
    \item …
  \end{description}
  The \emph{identity function} is common to all~$σ → σ$ types.
  \alert{In fact, this is the only such common value.}
\end{frame}

\begin{frame}{Values of polytypes (more examples)}
  \begin{description}[\(
      (β → γ)⨉(α→β)→(α→γ)
    \)]
    \item [$𝒫(σ)$] The empty set, \cc{[]}
    \item [$\cc{Pointer}(σ)$] The value \kk{nil}.
    \item [$σ ⨉ σ → σ$] Function \cc{second}
    \item [$(β → γ)⨉(α → β) → (α → γ)$] Function \kk{o}
    \item [$(σ → σ)→(σ → σ)$] \cc{id}, \cc{twice}, \cc{thrice}, \cc{fourth}, etc., and even function
    \cc{fixedpoint}
    (the function mapping any~$σ → σ$ function
    to~$\cc{id}:σ → σ$.
    \item [$\cc{Pair}(σ) = σ ⨉ σ$] empty
    \item [$\cc{Array}(σ,σ) = σ → σ$] empty
  \end{description}
\end{frame}

\begin{frame}{Polytypes ＆ software engineering}
  The polytype of a function is very telling of what it does. It is often
  easy to guess what a function does, just by considering its polytype.
  Many polytypes have only one value, which eliminates the guessing
  altogether
  \begin{description}[Slightly More Difficult]
    \item [Easy examples]▄⏎
    \begin{itemize}
      \item~$\cc{list}(σ)→ σ$
      \item~$\cc{list}(σ)→ \cc{list}(σ)$
      \item~$\cc{list}(σ)→ \cc{Integer}$
      \item~$σ → σ$
      \item~$σ ⨉ σ → σ$
      \item~$(β → γ)⨉(α → β) → (α → γ)$
    \end{itemize}
    \item [Slightly more difficult]▄⏎
    \begin{itemize}
      \item~$\cc{list}(σ)⨉ \cc{list}(σ)→ \cc{list}(σ ⨉ σ)$,
      \item~$(σ → σ)⨉ \cc{list}(σ)→ \cc{List}(σ)$,
      \item~$(σ ⨉ σ → σ) → σ ⨉ \cc{List}(σ)→ σ$
    \end{itemize}
  \end{description}
\end{frame}

\begin{frame}{Algebra of polytypes}
  \begin{itemize}
    \item There are software systems that promote reuse by supporting a search
          for functions based on their signatures.
    \item Clearly, the search must be insensitive to application of the commutative
          laws to product and choice.
    \item Further, the search should be made insensitive to choice of labels
  \end{itemize}
\end{frame}

\subunit[inclusion]{Inclusion polymorphism}

\begin{frame}{Inclusion polymorphism}
  \textbf{Inclusion Polymorphism:} The other kind of universal polymorphism.
  Arising from an inclusion relation between types or sets of values.
  \furnish{%
    Monomorphic typing with hidden,
    Ad hoc tree with hidden,
    Parametric tree with hidden,
    Inclusion with current
  }
  \input{four-polymorphisms.tikz}
\end{frame}

\begin{frame}{Subtyping}
  \squeezeSpace
  \furnish{%
    Monomorphic typing with hidden,
    Ad hoc tree with hidden,
    Parametric tree with hidden,
    Subrange with current,
    Inheritance with current
  }
  \input{four-polymorphisms.tikz}
  \par
  Most inclusion polymorphism is due to \emph{subtyping}, but not always.
  \begin{columns}
    \scriptsize
    \COL{0.35}
    \begin{block}{Definition (Subtyping: Version I)}
      Type~$A$ is a \emph{subtype} of the type~$B$ if~$A⊆B$.
    \end{block}
    \COL{0.58}
    \begin{block}{Definition (Subtyping: Version II)}
      Type~$A$ is a \emph{subtype} of the type~$B$,
      if every value of~$A$ can be \emph{coerced} into a value of~$B$.
    \end{block}
  \end{columns}
\end{frame}

\begin{frame}[allowframebreaks,fragile]{Inclusion polymorphism}
  \begin{description}
    \item [Builtin:]▄⏎
    \begin{itemize}
      \item \Pascal: The \kk{Nil} \emph{value} belongs to all pointer types.
            \item~\CPL: The \emph{value} 0 is polymorphic. It belongs to all pointer types.
      \item \CC: The \emph{type} \cc{\kk{void} *} is a \emph{super-type} of all pointer types.
    \end{itemize}
    \item [User Defined] Two Varieties
    \begin{description}
      \item [(not OO†{here, and henceforth OO = \textbf Object \textbf Oriented})] Subranges in \Pascal:
      \begin{code}{PASCAL}
TYPE
  Index = 1..100;
  Digit = '0'..'9';
      \end{code}
      \begin{itemize}
        \item Anything applicable to \cc{Integer} will be applicable to type \emph{Index}.
        \item Anything applicable to \cc{Char} will be applicable to type \emph{Digit}.
      \end{itemize}
      \item [OO] A subclass is also a subtype
      \begin{Code}{CPLUSPLUS}{Inheritance in \CC}
// a ¢\cc{Manager}¢ is kind of an ¢\cc{Employee}¢
class Manager: public Employee {¢¢
  // ¢…¢
}
      \end{Code}
    \end{description}
  \end{description}
\end{frame}

\begin{frame}[fragile=singleslide]{Subranges in \Pascal}
  \begin{Code}{PASCAL}{\Pascal subrange definition}
TYPE MonthLength = 28..31;
  \end{Code}
  \begin{itemize}
    \item Type \cc{MonthLength} has four values:
          \cc{28}, \cc{29},\cc{30},\cc{31}.
    \item Values of make a subset of type \cc{Integer}.
    \item Any operation that expects an \cc{Integer} value will
          happily accept a value of type \cc{MonthLength}.
    \item Type \cc{MonthLength} ‟\emph{inherits}” all operations
          of type \cc{Integer}.
  \end{itemize}
  \begin{block}{‟Inheritance” in \Pascal}
    A \Pascal subrange type ‟inherits” all the operations
    of its parent type; otherwise, no \Pascal type inherits any operations from
    another distinct type.
  \end{block}
\end{frame}

\begin{frame}[fragile=singleslide]{Subtypes in \Pascal}
  \Pascal recognizes only one restricted kind of subtype:
  \emph{subranges} of discrete atomic types.
  \begin{Code}{PASCAL}{Subranges in \Pascal}
TYPE Natural = 0..MAXINT;
     Small = -3..+3;
VAR i: Integer;
     n: Natural;
     s: Small;
  \end{Code}
  \begin{description}[Unsafe]
    \item [Safe] \cc{i:=n} and \cc{i:=s}
    \item [Unsafe] \cc{n:=i}, \cc{s:=i}, \cc{n:=s} and \cc{s:=n}
    (require run-time range check)
  \end{description}
  A value may belong to several (possibly many) subtypes.
  Run time check is required to verify that a value belongs to a certain subtype.
\end{frame}

\begin{frame}[fragile=singleslide]{Subtypes in \Ada: builtin types}
  In contrast, \Ada allows subtypes of \emph{all} atomic types,
  as well as user-defined, composite types.
  \begin{Code}{ADA}{Discrete types in \Ada}
subtype Natural is Integer range 0..Integer'last;
subtype Small is Integer range -3..+3;
  \end{Code}

  \begin{Code}{ADA}{Indiscrete types in \Ada}
subtype Probability is Float range 0.0..1.0;
  \end{Code}
\end{frame}

\begin{frame}[fragile=singleslide]{Subtypes in \Ada: array types}
  \begin{Code}{ADA}{Strings in \Ada}
type String is array (Integer range <>) of Character;
subtype String5 is String (1..5);
subtype String7 is String (1..7);
  \end{Code}
\end{frame}

\begin{frame}[fragile=singleslide]{Subtypes in \Ada: user defined types}
  \begin{code}{ADA}
type Sex is (f, m);
type Person (gender : Sex) is
  record
    name : String (1..8);
    age : Integer range 0..120;
  end record;
subtype Female is Person (gender => f);
subtype Male is Person (gender => m);
  \end{code}
\end{frame}

\begin{frame}[fragile=singleslide]{Hypothetical \ML with structural subtyping}
  \begin{Code}{EMEL}{Some geometric types}
type point = {x: real, y: real};
type circle = {x: real, y: real, r: real};
type box = {x: real, y: real, w: real, d: real};
  \end{Code}
  Assuming inheritance relationship being derived from structure†{in most mainstream PLs, including \Java and \CC, \emph{structure is
    derived from inheritance relationship}}, we have \[
  \cc{box} ≺ \cc{circle} ≺ \cc{point}.
\]
Operations associated of \cc{point} should be applicable to \cc{box}, e,.g., \[
\cc{move}: σ⊆\cc{Point}∙σ ⨉ \cc{Real} ⨉ \cc{Real} → σ.
\]
\end{frame}

\begin{frame}{Non-type parametric polymorphism}
  \begin{block}{What we have seen so far is…}
    \begin{TWOCOLUMNS}
      \begin{description}
        \item [Entity] Type (parameterized)
        \item [Parameter] Type
        \item [Output] Type (concrete)
      \end{description}
      \MIDPOINT
      \begin{description}
        \item [Entity] Function (parameterized)
        \item [Parameter] Type
        \item [Output] Function (concrete)
      \end{description}
    \end{TWOCOLUMNS}
  \end{block}
  \centering
  \emph{How about other entities?†{An example was shown above}}
\end{frame}

\subunit{summary}

\begin{frame}[fragile=singleslide]{Varieties of polymorphism}
  \squeezeSpace
  \begin{description}[Ad Hoc]
    \item [Ad Hoc] \textbf{Created by hand; caters for a limited number of types}
    \begin{description}[Overloading]
      \item [Overloading] A single identifier denotes several functions is an
      ad hoc term simultaneously
      \begin{itemize}
        \item Reuse is limited to names, but there are is reusable \emph{code}
      \end{itemize}
      \item [Coercion] A single function can serve several types thanks to
      implicit coercion between types
      \begin{itemize}
        \item Extending the utility of a single function, using implicit conversions
      \end{itemize}
    \end{description}
    \item [Universal] \textbf{Systematic, applies to many types}
    \begin{description}
      \item [Parametric] Functions that operate \emph{uniformly} on values of
      different types
      \item [Inclusion] Subtypes inherit functions from their supertypes
    \end{description}
  \end{description}
\end{frame}

\afterLastFrame

\exercises
\begin{enumerate}
  \item What (if any) kind of polymorphism?
        \begin{enumerate}
          \item operator ‟\cc{>=}” in \Pascal?
          \item … and in~\CPL?
          \item … and in \CC?
          \item function \cc{succ} in \Pascal?
          \item function \cc{chr} in \Pascal?
          \item the \kk{array} type constructor in \Pascal?
          \item the \kk{for}…\kk{to} iterative command in \Pascal?
          \item procedure \cc{new} in \Pascal?
          \item … and procedure \cc{dispose}?
          \item operator ‟\cc{+}” in \Pascal?
          \item and in~\CPL?
          \item … and in \Java?
        \end{enumerate}
  \item What are all the types of…
        \begin{enumerate}
          \item \cc{true} in \Pascal?
          \item \cc{June} in \Pascal?
                \item~\cc{0} in \Pascal?
          \item … and in~\CPL?
          \item … and in \CC?
          \item ‟\cc{❴❵}” in \CC?
          \item ‟\cc{[]}” in \Pascal?
        \end{enumerate}
  \item Write a \Pascal program that while using ranges makes a type error?
  \item Is the type error flagged at compile time? At runtime? How?
  \item Write a \CC program in which an \kk{enum} variable is overflowed.
  \item Is the type error flagged at compile time? At runtime? How?
  \item Repeat the above two questions for \Pascal.
  \item \Pascal has this problem of programs not being able to read files.
        Explain this problem.
  \item With respect to this problem, why does it still make sense to allow
        functions such as \cc{eof} which can take many different types of files?
  \item Classify the polymorphism kind (if any) of all of \Pascal's
        predefined
        functions, constants, and procedures.
\end{enumerate}
\references
\begin{itemize}
  %
  \D{Ad hoc Polymorphism}{http://en.wikipedia.org/wiki/Ad\_hoc\_polymorphism}
  \D{Function Overloading}{http://en.wikipedia.org/wiki/Function\_overloading}
  \D{Operator Overloading}{http://en.wikipedia.org/wiki/Operator\_overloading}
  \D{Parametric Polymorphism}{http://en.wikipedia.org/wiki/Parametric\_polymorphism}
  \D{Polymorphism}{http://en.wikipedia.org/wiki/Polymorphism\_(computer\_science)}
  \D{Type Conversion}{http://en.wikipedia.org/wiki/Type\_conversion}
\end{itemize}
\endinput
\begin{frame}[fragile]{Subtyping as subclassing}
  Consider the class Collection that allows the following
  operations:
  \begin{PASCAL}
insert (what: Integer)
isMember (what: Integer) : boolean
remove (what: Integer) : boolean
write ()
  \end{PASCAL}
  A class \cc{Queue} that allows the same operations plus the
  following:
  \begin{PASCAL}
removeFirst() : Integer or Unit
  \end{PASCAL}
  would be a subclass of Collection.
\end{frame}

\tikzset{Vertical Record/.style={%
  CONSv=3,
  name=#1,
  append after command={%
    coordinate (#1 1st) at ($ (#1.north)!1/6!(#1.south)$)
    coordinate (#1 2nd) at ($ (#1.north)!5/6!(#1.south)$)
  }
}}

\tikzset{Horizontal Record/.style={%
  CONSh=3,
  name=#1,
  append after command={%
    coordinate (#1 1st) at ($ (#1.west)!1/6!(#1.east)$)
    coordinate (#1 2nd) at ($ (#1.west)!5/6!(#1.east)$)
    [draw,fill=red] (#1 1st) circle (2pt)
    [draw,fill=red] (#1 2nd) circle (2pt)
  },
}}

\def\verticalRecord=#1[#2]{%
  \node[Vertical Record = #1,#2] {};
  \DT{#1 1st}
  \DT{#1 2nd}
}

\def\horizontalRecord=#1[#2]{%
  \node[Horizontal Record=#1,#2] {};
  \DT{#1 1st}
  \DT{#1 2nd}
}

\tikzset{Scalar Variable/.style={var,name=#1,pattern=grid,pattern color=red}}
\tikzset{cell/.style={minimum width=0.9cm,minimum height=0.5cm,rectangle,thin}}

\tikzset{>= triangle 45}
\tikzset{shorten >=3pt}

\begin{frame}{Memory management}
  \begin{Figure}[Mindmap of memory management]
      \begin{adjustbox}{scale={1}{0.85},}{\input{memory-management.tikz}}
      \end{adjustbox}
    \end{Figure}
\end{frame}

\begin{frame}[fragile]{Reference counting}
  \squeezeSpace
  \small
  \renewcommand\baselinestretch{0.85}
\begin{description}[Maintenance]
    \item [Idea] a ‟reference count” (RC) field in every variable
    \item [Invariant] 
\begin{itemize}
      \item RC is the number of references to the variable.
      \item The RC of all live variable is positive 
\end{itemize}
    \item [Initially] In allocation such
    as†{No other allocation command makes sense}:
    \begin{JAVA}
Thingy t = new Thingy(); // ¢\Java¢ syntax
    \end{JAVA}
    Set RC of the newly created \cc{Thingy} to 1.
    \item \begin{Code}{JAVA}{Maintenance}
Object o1 = new Object();
// Denote the newly allocated object by ¢$O₁$¢;
// Set ¢$\cc{RC}(O₁) ← 1$¢;
Object o2 = new Object();
// Denote the newly allocated object by ¢$O₂$¢;
// Set ¢$\cc{RC}(O₂) ← 1$¢
¢⋮¢
Object o2 = o1 // ¢$\cc{RC}(O₁)++$¢; ¢$\cc{RC}(O₂)--$¢;
    \end{Code}
  \item [De-allocation] After each \emph{decrement}, if~$\cc{RC}(O) = 0$:
    \textit{(i)} de-allocate~$O$;
    \textit{(ii)} decrement RC for all children of~$O$; and,
    \textit{(iii)} recursively de-allocate objects whose \cc{RC=0}; 
\end{description}
\end{frame}

\begin{frame}{Pros ＆ cons of reference counting}
  \squeezeSpace
\begin{description}[Pros ]
    \item [Pros] 
\begin{itemize}
      \item predictable performance
      \item smooth execution without interruptions
      \item Implementable in
\begin{description}\small
              \item [Manual Memory Management System] via smart pointers, or even as part of the language semantics.
              \item [Automatic Memory Management System] as part of the garbage collection system. 
\end{description}
      \item cost is proportional to actual
            computation, not to memory size 
\end{itemize}
    \item [Cons] 
\begin{itemize}
      \item Cannot deal with circular structures
      \item Is generally slow, incurring a huge ‟write barrier”†{the amount of work that needs to be done in each memory write} 
\end{itemize}
\end{description}
  \WF{Write barrier}{%
    \emph{The formidable write barrier excludes the universal application of RC
  for memory management}}
\end{frame}

\begin{frame}{What is garbage collection?}
  \squeezeSpace
  \WD[0.8]{Mark ＆ sweep GC algorithm}{%
    Invented by John McCarthy around 1959 as an enabling
    technology for \Lisp implementation, \emph{Garbage Collection} (GC)
    is a part of the program semantics and runtime, which
  automatically claims back all unused memory. }
  In simple words, de-allocation becomes the responsibility of the PL's runtime
  system, rather than the programmer's.
\begin{itemize}
    \item Programmer never de-allocates memory
    \item When memory becomes scarce, a GC
          procedure is applied to collect all unused variables
    \item \emph{Mark ＆ sweep}: the simplest GC algorithm 
\end{itemize}
  \alert{\emph{found in \Java, \Smalltalk, \Python, \Lisp, \ML, \Haskell, and⏎
  most functional, or modern OO languages.}}
\end{frame}

\begin{frame}{Why garbage collection?}
  GC prevents
\begin{itemize}
    \item Dangling references
    \item Memory leak
    \item Heap corruption
    \item Heap de-fragmentation (with a compacting collector). 
\end{itemize}
  Also, GC makes first-class functions value possible.
\end{frame}

\begin{frame}[fragile,t]{Mark ＆ sweep garbage collection}
  \newcommand\Callout[2]{%
    \node[
      shape=ellipse callout,
      draw,
      fill=teal!20,
      callout absolute pointer=(#1),
      shorten >=12pt,
      xshift=10ex,
      yshift=10ex,
      font=\large\sf,
    ]
    at (#1)
    {#2};
  }
  \begin{adjustbox}{}
    \begin{tikzpicture}
      \tikzstyle{highlight}=[draw=red,ultra thick]
      \tikzstyle{highlightArrow}=[draw=red,thick]
      \newcommand\firstLevel{}
      \newcommand\firstLevelArrow{}
      \newcommand\secondLevel{}
      \newcommand\secondLevelArrow{}
      \newcommand\thirdLevel{}
      \newcommand\thirdLevelArrow{}
      \newcommand\fourthLevel{}
      \only<24->{\renewcommand\firstLevel{highlight}}
      \only<25->{\renewcommand\firstLevelArrow{highlightArrow}}
      \only<26->{\renewcommand\secondLevel{highlight}}
      \only<27->{\renewcommand\secondLevelArrow{highlightArrow}}
      \only<28->{\renewcommand\thirdLevel{highlight}}
      \only<29->{\renewcommand\thirdLevelArrow{highlightArrow}}
      \only<30->{\renewcommand\fourthLevel{highlight}}
      \uncover<1->{%
        \node(store) [
          draw,
          minimum width=20cm,
          minimum height=10cm,
          fill=yellow!30,
        ] at (0,0) {};
      }
      \uncover<2>{%
        \foreach \y in {-4,...,4}
        \foreach \x in {-9,...,9} {%
          \pgfmathparse{mod(int(\x*\x*\x*\x + \y *\y *\y *\y),100)}
          \let\myresult\pgfmathresult
          \node[var,yscale=0.8,fill=blue!\myresult!red!\myresult!white] at (\x,\y) {};
        }
      }
      \uncover<16,56>{%
        \node(heap) [
          draw,
          minimum width=18.5cm,
          minimum height=1.5cm,
          fill=blue!10,
          yshift=-3.5cm,
          label=below:{\bf\large \Blue{Heap Data Structure}},
        ] at (store) {};
      }
      \uncover<10-16,52->{%
        \begin{scope}[
            start chain=going left,
            node distance=.4cm,
            every node/.style={%
              on chain,
              join,
              fill=green!50!black,
              minimum width=2mm,
              minimum height=3.5mm,
              rectangle,
            },
            every join/.style={->,>= stealth,shorten >=1pt}
          ]
          \node[xshift=-4.ex,yshift=-0.25cm] at (heap.east) {};
          \node[minimum width=4mm]{};
          \node[minimum width=8mm]{};
          \node[minimum width=10mm](free){};
          \node[minimum width=2mm]{};
          \node[minimum width=14mm]{};
          \node[minimum width=7mm]{};
          \node[minimum width=20mm]{};
          \visible<55>{%
            \node[minimum width=2mm]{};
            \node[minimum width=2mm]{};
            \node[minimum width=2mm]{};
            \node[minimum width=2mm]{};
          }
        \end{scope}
        \uncover<14-16,53->{%
          \node[above=1mm of free,text=green!50!black]{\bf\large List of Free Blocks};
        }
      }
      \uncover<11-13,15-16,33->{%
        \begin{scope}[
            start chain,
            node distance=.4cm,
            every node/.style={%
              on chain,
              join,
              fill=blue!50,
              minimum width=5mm,
              circle
            },
            every join/.style={<-,>= stealth,shorten <=1pt}
          ]
          % \uncover<1-52,56->{%
          \node[xshift=4.ex,yshift=0.25cm] at (heap.west) (a1) {};
          \node (a2) {};
          % }
          \node (a3) {};
          \node (a4) {};
          \node (a5) {};
          \node (a6) {};
          \uncover<1-52,56->{%
            % \invisible<53-55>{%
            \node (a7) {};
            \node (a8) {};
          }
          \node (a9) {};
          \node (a10) {};
          \node (a11) {};
          \node (a12) {};
        \end{scope}
      }
      \uncover<53->{\draw[->,>= stealth,shorten <= 1pt](a9)--(a6);}
      \begin{scope}[every node/.style={shape=circle,minimum width=5mm,fill=teal!50!}]
        \invisible<53-55>{%
          \uncover<50->{\node[GREEN] at (a1) {};}
          \uncover<49>{\node at (a1) {};}
          \uncover<48->{\node[GREEN] at (a2) {};}
          \uncover<47>{\node at (a2) {};}
        }
        \uncover<46>{\node at (a3) {};}
        \uncover<45>{\node at (a4) {};}
        \uncover<44>{\node at (a5) {};}
        \uncover<43>{\node at (a6) {};}
        \invisible<53-55>{%
          \uncover<42->{\node[GREEN] at (a7) {};}
          \uncover<41>{\node at (a7) {};}
          \uncover<40->{\node[GREEN] at (a8) {};}
          \uncover<39>{\node at (a8) {};}
        }
        \uncover<38>{\node at (a9) {};}
        \uncover<37>{\node at (a10) {};}
        \uncover<36>{\node at (a11) {};}
        \uncover<35>{\node at (a12) {};}
      \end{scope}
      \uncover<12-13,15-16,33->{%
        \invisible<53-55>{%
          \DT{a1}
          \DT{a2}
        }
        \DT{a3}
        \DT{a4}
        \DT{a5}
        \DT{a6}
        \invisible<53-55>{%
          \DT{a7}
          \DT{a8}
        }
        \DT{a9}
        \DT{a10}
        \DT{a11}
        \DT{a12}
      }
      \uncover<15-16,33->{%
        \node[below=1mm of a3]{\bf\large\Blue{List of Allocated Cells}};
      }
      \uncover<20-30,56>{\node(root) [
          draw,
          minimum width=16cm,
          minimum height=2.5cm,
          fill=red!10,
          xshift=0,
          yshift=2.7cm,
          label=above:{\Large\bf\Red{Root Set}}
        ] at (store) {};
      }
      \node(stack) [
        yshift=-0.2cm,
        xshift=3.5cm,
      ]{};
      \begin{scope}[% The Stack
          start chain=going left,
          node distance=0mm,
          every node/.style={on chain},
        ]
        \uncover<5,6,7,8,9,17-34,56>{%
          \node[Scalar Variable=scalar 1,xshift=-3ex] at (root.east) {};
        }
        \uncover<3,6,7,8,9,17-34,56>{%
          \node[var,\firstLevel] (stack 1){};\DT{stack 1}
        }
        \uncover<5,6,7,8,9,17-34,56>{%
          \node[Scalar Variable=scalar 2]{};
        }
        \uncover<7,8,9,22-30,56>{%
          \node[var] (dots 1){$·⋯·$};
        }
        \uncover<3,5,6,7,8,9,17-34,56>{%
          \node[Scalar Variable=scalar 3] {};
        }
        \uncover<6,7,8,9,17-34,56>{%
          \node[var,\firstLevel] (stack 2){};\DT{stack 2}
        }
        \uncover<3,5,6,7,8,9,17-34,56>{%
          \node[Scalar Variable=scalar 4]{};
        }
        \uncover<7,8,9,22-30,56>{%
          \node[fill=red!30,inner sep=0,minimum height=0.5cm] (dots 2){$·⋯⇐$};
        }
      \end{scope}
      \uncover<7,8,9,22-30,56>{%
        \node[above=0 of dots 1,text=red,font=\bf\Large] {Runtime Stack};
      }
      % Global Variables
      \uncover<3,4,5,6,8,9,17-34,56>{%
        \node[Scalar Variable=a] at ($ (root.west)!.3!(root.east)$) {};
      }
      \uncover<3,5,6,8,9,17-34,56>{%
        \node[Scalar Variable=b,yshift=-2] at ($ (a)!.5!(root.west)$) {};
      }
      \uncover<3,6,8,9,17-34,56>{%
        \node(c) [var,yshift=-6,\firstLevel] at (root.center) {};\DT{c}
        \node(d) [var,yshift=-3,\firstLevel] at ($ (a)!.5!(c)$) {}; \DT{d}
        \node(e) [var,yshift=-6,\firstLevel] at ($ (b)!.5!(a)$) {}; \DT{e}
        \node(f) [var,yshift=-6,\firstLevel] at ($ (b)!.5!(root.west)$) {}; \DT{f};
      }
      \uncover<8,9,21-30,56>{%
        \node(global) [
          draw,
          label=above:{\Red{\large\bf Global Variables}},
          color=red,
          cloud,
          cloud puffs=30,
          cloud puff arc=120,
          inner sep=0,
          minimum width=9.5cm,
          minimum height=1.6cm,
          yshift=-6,
          xshift=4
        ] at (a) {};
      }
      % Heap Variables
      \uncover<3,6,9,13,17-34,35,56>{%
        \horizontalRecord=v1[below=10ex of stack 2,\secondLevel]
      }
      \uncover<3,4,6,9,13,17-34,36,56>{%
        \horizontalRecord=v2[below=4ex of v1 1st,\thirdLevel]
      }
      \uncover<3,6,9,13,17-34,38,56>{%
        \verticalRecord=v3[below=18ex of c,\secondLevel]
      }
      \uncover<3,6,9,13,17-34,37,56>{%
        \verticalRecord=v4[right=of v3,\thirdLevel]
      }
      \uncover<3,4,6,9,13,17-34,41-53,56>{%
        \verticalRecord=x1[below=16ex of d,onslide={<42-> GREEN}]
      }
      \uncover<3,4,6,9,13,17-34,39-53,56>{%
        \node[Horizontal Record=x2,
          below=14ex of global.east,
          pattern=grid,
          pattern color=blue,
          onslide={<40->GREEN},
        ]{};
      }
      \uncover<3,4,6,9,13,17-34,46,56>{%
        \horizontalRecord=c1[below=12ex of e,\secondLevel]
      }
      \uncover<3,4,6,9,13,17-34,43,56>{%
        \verticalRecord=c2[below=15ex of e,xshift=8ex,\thirdLevel]
      }
      \uncover<3,4,6,9,13,17-34,45,56>{%
        \verticalRecord=c4[below=15ex of e,xshift=-8ex,\thirdLevel]
      }
      \uncover<3,4,6,9,13,17-34,49-53,56>{%
        \horizontalRecord=x3[below=14ex of global.west,xshift=-3ex,
          onslide={<50->GREEN},
        ]
      }
      \uncover<3,4,6,9,13,17-34,47-53,56>{%
        \horizontalRecord=x4[below=of x3,onslide={<48->GREEN}]
      }
      \uncover<3,4,6,9,13,17-34,44,56>{%
        \horizontalRecord=c3[below=8ex of c1,\fourthLevel]
      }
      % Variables call-outs
      \uncover<4>{%
        \Callout{a}{Hey, I am a Variable!}
        \Callout{c4}{Hi! Me too!}
        \Callout{v2}{Me 3!!!} (a2)
      }
      \uncover<6>{%
        \Callout{x3 2nd}{Notation for references};
      }
      % Null pointers
      \uncover<18,25-30,56>{%
        \path let \p1 = (v1.one), \p2 = (root.east) in node[groundRight=g1,yshift=2] at (\x2,\y1){};
        \node[groundRight=g2,below=4ex of g1,yshift=4pt] {};
        \path let \p1 = (v4.one), \p2 = (g2) in node[groundRight=g3,yshift=2] at (\x2,\y1){};
        \node[groundRight=g4, below=4ex of g3,yshift=4pt]{};
        \node[groundDown=g5,below=12ex of d,yshift=4pt]{};
        \node[groundLeft=G1,left=12ex of f.west] {};
        \node[groundRight=G2,below=31 ex of G1,xshift=6ex] {};
        \draw[->](f)->(G1);
        \draw[->](v1 2nd)->(g1.input);
        \VHPTR{stack 1}{g1.input}
        \draw[->](v2 2nd)->(g2.input);
        \VHPTR{v2 1st}{g3}
        \draw[->](v4 1st)->(g3);
        \draw[->](v4 2nd)->(g4);
        \draw[->](d.center)->(g5.input);
        \draw[->] (x4 1st) |- (G2);
      }
      \uncover<13,32->{%
        \uncover<13,49-52,56>{%
          \draw[->,bend left= 45] (a1) to (x3.west);
        }
        \uncover<13,47-52,56>{%
          \draw[->,bend right= 60] (a2) to (x4.east);
        }
        \uncover<13,46,56>{%
          \draw[->,bend left=120] (a3)..controls(f) and ($ (c1.north)!0.55!(f)$)..(c1.north west);
        }
        \uncover<13,45,56>{%
          \draw[->,bend right=30] (a4)to(c4.south);
        }
        \uncover<13,44,56>{%
          \draw[->,bend right=20] (a5)to(c3.south);
        }
        \uncover<13,43,56>{%
          \draw[->,bend right=20] (a6)to(c2.south);
        }
        \uncover<13,41-52,56>{%
          \draw[->,bend right=20] (a7)to(x1.south);
        }
        \uncover<13,39-52,56>{%
          \draw[->](a8)..controls ($ (x1.south east)!0.5!(v3.south west)$)
          and ($ (x1.north east)!0.4!(v3.north west)$)..(x2.west);
        }
        \uncover<13,38,56>{%
          \draw[->] (a9) to (v3.west);
        }
        \uncover<13,37,56>{%
          \draw[->] (a10) to (v4.west);
        }
        \uncover<13,36,56>{%
          \draw[->] (a11)..controls(g2) and (g1)..(v2.east);
        }
        \uncover<13,35,56>{%
          \draw[->] (a12)..controls(g4) and ($ (store.north east)!0.8!(g1)$)..(v1.east);
        }
      }
      % Intra-variables references
      \uncover<17-30,56>{%
        \draw[->,\firstLevelArrow](stack 2)->(v1);
        \draw[->,\firstLevelArrow](c)->(v3.north);
        \draw[->,\firstLevelArrow](e)->(c1.north);
        %
        \draw[->,\secondLevelArrow](v1 1st)->(v2.north);
        \draw[->,\secondLevelArrow](v3 1st) to (v4 1st);
        \draw[->,\secondLevelArrow](v3 2nd) to (v4 2nd);
        \draw[->,\secondLevelArrow,bend left=30](c1 2nd) to (c2.north);
        %
        \draw[->,\secondLevelArrow,bend left=30](c1 1st) to (c4.east);
        \draw[->,\thirdLevelArrow,bend left=30](c2 2nd) to (c3.east);
        \draw[->,\thirdLevelArrow,bend left=30](c4 2nd) to (c3.north);
        \draw[->,\thirdLevelArrow,bend left=30](c3 1st) to (c4.south);
        %
        \draw[->,bend left=30](c2 1st) to (c1.south);
        \draw[->,bend left=30](c3 2nd) to (c2.west);
        \draw[->,bend left=30](c4 1st) to (c1.west);
        \draw[->,bend right=30](x3 1st) to (x4.west);
        \draw[->,bend right=30](x4 2nd) to (x3.east);
        \draw[->] (x3 2nd) to (f.south);
      }
    \end{tikzpicture}
  \end{adjustbox}
  \alert{\large
    \only<1-3>{This is our storage bank,~}%
    \only<1>{…}%
    \only<2>{\Red{which contains many ‟cells”.}}%
    \only<3>{which contains many ‟cells”.
      \Red{Our interest, though, lies only with ‟allocated” cells.}%
    }%
    \only<4>{An allocated cell is called a \emph{variable}.}%
    \only<5-6>{Some variables follow \emph{value semantics};~}%
    \only<5>{…}%
    \only<6>{\Red{others contain \emph{references}}.}%
    \only<7-9>{Some belong in the \emph{runtime stack},~}%
    \only<7>{…}%
    \only<8>{\Red{others are ‟\emph{global}”}}%
    \only<8>{…}%
    \only<9>{others are ‟\emph{global}”;~}%
    \only<9>{\Blue{the rest are \emph{heap allocated}}.}%
    \only<10>{The heap is primarily a \emph{list of free blocks}!}%
    \only<11-13>{But, the heap also maintains another list,~}%
    \only<11>{…}%
    \only<12-13>{which keeps references~}%
    \only<12>{…}%
    \only<13>{to all \emph{heap allocated} cells!}%
    \only<14-16>{So, we have a \emph{list of free blocks} and~}%
    \only<14>{…}%
    \only<15>{\Red{a \emph{list of allocated cells}.~}}%
    \only<16>{a \emph{list of allocated cells}. Together, the two lists make the \emph{Heap Data Structure}}%
    \only<17-19>{Now, our variables reference each other,~}%
    \only<17>{…}%
    \only<18-19>{and have many null references,~}%
    \only<18>{…}%
    \only<19>{\Red{which we will \emph{not always} show.}}%%
    \only<20-22>{For \emph{garbage collection},
      we define the \emph{Root Set},~}%
    \only<20>{…}%
    \only<21-22>{which contains global variables, and~}%
    \only<21>{…}%
    \only<21>{the runtime stack.}%
    \only<23>{Staring from the root set, we conduct a \emph{mark} phase.}%
    \only<24-26>{We first \Red{mark} all variables in the root set,~}%
    \only<24>{…}%
    \only<25-26>{and then follow their references,~}%
    \only<25>{…}%
    \only<26>{to \Red{mark} variables referenced from the root set.}%
    \only<27-28>{Again, we follow references,~}%
    \only<27>{…}%
    \only<28>{to \Red{mark} variables two references away from the root.}%
    \only<29-30>{We keep following references,~}%
    \only<29>{…}%
    \only<30>{until we \Red{mark} \emph{all} variables reachable from the root set.}%
    \only<31-32>{Consider now on the entire set of variables.
    Which variables are garbage?~}%
    \only<32>{In the \GREEN{sweep} phase, we collect
      all variable blocks which are \GREEN{not} \Red{marked}.}%
    \only<33>{Recall the ‟List of Allocated Cells”?}%
    \only<34>{Let's iterate over it!}%
    \only<35-51>{Start at the first node,~}%
    \only<36-51>{\normalsize and iterate,~}%
    \only<37-51>{\normalsize and iterate,~}%
    \only<38-51>{\normalsize and iterate,~}%
    \only<39>{\normalsize and iterate,~}% (a8)(x2)
    \only<40-51>{\normalsize \GREEN{1\textsuperscript{\emph{st}} cell for recycling},~}% (a8) (x2)
    \only<41>{\normalsize and iterate,~}% (a7)(x1)
    \only<42-51>{\normalsize \GREEN{2\textsuperscript{\emph{2nd}} cell for recycling},~}%(a7) (x1)
    \only<43>{\normalsize and iterate,~}% (c2)
    \only<44-51>{\normalsize and iterate,~}% (c3)
    \only<45-51>{\normalsize and iterate,~}% (c4)
    \only<46-51>{\normalsize and iterate,~}% (c1)
    \only<47>{\normalsize and iterate,~}% (a2) (x4)
    \only<48-51>{\normalsize \GREEN{3\textsuperscript{\emph{rd}} cell for recycling},~}% (a2) (x4)
    \only<49>{\normalsize and iterate,~}% (a1) (x3)
    \only<50-51>{\normalsize \GREEN{4\textsuperscript{\emph{th}} cell for recycling},~}% (a1) (x3)
    \only<51-51>{\normalsize until we are done!}%
    \only<52-55>{\normalsize All that remains is,~}%
    \only<52>{\normalsize …}%
    \only<53-55>{\normalsize to remove the cells destined for recycling from
      the ‟\emph{\Blue{List of Allocated Cells}}”,~}%
    \only<53>{\normalsize …}%
    \only<54-55>{\normalsize to claim back the memory they occupy,~}%
    \only<54>{\normalsize …}%
    \only<55>{\normalsize and to add these memory blocks to the back to the ‟\emph{\GREEN{List of Free Cells}}”.}%
    \only<56>{This diagram depicts the main points}
  }
\end{frame}

\begin{frame}{Summary: mark ＆ sweep garbage collection}
\begin{description}[Release]
    \item [Mark] mark all cells as unused
    \item [Sweep] unmark all cells in use (stack, global variables),
    and cells which can be accessed, directly or indirectly,
    from these
    \item [Release] all cells which remain marked 
\end{description}
\end{frame}

\begin{frame}{Delicate issues of the marking process}
\begin{itemize}
    \item Do not visit an object more than once
    \item Do not get stuck in a loop.
    \item Typical implementations:
\begin{itemize}
            \item Breadth-first search
            \item Depth-first search 
\end{itemize}
    \item Marking:
\begin{itemize}
            \item Can be done by ‟raising” a bit in each object
            \item More efficient procedure:
\begin{itemize}
                    \item Initially, all objects are ‟0”
                    \item In first collection, marking is by changing the bit to ‟1”
                    \item In second collection, marking is by changing the bit to ‟0”
                    \item In third collection, marking is by changing the bit to ‟1”
                          \item~$⋮$ 
\end{itemize}
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Stop ＆ copy garbage collection}
\begin{itemize}
    \item Divide the heap into two regions:
\begin{description}[Region II]
            \item [Region I] takes all allocations
            \item [Region II] is put on hold 
\end{description}
    \item When region I is exhausted, copy live
          (reachable) variables to region II
    \item Switch the roles of the two regions 
\end{itemize}
\end{frame}

\begin{frame}{Defragmentation}
\begin{itemize}
    \item Can be done whenever the GC detects \alert{memory fragmentation}:
\begin{itemize}
            \item Lots of memory is available
            \item All memory is fragmented into small consecutive chunks.
            \item Program requires a large consecutive memory, e.g., for
                array. 
\end{itemize}
    \item Can be done in each collection cycle:
\begin{itemize}
            \item Presumably slower
            \item Often performs better due to caching and ‟locality
                  of reference”. 
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Predicaments of garbage collection}
\begin{description}[▄]
    \item [Memory/Time Resources] \emph{could be saved using programmers' knowledge.}
    \item [Decreased Performance] \emph{of the[Real]core program}
    \item [Uneven Performance] \emph{with ‟embarrassing pauses” for GC cycles}
    \item [Unpredictable Performance] \emph{the program can never know when a GC cycle may start}
    \item [Not for Real Time] \emph{which requires predictable performance}
    \item [Not for Transactions] \emph{a transaction may time out with no good reason}
    \item [Hinder Interactiveness] \emph{pauses can lead to user abandonment}
    \item [Incompatible with ‟Resource Allocation Is Initialization”] \emph{cannot rely
    on the destructor of a file object to close the file} 
\end{description}
\end{frame}

\begin{frame}{Some responses to these predicaments}
\begin{description}[Generational GC]
    \item [Generational GC] collects variables at the nursery first, where mortality is high
    \item [Incremental GC] Can perform some computation and resume it later.
    \item [Concurrent GC] Can run concurrently to the program.
    \item [Realtime GC] Obeys time constraints 
\end{description}
  Concurrency, predictability, etc., always
  incur a performance toll.
\end{frame}

\begin{frame}[fragile]{Memory leak in garbage collection?}
  \squeezeSpace
\begin{itemize}
    \item GC can only claim reachable variables
    \item If a programmer forgets to nullify references,
          then a pseudo memory leak may occur 
\end{itemize}
  \begin{TWOCOLUMNS}
    Define a class \texttt{Leak}
    \begin{code}{JAVA}
public class Leak {¢¢
  ¢…¢
}
    \end{code}
    whose contents is:
    \begin{code}{JAVA}
private Leak next;
private int[] data;
¢¢
private Leak(Leak next) {¢¢
  this.next = next;
  this.data = new int[1<<25];
}
¢¢
private static Leak cons(Leak l) {¢¢
  return new Leak(l);
}
    \end{code}
    \MIDPOINT
    \begin{code}{JAVA}
public static void
  main(String[] args) {¢¢
    Leak l = new Leak(null);
    final Runtime r = Runtime.getRuntime();
    for (int i = 0; i < 100; ++i) {¢¢
      System.out.println(
          i + ": " +
          r.freeMemory());
    l = cons(l);
  }
}
    \end{code}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile]{Output of the above program}
  \begin{session}
% javac Leak.java ; java Leak
0: 123021432
1: 123086952
2: 123152472
3: 123217992
4: 123283512
5: 123349032
6: 123414552
7: 123480072
8: 123545592
9: 70526952
Exception in thread "main" java.lang.OutOfMemoryError: Java heap space
  at Leak.<init>(Leak.java:16)
  at Leak.cons(Leak.java:12)
  at Leak.main(Leak.java:8)
  \end{session}
\end{frame}

\begin{frame}[fragile]{Semantical memory leak†{%
    Note that the previous example exhausted memory for the sake
    of demonstration; it did not really create semantic garbage.
  }}
  \squeezeSpace[6]
  \WD[0.75]{Semantical garbage}{%
    A variable which the program will never use again,
    but still keeps a reference to it, is called \emph{semantic garbage}.}
  \begin{TWOCOLUMNS}
    \begin{code}{JAVA}
class Huge {¢¢
  Huge() {¢¢ // Constructor:
    // Allocates lots of data and stores
    // it in the newly created object
  }
}
    \end{code}
    \MIDPOINT
    \begin{code}{JAVA}
void f() {¢¢
  Huge semanticGarbage = new Huge();
  heavy.computation(new Indeed(100));
  System.exit(1);
}
    \end{code}
  \end{TWOCOLUMNS}
  \begin{Block}[minipage,width=0.8\columnwidth]{The semantic garbage predicament}
  All sophisticated GC algorithms contend in vain against semantic garbage
  \end{Block}
\end{frame}

\begin{frame}[fragile]{GC ＆ the stack: escape analysis}
\begin{itemize}
    \item GC is always slower than stack-based memory management.
    \item In a pure GC, there are no automatic variables.
    \item In \Java, variables can be:
\begin{description}[Stack allocated]\small
            \item [Stack allocated] primitive types: \kk{int}, \kk{double}, \kk{boolean} etc.
              \begin{quote}
                No references to primitive types in \Java.
              \end{quote}
            \item [Stack allocated] References to classes and arrays.
              \begin{quote}
                No references to primitive types in \Java.
              \end{quote}
            \item [Heap allocated] Classes and arrays (accessed only by references) 
\end{description}
\end{itemize}
  \begin{TWOCOLUMNS}[0.09\columnwidth]
    \begin{Code}{JAVA}
  {Seemingly Innocent Program}
void foo() {¢¢
  int a[] = new int[1 << 20];
  List<Integer> b = new ArrayList<Integer>();
  ¢⋮¢ // does ¢a¢ gets assigned to global variables?
}
for (int i = 0; i < 1<<20; i++)
  f(); // Lots of GC activity
    \end{Code}
    \MIDPOINT
    \alert{With \emph{escape analysis} a smart compiler can determine that
      variables~\cc{a} and~\cc{b} never ‟escape” function \cc{foo()}, and then can be
    safely claimed when this function terminates.}
  \end{TWOCOLUMNS}
\end{frame}

\afterLastFrame
\references
\begin{itemize}
  %
  \D{Copying Collector}{http://en.wikipedia.org/wiki/Garbage\_collection\_(computer\_science)＃Copying\_vs.\_mark-and-sweep\_vs.\_mark-and-don.27t-sweep}
  \D{Dangling Reference}{http://en.wikipedia.org/wiki/Dangling\_reference}
  \D{Garbage Collection}{http://en.wikipedia.org/wiki/Garbage\_collection\_(computer\_science)}
  \D{Handle}{https://en.wikipedia.org/wiki/Handle\_(computing)}
  \D{Heap Corruption}{http://www.efnetcpp.org/wiki/Heap\_Corruption}
  \D{Heap}{https://en.wikipedia.org/wiki/Heap\_(programming)}
  \D{Mark and Sweep}{http://en.wikipedia.org/wiki/Mark\_and\_sweep＃Na.C3.AFve\_mark-and-sweep}
  \D{Memory Compaction}{http://en.wikibooks.org/wiki/Memory\_Management/Memory\_Compacting}
  \D{Memory Leak}{http://en.wikipedia.org/wiki/Memory\_leak}
  \D{Memory Management}{http://en.wikipedia.org/wiki/Memory\_management}
  \D{Memory Safety}{https://en.wikipedia.org/wiki/Memory\_safety}
  \D{Reference Counting}{http://en.wikipedia.org/wiki/Reference\_counting}
  \D{Stack-based Memory Allocation}{http://en.wikipedia.org/wiki/Stack-based\_memory\_allocation}
  \D{Static Local variables}{http://en.wikipedia.org/wiki/Local\_variable＃Static\_local\_variables}
  \D{Unreachable Variables}{https://en.wikipedia.org/wiki/Unreachable\_memory} 
\end{itemize}

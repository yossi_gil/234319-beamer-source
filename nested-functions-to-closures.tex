\subunit{Functions as first class values?}

\begin{frame}{Functions are supposed to be values}
  \begin{itemize}
    \item In discussing \emph{type constructors} we introduced the
        \alert{mapping type constructor}, e.g., in \ML
          we have the type \cc{int->int}.
    \item Values of type mapping, can be realized as
          \begin{itemize}
            \item Arrays
            \item Functions
          \end{itemize}
    \item It is often the case that functions are not ‟pure” mappings.
          \begin{itemize}
            \item In the imperative paradigm, functions may have side effects
            \item Even in the functional paradigm, functions may
                  depend on values which are \emph{not} parameters
          \end{itemize}
    \item Do function values behave just like other values?
  \end{itemize}
\end{frame}

\begin{frame}{Discrimination against function values}
  In \Pascal
  \begin{itemize}
    \item Can define variables of almost any type, \Red{but not of type function (\textbf{Discrimination})}.
    \item Functions can take function values as parameters, but \Red{functions cannot return function values (\textbf{Discrimination})}.
  \end{itemize}
\end{frame}

\begin{frame}{Function values in~\CPL}
  In~\CPL, function values are realized as ‟\alert{function pointers}”
  \begin{itemize}
    \item Can be stored in variables
    \item Can be stored in arrays
    \item Can be fields of structures
    \item Can be passed as parameters
    \item Can be returned from functions
  \end{itemize}
  But,~\CPL
  \begin{itemize}
    \item \Red{does not provide means for creating function values at runtime (\textbf{Discrimination}!)}
    \item \Red{does not allow nested functions (\textbf{Discrimination}!)}
  \end{itemize}
\end{frame}

\begin{frame}[fragile,label=current]{Nested functions in Gnu-\CPL}
  \squeezeSpace
  Gnu-\CPL is a~\CPL dialect, supporting nested functions
  \begin{code}[watermark text=Gnu-\CPL]{CEEPL}
int isRightTriangle(double a, double b, double c) {¢¢
  double sqr(double x) {¢¢
    return x * x;
  }
  return sqr(a) + sqr(b) == sqr(c);
}
  \end{code}
  \begin{itemize}
    \item Function \cc{sqr} is \emph{nested} in function \cc{isRightTriangle}
    \item Currently, \cc{sqr} is inaccessible outside \cc{isRightTriangle}
    \item Function \cc{sqr} may ‟escape” the boundaries of \cc{isRightTriangle}
          (e.g., by saving \cc{＆sqr} in a global variable).
    \item Closures are all about escapes from the nest.
  \end{itemize}
  \Red{\textbf{Discrimination:} in Gnu-\CPL, programmer is on his own when taking the address of nested functions!}
\end{frame}

\begin{frame}{Topics in this units}
  \begin{itemize}
    \item Nested functions
    \item Name, entity, binding
    \item Scope and environment
    \item Lexical scoping/dynamic scoping
    \item Closures
  \end{itemize}
\end{frame}

\subunit[nested-functions]{Nested functions}

\begin{frame}[fragile=singleslide,label=current]{The nest is accessible to nested functions}
  \squeezeSpace[3.5]
  {\large\Red{Why use nested function?}}
  \begin{itemize}
    \item Nested functions can access variables and definitions of nest
    \item Saves lots of argument passing
  \end{itemize}
  \Red{An implementation of the quick sort algorithm with nested functions (nesting diagram):}
  \begin{CENTER}
    \begin{Figure}[Function nesting structure for an implementation of the quick sort algorithm]
      \begin{adjustbox}{H=0.59}
        \input{qsort-nesting.tikz}
      \end{adjustbox}
    \end{Figure}
  \end{CENTER}
\end{frame}

\begin{frame}[fragile=singleslide,label=current]{‟Inheritance” of environment with nested functions}
  \begin{TWOCOLUMNSc}[-4ex]
    \begin{adjustbox}{H=0.59}
      \input{qsort-nesting.tikz}
    \end{adjustbox}
    \MIDPOINT
    \begin{Figure}[Nesting tree of the \protect\ccn{qsort} example]
      \begin{adjustbox}{}
        \input{qsort-nesting-tree.tikz}
      \end{adjustbox}
    \end{Figure}
    \begin{code}[watermark text=Gnu-\CPL,minipage,width=37ex]{CEEPL}
void sort(int a[], int n) {¢¢
  void swap(int i, int j) {¢¢
    // ¢\textrm‟¢inherits¢\textrm”¢, and uses ¢\cc{a}¢
    // ¢\textrm‟¢inherits¢\textrm”¢, without using, ¢\cc{n}¢
    const int t = a[i];
    a[i] = a[j];
    a[j] = t;
  }
  ¢…¢
}
    \end{code}
  \end{TWOCOLUMNSc}
\end{frame}

\begin{frame}[fragile=singleslide,label=current]{‟Inheritance” of environment from function \protect\cc{sort}}
  \begin{TWOCOLUMNSc}[-2ex]
    \begin{code}[watermark text=Gnu-\CPL,minipage,width=37ex]{CEEPL}
void sort(int a[], int n) {¢¢
  void qsort(int from, int to) {¢¢
    int pivot() {¢$⋯$¢}
    if (from == to)
      return;
    const int p = pivot();
    qsort(from, p); // ¢\textrm\itshape \nth{1}¢ recursive call
    qsort(p, to); // ¢\textrm\itshape \nth{2}¢ recursive call
  }
  qsort(0, n);
}
    \end{code}
    \MIDPOINT
    \begin{Figure}
      \begin{adjustbox}{W=0.95}
        \input{sort-inheritance-nesting-tree.tikz}
      \end{adjustbox}
    \end{Figure}
  \end{TWOCOLUMNSc}
  \begin{itemize}
    \item At run time, there might be several instances of the recursive function \cc{qsort};
    \item All these instances ‟inherit” array~\cc{a} and integer~\cc{n} of function \cc{sort}
  \end{itemize}
\end{frame}

\begin{frame}[fragile,label=current]{Environment ‟inherited” by function \protect\cc{pivot}}
  \squeezeSpace[10]
  \begin{TWOCOLUMNSc}[-18ex]
    \begin{code}[watermark text=Gnu-\CPL,minipage,width=36ex]{CEEPL}
int pivot() {¢¢
   // ¢\textrm‟¢inherits¢\textrm”¢
   // ¢\qquad¢ from ¢\cc{sort}¢: ¢\cc{a}¢,
   // ¢\qquad¢ from ¢\cc{qsort}¢: ¢\cc{from}¢, ¢\cc{to}¢
  int last = to - 1;
  int pivot = a[first];
  int split = from;
  swap(split, last);
  for (int i = first; i < last; i++)
    if (a[i] < pivot)
      swap(split++, i);
  swap(split, last);
  return split;
}
    \end{code}
    \MIDPOINT
    \begin{quote}
      \begin{Figure}
        \begin{adjustbox}{}
          \input{pivot-inheritance-nesting-tree.tikz}
        \end{adjustbox}
      \end{Figure}
    \end{quote}
  \end{TWOCOLUMNSc}
  \squeezeSpace[5]
  \begin{itemize}
    \item When \cc{pivot} executes, there is
          \begin{itemize}
            \item a single instance of function \cc{sort}
            \item one or more instances of the recursive function \cc{qsort}
          \end{itemize}
    \item Function \cc{pivot} inherits
          \begin{itemize}
            \item integers \cc{from} and \cc{to} from the \alert{correct} instance of \cc{qsort}
            \item integer array~\cc{a} from the \alert{single} instance of \cc{sort}
          \end{itemize}
  \end{itemize}
\end{frame}

\subunit{The environment}
\begin{frame}{Reminder: CPU ＆ memory in the classical model}
  \def\showCPU{}
  \undef\short{}
  \centering
  \begin{CENTER}[0.95\columnwidth]
    \begin{Figure}
      \begin{adjustbox}{}
        \input{classical-memory-map.tikz}
      \end{adjustbox}
    \end{Figure}
  \end{CENTER}
\end{frame}

\begin{frame}[fragile]{Understanding the machine stack}
  \begin{TWOCOLUMNS}
    \begin{code}{CEEPL}
int gcd(int m, int n) {¢¢
  if (m == n)
    return m;
  int m1 = m;
  int n1 = n;
  if (m > n)
    m1 = m % n;
  else
    n1 = n % m;
  return gcd(m1, n1);
}
    \end{code}
    \MIDPOINT
    How does this function‟\emph{exist}” at runtime?
    \setbeamercovered{transparent}
    \begin{description}[Per activation]
      \item [Common] sequence of bytes, representing machine code
      \item [Per activation] the ‟\emph{environment}”:
      \begin{itemize}
        \item Parameters
        \item Local variables
      \end{itemize}
    \end{description}
  \end{TWOCOLUMNS}
  \onslide<+->
\end{frame}

\begin{frame}[fragile]{A stack frame of function \protect\cc{gcd}}
  \begin{TWOCOLUMNSc}[-10ex]
    \begin{code}{JAVA}
int gcd(int m, int n) {¢¢
  if (m == n)
    return m;
  int m1 = m;
  int n1 = n;
  if (m > n)
    m1 = m % n;
  else
    n1 = n % m;
  return gcd(m1, n1);
}
    \end{code}
    \MIDPOINT
    \begin{Figure}[Activation record (implemented as stack frame) for function \protect\ccn{gcd}]
      \begin{adjustbox}{}
        \input{activation-record.tikz}
      \end{adjustbox}
    \end{Figure}
  \end{TWOCOLUMNSc}
\end{frame}

\begin{frame}[fragile,label=current]{Structure of an activation record}
  \squeezeSpace
  \begin{table}[H]
    \mode<presentation>{\scriptsize}
    \begin{adjustbox}{}
      \coloredTable
      \begin{tabular}{*2l p{0.42\columnwidth}c}
        \toprule
        \textbf{Region} & \textbf{Managed by} & \textbf{Content} & \textbf{Fixed Size}⏎
        \midrule
        Environment\footnotemark & Caller & \multicolumn1c? & ✓⏎
        Parameters & Caller & Return variable, actual & ✓\footnotemark⏎
        Saved state & Architecture & Saved registers, saved PC, saved SP & ✓⏎
        Local state & Callee & Automatic variables, intermediate results & ✗⏎
        \bottomrule
      \end{tabular}
    \end{adjustbox}
    \footnotetext[1]{To be discussed below}
    \footnotetext[2]{variable size for \alert{variadic functions}, i.e., functions such as \cc{printf} whose number of parameters is not fixed}
    \Caption[The four regions of an activation record.]
  \end{table}
  \begin{itemize}
    \item Each call of \cc{gcd}
          \begin{itemize}
            \item \textbf{Caller:} push arguments
            \item \textbf{Architecture:} push registers and jump to \cc{gcd} (in response to a hardware \kk{call} instruction)
            \item \textbf{Callee:} Allocate local variables and use stack for intermediate results.
          \end{itemize}
    \item Each return from a call
          \begin{itemize}
            \item \textbf{Callee:} pop the frame out of the stack
            \item \textbf{Architecture:} pop registers and jump saved location (in response to a hardware \kk{ret} instruction)
            \item \textbf{Caller:} pop arguments
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Activation record as a contract}
  A function call has two parties
  \begin{itemize}
    \item The caller
    \item The callee
  \end{itemize}
  An activation record represents the \Red{contract} of \emph{interface} between
  the two:
  \begin{itemize}
    \item \alert{Saved state} everything that is required to reconstruct
          the \alert{caller}'s state at the end of the call;
          typically, saved registers.
    \item \alert{Local state} local variables of the \alert{callee}, intermediate results, etc.
    \item \alert{Arguments} values that the caller sent to the \alert{callee}
    \item \alert{Environment} variables, functions, definitions, etc.\
          defined by the \alert{caller} and used by the \alert{callee}.
  \end{itemize}
  An activation record is often \alert{realized} by a stack frame.
\end{frame}

\begin{frame}[fragile]{What's the ‟environment”?}
  \WD{Environment (first approximation)}{%
    Variables, functions, constants, etc., of the \alert{caller}, which can be used by \alert{callee}.
  }
  \begin{itemize}
    \item Does not exist in~\CPL/\CC
    \item In \Pascal, definitions made in a function, are
          available to any nested function.
    \item Limited forms available in Gnu-\CPL and \Java
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Acquired bindings in nested functions}
  What's the environment of \cc{pivot}?
  \begin{code}[watermark text=Gnu-\CPL]{CEEPL}
void sort(int a[], int n) {¢¢
  void swap(int i, int j) {¢¢ ¢…¢}
  void qsort(int from, int to) {¢¢
    int pivot() {¢¢ ¢\Red{???}¢}
  }
  ¢…¢
}
  \end{code}
  \begin{TWOCOLUMNS}
    \begin{itemize}
      \item Function \cc{sort}
      \item Function \cc{swap}
      \item Function \cc{qsort}
      \item Function \cc{pivot}
    \end{itemize}
    \MIDPOINT
    \begin{itemize}
      \item Arguments to function \cc{sort}
            \begin{itemize}
              \item Array~\cc{a}
              \item Integer~\cc{n}
            \end{itemize}
      \item Arguments to function \cc{qsort}
            \begin{itemize}
              \item Integer \cc{from}
              \item Integer \cc{to}
            \end{itemize}
    \end{itemize}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}{Name \vs entity}
  \begin{itemize}
    \item But what's really in the phrase
          \Q{‟variables, functions, constants, etc.”}
    \item We distinguish between
          \begin{itemize}
            \item Name
            \item Entity
          \end{itemize}
          Note that:
          \begin{itemize}
            \item Entities may have no name
                  (e.g., anonymous functions, classes, allocated variables, etc.)
            \item Entities may have more than one name
                  (e.g., type aliases)
            \item In some cases, a name might have a name,
                  (e.g., tetragrammaton, shemhamphorasch, but also found in
                  reflective programming)
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Binding}
  \WD{Binding}{%
    Binding is the tie between a name and an entity
  }
  The phrase ‟\alert{variables, functions, constants, etc.}”, means
  \begin{quote}
    the set of bindings in the \alert{caller} available to the \alert{callee}
  \end{quote}
\end{frame}

\begin{frame}{The environment: a more precise definition}
  \squeezeSpace
  \WD{Environment (second approximation)}{%
    The \Red{environment} of a function, is the set of
    set of bindings of the caller which are available to the callee.
  }
  In \Pascal, the environment includes
  \begin{itemize}
    \item The \kk{CONST} section of the caller (binding of names to values)
    \item The \kk{VAR} section of the caller (binding of names to variables)
    \item The \kk{TYPE} section of the caller (binding of names to types)
    \item The \kk{LABEL} section of the caller (binding of names (integers) to program locations)
    \item Functions and procedures defined within the caller (binding of names
          to functions and procedure values)
  \end{itemize}
  In~\CPL/\CC, there is no ‟caller environment”
\end{frame}

\begin{frame}[fragile]{Environment of a nested function}
  \squeezeSpace[2]
  \centering
  {\large\Red{What's the environment of function \cc{pivot}?}}
  \begin{code}[watermark text=Gnu-\CPL]{CEEPL}
void sort(int a[], int n) {¢¢
  void swap(int i, int j) {¢¢ ¢…¢}
  void qsort(int from, int to) {¢¢
    int pivot() {¢¢ ¢\Red{???}¢ }
  }
}
  \end{code}
  \begin{TWOCOLUMNS}[-8ex]
    \Red{Function names in the environment:}
    \begin{itemize}
      \item The \alert{binding} of the names: \cc{sort},
            \cc{swap}, \cc{qsort} and \cc{pivot}
            to the ‟functions”
      \item Binding here is of names to the pointers to functions.
    \end{itemize}
    \MIDPOINT
    \Red{Function arguments in the environment:}
    \begin{itemize}
      \item Binding of names of arguments to function \cc{sort}
      \item Binding of names of arguments to function \cc{qsort}
            \begin{itemize}
              \item These bindings are \alert{distinct} in \alert{each} recursive calls.
              \item The semantics of Gnu-\CPL are such that \cc{pivot} acquires
                    the ‟\alert{correct}” bindings.
            \end{itemize}
    \end{itemize}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}{Environment \vs scope}
  \squeezeSpace
  In simple words…
  \begin{itemize}
    \item \textbf{Q:} What's the environment?
    \item \textbf{A:} All variables which are in ‟scope”
    \item \textbf{Q:} What's scope?
    \item \textbf{A:} The extent of locations in the program in which a binding is recognized.
  \end{itemize}
  Two scoping (binding) rules are found in PLs
  \begin{description}[Dynamic scoping]
    \item [Lexical scoping] Bindings not made in function, are determined by \alert{where} the
    function is \Red{defined}.
    \item [Dynamic scoping] Bindings not made in function, are determined by \alert{when} the
    function is \Red{active}.
  \end{description}
  Environment could be
  \begin{itemize}
    \item Defined statically (lexical scoping)
    \item Defined dynamically (dynamic scoping)
  \end{itemize}
\end{frame}

\subunit[dynamic-scoping]{Dynamic scoping}

\begin{frame}[label=current]{What's dynamic scoping}
  \squeezeSpace
  \begin{Block}{Inheritance of bindings?}
    When does~$f₂$ ‟inherit” bindings made in~$f₁$?
  \end{Block}
  \textbf{Answer I/II:} (\alert{dynamic scoping}) If~$f₂$ is \alert{called} by~$f₁$.
  \qq{(sometimes called \alert{dynamic binding})}
  \begin{TWOCOLUMNSc}
    \begin{Figure}[Function~$f₂$ called by~$f₁$ called by~$f₀$]
      \begin{adjustbox}{H=0.5}
        \input{f0-f1-f2-call.tikz}
      \end{adjustbox}
    \end{Figure}
    \MIDPOINT
    \squeezeSpace
    \begin{itemize}
      \item Inheritance is inductive:~$f₂$ inherits bindings of~$f₀$ if~$f₁$ is called by~$f₀$.
      \item Environment defined by program dynamics
      \item Conversely, the scope of bindings made in~$f₁$ is:
            \begin{itemize}
              \item all functions called, directly, or indirectly by~$f₁$
              \item determined dynamically
              \item hence the term \emph{dynamic scoping}
            \end{itemize}
    \end{itemize}
  \end{TWOCOLUMNSc}
\end{frame}

\begin{frame}{Dynamic scoping}
  \squeezeSpace
  \WD[0.8]{Dynamic scoping}{%
    The environment is determined by calls; if~$f₁$
    calls~$f₂$, then~$f₂$ ‟inherits”
    bindings made by~$f₁$.
  }
  Dynamic scoping cares not whether there is any relationship between~$f₁$ and~$f₂$.
  \begin{itemize}
    \item Found in \Lisp (but not in \textsc{Scheme})
    \item Found in \TeX (text processing PL used to produce this material)
    \item Found in \Perl!
    \item Found in \Bash!
    \item Found in HTML/CSS!
  \end{itemize}
  The de-facto semantics of the~\CPL preprocessor.
\end{frame}

\begin{frame}[fragile,label=current]{Semantics of dynamic scoping}
  \squeezeSpace[4]
  \begin{TWOCOLUMNS}[-6ex]
    \begin{code}[watermark text=Gnu-\CPL]{CEEPL}
void (*exchange)(int, int);
¢¢
void sort(int a[], int n) {¢¢
  void swap(int i, int j) {¢¢
    const int t = a[j];
    a[i] = a[j];
    a[j] = t;
  }
  ¢…¢
  exchange = swap;
  ¢…¢
}
    \end{code}
    \MIDPOINT
    \begin{code}[watermark text=pseudo Gnu-\CPL]{CEEPL}
  ¢…¢
¢＃¢define SIZEOF(z) (sizeof(z) / sizeof(z[0]))
¢¢
int main(int argc, char **argv) {¢¢
  int a[500];
  int b[500];
  ¢…¢
  sort(b, SIZEOF(b));
  ¢…¢
  (*exchange)(10,12);
  ¢…¢
}
    \end{code}
  \end{TWOCOLUMNS}
  \squeezeSpace[-2]
  \begin{LEFT}
    What would work?
    \begin{itemize}[<+->]
      \item \cc{sort} calling \cc{qsort}? ⚓Yes.
      \item \cc{qsort} calling \cc{qsort}? ⚓Yes.
      \item \cc{qsort} calling \cc{pivot}? ⚓Yes.
      \item \cc{main} calling \cc{swap}? ⚓Yes.
    \end{itemize}
  \end{LEFT}
\end{frame}

\begin{frame}{The runtime bindings dictionary}
  The ‟\alert{binding dictionary}”:
  \begin{itemize}
    \item Local state is represented by a dictionary
    \item Dictionary supports mapping between names and entities
    \item Typical entities are variables, constants, and functions.
    \item Typical implementations of dictionary are linked list and hash table.
  \end{itemize}
  The environment:
  \begin{itemize}
    \item The environment is represented as a ‟back pointer” (BP) to the most recent
          stack frame.
    \item Thus, we have a ‟stack of dictionaries”.
    \item Search for name is carried out by searching in the stack of dictionaries.
    \item There are means to make this search quite efficient.
  \end{itemize}
\end{frame}

\subunit[lexical-scoping]{Lexcial scoping}

\begin{frame}[label=current]{Lexical scoping}
  \squeezeSpace[5]
  \begin{Block}{Inheritance of bindings?}
    When does~$f₂$ ‟inherit” bindings made in~$f₁$?
  \end{Block}
  \textbf{Answer II/II:} (\alert{lexical scoping}) If~$f₂$ is defined \alert{inside}~$f₁$
  \qq{(sometimes called \alert{static scoping}, or \alert{static scoping})}
  \begin{TWOCOLUMNS}
    \squeezeSpace
    \begin{Figure}[Function~$f₂$ defined within~$f₁$ defined within~$f₀$]
      \begin{adjustbox}{}
        \input{f0-f1-f2-nesting.tikz}
      \end{adjustbox}
    \end{Figure}
    \MIDPOINT
    \begin{itemize}
      \item By induction,~$f₂$ inherits bindings of~$f₀$ if~$f₁$ is
            defined inside~$f₀$.
      \item Environment defined by static code structure.⏎
    \end{itemize}
    \squeezeSpace[2]
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}{Intricacies in lexical scoping}
  \WD[0.8]{Lexical scoping}{%
    The environment is determined by scope; if~$f₁$
    is defined in~$f₂$, then~$f₂$ ‟inherits”
    bindings made by~$f₁$.
  }
  \begin{itemize}
    \item There might be more than one activation record of~$f₁$ on the stack
    \item The caller of~$f₂$ might not be~$f₁$
    \item Lexcical scoping means the \alert{most recent version} of~$f₁$.
  \end{itemize}
  \begin{Block}[minipage,width=0.7\columnwidth]{Lexical is \text{static}?}
    Lexical scoping is not entirely static. The correct
    edition of the nest must be determined dynamically.
  \end{Block}
\end{frame}

\begin{frame}[fragile,label=current]{Lexical nesting does not imply call order}
  \begin{code}[watermark text=Gnu-\CPL]{CEEPL}
void sort(int a[], int n) {¢¢
  void swap(int i, int j) {¢¢ ¢…¢ }
  void qsort(int from, int to) {¢¢
    int pivot() {¢¢ ¢…¢ }
  }
}
  \end{code}
  \begin{table}[H]
    \begin{adjustbox}{}
      \coloredTable
      \begin{tabular}{l*3c}
        \toprule
                             & \cc{pivot} & \cc{qsort} & \cc{sort}⏎
        \midrule
        \cc{swap} nested in & ✗ & ✗ & ✓⏎
        \cc{swap} called by & ✓ & ✗ & ✗⏎
        \cc{qsort} nested in & ✗ & ✗ & ✓⏎
        \cc{qsort} called by & ✗ & ✓ & ✓⏎
        \bottomrule
      \end{tabular}
    \end{adjustbox}
    \Caption[‟nested in” and ‟called by” relationships in the \ccn{qsort} example]
  \end{table}
  \begin{itemize}
    \item ‟nested in” does not imply ‟called by”.
    \item ‟called by” does not imply ‟nested in”!
  \end{itemize}
\end{frame}

\begin{frame}[fragile,label=current]{Convoluted calls}
  \squeezeSpace[2]
  \begin{TWOCOLUMNSc}
    \begin{Figure}[Deep function nesting structure:
        function~$f_{i+1}$ defined in~$fᵢ$ for~$i=0,1,2,3$;
~$g_{j+1}$ defined in~$gᵢ$ for~$j=1,2$; and,
~$g₁$ defined in~$f₁$.
      ]
      \begin{adjustbox}{}
        \input{f0-f1-f2-f3-f4-g1-g2-g3-nesting.tikz}
      \end{adjustbox}
    \end{Figure}
    \MIDPOINT
    The caller of~$f₂$ is not necessarily~$f₁$; it could be
    \begin{itemize}
      \item function~$f₂$ itself
      \item function~$f₃$ defined in~$f₂$
      \item function~$f₄$ defined in~$f₃$
      \item …
      \item another function~$g₁$ defined in~$f₁$
      \item function~$g₂$ defined in~$g₁$
      \item function~$g₃$ defined in~$g₂$
      \item …
    \end{itemize}
    \Red{But \Red{not} function~$f₀$ within which~$f₁$ is defined!}
  \end{TWOCOLUMNSc}
\end{frame}

\begin{frame}{Back pointers in the quick sort example}
  \squeezeSpace
  Consider the following chain of calls:
  \scriptsize
  \[
    \cc{main()}→
    \cc{sort()}→
    \cc{qsort()}→
    \cc{qsort()}→
    \cc{qsort()}→
    \cc{pivot()}→
    \cc{swap()}
  \]
  \begin{Figure}
    \begin{adjustbox}{H=0.89}
      \input{back-pointer.tikz}
    \end{adjustbox}
  \end{Figure}
\end{frame}

\begin{frame}[label=current]{Managing lexcial scoping with back pointers}
  \begin{itemize}
    \item The environment is represented as a ‟back pointer” (BP)
    \item The BP is part of the stack frame
    \item BP points at the stack frame of the
          \alert{most recent version of nest}
  \end{itemize}
  Using the back pointer:
  \begin{itemize}
    \item To access variables defined in~$f₁$ function~$f₂$ traverses the BP,
          to find the
          local state of~$f₁$
    \item To access variables defined in~$f₀$ function~$f₂$ hop twice through
          the BP list,
          to find the local state of~$f₀$
    \item To access variables defined in~$f_{-1}$
          (the function within which~$f₀$ is defined),
          make three hops along the BP list.
    \item …
  \end{itemize}
\end{frame}

\begin{frame}{Maintaining the back pointer}
  \begin{itemize}[<+->]
    \item Let
          \[
            \mathbf f∈❴f₁, f₂, f₃,…, g₁, g₂, g₃,…❵
          \]
          be a function ready to call~$f₂$.
          \item~$𝒻$ must traverse the back pointers list to find
          \Q<+->{the ‟most recent” stack frame of~$f₁$.}
    \item Copy this address to the BP of the stack frame of
          the newly created stack frame
          for~$f₂$.
  \end{itemize}
  \onslide<+->{Number of hops?}
  \begin{itemize}[<+->]
    \item If~$\mathbf f= f₁$, then no hops.
    \item If~$\mathbf f= f₂$, then one hop.
    \item If~$\mathbf f= f₃$, then two hops.
    \item If~$\mathbf f= g₁$, then one hops.
    \item If~$\mathbf f= g₂$, then two hops.
    \item …
  \end{itemize}
\end{frame}

\begin{frame}{A simplified implementation of lexcial scoping}
  \begin{itemize}
    \item Suppose that all bindings in the environment are read-only.
    \item Then, instead of juggling the BP, one can simply copy the entire
          set of bindings as arguments.
  \end{itemize}
\end{frame}

\subunit{Escaping the nest}

\begin{frame}[fragile=singleslide,label=current]{Function variables ＆ dangling references}
  \squeezeSpace
  Similarly, suppose that \Pascal had function variables…
  \squeezeSpace
  \begin{TWOCOLUMNS}[2ex]
    \begin{code}[watermark text=Pseudo-\Pascal]{PASCAL}
Program Main;
VAR savedF: Integer -> Char; (* ¢✗¢ *)
  (* (no function variables) *)
  Procedure P;
    VAR m: Integer;
  Function f(n: Integer): Char;
    Begin (* uses inherited ¢m¢ *)
      if (m - n > 0) f:='p' else f:='n'
    end; (* function f*)
  Begin (* Procedure p *)
    readln(m);
    savedF := f; (* ¢✗¢! *)
  end (* Procedure p *)
Begin (* Program main *)
  P;
  ¢…¢
  savedF(2) (* ¢✗¢! *)
  ¢…¢
end.
    \end{code}
    \MIDPOINT
    \begin{itemize}
      \item The \alert{environment} ‟dies” as a stack frame is popped.
      \item {}\CPL forbids nested functions for this reason.
      \item \Pascal forbids function variable for this reason
      \item To make first class ‟function values”, the
            activation record cannot be allocated on the hardware stack.
    \end{itemize}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile]{Nested functions escaping the nest (Gnu-\CPL)}
  \squeezeSpace[5]
  \begin{TWOCOLUMNS}
    \begin{lCode}[watermark text=Gnu-\CPL]{CEEPL}{Function \ccn{add} nested in \ccn{makeAdder}}
#include <stdio.h>
¢¢
typedef int (*F)(int);
¢¢
// The nest:
F makeAdder(int a) {¢¢
  // The nested:
  int add(int x) {¢¢
    // The use of environment (variable ¢\cc a¢)
    const int ¢¢$¢\V$¢ = a + x;
    printf("f.¢¢ add(): return %d\n", ¢¢$¢\V$¢);
    return ¢¢$;¢\V$¢
  }
  // The escape:
  return add;
}
¢¢
// Refuge for escaped values:
F add5, add7;
    \end{lCode}
    \MIDPOINT
    \begin{itemize}
      \item~\cc{F} is an alias for the type of a function that takes an integer and returns an integer.
      \item Function \cc{add} is \alert{nested} in
            function \cc{makeAdder}, the \alert{nest}.
      \item The nested \underline{uses} the \alert{environment} (e.g., variables) of the nest.
      \item The nested \alert{escapes} the nest.
      \item The escaped value will be stored in global variables \cc{add5} and \cc{add7}, the \alert{refuge}.
    \end{itemize}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile]{Forcing access to lost environment}
  \squeezeSpace[5]
  \begin{TWOCOLUMNS}[2ex]
    \begin{lCode}[watermark text=Gnu-\CPL,output={f.{} add(): return 7⏎Segmentation fault (core dumped)}]{CEEPL}{Lost environment}
// Provide refuge to the escaping:
void initialize() {¢¢
  add5 = makeAdder(5);
  add7 = makeAdder(7);
}
¢¢
int dozen() {¢¢
  // Use the nested that escaped:
  return add7(add5(0)); // ¢✗¢
}
¢¢
// Force program crash:
int main() {¢¢
  initialize();
  printf("dozen = %d?\n", dozen());
  return 0;
}
    \end{lCode}
    \MIDPOINT
    \begin{itemize}
      \item Store in the refuge two versions of function \cc{add}, the \alert{nested that escaped}.
      \item Both use the environment of the nest,function \cc{makeAdder}.
      \item But function \cc{makeAdder} is not active anymore.
      \item So, we are doomed for trouble…
    \end{itemize}
  \end{TWOCOLUMNS}
\end{frame}

\subunit{Closures}
\begin{frame}[fragile]{What are closures?}
  \begin{itemize}
    \item Closures can be thought of as
          \begin{itemize}
            \item First class nested functions.
            \item Functions that close over their environment.
            \item Functions whose activation record is heap managed (rather than stack managed).
          \end{itemize}
    \item Many modern PLs support closures, where a
          function can be created in a way that it ‟keeps with
          it” the variables that exist in its scope.
    \item Often used in functions-that-return-functions
    \item You can’t do that in \Pascal,~\CPL, \CC
    \item Very common in functional languages,
          \ML, \Python, \JavaScript and \Java (since version 8.0; uses a hack).
  \end{itemize}
\end{frame}

\begin{frame}[fragile,label=current]{Closures in \JavaScript}
  \begin{TWOCOLUMNS}
    \begin{Code}[watermark text=\JavaScript]{CSHARP}{A closures factory}
function makeAdder(i) {¢¢
  var delta = i;
  function adder(n) {¢¢
    return n + delta;
  }
  return adder;
}
    \end{Code}
    \MIDPOINT
    \begin{Code}[watermark text=\JavaScript]{CSHARP}{Usage}
var add3 = makeAdder(3);
document.write(add3(7));
document.write(add3(12));
var add8 = makeAdder(8)
document.write(add8(12));
    \end{Code}
  \end{TWOCOLUMNS}
  \medskip▄⏎
  \begin{LEFT}
    \textbf{Output:}⏎
    \begin{session}
10
15
20
    \end{session}
  \end{LEFT}
\end{frame}

\begin{frame}{Closures and lifetime}
  \begin{itemize}
    \item \textbf{Q:} what's the lifetime of a variable enclosed in a closure?
    \item \textbf{A:} the lifetime of the enclosing value.
          \begin{itemize}
            \item In this example, the lifetime of ‟\cc{delta}” for
                  each function returned by \cc{makeAdder} is the
                  lifetime of the return value of \cc{makeAdder}.
            \item The same as lifetime of fields in a record allocated on the heap: live as long as the record is
                  still allocated.
          \end{itemize}
  \end{itemize}
  In general, you cannot have closures without using GC, rather than the stack for implementation of activation records.
  With closures, activation records are allocated from the heap.
\end{frame}

\begin{frame}{Closures in \ML}
  Just as in \JavaScript, in \ML, all functions are closures.
  \begin{itemize}
    \item Standard programming idiom of the language
    \item Also supports anonymous functions
    \item Function values are first class values (including the environment)
  \end{itemize}
\end{frame}
\afterLastFrame

\exercises
\begin{enumerate}
  \item How would you argue that~\CPL supports first class functions?
  \item Why is \CC forced to support nested functions?
  \item Explain why~\CPL does not have nested functions.
  \item Enumerate all operations allowed on first-class functions.
  \item Which data stored in the activation records of closures are not part of the data stored with closures?
  \item Are nested functions in \CC first class?
  \item Suppose that the nested function \cc{sqr} escapes \cc{isRightTriangle}, its nest.
        Is there a risk for dangling reference? If yes, write a code for demonstrating it.
        If not, explain why note.
  \item Why do closures require GC?
  \item Suppose that in the quick-sort example, we write
        \begin{Code}[watermark text=Gnu-\CPL]{CEEPL}{The Escape}
#define SIZEOF(z) (sizeof(z)/sizeof(z[0]))
¢¢
void (*exchange)(int, int);
¢¢
void sort(int a[], int n) {¢¢
  void swap(int i, int j) {¢¢
    ¢…¢
  }
  ¢…¢
  exchange = swap;
  ¢…¢
}
¢¢
int main(int argc, char **argv) {¢¢
  int a[500];
  ¢…¢
  sort(a, SIZEOF(a));
  ¢…¢
  (*exchange)(10,12);
  ¢…¢
}
  \end{Code}
  Would this work? Does swapping take place in array~\cc{a} or~\cc{b}?
  \item Explain why the fact that~\CPL has variadic functions dictates that
        the caller has clean the arguments from the stack.
  \item In~\CPL the calling function is responsible for removing the pushed
        arguments from the stack. Why is this the only reasonable
        implementation? (Hint: think of~\CPL's weak type system.)
  \item Explain why letting the called function to remove the arguments from
        the stack is more efficient than having the caller do so.
        (Hint: check out the documentation of \kk{ret} machine instruction(s) in
        e.g., the X86 hardware architecture.)
  \item Why does \Pascal refuse to give equal rights to functions?
  \item Which data items stored with closures do not occur in activation records?
        \item~\CPL has weak typing in function calls. How does the fact
        that the caller is in charge of cleaning arguments from the stack makes
~\CPL a bit more robust?
  \item Describe a method for implementing recursive nested functions with lexical scoping without back pointers.
        (Hint: you would have to pass more arguments in the stack. Which argument passing mode would you use?)
  \item How are closures different than anonymous functions?
  \item Can closures be implemented with stack frames? Explain.
  \item Enumerate the data items that closures have to store.
  \item Describe a situation in which you want your closure to be anonymous?
  \item When are closures planned for \Java? What's the proposed syntax? Any restrictions on the implementation?
  \item When are closures planned for \CC\X\@? What's the proposed syntax? Any restrictions on the implementation?
\end{enumerate}
\references
\begin{itemize}
  %
  \D{Anonymous function}{https://en.wikipedia.org/wiki/Anonymous\_function}
  \D{Call stack}{https://en.wikipedia.org/wiki/Call\_stack}
  \D{First-class function}{https://en.wikipedia.org/wiki/First-class\_function}
  \D{Funarg problem}{https://en.wikipedia.org/wiki/Funarg\_problem}
  \D{Higher-order function}{https://en.wikipedia.org/wiki/Higher-order\_function}
  \D{Name binding}{https://en.wikipedia.org/wiki/Name\_binding}
  \D{Nested function}{https://en.wikipedia.org/wiki/Nested\_function}
  \D{Nesting}{https://en.wikipedia.org/wiki/Nesting\_(computing)}
  \D{Scope}{https://en.wikipedia.org/wiki/Scope\_(computer\_science)}
  \D{Stack-based memory allocation}{https://en.wikipedia.org/wiki/Stack-based\_memory\_allocation}
  \D{Variadic functions}{https://en.wikipedia.org/wiki/Variadic\_function}
\end{itemize}

\begin{frame}{What are sequencers?}
  \WD{Sequencers}{\Red{Sequencers} are atomic commands whose
  execution alters the ‟normal” (structural) flow of control.}
  Examples:
  \begin{description}
    \item [\kk{goto}] from any program point to another
    \item [\kk{return}] to the end of an enclosing function
    \item [\kk{break}] out of an enclosing iteration
    \item [\kk{continue}] to the head of an enclosing iteration
    \item [\kk{throw}] exception, that transfers control to a handler in an invoking function
  \end{description}
\end{frame}

\begin{frame}{Labels}
  To denote where \kk{goto} will go to, one needs a \emph{label}
  \WD[0.82]{Label}{A \Red{label} an \alert{entity} which denotes
    an empty command in the program text; typically there are non-empty commands
  before and after the empty command that the label denotes}
  Labels are a \emph{deliberately disprivileged} entities.
\end{frame}

\begin{frame}[fragile]{Label ‟literal” ＆ first class labels}
  \begin{TWOCOLUMNS}
    \begin{centering}
      \alert{Literal labels}⏎
    \end{centering}
    The label itself ‟is” the empty command which it denotes:
    \begin{itemize}
      \item \emph{Identifiers.} as in~\CPL and assembly PLs
      \item \emph{Integers.} as in \Pascal, \Basic and \Fortran
        \qq{In \Basic, all commands must be labeled in a strictly ascending order}
    \end{itemize}
    \MIDPOINT
    \begin{centering}
      \alert{First class labels}⏎
    \end{centering}
    \Basic, PL/I, and some other obscure languages,
    treat labels as first class values, which can be
    \begin{itemize}
      \item stored in variables
      \item passed as arguments,
      \item returned by functions, etc.
    \end{itemize}
    \begin{code}[watermark text=]{CPLUSPLUS}
l1: ¢\kk{label}¢ v = a > b?
                     l1: l2;
¢$⋮$¢
l2: goto v;
    \end{code}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile]{Declared \vs ad-hoc labels}
  \begin{description}
    \item [Declared label] as in \Pascal; labels must be declared before they are used
    \item [Ad-hoc labels] as in~\CPL/\CC
    \begin{FORTRAN}
      int main() {¢¢
        http://www.cs.technion.ac.il/yogi;
        printf("Page loaded successfully\n");
      }
    \end{FORTRAN}
  \end{description}
\end{frame}

\begin{frame}[fragile]{☡Ad-hoc labels may generate subtle bugs}
  \begin{code}{CEEPL}
int isPerect(unsigned n) {¢¢
  switch (n) {¢¢
    defualt:
      return 0;
    case 6:
    case 28:
    case 496:
    case 8128:
    case 0x1FFF000u:
  }
  return 1;
}
  \end{code}
  \alert{spelling error in ‟\kk{defualt}”}
\end{frame}

\begin{frame}{The persecuted goto}
  Restrictions on \kk{goto}…
  \begin{itemize}
    \item \emph{Only within a block structure.} \Fortran,
    \item \emph{Only \kk{goto} within a function.}
          \item~\CPL does not allow inter-functional \kk{goto}, but \kk{goto}s are allowed in and out of a block.
    \item \emph{No \kk{goto} from a bracketed command into itself.} \Pascal
    \item \emph{No \kk{goto} into a loop or into a conditional.}~\CPL
    \item \emph{No \kk{goto} into a compound command.} \Pascal
    \item \emph{No \kk{goto} into a nested function.} \Pascal and \Algol
    \item \emph{No \kk{goto} at all.} \Java!
  \end{itemize}
\end{frame}

\begin{frame}{\protect\kk{Goto} to a nesting function}
  Labels obey scope rules:
  \begin{itemize}
    \item If a variable of a nesting function is recognized in a nested function,
          the nested function can also \kk{goto} to a label defined the 
            nesting function.
    \item In case of recursion, labels denote a program point in the current activation.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Problems of structured programming}
  A large portion of all software is dedicated to dealing with exceptional cases, erroneous
  inputs and funny situations, e.g., \Pascal code tends to be heavily nested and difficult to read:
  \renewcommand\codesize\tiny
  \begin{code}[minipage,width=0.9\columnwidth]{PASCAL}
If ¢\textrm{some error was discovered}¢ then Begin
  ¢\textrm{deal with it}¢
End else Begin
  ¢\textrm{do a little bit more processing}¢
  If ¢\textrm{another error was discovered}¢ then Begin
    ¢\textrm{deal with this error}¢
  end else Begin
    ¢\textrm{continue processing}¢
    If ¢\textrm{another problem has occurred}¢ then Begin
       ¢\textrm{deal with it}¢
    end else Begin
      ¢\textrm{work a little bit more}¢
      If ¢\textrm{oops, a problem of a different kind was found}¢ then
      Begin
        ¢\textrm{do something about it}¢
      end else Begin
        ¢\textrm{continue to work}¢
      end
    end
  end
end
  \end{code}
\end{frame}

\begin{frame}{Escapes}
  \WD{Escape}{An \Red{\kk{escape}} is a special kind of \kk{goto} which terminates the execution
  of a compound command in which it is nested}
  Makes single entry, multiple exit commands:
  \begin{itemize}
    \item \kk{exit} in \Ada
    \item \kk{break} in~\CPL/\CC/\Java
  \end{itemize}
  Useful for simplifying nesting structure
\end{frame}

\begin{frame}{Varieties of escape}
  \begin{itemize}
    \item \emph{Escape any enclosing Loop.} \kk{exit}~$ℓ$ in \Ada and \kk{break}~$ℓ$ in \Perl/\Java, where~$ℓ$ is a label of an enclosing loop
    \item \emph{Escaping out of a Function.} \kk{return} in~\CPL and \Fortran
    \item \emph{Terminal escape.} terminate the execution of the whole program; \kk{halt} in \Fortran.
    \item \emph{Specialized escape.} \kk{break} out of a \kk{switch} command in~\CPL
  \end{itemize}
\end{frame}

\begin{frame}{Continue}
  \WD{Continue}{A \Red{\kk{continue}} is a special kind of an \kk{escape} which can only occur within
    a iteration command; it terminates the
    execution of the \emph{current} iteration and if there is a next iteration,
  it proceeds to it.}
  Just like \cc{break}, useful for simplifying nesting structure.
  \begin{itemize}
    \item \emph{Continue any Enclosing Loop.} \kk{continue}~$ℓ$ in \Java, where~$ℓ$ is a label of an enclosing iteration command,
          proceeds to the next iteration of the of the iteration command marked by~$ℓ$.
    \item Cannot be emulated by \Pascal \kk{goto}, due to restrictions that \Pascal places on \kk{goto}.
  \end{itemize}
\end{frame}

\afterLastFrame
\exercises
\begin{enumerate}
  \item How can you emulate \kk{break} using \kk{goto} in \Pascal?
  \item Give an example in which \kk{continue} can be used to simplify nesting structure.
  \item Why did \Pascal's designer decide to identify labels with integers.
  \item Give an example in which \kk{return} can be used to simplify nesting structure.
  \item Why did \Pascal's designer decide to require that labels are pre-declared.
  \item Give an example in which \kk{break} out of a loop can be used to simplify nesting structure.
        \item~\CPL does not make it possible to \kk{break} out of more than one loop. How can you do something
        similar with \kk{return} and auxiliary functions?
  \item Compare stored labels with continuations. What's common and how are they different?
\end{enumerate}

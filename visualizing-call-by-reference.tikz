\begin{tikzpicture}[remember picture]
  \let\oldcc=\cc
  \def\cc#1{\oldcc{\textcolor{white}{#1}}}
  \makeatletter 
  \newcommand\placeLabel[3]{
    \node[] at ($0.5*(#2) + 0.5*(#3) + (0,-3ex)$ ){\textcolor{white}{#1}};
  }
	\begin{scope}[start chain=going below, node distance=0pt,
			label={text=white},
			record/.style={label,minimum width=14ex,minimum height=10,on chain,rectangle,thick,draw=black,fill=orange!80!black,text=white},
			var/.style={text=black,minimum width=4ex,rectangle,draw=olive,fill=yellow},
			ref/.style={text=black,minimum width=4ex,rectangle,draw=red,fill=blue!20}
		]
    \node (main) [record,remember picture] {%
			\tikz[remember picture]{%
        \node (m1) [var,label=above:\cc{m}]{$54$};
        \node (n1) [right=4ex of m1,var,label=above:\cc{n}]{$24$};
			}
		}; 
    \node[rotate=-90,xshift=0ex,yshift=-1.5ex] at (main.east) {\textcolor{white}{\cc{main}}};
    \draw[color=white] (main.north east) ++(-3ex,-0.1) -- ($ (main.south east) +(-3ex,0.1) $);
		\node (gcd1) [record] {%
			\tikz{%
        \node (m2) [ref,label=above:\cc{m}]{$\textcolor{Red}{\bullet}$};
        \node (n2) [right=4ex of m2,ref,label=above:\cc{n}]{$\textcolor{Red}{\bullet}$};
			}
		};
    \node[rotate=-90,xshift=0ex,yshift=-1.5ex] at (gcd1.east) {\textcolor{white}{$\cc{gcd}_1$}};
    \draw[color=white] (gcd1.north east) ++(-3ex,-0.1) -- ($ (gcd1.south east) +(-3ex,0.1) $);
		\draw [-{>[sep=2pt]}] (m2.center) to  [bend left=60]   (m1.west);
		\draw [-{>[sep=2pt]}]  (n2.center) to  [bend right=60]   (n1.east);
		\node (gcd2) [record] {%
			\tikz{%
        \node (m3) [ref,label=above:\cc{m},font=\rm]{$\textcolor{Red}{\bullet}$};
        \node (n3) [right=4ex of m3,ref,label=above:\cc{n},font=\rm]{$\textcolor{Red}{\bullet}$};
			}
		};  
    \node[rotate=-90,xshift=0ex,yshift=-1.5ex] at (gcd2.east) {\textcolor{white}{$\cc{gcd}_2$}};
    \draw[color=white] (gcd2.north east) ++(-3ex,-0.1) -- ($ (gcd2.south east) +(-3ex,0.1) $);
		\draw [-{>[sep=2pt]}] (m3.center) to  [bend right=30]   (n2.south);
		\draw [-{>[sep=2pt]}]  (n3.center) to  [bend left=30]   (m2.south);
		\node (gcd3) [record] {%
			\tikz{%
        \node (m4) [ref,label=above:\cc{m},font=\rm]{$\textcolor{Red}{\bullet}$};
        \node (n4) [right=4ex of m4,ref,label=above:\cc{n},font=\rm]{$\textcolor{Red}{\bullet}$};
			}
		};
    \node[rotate=-90,xshift=0ex,yshift=-1.5ex] at (gcd3.east) {\textcolor{white}{$\cc{gcd}_3$}};
    \draw[color=white] (gcd3.north east) ++(-3ex,-0.1) -- ($ (gcd3.south east) +(-3ex,0.1) $);
		\draw [-{>[sep=2pt]}] (m4.center) to  [bend right=30]   (n3.south);
		\draw [-{>[sep=2pt]}]  (n4.center) to  [bend left=30]   (m3.south);
		\node (gcd4) [record] {%
      \tikz{%
        \node (m5) [ref,label=above:\cc{m},font=\rm]{$\textcolor{Red}{\bullet}$};
        \node (n5) [right=4ex of m5,ref,label=above:\cc{n},font=\rm]{$\textcolor{Red}{\bullet}$};
			}
		};
    \node[rotate=-90,xshift=0ex,yshift=-1.5ex] at (gcd4.east) {\textcolor{white}{$\cc{gcd}_4$}};
    \draw[white] (gcd4.north east) ++(-3ex,-0.1) -- ($ (gcd4.south east) +(-3ex,0.1) $);
		\draw [-{>[sep=2pt]}] (m5.center) to  [bend right=30]   (n4.south);
		\draw [-{>[sep=2pt]}] (n5.center) to  [bend left=30]  (m4.south);
	\end{scope}
	\coordinate[yshift=-3ex,xshift=-2pt] (A) at (gcd4.south west); 
	\coordinate[yshift=2pt,xshift=-2pt] (B) at (main.north west); 
	\coordinate[yshift=2pt,xshift=2pt](C) at (main.north east); 
	\coordinate[yshift=-3ex,xshift=2pt] (D) at (gcd4.south east); 
	%
	\draw[ultra thick] (A)-- (B) --(C) -- (D); 
	\node[below=1 ex of gcd4] {\Huge$\Downarrow$};
	\node[above=1 ex of main] {\textbf{Stack}};
\end{tikzpicture}

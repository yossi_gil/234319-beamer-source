\begin{frame}{Flowcharts: another method for command composition}
  \squeezeSpace[3]
  \begin{columns}
    \column[c]{0.35\columnwidth}
    \begin{itemize}
      \item Nodes :
            \begin{itemize}
              \item I/O:
                    \begin{itemize}
                      \item \textbf{read}
                      \item \textbf{print}
                      \item \textbf{display}
                      \item …
                    \end{itemize}
              \item Controls:
                    \begin{itemize}
                      \item \textbf{start}
                      \item \textbf{stop}
                    \end{itemize}
            \end{itemize}
            \begin{itemize}
              \item Empty: \textbf{skip}
              \item assignment
              \item Decision point
              \item …
            \end{itemize}
            \footnotesize\it nodes are in fact atomic commands
      \item Edges: \textbf{goto}
    \end{itemize}
    \column[c]{0.6\columnwidth}
    \begin{Figure}[An intriguing finite automaton computing (in a non-sensible manner) a function that does make sense]
        \begin{adjustbox}{H=0.78}
          \input finite-automaton.tikz
        \end{adjustbox}
    \end{Figure}
  \end{columns}
\end{frame}

\begin{frame}{Flowchart for ‟\protect\textbf getting \protect\textbf things \protect\textbf done (GTD)”}
  \alert{How to process items in your incoming mailbox?}
  \begin{Figure}
      \begin{adjustbox}{}
        \input gtd.tikz
      \end{adjustbox}
  \end{Figure}
\end{frame}

\begin{frame}{Pros ＆ cons of flowcharts}
  \begin{TWOCOLUMNS}
    \alert{Pros}
    \begin{itemize}
      \item Very visual
      \item Very colorful
      \item Can be aesthetically pleasing
      \item Can be understood by almost any one
    \end{itemize}
    \MIDPOINT
    \alert{Cons}
    \begin{itemize}
      \item Do not scale
      \item Many competing standards
      \item Not necessarily planar
      \item Spaghetti code
      \item No one can really understand them
    \end{itemize}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile]{Challenge of understanding spaghetti code}
  \squeezeSpace
  \begin{TWOCOLUMNS}[-0.16\columnwidth]
    \small\it
    \begin{itemize}[<+->]
      \item The program on the right does something useful!
            \savepause{spaghetti}
      \item Many intersecting spaghetti edges
      \item No obvious meaningful partitioning of the chart
            \savepause{simple:node}
      \item Only a few nodes with one entry and one exit
            \savepause{decision:node}
      \item all decision nodes have two (or more) outgoing edges
            \savepause{two:incoming}
      \item \savepause{three:incoming}
            Some nodes with two incoming edges\uncover<+->{, even three!}
    \end{itemize}
    \MIDPOINT
    \begin{Figure}
      \begin{adjustbox}{H=0.78}
        \input{gotos.tikz}
      \end{adjustbox}
    \end{Figure}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}{Structured programming}
  …⚓is a programming paradigm,⚓characterized by⚓
  \begin{itemize}[<+->]
    \item ‟\alert{Three Controls:} precisely three ways for marshaling control:
          \begin{enumerate}
            \item \alert{Sequence}, \emph{e.g.,} \kk{begin}~$C₁;C₂…;Cₙ)$ \kk{end} for~$n>=0$
            \item \alert{Selection}, \emph{e.g.,} \kk{if} … \kk{then} … \kk{elseif} … \kk{else} … \kk{endif}.
            \item \alert{Iteration}, \emph{e.g.,} \kk{while} … \kk{do} … \kk{done}
          \end{enumerate}
    \item \alert{Structured Control:}⚓▄⏎
          \begin{itemize}
            \item all control commands are in fact, command constructors.
            \item control is marshalled through the program structure.
          \end{itemize}
  \end{itemize}
  ⚓\WT[0.6]{The structured programming theorem (B\"ohm-Jacopini, 1966)}{
    Every flowchart graph~$G$, can be converted into
    an equivalent structured program,~$P(G)$.
  }
\end{frame}

\begin{frame}{Nassi-Shneiderman diagram}
  \framesubtitle{A More Sane Graphical Approach}
  \setbeamercovered{highly dynamic}⚓
  \begin{description}[<+->]
    \item [Main Idea] Programming is like tiling the plane.
    \item [Also Called] \Red{NSD}, and ‟\Red{structograms}”
    \item [Thought Of As] the visiual definition of \alert{structured programming}
    \item [Principles:]▄⏎
    \begin{enumerate}[<+->]
      \item every command is drawn as a rectangle
      \item every command has exactly:
            \begin{itemize}[<+->]
              \item One entry point
              \item One exit point
            \end{itemize}
      \item a command may contain other commands
      \item a command may be contained in other commands
    \end{enumerate}
  \end{description}
\end{frame}

\begin{frame}{Compound commands in Nassi-Shneiderman diagrams}
  \begin{Figure}
    \begin{adjustbox}{}
      \input{ns-compound.tikz}
    \end{adjustbox}
  \end{Figure}
  \begin{TWOCOLUMNS}
    \begin{itemize}[<+->]
      \item Compound commands are rectangles which have smaller rectangles in them
      \item Each rectangle may contain in it one, two, or more rectangles
      \item Correspond to our familiar command constructors
    \end{itemize}
    \MIDPOINT
    \begin{itemize}[<+->]
      \item Color is not part of the diagram
      \item But we can add it anyway…
    \end{itemize}
  \end{TWOCOLUMNS}
  \savepause{before-diagram}
\end{frame}

\begin{frame}{Matrix multiplication with Nassi-Shneiderman diagram}
  \squeezeSpace
  \begin{Figure}
    \begin{adjustbox}{}
      \input{ns-matrix-multiplication.tikz}
    \end{adjustbox}
  \end{Figure}
  \begin{quote}\raggedleft\itshape\scriptsize
    Based on an example provided by the original⏎
    October 1973 ‟SIGPLAN \emph{Notices}” article by Isaac Nassi ＆
    Ben Shneiderman
  \end{quote}
\end{frame}

\begin{frame}{Factorial with Nassi-Shneiderman diagram}
  \begin{Figure}
      \begin{adjustbox}{}
        \input{ns-factorial.tikz}
      \end{adjustbox}
  \end{Figure}
\end{frame}

\begin{frame}{More Nassi-Shneiderman notations}
  \begin{Figure}
    \begin{adjustbox}{}
      \input{ns-more-compound.tikz}
    \end{adjustbox}
  \end{Figure}
\end{frame}

\begin{frame}{Even more Nassi-Shneiderman notations}
  \squeezeSpace
  Nassi and Shneiderman did not fully work out the semantics of NSD;
  \begin{itemize}
    \item not in any formal notation;
    \item not in ‟legalese”.
    \item not in mock of ‟legalese”.
  \end{itemize}
  Some notation may be intriguing…
  \par⚓\begin{Figure}
    \begin{adjustbox}{}
      \input{ns-weird.tikz}
    \end{adjustbox}
  \end{Figure}
  \par⚓did not really catch…
\end{frame}

\afterLastFrame

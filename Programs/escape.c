typedef int (*F)(int);

#include <stdio.h>

F makeAdder(int a) {
  // Nested function
  int add(int x) {
    const int $ = a + x; // ¢\cc a¢ is drawn from context
    printf("add: returning %d\n", $);
    return $; 
  }
  return add;
}

F add5, add7;

void init() {
  add5 = makeAdder(5);
  add7 = makeAdder(7);
}

int dozen() {
  return add7(add5(0)); // ¢✗¢
}

int main() 
{
  init();
  printf("dozen = %d?\n", dozen());
  return 0;
}

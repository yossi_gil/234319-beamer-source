#include <iostream>
#include <vector>
#include <string>
#include <list>
using namespace std;

//assuming you put the first that comes in the week as 1 and the second as two 
#define WORKDAY1 2              //MONDAY
#define WORKDAY2 4              //WEDNESDAY
#define FIRST_DAY_OF_SEM 27,7,2015
#define LAST_DAY_OF_SEM 10,9,2015
//assuming you put the first that comes in the week as 1 and the second as two 
#define DAYS_IN_FEBUARY 28
//typedef enum DAYS_IN_MONTHES{0,31,DAYS_IN_FEBUARY,31,30,31,30,31,31,30,31,30,31} daysOfMonth; 
//NOT TAKING CARE OF SHANA MEUBERET 
//ASSUMING CORRECTNESS OF LOTS OF STUFF CAUSE I DONT WANT TO DEAL WITH EXCEPTIONS 
//NO CODE CONVENTION 

//Date static functions: 
int MONTH_TO_DAY(int month) {
  //gets a month, assuming between 1 to 12
  //returns the days till the first day of month
  int days = 0;
  while (month > 1) {
    //takes the one before - meaning when month is two we need to add the sumDays of JANUARY
    static int daysOfMonth[] = { 0, 31, DAYS_IN_FEBUARY, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    days += daysOfMonth[month - 1];
    month--;
  }
  return days;
}

//gets number of days, assuming 0<days<365
//returns the date in the year it resembles
pair<int, int> DAYS_TO_DATE(int days) {
  int day = 1;
  int month = 1;
  while (MONTH_TO_DAY(month) + day < days)
    month++;
  if (MONTH_TO_DAY(month) + day == days)
    return pair<int, int>(month, day);
  //else, need to add days to it
  month--;
  while (MONTH_TO_DAY(month) + day < days)
    day++;
  return pair<int, int>(month, day);
}

class Date {
  public:
    //no need to encapsulate, cause its a small program 
    int day;
    int month;
    int year;                   //works only after 0 but thats fine 
    Date(int day, int month, int year) : day(day), month(month), year(year) { }
    //default destructor
    //returns the num of days different
    int operator-(Date & d) {
      return ((year - d.year) * 365) + (MONTH_TO_DAY(month) - MONTH_TO_DAY(d.month)) + (day - d.day);
    }

    Date & operator+=(int num) {
      int total = MONTH_TO_DAY(month) + day + num;
       while (total > 365) {
        year++;
        total -= 365;
      }     
      const pair<int, int> p = DAYS_TO_DATE(total);
      month = p.first;
      day = p.second;
      return *this;
    }

    //returns the name of the day, meaning from 1 to 7 - sunday,monday and so on
    //took from stack overflow:
    int getNameDay() {
      static int t[] = { 0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4 };
      year -= month < 3;
      return ((year + year / 4 - year / 100 + year / 400 + t[month - 1] + day) % 7) + 1;
    }

    void printDate() {
      cout << day << "/" << month << '/' << year << endl;
    }

    bool operator!=(Date & d) {
      return year != d.year || month != d.month || day != d.day;
    }

    bool operator<=(Date & d) {
      return ((d - (*this)) >= 0);
    }
};

//a class to combine the date with the material 
static int lessonCounter = 1;   //bad coding but easy implementing 
class TutoringHour {
  public:
    Date d;
    string first;
    string second;
    TutoringHour(Date d, string f, string s) :
        d(d), first(f), second(s) {
    }
    void print() {
      cout << "===========" << endl;
      d.printDate();
      cout << "--num " << lessonCounter++ << " --" << endl;
      cout << "===========" << endl;
      if (first == "")
        first = "--undefined--";
      if (second == "")
        second = "--undefined--";
      cout << "First hour material - " << first << endl;
      cout << "Second hour material - " << second << endl;
      cout << endl;
    }
};

int main() {

//gets all of the course material organized by tutorial nums, first hour of the course than second hour
//puts two at a time every monday and thursday between 27/07/15 to 16/09/15
//vector<string> material;
  /* TESTING
   Date startingDay(27,7,2015);
   Date lastDay(18,9,2015);
   cout << "difference is - " << lastDay - startingDay << endl;
   cout << "starting day is - " << startingDay.getNameDay() << endl;
   cout << "last day is - " << lastDay.getNameDay() << endl;
   cout << "ending dadfdsdfdsds " <<  endl;
   startingDay+=7;
   startingDay.printDate();
   cout << "starting day is - " << startingDay.getNameDay() << endl;
   */
  Date dayNow(FIRST_DAY_OF_SEM);
  Date lastDay(LAST_DAY_OF_SEM);
  int countBetween;

//getting the first day of the semester that I am working on
  while (dayNow.getNameDay() != WORKDAY1 && dayNow.getNameDay() != WORKDAY2)
    dayNow += 1;
  if (dayNow.getNameDay() == WORKDAY1)
    countBetween = (WORKDAY2 - WORKDAY1) % 7;
  if (dayNow.getNameDay() == WORKDAY2)
    countBetween = (7 + (WORKDAY1 - WORKDAY2)) % 7;
  list<TutoringHour> calender;
  while (dayNow <= lastDay) { //if doesnt work, to change to dayNow - lastDay <= 0
    //print update and after it the lesson
    //assuming the number of inputs matches the course time
    string FirstHour;
    cin >> FirstHour;
    string SecondHour;
    cin >> SecondHour;
    calender.push_back(TutoringHour(dayNow, FirstHour, SecondHour));
    //printing the calender
    //updating as needed
    dayNow += (countBetween);
    countBetween = 7 - countBetween;  //if 5 will be 2 if 2 will be 5
  }
  for (list<TutoringHour>::iterator i = calender.begin(); i != calender.end(); ++i)
    (*i).print();
  return 0;
}

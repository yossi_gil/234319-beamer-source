fun min(a, b) = if a < b then a else b;
fun max(a, b) = a + b - min(a, b);
fun min3(a, b, c) = min(a, min(b, c));
fun max3(a, b, c) = max(a, max(b, c));
fun med(a, b, c) = a + b + c - min3(a, b, c) - max3(a, b, c);
med(16,18,13);

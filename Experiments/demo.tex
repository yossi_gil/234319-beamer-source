
%%
%% This is the file 'contest.tex'
%%
\PassOptionsToPackage {dvipsnames,override}{xcolor}
\PassOptionsToPackage{pdfborder={0 0 0},bookmarksnumbered,backref,naturalnames}{hyperref}
\documentclass[twocolumn,12pt]{article}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[fontsize=12pt,baseline=14pt]{grid}
\usepackage[top=5.5cm,
  bottom=2.5cm,
  left=2.5cm,
  right=2.5cm,
]{geometry}
\usepackage{xparse}
\usepackage{fourier}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{MnSymbol}
\usepackage{tikz}
\usepackage{hyperref}
\hypersetup{
  colorlinks=true,
  linkcolor=headtitle,
  citecolor=grlnblue,
  urlcolor=grlngray}

\usetikzlibrary{calc,decorations.markings}
\usepackage{beamerarticle}
\usepackage{00}
\usepackage{gridleno}

\NewDocumentCommand\fgroup{O{1}O{0}mm}{%
  \pi_#1(#3,#4_#2)}
\NewDocumentCommand\erren{O{n}O{R}}{%
  \mathbb{#2}^#1}
\NewDocumentCommand\oper{O{f}O{g}}{%
  #1\ast#2}
\NewDocumentCommand\clase{O{f}}{%
  [#1]}
\NewDocumentCommand\opercl{O{f}O{g}}{%
  \clase[#1]\ast\clase[#2]}
\NewDocumentCommand\invclase{O{f}}{%
  \clase[\bar{#1}]}

\course{Programming Languages}
\courseid{234319}
\professor{Yossi Gil}
\term{Semester A / 2014-2015}
\topic{...}
\date{\today}





\hbadness=99999 % Make TeX remain silent

\begin{document}

\makeheader

\begin{summary}
\tableofcontents
\end{summary}

\section{Definitions and basic properties}

We shall prove several properties of the fundamental group. In particular, we shall show that the group is, up to isomorphism, independent of the choice of base point (provided that \(X\) is path connected). We shall also show that the group is a topological invariant of the space \(X\), the fact that is of crucial importance in using it to study homeomorphism problems.

\begin{definition}
Let \(X\) be a space; let \(x_0\) be a point of \(X\). A path in \(X\) that begins and ends at \(x_0\) is called a \emph{loop} based at \(x_0\). The set of path homotopy classes of loops based at \(x_0\), with the operation \(\ast\), is called the \emph{fundamental group} of \(X\) relative to the \emph{base point} \(x_0\). It is denoted by \(\fgroup{X}{x}\).
\end{definition}

The operation \(\ast\), when restricted to this set, satisfies the axioms for a group. Given two loops \(f\) and \(g\) based at \(x_0\), the composition \(\oper\) is always defined and it is a loop based at \(x_0\). Associativity, the existence of an identity element \(\clase[e_{x_0}]\), and the existence of an inverse \(\invclase\) for \(\clase\) are immediate.

\begin{example}
Let \(\erren\) denote the standard euclidean \(n\)\nobreakdash-space. Then the group \(\fgroup{\erren}{x}\) is the trivial group consisting of the identity alone. For if \(f\) is a loop in \(\erren\) based at \(x_0\), the straight line homotopy
\begin{gridenv}
\begin{equation*}
  F(s,t) = tx_0 + (1 - t)f(s)
\end{equation*}
\end{gridenv}
is a path homotopy between \(f\) and the constant loop \(e_{x_0}\).
\end{example}

\begin{example}
More generally, if X is any \(\emph{convex}\) subset of \(\erren\), then \(\fgroup{X}{x}\) is the trivial (one single element) group. The straight-line homotopy will work once again, for convexity of \(X\) means that for any two points \(x\) and \(y\) of \(X\), the straight-line segment
\begin{gridenv}
\begin{equation*}
  \{\, tx_0 + (1 - t)y \mid 0\leq t\leq 1\,\}
\end{equation*}
\end{gridenv}
between them lies in \(X\). In particular, the \emph{unit ball} \(\erren[n][B]\) in \(\erren\),
\begin{gridenv}
\begin{equation*}
  \erren[n][B] = \{\, x \mid  x_1^2 + \cdots + x_n^2\leq 1 \,\},
\end{equation*}
\end{gridenv}
has trivial fundamental group.
\end{example}
An immediate question one asks is the ex\-tent to which the fundamental group depends on the base point. The answer is given in Corollary~\ref{cor:indbase}, which follows.

\begin{definition}
Let \(\alpha\) be a path in \(X\) from \(x_0\) to \(x_1\). We define a map
\begin{gridenv}
\begin{equation*}
  \hat{\alpha}:\fgroup{X}{x} \rightarrow \fgroup[1][1]{X}{x}
\end{equation*}
\end{gridenv}
by the equation
\begin{gridenv}
\begin{equation*}
  \hat{\alpha}(\clase) = \opercl[\bar{\alpha}][f]\ast\clase[\alpha].
\end{equation*}
\end{gridenv}
\end{definition}
The map \(\hat{\alpha}\) is pictured in Figure~\ref{fig:indbase}. It is well-defined because the operation \(\ast\) is well-defined. If \(f\) is a loop based at \(x_0\), then \(\oper[\bar{\alpha}][(\oper[f][\alpha])]\) is a loop based at \(x_1\). Hence \(\hat{\alpha}\) maps \(\fgroup{X}{x}\) into \(\fgroup[1][1]{X}{x}\), as desired.

\begin{figure}
\centering
\begin{tikzpicture}[
  point/.style={circle,inner sep=2pt,fill},
  decoration={markings,
  mark=at position 0.5 with {\arrow{stealth}}}
  ]
\draw[fill=summarybg,draw=none] (3,3) rectangle (-3,-3);

\path[fill=white] (0,0) circle [radius=2.5cm];
\node[point,label=left:$x_0$] at (-1,-0.2) (a) {};
\node[point,label=right:$x_1$] at (1.2,1.3) (b) {};

\draw[postaction={decorate},shorten <= 1pt,shorten >= 1pt] (b.south) .. controls (-0.5,2) and (0.5,1.8) .. node[below=7pt] {$\bar{\alpha}$} (a.south);

\draw[postaction={decorate}] ($(a.north)+(-1pt,0)$) .. controls ($(0.5,1.8)+(0,10pt)$) and ($(-0.5,2)+(0,2pt)$) .. node[left=4pt] {$\alpha$} (b.north west);

\draw[postaction={decorate}] (a.south east) .. controls (-0.5,-3)  and (2,1) .. node[label=below:$f$] {} (a.south west);
\end{tikzpicture}
\caption{}
\label{fig:indbase}
\end{figure}

\begin{theorem}
The map \(\hat{\alpha}\) is a group homomorphism.
\end{theorem}

\begin{Proof}
To prove that \(\hat{\alpha}\) is a homomorphism, we compute
\begin{gridenv}
\begin{multline*}
  \hat{\alpha}(\clase) \ast \hat{\alpha}(\clase[g]) \\ 
  = ( \clase[\bar{\alpha}]\ast \clase \ast \clase[\alpha]) \ast (\clase[\bar{\alpha}]\ast \clase[g] \ast \clase[\alpha])\\
= ( \clase[\bar{\alpha}]\ast \clase \ast \clase[g] \ast \clase[\alpha]) = \hat{\alpha}(\clase \ast \clase[g]).
\end{multline*}
\end{gridenv}
This proof uses the groupoid properties of \(\ast\). To show that \(\hat{\alpha}\) is an isomorphism, we show that if \(\beta\) denotes the path \(\bar{\alpha}\), which is the reverse of \(\alpha\), then \(\hat{\beta}\) is the inverse for \(\hat{\alpha}\). We compute, for each element \(\clase[h]\) of \(\fgroup[1][1]{X}{x}\),
\begin{gridenv}
\begin{equation*}
  \hat{\alpha}(\hat{\beta}(\clase[h])) = \clase[\bar{\alpha}] \ast (\clase[\alpha] \ast \clase[h] \ast \clase[\bar{\alpha}]) \ast \clase[\alpha] = \clase[h]
\end{equation*}
\end{gridenv}
A similar computation shows that \(\hat{\beta}(\hat{\alpha}(\clase))=\clase\), for each class \(\clase\) in \(\fgroup{X}{x}\).
\end{Proof}

\begin{corollary}\label{cor:indbase}
If X is path connected and \(x_0\) and \(x_1\) are two points of \(X\), then \(\fgroup{X}{x}\) is isomorphic to \(\fgroup[1][0]{X}{x}\).
\end{corollary}

Suppose that \(X\) is a topological space. Let \(C\) be the path component of \(X\) containing \(x_0\). It is easy to see that \(\fgroup[1][0]{C}{x}=\fgroup{X}{x}\), since all loops and homotopies in \(X\) that are based at \(x_0\) must lie in the subspace \(C\). Thus \(\fgroup{X}{x}\) depends only on the path component of \(X\) containing \(x_0\), and gives us no information whatever about the rest of \(X\). For this reason, it is usual to deal only with path-connected spaces when studying the fundamental group.

If \(X\) is path connected, then all the groups \(\fgroup[1][]{X}{x}\) are isomorphic, so it is tempting to try to ``identify'' all these groups with one another, and to speak simply of the fundamental group of the space \(X\), without reference to base point. The difficulty with this approach is that there is no \emph{natural} way of identifying \(\fgroup{X}{x}\) with \(\fgroup[1][1]{X}{x}\); different paths \(\alpha\) and \(\beta\) from \(x_0\) to \(x_1\) may give raise to different isomorphisms between these groups. For this reason, onitting the base point can lead to error.

\section{Simply Connected Spaces}

\begin{definition}
A space \(X\) is said to be simply connected if it is a path-connected space and if \(\fgroup{X}{x}\) is the trivial group for some \(x_0\) in \(X\), and hence for every \(x\) in \(X\).
\end{definition}

\begin{lemma}
In a simply connected space \(X\), any two paths having the same initial and final points are path homotopic.
\end{lemma} 

\begin{proof}
Let \(f\) and \(g\) be two paths from \(x_0\) to \(x_1\). Then \(\oper[f][\bar{g}]\) is defined and is a loop on \(X\) based at \(x_0\). Since \(X\) is simply connected, \(\oper[f][\bar{g}]\sim_p e_{x_0}\). Applying the groupoid properties, we see that
\begin{gridenv}
\begin{equation*}
\clase[(\oper[f][\bar{g}]) \ast g ] = \clase[\oper[e_{x_0}][g]] = \clase[g].
\end{equation*}
\end{gridenv}
But
\begin{gridenv}
\begin{equation*}
\clase[(\oper[f][\bar{g}]) \ast g ] = \clase[f \ast (\oper[\bar{g}][g]) ]=\clase[\oper[f][e_{x_0}]] = \clase[f].
\end{equation*}
\end{gridenv}
Thus \(f\) and \(g\) are path homotopic.
\end{proof}

\section{The fundamental group is a topological invariant}

It should be intuitively clear that the fundamental group is a topological invariant of the space \(X\). A convenient way to prove this fact formally is to introduce the notion of the ``homomorphism induced by a continuous map''.

Suppose that \(h:X\rightarrow Y\) is a continuous map that carries the point \(x_0\) of \(X\) to the point \(y_0\) of \(Y\). We often denote this fact by writting
\begin{gridenv}
\begin{equation*}
  h: (X,x_0)\rightarrow (Y,y_0).
\end{equation*}
\end{gridenv}
If \(f\) is a loop in \(X\) based at \(x_0\), then the composite \(h\circ f:I\rightarrow Y\) is a loop in \(Y\) based at \(y_0\). The correspondence \(f\mapsto h\circ f\) thus gives rise to a map carrying \(\fgroup{X}{x}\) to \(\fgroup{Y}{y}\).
\begin{exercises}
\begin{enumerate}
  \item A subset \(A\) of \(\erren\) is said to be \emph{star convex} if for some point \(a_0\) of \(A\), all the line segments joining \(a_0\) to other points of \(A\) lie in \(A\).
  \begin{enumerate}
    \item Find a star convex set that is not convex.
    \item Show that if \(A\) is star convex, \(A\) is simply connected.
    \item Show that if \(A\) is star convex, any two paths in \(A\) having the same initial and final points are path homotopic. 
  \end{enumerate}
  \item Let \(x_0\) and \(x_1\) be two given points of the path-connected space \(X\). Show that the group \(\fgroup[1][1]{X}{x}\) is abelian if and only if for every par \(\alpha\) and \(\beta\) of paths from \(x_0\) to \(x_1\), we have \(\hat{\alpha}=\hat{\beta}\).
  \item Let \(A\subseteq X\) and let \(r:X\rightarrow A\) be a retraction. Given \(a_0\in A\), show that
\begin{gridenv}
\begin{equation*}
  r_\ast:\fgroup[1][0]{X}{a}\rightarrow \fgroup[1][0]{A}{a}
\end{equation*}
\end{gridenv}
is surjective.
  \item Let \(A\) be a subset of \(\erren\); let \(h: (A,a_0)\rightarrow (Y,y_0)\). Show that if \(h\) is extendable to a continuous map of \(\erren\) into \(Y\), then \(h_\ast\) is the zero homomorphism.
\end{enumerate}
\end{exercises}

\begin{thebibliography}{9}
  \bibitem{munkres} Munkres, James R. \emph{Topology, A First Course}. Prentice Hall, Inc., 1975.
  \bibitem{willard} Willard, Stephen. \emph{General Topology}. Massa\-chusetts: Addison- Wesley, 1970.
\end{thebibliography}

\end{document}


\begin{frame}[<+->]{Linguistics of PLs: syntax ＆ semantics}
  \squeezeSpace
  {⚓}Two components of linguistics (as in natural linguistics):
  \begin{description}[Semantics]
    \item [Syntax] Which text files are correct programs? How expressions, commands, declarations, etc., are put together to form a program?
    \item [Semantics] What's the meaning of correct programs? Behavior when executed on a computer?
  \end{description}
  {⚓}Means for specification:
  \begin{description}[Semantics]
    \item [Syntax] Regular expressions, context-free grammars, BNF form, EBNF form, syntax diagrams
    (briefly touched here, subject of ‟\emph{Automata ＆ formal Languages}")
    \item [Semantics] Tutorials,
    user guides, handbooks, Wikipedia entries, language legalese (briefly touched here), and
    formal semantics (outside our scope).
  \end{description}
  {⚓}Common theme: \alert{recursively defined sets}…
\end{frame}

\subunit[re]{Regular expressions}

\begin{frame}[<+->]{Recursively defined sets}
  Also known as \emph{inductively defined sets}
  \WD{Who is Jewish?} {\mbox{}⏎%
    \begin{itemize}
      \item Mother Sarah was Jewish.
      \item Father Abraham was Jewish.
      \item People who converted are Jewish.
      \item People born to a Jewish mother are Jewish.
    \end{itemize}
  }
  {⚓}
  Other natural examples:
  \begin{itemize}
    \item Who is a Muslim?
    \item Who can call himself a ‟\emph{Dr.}”?
    \item Who can call himself a ‟\emph{Rabbi}”?
  \end{itemize}
\end{frame}

\newcommand\textbox[1]{\text{\parbox{6ex}{\scriptsize\begin{centering} \alert{\emph{#1}}\end{centering}}}}

\begin{frame}[<+->]{The set of strings over an alphabet}
  \begin{itemize}
    \item Regular expressions is a language for defining a subset of the strings over a given alphabet
    \item But, what are ‟\alert{strings over a given alphabet}”?
          \begin{itemize}
            \item Let~$Σ$ be an \alert{alphabet}
            \item i.e., a set of \alert{letters} (also called \alert{characters})⚓, e.g.,
                  \begin{equation*}
                    Σ = ❴\cc{a},\cc{b},\cc{c},\cc{d},\cc{e},~\cc{f} ❵
                  \end{equation*}
                  \item~$Σ$ can be used to write \alert{string}s,
                  ⚓e.g., \cc{abba}, ⚓\cc{baa}, ⚓\cc{daffacc}, ⚓\cc{cafe}, ⚓\cc{decaf}, ⚓\cc{dafcaa}
            \item Let~$ε$ denote the \alert{empty string}.
            \item Let~$Σ^*$ be the set of all strings over~$Σ$⚓,
                  \begin{equation}
                    \begin{split}
                      Σ^* = ❴ &
                      \overbrace{\mathstrut ε}^{\text{length~0 string}},
                      \overbrace{\mathstrut~\cc{a},~\cc{b}, …,~\cc{f}} ^{\text{length~1 strings}},
                      ⏎
                      &\qquad\qquad
                      \overbrace{\mathstrut \cc{aa}, \cc{ab}, …, \cc{ff}} ^{\text{length~2 strings}},
                      \overbrace{\mathstrut \cc{aaa}, …, \cc{fff}} ^{\text{length~3 strings}},
                      …
                      ❵
                    \end{split}
                    \synopsis{$Σ^*$, the set of all strings over the alphabet~$Σ = ❴\cc{a},\cc{b},\cc{c},\cc{d},\cc{e},~\cc{f}❵$}
                  \end{equation}
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[<+->]{Recursive definition of the set of non-empty strings}
  \squeezeSpace
  Given an alphabet~$Σ$, the set~$Σ⁺$ is defined by
  \begin{Indent}
    \begin{description}[Concatenation]
      \item [All letters] All letters in~$Σ$ are present in~$Σ⁺$
      \begin{equation}
        ℓ∈Σ⇒ℓ∈Σ⁺
        \synopsis{Atoms of~$Σ⁺$}
      \end{equation}
      \item [Concatenation] Set~$Σ⁺$ is closed under concatenation:
      \begin{equation}
        α,β∈Σ⁺⇒αβ∈Σ⁺
        \synopsis{Concatenation constructor of~$Σ⁺$}
      \end{equation}
      where~$αβ$ is the string obtained by concatenating~$α$ and~$β$
      \item [Minimality] there are no other members of~$Σ⁺$
      \begin{equation}
        γ∈Σ⁺⇒\text{$γ∈Σ$ \emph{or}~$∃α,β∈Σ⁺∙γ=αβ$}
        \synopsis{All members of~$Σ⁺$ are either atomic strings, or constructed by concatenation from other strings in~$Σ⁺$}
      \end{equation}
    \end{description}
  \end{Indent}
\end{frame}

\begin{frame}[<+->]{Other recursive definitions of the set of strings}
  \begin{itemize}
    \item There are many equivalent definitions of~$Σ⁺$
    \item All these definitions must be recursive
    \item A string~$α∈Σ⁺$ can be constructed in several ways by the above definition
    \item There is an alternative definition of~$Σ⁺$ by which every~$α∈Σ⁺$ has a single, unique construction.
  \end{itemize}
\end{frame}

\begin{frame}[<+->]{Set of strings over an alphabet---defined recursively}
  \WD[0.75]{Set of strings over an alphabet}{%
    Given an alphabet~$Σ$, the set~$Σ^*$ is defined by
    \begin{itemize}
      \item~$ε$, the empty string is in~$Σ^*$,
      \begin{equation*}
        ε ∈ Σ^*
      \end{equation*}
      \item if~$ℓ$ is a letter,~$ℓ∈Σ$, and~$α∈ Σ^*$ is a string, then
            \begin{equation*}
              ℓα∈ Σ^*,
            \end{equation*}
            where~$ℓα$ is the string obtained by prefixing letter~$ℓ$ to string~$α$
      \item there are no other members of~$Σ^*$
    \end{itemize}
  }
\end{frame}

\begin{frame}{Examples of regular expressions}
  \begin{table}[H]
    \begin{centering}
      \coloredTable
      \begin{tabular}{rl}
        \toprule
        \large\textbf{RE}                               & \large~$S⊆Σ^*$ ⚓⏎
        \midrule
        \cc{a}                                          & ⚓$❴\cc{a}❵$⚓⏎
        \cc{b}                                          & ⚓$❴\cc{b}❵$⚓⏎
        $ε$                                            & ⚓$❴ε❵$⚓⏎
        $\cc{ab}$                                       & ⚓$❴\cc{ab}❵$⚓⏎
        $\cc{a}|\cc{b}$                                 & ⚓$❴\cc{a},\cc{b}❵$⚓⏎
        $\cc{a}^*$                                      & ⚓$❴ε,~\cc{a}, \cc{aa}, \cc{aaaa},… ❵$⚓⏎
        $(\cc{da}|\cc{ba})~\cc{a}^*(\cc{d}|\cc{b})$     & ⚓$❴\cc{bab}, \cc{bad}, \cc{dab}, \cc{dad}, \cc{baab}, \cc{baad},…❵$⚓⏎
        $(\cc{a}|\cc{b}|\cc{c}|\cc{d}|\cc{e}|\cc{f})^*$ & ⚓$Σ^*$⏎
        \bottomrule
      \end{tabular}
    \end{centering}
    \Caption
  \end{table}
\end{frame}

\begin{frame}[<+->]{Regular expressions as a recursively defined set}
  Given an alphabet~$Σ$, the set~$\textbf{RE}(Σ)$ is defined by:
  \begin{description}[Kleene closure]
    \item [All strings] \begin{equation}
    Σ^*⊆\textbf{RE}(Σ)
    \synopsis{All strings can be thought of as regular expressions}
    \end{equation}
    \item [Alternation] \begin{equation}
    e₁,e₂∈\textbf{RE}(Σ)⇒(e₁|e₂) ∈ \textbf{RE}(Σ)
    \synopsis{Regular expressions constructor I: \emph{alternation}}
    \end{equation}
    \item [Concatenation] \begin{equation}
    e₁,e₂∈\textbf{RE}(Σ)⇒(e₁e₂) ∈ \textbf{RE}(Σ)
    \synopsis{Regular expressions constructor II: \emph{concatenation}}
    \end{equation}
    \item [Kleene closure] \begin{equation}
    e∈\textbf{RE}(Σ)⇒(e^*) ∈ \textbf{RE}(Σ)
    \synopsis{Regular expressions constructor III: \emph{Kleene closure}}
    \end{equation}
  \end{description}
\end{frame}

\begin{frame}{Semantics can be recursively defined as well}
  The semantics of~$e ∈ \textbf{RE}(Σ^*)$ is a set~$S(e)$,~$S⊆Σ^*$
  \begin{itemize}
    \item Strings
          \begin{equation*}
            e ∈ Σ^*⇒S(e) = ❴e❵
          \end{equation*}
    \item Alternation:
          \begin{equation*}
            e = (e₁|e₂)⇒S(e) = S(e₁)∪S(e₂)
          \end{equation*}
    \item Concatenation:
          \begin{equation*}
            e = (e₁e₂)⇒S(e) = ❴αβ\,|\, \text{$α∈S(e₁)$ \emph{and}~$β∈S(e₂)$}
          \end{equation*}
    \item Kleene closure:
          \begin{equation*}
            e = (e')^*⇒S(e) = \bigcup_{i=0}^∞S\big(\overbrace{e'⋯e'}^{\text{$i$ times}}\big)
          \end{equation*}
  \end{itemize}
\end{frame}

\begin{frame}{Syntactic sugaring ＆ other variations in regular expression}
  \squeezeSpace
  \begin{itemize}[<+->]
    \item RE were adopted in various systems, including text editors, languages for text processing, shell scripts.
    \item adoptions varying syntax for the same underlying concept
    \item most adoptions offer syntactic sugaring\medskip\par
  \end{itemize}
          \begin{table}[H]
              \begin{adjustbox}{}
              \coloredTable
              \begin{tabular}{llp{33ex}}
                \toprule
                \textbf{Sugar}  & \textbf{Meaning}           & \textbf{Semantics}⚓⏎
                \midrule
                \cc{[a-z]}⚓              & inclusive range range & letters~\cc{a} through~\cc{z}⚓⏎
                \cc{[\^{}0-9]}⚓          & exclusive range & all characters in the alphabet, except digits~\cc{0} through~\cc{9}⚓⏎
                \cc{a?}⚓                 & optional & $❴\cc{a},ε❵$⚓⏎
                \cc{\textvisiblespace+}⚓ & one or more & one or more spaces{⚓}⏎
                \cc{UPPER = [A-Z]}⚓      & naming & name a RE to be used in the definition of other REs⚓⏎
                \cc{\$❴UPPER❵[a-z]}⚓ & using name & upper case letter followed by a lower case letter⏎
                \bottomrule
              \end{tabular}
          \end{adjustbox}
            \Caption[Typical syntactical sugar with regular expressions]
          \end{table}
          \squeezeSpace[1]
          \pause \textbf{Note:} recursive use of names is never allowed.
\end{frame}

\begin{frame}[<+->]{Three components of a recursive definition}
  \begin{enumerate}
    \item \textbf{\emph{Atoms.}} e.g., the empty string is in~$Σ^*$
    \item \textbf{\emph{Constructors.}} how to make \emph{compound} members out of the \emph{atoms} and compound members constructed previously.
    \item \textbf{\emph{Minimality.}} usually implicit{⚓}, but can be phrased as
          \begin{itemize}
            \item The set has no members other than the atoms or the compound members constructed by the construction rules.
            \item The set is the \emph{intersection} of all sets which are consistent with the atoms and the construction rules specification.
            \item The set is the smallest set that is consistent with the atoms and the construction rules specification.
          \end{itemize}
  \end{enumerate}
\end{frame}

\begin{frame}[<+->]{Recursively defined sets in PLs}
  \begin{itemize}
    \item \emph{Arithmetical expressions.}
          \begin{description}[Constructors]
            \item [Atoms] literals, references to named entities.…
            \item [Constructors] mathematical operators, user-defined functions,…
          \end{description}
    \item \emph{Executable statements (commands) in~\CPL.}
          \begin{description}[Constructors]
            \item [Atoms] assignment, \kk{return},…
            \item [Constructors] \kk{if}, \kk{for}, \cc{❴…❵},…
          \end{description}
    \item \emph{Types in~\CPL.}
          \begin{description}[Constructors]
            \item [Atoms] \kk{int}, \kk{char},…
            \item [Constructors] aka \emph{type constructors}
          \end{description}
          \begin{itemize}
            \item ‟points to”,
            \item ‟array of”,
            \item ‟record with fields”, \emph{and},
            \item ‟function taking type~$τ$ and returning type~$σ$”.
            \item …
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Example: types in \Java}
  \begin{itemize}
    \item \Java's types are recursively defined.
    \item Type constructors are e.g., \kk{class},
          array, and \kk{enum}.
    \item Atomic types make the recursion base
    \item Atomic type are denoted in \Java by \emph{reserved words}:\medskip
          \begin{table}[H]
            \begin{centering}
              \coloredTable
              \begin{tabular}{ll}
                \toprule
                \textbf{Kind}  & \textbf{Types}⏎
                \midrule
                Integral types & \kk{byte}, \kk{short}, \kk{int}, \kk{long}⏎
                Floating types & \kk{float}, \kk{double}⏎
                Other types    & \kk{boolean}, \kk{char}⏎
                \bottomrule
              \end{tabular}
              \Caption[Atomic types in \Java]
            \end{centering}
          \end{table}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Compound \vs atomic members}
  \begin{description}[Compound member]
    \item [Atomic member] indivisible, has no components which are members
    \item [Compound member] has smaller components which are members
  \end{description}
  \begin{Figure}[A \protect\Pascal compound command with two atomic commands in it]
    \begin{adjustbox}{}
      \input{Tikz/compound-command}
    \end{adjustbox}
  \end{Figure}
\end{frame}

\newcommand\atomic[1]{{\normalsize \ovalbox{#1}}}

\begin{frame}{Decomposing a compound expression}
  \begin{Figure}
    \begin{adjustbox}{}
      \ovalbox{%
        {\Large
          \cc{%
            Sin(
            {\large \ovalbox{(\atomic a + \atomic 3)}
              * \ovalbox{(\atomic a + \atomic c)}} )
          }
        }
      }
    \end{adjustbox}
  \end{Figure}
  Some (but not all) of the compound expressions in the decomposition
  tree of the largest compound expression are marked as well.
\end{frame}

\begin{frame}{Observations}
  \begin{itemize}[<+->]
    \item in an atomic command may contain a compound expression
    \item this does not make the command less atomic
    \item an expression never contains commands (at least not in \Pascal)
    \item constructors are denoted by
          keywords as well
    \item these keywords can be thought of punctuation
    \item or as sort of ‟names” of the constructors
  \end{itemize}
\end{frame}

\subunit[ebnf]{EBNF}

\begin{frame}[fragile]{\protect\textbf Extended \protect\textbf Backus-\protect\textbf Naur \protect\textbf form (EBNF)}
  \begin{PASCAL}
<if-stmt> = if <expression> then <statement> [ else <statement> ]
  \end{PASCAL}
  \begin{itemize}
    \item A meta-notation for describing the grammar of a language
          \begin{itemize}
            \item Terminals = actual legal strings, written as is, or inside~$"+"$
            \item Nonterminals = concepts of the language, written~$<$program$>$ or
                  \underline{program} or \emph{program} in different variants
            \item Rules = expanding a non-terminal to a series of nonterminals and Ts
          \end{itemize}
    \item One nonterminal is designated as the start of any \emph{derivation}
    \item A sequence of terminals not derivable from start symbol by rules of the grammar is illegal
          \item~|~is choice among several possibilities
          \item~\cc{[}…~\cc{]} enclose optional constructs
    \item a pair of ‟❴” and ‟❵” encloses zero or more repetitions
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Example of an EBNF}
  \begin{TWOCOLUMNS}
    \begin{Code}[capture=minipage, width=25ex, watermark text=]{JAVA}{Terminals}
v n + - ( )
    \end{Code}
    \medskip
    \begin{Code}[capture=minipage,width=25ex, watermark text=]{JAVA}{Nonterminals}
<a> <m> <F> <E> <T>
    \end{Code}
    \medskip
    \begin{Code}[capture=minipage,width=25ex, watermark text=]{JAVA}{\mbox{Start Symbol}}
<E>
    \end{Code}
    \MIDPOINT
    \begin{Code}[capture=minipage,width=25ex, watermark text=]{JAVA}{Rules}
<a> = + | -
<m> = * | /
<F> = v | n
<F> = ( <E> )
<E> = <T> {<a> <T>}
<T> = <F> {<m> <F>}
    \end{Code}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile]{More readable way for writing an EBNF}
  The common way for presenting an EBNF
  \begin{itemize}
    \item Employ meaningful names
    \item ‟Rules” is the only section; no
          \begin{itemize}
            \item terminals list
            \item non-terminals list
            \item definition of a start symbol
          \end{itemize}
  \end{itemize}
  \begin{Code}[watermark text=EBNF]{JAVA}{Context free grammar for expressions}
<expression> = <term> {<add-op> <term>}
<term> = <factor> {<mult-op> <factor>}
<factor> = <variable-name>
         | <number>
         | ( <expression> )
<add-op> = + | -
<mult-op> = * | /
  \end{Code}
\end{frame}

\begin{frame}[fragile]{Interpretation of the ‟expression” grammar}
  \begin{Code}[watermark text=EBNF]{JAVA}{Context free grammar for expressions}
<expression> = <term> {<add-op> <term>}
<term> = <factor> {<mult-op> <factor>}
<factor> = <variable-name>
         | <number>
         | ( <expression> )
<add-op> = + | -
<mult-op> = * | /
  \end{Code}
  \begin{itemize}
    \item Terminals never occur at the left hand side of rules:~\cc{+},~\cc{-},~\cc{*} and~\cc{/}.
    \item Non-terminals should always occur at the left hand side rules:
          \begin{itemize}
            \item \cc{\textless expression \textgreater}
            \item \cc{\textless term\textgreater}
            \item \cc{\textless factor\textgreater}
          \end{itemize}
    \item Start symbol is \cc{\textless expression\textgreater}
    \item Forget about \cc{\textless number\textgreater} and \cc{\textless variable-name\textgreater} for now.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Understanding the ‟expression” grammar}
  \begin{Code}[watermark text=EBNF]{JAVA}{Context free grammar for expressions}
<expression> = <term> {<add-op> <term>}
<term> = <factor> {<mult-op> <factor>}
<factor> = <variable-name>
         | <number>
         | ( <expression> )
<add-op> = + | -
<mult-op> = * | /
  \end{Code}
  \begin{itemize}[<+->]
    \item Is~$a + 2 / b - c * 7~$ a legal expression?
    \item Yes, because there is a sequence of rule applications, starting
          from~\cc{\textless expression\textgreater} that yields this string
          (these can be drawn as a ‟syntax tree", also called ‟parse tree”)
    \item How about~$a * (b +c)$?
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Many variants for writing an EBNF}
  EBNFs are often written in a form intended to be readable, but only
  to the educated reader:
  \begin{Code}[watermark text=EBNF]{JAVA}{Context free grammar for expressions (another EBNF
        syntactical variant)}
Expression ::= Term (('+' | '-') Term)*
Term ::= Factor (('*' | '/') Factor )*
Factor ::= Variable-Name
        | Number
        | '(' Expression ')'
  \end{Code}
  \begin{itemize}
    \item First rule defines the start symbol
    \item Terminals never occur in the left
    \item Use more RE-like syntax for right hand of rules
    \item Terminals show between quotes
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Terminals can be regular expression as well}
  Don't forget \cc{Variable-name} and \cc{Number}
  \begin{itemize}
    \item Can potentially be specified in EBNF
    \item Usually have no recursion in them
    \item Are usually written as regular-expression
    \item Are though of as \emph{tokens} or \emph{non-terminals}
  \end{itemize}
  \begin{Code}[watermark text=EBNF]{JAVA}{Context free grammar for expressions with regular expressions
    for tokens}
Expression ::= Term (('+' | '-') Term)*
Term ::= Factor (('*' | '/') Factor )*
Factor ::= Variable-Name
        | Number
        | '(' Expression ')'
Variable-Name := [a-zA-Z][a-zA-Z0-9]*
Number := [+|-]? [0-9]+
  \end{Code}
\end{frame}

\begin{frame}[fragile]{BNF \vs EBNF}
  only strings of (terminals/non-terminals) can be used on the left hand side;
  no regular expressions in the original \textbf Backus \textbf Naur \textbf Form
  \begin{Code}[watermark text=BNF]{JAVA}{Context free grammar for expressions (plain BNF)}
Expression = Terms
Terms: Term
Terms: Term Addition Terms;
Term: Factors;
Factors: Factor:
Factors: Factor Multiplication Factor;
Factor: Variable-Name;
Factor: Number;
Factor: '(' Expression ')';
Addition: '+';
Addition: '-';
Multiplication: '*';
Multiplication: '/';
  \end{Code}
\end{frame}

\begin{frame}{Ambiguity in context free grammars}
  If there is a sequence of terminals with more than one derivation tree.
  \begin{itemize}
    \item Syntactical ambiguity often leads to ambiguious semantics, since there are several
          possible ways to interpret the input.
    \item Good PL design avoids ambiguity
    \item It is algorithmically impossible to determine whether a BNF gives rise to ambiguity
  \end{itemize}
\end{frame}

\begin{frame}{Expressive power of context free grammars}
  Some syntactical cannot be expressed even with EBNF\ignore\@. Examples
  \begin{itemize}
    \item Every variable used is previously declared
    \item The number of arguments in a procedure call equals the number of arguments in the declaration of the procedure
  \end{itemize}
  Much more on grammars and identifying legal programs you will learn in the courses \emph{Automata and Formal Languages and Compilation}
\end{frame}

\afterLastFrame

\exercises
\begin{enumerate}
  \item Let~$Σ^{**}$ be the set of strings over the alphabet~$Σ^*$.
        \begin{enumerate}
          \item give a recursive definition for~$Σ^{**}$.
          \item explain why there is an exponential number of strings in~$Σ^{**}$ for every string in~$Σ^*$
          \item explain why the cardinalities of~$Σ^{**}$ and~$Σ^{*}$ is the same.
        \end{enumerate}
  \item Give a recursive definition for~$Σ⁺$, the set of strings over the alphabet~$Σ^*$.
  \item Show that
        \begin{equation*}
          \textbf{RE}(Σ) = \textbf{RE}\left(\textbf{RE}(Σ)\right)
        \end{equation*}
  \item Give set theoretical considerations why most subsets of~$Σ^*$
        cannot be described as regular expressions.
  \item Employ set theoretical considerations to
        \begin{enumerate}
          \item explain why~$Σ^∞$, the set of all \alert{infinite} strings over~$Σ$,
                has no finite recursive definition, i.e., a recursive definition in which
                both the number of atoms and the number of constructors is finite.
          \item determine whether there is such a recursive definition in which only the number of
                atoms is infinite? which cardinality should it be?
          \item determine whether there is such a recursive definition in which only the number of
                constructors is infinite? which cardinality should it be?
          \item Give an example of a formal language which is not a PL?
        \end{enumerate}
\end{enumerate}

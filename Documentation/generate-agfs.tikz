\begin{tikzpicture}
  \tikzstyle{pdf} = [fill=red!20,anchor=west]
  \tikzstyle{lua} = [fill=yellow!50,ellipse,anchor=west,node contents=\textsc{Lua}\LaTeX,draw]
  \tikzstyle{arrow} = [draw,->,>=latex,shorten >=2pt]

\node (0-0) [fill=cyan!20,draw] {\cc{0-0.tex}};
\node (title) [fill=blue!20,draw,anchor =east, above=6 ex of 0-0.west] {\cc{0-0-title.tex}};
\node (body) [fill=teal!20,draw,anchor =east,below=6 ex of 0-0.west] {\cc{0-0-body.tex}};
\node (lua) [lua,right=6 ex of 0-0,label=\cc{./+init}];
\node (pdf) [pdf,right=6 ex of lua] {\cc{0-0.pdf}};

\begin{scope}[start chain=going {at= (\tikzchainprevious.west),shift=(-90:1)},every node/.style={fill=green!30,on chain,align=left, anchor=west}]
\node (out1) [ xshift=5ex, anchor=west,below=6 ex of lua.east] {\cc{\_article-+-+.tex}};
\node (out2) [anchor=west] {\cc{\_article-1-0-preliminaries.tex}};
\node (out3) [anchor=west] {\cc{\_article-1-2-motivation.tex}};
\node (out4) [anchor=west] {\cc{\_article-1-4-schedule.tex}};
\node (out5) [anchor=west] {\cc{\_article-1-1-administration.tex}};
\node (out6) [anchor=west] {\cc{\_article-1-3-hello.tex}};
\node (out7) [anchor=west] {\cc{\_article-1-+-preliminaries.tex}};
\end{scope}

\draw[arrow] (title) --(0-0);
\draw[arrow] (body) --(0-0);
\draw[arrow] (0-0) --(lua);
\draw[arrow] (lua) --(pdf);
\draw[arrow] (lua) |-(out1.west);
\draw[arrow] (lua) |-(out2.west);
\draw[arrow] (lua) |-(out3.west);
\draw[arrow] (lua) |-(out4.west);
\draw[arrow] (lua) |-(out5.west);
\draw[arrow] (lua) |-(out6.west);
\draw[arrow] (lua) |-(out7.west);

\draw[arrow] (out1.east) -- ($(out1.west)+(43ex,0) $) node (l1) [lua];
\draw[arrow] (out2.east) -- ($(out2.west)+(43ex,0) $) node (l2) [lua];
\draw[arrow] (out3.east) -- ($(out3.west)+(43ex,0) $) node (l3) [lua];
\draw[arrow] (out4.east) -- ($(out4.west)+(43ex,0) $) node (l4) [lua];
\draw[arrow] (out5.east) -- ($(out5.west)+(43ex,0) $) node (l5) [lua];
\draw[arrow] (out6.east) -- ($(out6.west)+(43ex,0) $) node (l6) [lua];
\draw[arrow] (out7.east) -- ($(out7.west)+(43ex,0) $) node (l7) [lua];

\path[arrow] (l1.east) -- +(4ex,0)  node[pdf] {\cc{\_article-+-+.pdf}};
\path[arrow] (l2.east) -> +(4ex,0)  node[pdf] {\cc{\_article-1-0-preliminaries.pdf}};
\path[arrow] (l3.east) -> +(4ex,0)  node[pdf] {\cc{\_article-1-2-motivation.pdf}};
\path[arrow] (l4.east) -> +(4ex,0)  node[pdf] {\cc{\_article-1-4-schedule.pdf}};
\path[arrow] (l5.east) -> +(4ex,0)  node[pdf] {\cc{\_article-1-1-administration.pdf}};
\path[arrow] (l6.east) -> +(4ex,0)  node[pdf] {\cc{\_article-1-3-hello.pdf}};
\path[arrow] (l7.east) -> +(4ex,0)  node[pdf] {\cc{\_article-1-+-preliminaries.pdf}};
\end{tikzpicture}

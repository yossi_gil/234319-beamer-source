\subunit{Generators}
\begin{frame}[fragile=singleslide,label=current]{A simple \protect\Python generator}
  A generator is defined just like a function, except that it uses \kk{yield} instead of \kk{return}
  \begin{code}{PYTHON}
def countdown(n):
  while n >= 0:
    yield n
    n -= 1
  \end{code}
  Now,⏎
  \begin{Code}[output={10 … 9 … 8 … 7 … 6 … 5 … 4 … 3 … 2 … 1 … 0 …⏎Happy new year! X! X! X!}]{PYTHON}{on December \nth{31}, \ccn{23:59:50}:}
for i in countdown(10):
  print i, ".¢¢.¢¢.",
print "Happy new year!", "X!", "X!", "X!"
  \end{Code}
\end{frame}

\begin{frame}[fragile=singleslide,label=current]{A generator built on top of another}
  \cc{range} is a builtin generator; \cc{range($n$)} yields~$0,1,…,n-1$.
  \begin{Code}{PYTHON}{A generator yielding an arithmetic progression}
def arithmeticProgression(a1,d,n):
  for i in range(n):
    yield a1 + i*d
  \end{Code}
  Using the ‟compound” generator
  \begin{code}[output={3 7 11 15 19},minipage,width=44ex]{PYTHON}
for ai in arithmeticProgression(3, 4, 5):
  print ai,
  \end{code}
\end{frame}

\begin{frame}[fragile=singleslide]{Functions \vs generators in \Python}
  \squeezeSpace[2]
  \begin{session}
Python 2.7.8 (default, Oct 20 2014, 15:05:19)
[GCC 4.9.1] on linux2
Type "help", "copyright", "credits" or "license"
   for more information.
>>> range
<built-in function range>
  \end{session}
  Not much distinction between functions and generators:
  \begin{TWOCOLUMNSc}[-10ex]
    \begin{lCode}{PYTHON}{Definition}
def fu(n):
  return n
def countdown(n):
  while n >= 0:
    yield n
    n -= 1
    \end{lCode}
    \MIDPOINT
    Checking out values of \cc{fu} and \cc{countdown}:
    \begin{session}
>>> fu
<function fu at 0x7fc0e82f47d0>
>>> countdown
<function countdown at 0x7fc0e82f4848>
    \end{session}
  \end{TWOCOLUMNSc}
\end{frame}

\begin{frame}[fragile=singleslide]{Functions \vs generators in \protect\Python}
  \squeezeSpace
  ¶A generator looks like a function, but behaves like an iterator:
  \squeezeSpace[-2]
  \begin{table}[H]
    \begin{adjustbox}{}
      \coloredTable
      \begin{tabular}{T{0.16}T{0.44}T{0.38}}
        %* \end{tabular}
        \toprule
        & \textbf{Function \cc{fu}} & \textbf{Generator \cc{gen}}⏎
        \midrule
        \alert{Definition:} &
        \vspace{-4ex}
        \begin{minipage}[t]{\linewidth}
          \begin{lcode}{PYTHON}
def fu(x):
  return x
          \end{lcode}
        \end{minipage}
        &
        \vspace{-4ex}
        \begin{minipage}[t]{\linewidth}
          \begin{lcode}{PYTHON}
def gen(x):
  yield x
          \end{lcode}
        \end{minipage}
        ⏎
        \alert{Invoking as function:} &
        \vspace{-4ex}
        \begin{minipage}[t]{\linewidth}
          \begin{lcode}[output=3]{PYTHON}
fu(3)
          \end{lcode}
        \end{minipage}
        &
        \vspace{-4ex}
        \begin{minipage}[t]{\linewidth}
          \begin{lcode}[output=<generator object gen at 0x7f65af3eae60>]{PYTHON}
gen(3)
          \end{lcode}
        \end{minipage}
        ⏎
        \alert{Invoking as generator:} &
        \vspace{-4ex}
        \def\out{
          Traceback (most recent call last):⏎
          File "<stdin>", line 1, in <module>⏎
        TypeError: 'int' object is not iterable}
        \begin{minipage}[t]{\linewidth}
          \begin{lcode}[output={\out}]{PYTHON}
for i in fu(3):
  print i
          \end{lcode}
        \end{minipage}
        &
        \vspace{-4ex}
        \begin{minipage}[t]{\linewidth}
          \begin{lcode}[output=3]{PYTHON}
for i in gen(3):
  print i
          \end{lcode}
        \end{minipage}
        ⏎
        \bottomrule
        %* \begin{tabular}
      \end{tabular}
    \end{adjustbox}
    \Caption
    \label{Table:functions-vs-generators-in-python}
  \end{table}
\end{frame}

\begin{frame}[fragile=singleslide]{Generators \vs iterators}
  \begin{TWOCOLUMNS}
    \par
    \alert{Generator is a function…}
    \WD[0.94]{Generator}{%
      A \Red{generator} is a function that \emph{produces}
      a \emph{sequence} of results, instead of \emph{returning} a single result.
    }
    The sequence may be empty, or infinite.
    \MIDPOINT
    \par
    \alert{Iterator is an object}…
    \WD[0.94]{Iterator}{%
      An \Red{iterator} is an object which can be used to
      traverse a datastructure such as a list.
    }
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile=singleslide,label=current]{Generators \vs functions}
  \begin{table}[H]
    \begin{adjustbox}{}
      \coloredTable
      \begin{tabular}{T{0.25}T{0.37}T{0.37}}
        %* \end{tabular}
        \toprule
        & \textbf{Function} & \textbf{Generator}⏎
        \midrule
        \alert{＃ results:} & {}$1$ & {}$0,1,2,…,∞$⏎
        \alert{Terminology:} &
        \begin{itemize}
          \item return
          \item call / invoke
        \end{itemize}
        &
        \vspace{-3.5ex}
        \begin{itemize}
          \item yield / produce
          \item initiate
          \item retrieve
        \end{itemize}
        ⏎
        \bottomrule
        %* \begin{tabular}
      \end{tabular}
    \end{adjustbox}
    \Caption
  \end{table}
  \begin{itemize}
    \item A function must return one value, even if it has nothing to offer.
    \item Generators have two kinds of ‟invocations”:
          \begin{description}
            \item [initiate] start the production process
            \item [retrieve] obtain the next element from the sequence
          \end{description}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide,label=current]{Generators in \protect\Icon}
  \squeezeSpace[2]
  In \Icon, \emph{all} expressions are generators:
  \begin{description}[‟Product of sequences”]
    \item [Singleton generators]~\cc{3} is a generator of the sequence \[❴3❵\]
    \item [Or generator] \cc{3|4|5} is a generator of the sequence \[❴ 3,4,5❵\]
    \item [Depth first generation] \cc{(30|40|50)+(1|2)} is a generator of the sequence \[❴31, 32, 41, 42, 51, 52 ❵\]
    \item [‟Product” of sequences] \cc{(1|2|3)*f() + (1|2) * g()} is a generator of a sequence with~$6⨉n⨉m$ values, where
    \begin{itemize}
      \item~$n$ is the number of values that~$f$ yields
      \item~$m$ is the number of values that~$g$ yields
    \end{itemize}
  \end{description}
\end{frame}

\begin{frame}[fragile=singleslide,label=current]{Comparison generators}
  \squeezeSpace[2]
  Comparison generators realize arithmetical conditions
  \begin{itemize}
    \item \cc{i == j} yields \[❴j❵\] if~$i=j$; otherwise, the empty sequence \[❴❵\]
    \item \cc{i == (3|4|5)} succeeds if~$i∈❴3,4,5❵$; it yields either one of
          \begin{itemize}
            \item~$❴3❵$
            \item~$❴4❵$
            \item~$❴5❵$
            \item~$❴❵$
          \end{itemize}
          depending on the value of~\cc{i}.
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide,label=current]{Neat mathematical syntax with comparison generators}
  \squeezeSpace
  A familiar mathematical notation:
  \begin{equation}
    0≤i<j≤n.
    \synopsis{The mathematical condition ‟captures” by the \Icon expression \cc{0 <= i < j <= m}}
  \end{equation}
  \begin{description}[\Java: \Red{cumbersome}]
    \item [\Java: \Red{cumbersome}] \cc{0 <= i ＆＆ i < j ＆＆ j <= n}
    \item [\Icon: \alert{elegant}] \cc{0 <= i < j <= n} yields:
    \begin{itemize}
      \item~$❴n❵$ if the condition holds
      \item~$❴❵$ if the condition fails
    \end{itemize}
  \end{description}
  \begin{Code}[watermark text=\Icon,minipage,width=43ex]{PASCAL}{Same \ccn{i <= i ＆＆ i < j ＆＆ j <= n} with added parenthesis}
(((0 <= i) < j) <= n)
  \end{Code}
  Each conditional expression yields its second argument as input to the next conditional
  \begin{itemize}
    \item \cc{0 <= i} yields either~$❴i❵$ or~$❴❵$
    \item \cc{0 <= i < j} yields either~$❴j❵$ or~$❴❵$
    \item \cc{0 <= i < j <= n} yields either~$❴n❵$ or~$❴❵$
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide,label=current]{Control with generators}
  Iteration and conditional are both generating contexts:
  \begin{itemize}
    \item \kk{if} statement:
          \begin{itemize}
            \item Execute body if a value is yielded
            \item Triggered at most once, by the \alert{first} yielded value
          \end{itemize}
    \item \kk{while} statement:
          \begin{itemize}
            \item Execute body for each value yielded
            \item Triggered by \alert{all} yielded values
          \end{itemize}
          \begin{Code}[watermark text=\Icon]{PASCAL}{A simple \Icon loop}
while line := read() do
  write(line)
    \end{Code}
    \begin{Code}[watermark text=\Icon,width=30ex]{PASCAL}{A loop with empty body}
while write(read())
    \end{Code}
  \end{itemize}
\end{frame}

\begin{frame}[fragile,label=current]{Generators in \CSharp}
  \squeezeSpace
  In statically typed PLs, generators call for sophisticated typing
  \begin{code}{CSHARP}
using System; using System.Collections.Generic;
¢¢
public class Program {¢¢
    static void Main() {¢¢
      foreach (int p in Powers(2, 30)) {¢¢
        Console.Write(p); Console.Write(" ");
      }
    }
    public static IEnumerable<int>
      Powers(int base, int count) {¢¢
        int result = 1;
        for (int i = 0; i < count; ++i) 
          yield return result *= base;
        yield break; // optional
    }
}
  \end{code}
  Type of function \cc{Powers} is
  \qq{\cc{int*int->IEnumerable<int>}}
\end{frame}

\begin{frame}[fragile]{Summary of generators}
  \begin{itemize}
    \item Like closures, generators \alert{close} over their environment
    \item Activation records of generators, include also the
          \begin{description}
            \item [Saved context of the callee] makes it possible for the callee (the generator) to resume its state
          \end{description}
    \item For convenience, we shall call the term ‟generating record” for this augmented activation record.
    \item A generating record supports three conceptual operations:
          \begin{description}[resumable()]
            \item [resume()] resume execution from the last saved context.
            \item [resumable()] determine whether resumption is possible.
            \item [retrieve()] obtain the value of the generator
          \end{description}
    \item A ‟generation contexts” tries to exhaust the generator using these
          three operations.
  \end{itemize}
  In many ways, generators are just like predicates of \Prolog.
\end{frame}

\subunit{Coroutines}
\begin{frame}[label=current]{What are coroutines?}
  \squeezeSpace
  \setbeamercovered{dynamic}
  \WD[0.74]{Coroutine}{A \Red{coroutine} is a function, which can
    \begin{itemize}
      \item \textbf{suspend} anywhere in its execution by executing a \kk{yield} command.
      \item \textbf{resume} a suspended execution, proceeding to the command
            that follows the suspending \kk{yield}
    \end{itemize}
  }
  Coroutines \vs generators:
  \begin{itemize}
    \item Generators are also called semicoroutines
    \item Generator can only \cc{yield} to its caller
    \item Coroutine typically \cc{yield} to another coroutine
  \end{itemize}
\end{frame}

\begin{frame}[label=current]{Overloading of ‟yield”: two unrelated meanings}
  \squeezeSpace
  Two overloaded meanings of the word ‟\alert{yield}” in English:
  \begin{quote}
    \let\COL=\columnwidth
    \begin{Figure}[Two overloaded meanings of the verby ‟\emph{yield}”]
      \begin{tabular}{cc}
        \alert{to produce}                                               & \alert{to let other take control}⏎
        \adjincludegraphics [max width=0.4\COL]{Graphics/yield-farm.png} & \adjincludegraphics[max width=0.4\COL]{Graphics/yield-sign.png}⏎
      \end{tabular}
    \end{Figure}
  \end{quote}
  In theory of PL:
  \begin{description}
    \item [Generators] \emph{to produce}
    \item [Coroutines] \emph{to let other take control}
  \end{description}
\end{frame}

\begin{frame}[fragile]{Unconvincing example of coroutines}
  \squeezeSpace[3]
  A sow and its two little piglets
  may all die in the pigsty
  \begin{code}[watermark text=pseudo \CSharp]{CSHARP}
void sow() {¢¢ for (;;) {¢¢
   defecate();
   if (hungry) feed();
   if (thirsty) drink();
   yield piglet1;
}}
  \end{code}
  \begin{TWOCOLUMNS}
    \begin{code}[watermark text=pseudo \CSharp]{CSHARP}
void piglet1() {¢¢ for (;;) {¢¢
  defecate();
  if (hungry || thirsty) suck();
  yield piglet2;
}}
    \end{code}
    \MIDPOINT
    \begin{code}[watermark text=pseudo \CSharp]{CSHARP}
void piglet2() {¢¢ for (;;) {¢¢
  defecate();
  if (hungry || thirsty) {¢¢
    suck();
    yield sow;
  }
}}
    \end{code}
  \end{TWOCOLUMNS}
  If \cc{piglet2} is not hungry neither thirsty.
  \begin{itemize}
    \item \cc{sow} may die
    \item \cc{piglet1} will then die
    \item \cc{piglet2} himself will eventually die
  \end{itemize}
\end{frame}

\begin{frame}{Issues with the pigsty}
  It is not clear at all
  \begin{itemize}
    \item how the coroutines are initiated?
    \item how each coroutine gains access to the other?
    \item how can one create multiple instances of the same coroutine?⏎
          (create several active invocations of (say) \cc{piglet2()})
    \item why coroutines are better than plain \alert{threads}?
  \end{itemize}
  So, why the heck do we need these?
\end{frame}

\begin{frame}[label=current]{Coroutines \vs threads}
  Both offer multitasking:
  \begin{description}
    \item [Threads] preemptive; execution can be interrupted at any point
    \item [Coroutine] cooperative; control passed when the
  \end{description}
  Zillions of weird scheduling schemes?
  \begin{description}
    \item [Threads] Yes!
    \item [Coroutine] only with careless programming
  \end{description}
  Race conditions?
  \begin{description}
    \item [Threads] Yes!
    \item [Coroutine] No!
  \end{description}
  Generally speaking, coroutines grant the programmer fine control
  of scheduling, but it may take skill to effectively use this control.
\end{frame}

\begin{frame}[label=current]{The sad story of coroutines}
  \begin{itemize}
    \item invented in the early 60's (if not earlier)
    \item mostly forgotten; often discarded as ‟cumbersome multitasking”
    \item have (partial) implementation in mainstream PLs such
          as~\CPL, \CC, \Java, \Python, \Perl, Object-\Pascal, \Smalltalk and more
    \item implementations did not really catch
  \end{itemize}
  Hence, the poorly designed syntax in the pigsty.
\end{frame}

\begin{frame}[fragile]{Killer application of coroutines}
  \squeezeSpace
  \centering
  {\Large\Red{Event loops:}}
  \begin{TWOCOLUMNS}[-2ex]
    \begin{lCode}[watermark text=Pseudo-\CSharp]{CSHARP}{Events dispatcher}
void dispatcher() {¢¢
  // Yields to appropriate coroutine
  for (;;) {¢¢
    Message m = getMessage();
    push(m); // save message for later
    Area a = findScreenArea(m);
    Coroutines cs = findWindows(a);
    for (Coroutine c: cs) {¢¢
      yield(c);
      if (consumed(m))
        break;
        // otherwise, try another ¢\cc{c}¢
    }
  }
}
    \end{lCode}
    \MIDPOINT
    \begin{lCode}[watermark text=Pseudo-\CSharp]{CSHARP}{A co-routine window function}
void myWindow(Rectangle r) {¢¢
  // Initialize window here, and, then, …
  yield dispatcher;
  for (;;) {¢¢
    switch (Message m = getMessage()) {¢¢
      case M1: ¢…¢
      case M2: ¢…¢
      ¢…¢
      default: // Message is not my responsibility:
        push(m)
    }
    yield dispatcher;
  }
}
    \end{lCode}
  \end{TWOCOLUMNS}
  \medskip
  Useful in browser applications such as \cc{Gmail}
\end{frame}

\subunit{Continuations}
\begin{frame}[label=current]{Continuations}
  Quite an obscure and abstract definition:
  \WD[0.74]{Continuation}{A \Red{continuation} is an abstract representation of the control state of a computer program}
  More plainly, a continuation is making the activation record
  (more generally, the generating record) into a
  first class citizen.
  \begin{itemize}
    \item Invented many years ago
    \item Has full implementation in many dead and obscure PLs
    \item Has partial/non-official implementation in many mainstream PLs
    \item Did not catch (yet!)
  \end{itemize}
\end{frame}

\begin{frame}[label=current]{Data stored in a continuation}
  \begin{description}[Termination status]
    \item [Code] Where the function is stored in memory, its name, and other meta information, e.g., for debugging.
    \item [Environment] Activation record, including back-pointers as necessary.
    \item [CPU state] The saved registers, most importantly, the \cc{PC} register
    \item [Result] Last result \kk{return}ed/\kk{yield}ed by the function
    \item [Termination status] can the continuation be continued
  \end{description}
\end{frame}

\begin{frame}[label=current]{Operations on continuations}
  \squeezeSpace[2]
  \begin{table}[H]
    \begin{adjustbox}{}
      \coloredTable
      \begin{tabular}{T{0.3}T{0.6}}
        \toprule
        \textbf{Operation}          & \textbf{Semantics}⏎
        \midrule
        $c←\cc{initiate}(f(…))$ & Create a new continuation for the call~$f(…)$⏎
        $c'←\cc{clone}(c)$        & Save execution state for later⏎
        $\cc{resumable}(c)$         & Determine whether resumption is possible⏎
        $c←\cc{resume}(c)$        & Resume execution of continuation~$c$⏎
        $\cc{retrievable}(c)$       & Determine whether computation produced a result⏎
        $x←\cc{retrieve}(c)$      & Obtain the produced result of~$c$
        \begin{description}
        \item [generator] a generated value
        \item [continuation] a continuation record
        \end{description}
        ⏎
        \bottomrule
      \end{tabular}
    \end{adjustbox}
    \Caption
  \end{table}
  Continuations are slightly more general than ‟\alert{generation records}”
  in that they may \kk{yield} continuations, rather than just plain values.
\end{frame}

\begin{frame}{Creating continuations by the dispatcher in the event loop example}
  \squeezeSpace
  When user hits a ‟new window” button:
  \begin{enumerate}
    \item Create a rectangle:
          \qq{\cc{Rectangle r = \kk{new} Rectangle(\text{…});}}
    \item Initiate the continuation record (without running it):
          \qq{\cc{Continuation c = initiate(myWindow(r));}}
    \item Let function \cc{myWindow} ‟run” to initialize itself a bit:
          \qq{\cc{c = resume(c);}}
          The function inside~\cc{c} will \kk{yield} to the dispatcher, so we may continue working
    \item Associate rectangle with window:
          \qq{\cc{dictionary.add(r,c);}}
  \end{enumerate}
  Now, the dispatcher may continue waiting for events, and dispatch
  to the right continuation
\end{frame}

\begin{frame}{Dispatcher main loop with continuations}
  \squeezeSpace
  \begin{enumerate}
    \item Obtain a keyboard event, a mouse event, or some other message:
          \qq{\cc{Message m = getMessage();}}
    \item Save this message so it can be consumed by one of our windows coroutines:
          \qq{\cc{push(m);}}
    \item Find the screen area into which this message belongs:
          \qq{\cc{Area a = findScreenArea(m);}}
    \item Find all window coroutines associated with this message
          \qq{\cc{Continuations cs = findWindows(a);}}
    \item Iterate over all of them:
          \qq{\cc{\kk{for} (Continuation c: cs)}}
          \begin{itemize}
            \item Give continuation~\cc{c} an opportunity to consume message~\cc{m}
                  \qq{\cc{c = resume(c);}}
            \item If~\cc{c} is dead, remove it
                  \qq{\cc{\kk{if} (!resumable(c)) dictionary.remove(c);}}
            \item If message consumed, no need to loop further
                  \qq{\cc{\kk{if} (consumed(m)) \kk{break};}}
          \end{itemize}
  \end{enumerate}
\end{frame}

\begin{frame}[label=current]{What can you can with continuations?}
  \begin{itemize}
    \item Good old function calls
    \item Closures
    \item Generators
    \item Exceptions
    \item Coroutines
  \end{itemize}
  With continuations, you can conceal the fact that Web server should be
  stateless;
  (you just provide the client with the server's continuation record)
\end{frame}

\subunit{Summary}
\begin{frame}{Main concepts of this unit}
  \begin{itemize}
    \item Generators: \Python, \CSharp, and \Icon
    \item Coroutines \vs threads,
    \item Continuations
  \end{itemize}
\end{frame}

\begin{frame}[label=current]{Implementation of advanced abstractions}
  \begin{table}[H]
    \begin{adjustbox}{}
      \coloredTable
      \begin{tabular}{ll}
        \toprule
        \textbf{Abstraction} & \textbf{Mechanism}⏎
        \midrule
        Ordinary function    & ⚓Stack frame⏎
        Nested function      & ⚓Stack frame with back pointers⏎
        Call by name         & ⚓Thunks⏎
        Thunks               & ⚓Nested functions⏎
        Closures             & ⚓Activation record on heap⏎
        Generators           & ⚓Generating record⏎
        Coroutines           & ⚓Continuations⏎
        \bottomrule
      \end{tabular}
    \end{adjustbox}
    \Caption[]
  \end{table}
\end{frame}

\afterLastFrame

\exercises
\begin{enumerate}
  \item Refer back to \cref{Table:functions-vs-generators-in-python}.
        Classify each part of the text enclosed by boxes as one of:
        \begin{itemize}
          something that the \Python interpreter takes as \emph{input},
          \item text that the interpreter produces as \emph{output}, and,
          \item \emph{error messages} produced by the
                the interpreter.
        \end{itemize}
  \item What's the difference between coroutines and generators?
  \item Which items stored in activation records are not part of the data
        stored with continuations?
  \item What's non-preemptive multitasking? How is it related to coroutines?
        Which is more general?
  \item Enumerate the data items that continuations have to store.
  \item What's the difference between \kk{static}- and non-\kk{static}
        nested classes?
  \item What's the difference between generators and iterators?
  \item Discuss the restrictions placed on nested classes in \CC.
  \item Discuss the restrictions placed on anonymous functions in Gnu-\CPL.
  \item Given is a language which only has generators, can you use these to
        implement coroutines?
  \item Why do continuations require garbage collection?
  \item What are the two meanings of the word ‟yield”? How are these meaning
        used in generators and
  \item Are data items stored with activation records different in
        \CPL and in \Pascal?
  \item Enumerate the data items stored in an activation record?
  \item What information is contained in coroutines and is missing in closures?
  \item Can one emulate closures with coroutines? Explain.
  \item Are \emph{goroutines} of the \Go PL (look for keyword \kk{go} in the PL's specification) a kind of coroutines?
  \item Experts claim that all ‟implementation of all web services are
        \emph{poor-man's continuations}”. Explain and elaborate.
  \item Can coroutines be implemented with closures? Explain.
\end{enumerate}
\references
\begin{description}\D{Continuation}{https://en.wikipedia.org/wiki/Continuation}
  \D{Coroutine}{https://en.wikipedia.org/wiki/Coroutine}
  \D{Event-loop}{https://en.wikipedia.org/wiki/Event\_loop}
  \D{Generator}{https://en.wikipedia.org/wiki/Generator\_(computer\_programming)}
  \D{Iterator}{https://en.wikipedia.org/wiki/Iterator}
  \D{Nonpreemptive multitasking}{https://en.wikipedia.org/wiki/Nonpreemptive\_multitasking}
  \D{Thread}{https://en.wikipedia.org/wiki/Thread\_(computing)}
\end{description}

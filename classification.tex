\begin{frame}[< +-| hl@+ >]{Criteria for the classification of type systems}
  \setbeamercovered{transparent}
  \begin{description}[Time of Enforcement]
    \item [Existence] Does the language include a type system at all?
    \item [Sophistication level] Assuming a PL has a type system, how rich is it?
    \item [Orthogonality] Discriminatory \vs non-discriminatory
    \item [Strength] How strictly are the typing rules enforced?
    \item [Time of enforcement] What stage is type checking performed? Static \vs dynamic typing
    \item [Responsibility] Is the programmer responsible for type declarations
    or is it the compiler?
    Explicit \vs implicit typing
    \item [Equivalence] When can one type replace another?
    \item [Flexibility] To what extent does the type system restrict the user’s expressiveness? Polymorphic typing?
  \end{description}
  \onslide<+->{}
\end{frame}

\begin{frame}[<+- | @hl+>]{In this section…}
  \setbeamercovered{transparent}
  We will discuss criteria
  \begin{enumerate}
    \item Existence
    \item Sophistication level
    \item Orthogonality
    \item Strength
    \item Time of enforcement,
    \item Responsibility
          \setcounter{temp}{\theenumi}
  \end{enumerate}
  ⚓However, ⚓
  \begin{enumerate}\setcounter{enumi}{\thetemp}
    \item \Red{Equivalence}
    \item \Red{Flexibility}
  \end{enumerate}
  \onslide<+->{\alert{deserve their own \only<+->{\textbf{(fat) }}sections.}}
\end{frame}

\subunit[existence]{Existence ＆ sophistication level}

\begin{frame}[<+-| hl@+>]{Existence of a type system?}{Typed \vs untyped}
  \setbeamercovered{transparent}
  A PL can be… ⚓
  \begin{description}
    \item [Typed]\emph{The set of values can be broken into sets, with more or less
    uniform behavior under the same operation of the values in each set.}
    \Q{\CPL, \Pascal, \ML, \Ada, \Java, and most other PLs}
    \item [Untyped]\emph{Each value has its own unique set of permissible operations,
    and their semantics are particular to the value.}
    \Q{\Lisp, \Prolog, \Mathematica, …}
  \end{description}
  \onslide<+->{}
\end{frame}

\begin{frame}{Example I: \Lisp is an untyped language}
  \squeezeSpace[3]
  In \Lisp…
  \begin{itemize}
    \item All values are \emph{S}-expressions
    \item An \emph{S}-expressions is an unlabeled binary trees, whose leaves are \emph{atoms}
    \item Basic operations:
          \bgroup
          \footnotesize
          \begin{tabular}{|rl|}
            \hline
            \cc{Car}  & extracting left subtree⏎
            \cc{Cdr}  & extracting right subtree⏎
            \cc{Cons} & construct a tree from two subtrees⏎
            \cc{Null} & determine whether a tree is the atom \cc{NIL}⏎
            \hline
          \end{tabular}
          \egroup
  \end{itemize}
  \begin{Block}[minipage,width=0.7\columnwidth]{Errors}
    Legality of operations is determined by tree structure and values at the leaves
  \end{Block}
\end{frame}

\begin{frame}{Example II: \Mathematica is untyped as well}
  \squeezeSpace[2]
  \Mathematica is a language for symbolic mathematics:
  \squeezeSpace[2]
  \begin{TWOCOLUMNS}
    \begin{itemize}[<+- | hl@+>]
      \item Values are symbolic mathematical expressions
      \item Expressions are trees
    \end{itemize}
    \MIDPOINT
    \begin{itemize}[<+- | hl@+>]
      \item All expressions have a \emph{head} and \emph{body}
            \begin{itemize}[<+- | hl@+>]\footnotesize
              \item \emph{head} is a symbol
              \item \emph{body} is a list of expressions
              \item …▄⚓\q{most details are not so interesting}
            \end{itemize}
    \end{itemize}
  \end{TWOCOLUMNS}
  \W[0.69]{The crucial point}{%
    \textbf{Manipulation of an Expression:} still requires
    \begin{itemize}
      \item \emph{check of legality}
      \item \emph{determine of semantics}
    \end{itemize}
    ⚓These are determined by
    \begin{itemize}
      \item \Red{tree structure} of the expression
      \item \Red{values} residing in internal nodes ＆ leaves.
    \end{itemize}
  }
\end{frame}

\begin{frame}{Degenerate type systems}
  \squeezeSpace
  \setbeamercovered{transparent}
  \begin{itemize}[<+-| hl@+>]
    \item Very few (typically one or two) primitive types
    \item Very few type constructors
  \end{itemize}
  \begin{description}[DOS batch language]
    \item <+-> [\BCPL (\CPL’s ancestor)] the only data type is a machine word
    \item <+-> [DOS batch language] the only data type is a string, ⚓\emph{which sometimes look like a file name}
    \item <+-> [Ancient \Fortran]
    \begin{itemize}
      \item <+-> Several ‟full blown” primitive types for scientific computation
      \item <+-> Single type constructor: ‟array'
    \end{itemize}
    \item [\AWK] two data types
    \begin{description}
      \item [strings] \item [numbers] possible interpretation of some strings
    \end{description}
    only one ‟type constructor”:
    \begin{itemize}
      \item \emph{associative array}
      \item No easy way to ‟\emph{create arrays of arrays}”
    \end{itemize}
  \end{description}
  \emph{most scripting PLs including \Bash, and~\CPL-Shell are similar to \AWK}
\end{frame}

\begin{frame}{Sophistication level of the type system}
  \setbeamercovered{transparent}
  \begin{enumerate}[<+-| hl@+>]
    \item no typing
    \item degenerate typing
    \item non-recursive type systems
          \Q{as in \Fortran}
    \item recursive type systems
          \Q{\text{as in \Pascal}}
    \item functions as first-class values
          \Q{as in \ML}
    \item highly advanced type constructors
          \Q{‟\emph{monads}” of \Haskell}
  \end{enumerate}
  \onslide<+->{}
\end{frame}

\begin{frame}{Beyond types ＆ \emph{non-standard} types}
  Modern languages use the typing system to indicate more than just the type returned by a function.
  \begin{description}
    \item [\Java] the type of a method includes a list of the types of
    the exceptions that it can throw.
    \begin{itemize}
      \item If method~$M₁$ declares that it might throw exceptions of
            type~$E$, and method~$M₂$ includes a call to~$M₁$, then either:
      \item The call is inside a \kk{try}…\kk{catch}\cc{($E$)} block, or
            \item~$M₂$ must also declare that it may throw~$E$.
      \item The "catch-or-declare" principle.
    \end{itemize}
    \item [\CC] a \kk{const} method cannot invoke a non-\kk{const} method.
    \item [\Haskell] the type system indicates which functions perform I/O operations.
  \end{description}
  Shared theme: statically detect and prevent potential bugs.
\end{frame}

\subunit[orthogonality]{Orthogonality}

\begin{frame}[fragile]{Orthogonality, discrimination ＆ being second-class }
  \begin{Definition}{Discriminatory type system}
    A type system is \Red{discriminatory} if one of its type
    constructors is ‟\emph{discriminatory}”, i.e.,
    it is applicable to some types, but not to others.
  \end{Definition}
  \begin{code}{CPLUSPLUS}
int a = 3;
int& reference = a; // ¢✓¢
int&& illegal = reference; // ¢✗¢
  \end{code}
  ⚓In \CC, the ‟reference to” type constructor is
  (self) discriminatory.
  ⚓There are types of references to almost everything.
  ⚓But, there are no references to references.
\end{frame}

\begin{frame}{☡Discrimination \vs second-class types}
  \itshape \bfseries
  Once upon a time,
  ⚓in the far~far away kingdom called calligraphic capital~$𝓣$
  ⚓†{when we say ‟kingdom” we really mean ‟\emph{type system}”}, ⚓lived a type named little~$τ$,
  ⚓and as we say in math, little~$τ∈𝓣$.⚓▄ Little~$τ$ was looking for an employment by one of…⚓\Q{$C₁,…,Cₙ∈𝓣$}
  ⚓the type constructors of kingdom calligraphic capital~$𝓣$.⚓
  \begin{description}
    \item [] So, little~$τ$ went to type constructor capital~$C₂$,
    ⚓but capital~$C₂$ said ⚓\Red{No}
    \begin{description}
      \item [] So, little~$τ$ went to type constructor capital~$C₇$,
      ⚓but capital~$C₇$ also said
      ⚓\Red{No}
      \item [] and capital~$C₃$ also said ⚓\Red{No}⚓\item [] and capital~$C₅$ said ⚓\Red{No}⚓\item [] but, then~capital~$C₁$ said ⚓\GREEN{\textbf{Yes}}⚓
    \end{description}
  \end{description}
  so, what did little~$τ$ do?⚓…
\end{frame}

\begin{frame}{☡More on discrimination \vs second-class types}
  \itshape \bfseries
  Little~$τ$ was close to despair.
  ⚓She thought of capital~$C₁$,
  ⚓and then of capital~$C₂$,
  ⚓and,
  ⚓and,
  ⚓all the other~$Cᵢ$
  ⚓who would not employ her,
  ⚓and then she realized that she was not a \Red{first-class type},
  ⚓which means,
  ⚓errghh…
  ⚓that little~$τ$ must be
  ⚓\Q{\Red{\textbf{second-class} type}}
  ⚓\begin{Definition}{second-class types}
  If a type~$τ$ is being discriminated against by
  most (or even just very many) type constructors, then \emph{we tend
    to say} that~$τ$ is a \Red{second-class type}.
  \end{Definition}
  \onslide<+->{}
\end{frame}

\begin{frame}{☡Understanding orthogonality of a PL}
  \setbeamercovered{transparent}
  \bfseries\itshape
  Saying that~$τ$ is a \Red{second-class type} is ⚓cruel and unjust.⚓⏎
  It lets us refrain from
  ⚓accusing so many type constructors as being
  ⚓evil and \Red{discriminatory}.⚓⏎
  ▄
  But placing a ‟blame” on a \emph{second-class types}
  ⚓allows more compact expression and understanding of the
  ⚓\begin{Block}{}
  ⚓\Large language orthogonality
  \end{Block}
\end{frame}

\begin{frame}{Orthogonality in \Pascal}
  \setbeamercovered{transparent}
  \begin{description}[<+-| hl@+>]
    \item [Non-discriminatory type constructor]▄⏎
    you can create \kk{array}s of anything,
    \item [Discriminatory type constructor]▄⏎
    you can create \kk{set}s of \kk{Boolean}s and of \kk{Char},
    but not of \kk{Integer} or of ‟\kk{set of Boolean}”.
    \item [Second-class type]▄⏎
    there are no ‟arrays of functions”, no ‟records of functions”,
    no ‟sets of functions”, no ‟pointers to functions”, etc.,
    \Q{so we must ‟conclude” that \Red{functions} are second-class types.}
  \end{description}
  \onslide<+->{}
\end{frame}

\begin{frame}{First-＆ second-class values in \Pascal}
  \begin{itemize}
    \item \alert{First-class values}
          \qq{Only simple, atomic values: truth values, characters, enumerands, integers, reals, and also pointers.}
    \item \alert{Second-class values}
          \qq{can be passed as arguments, but cannot be stored, or returned, or used as components in other values
            \begin{itemize}
              \item composite values (records, arrays, sets and files): cannot be returned!
              \item procedure and function abstractions
              \item references to variables (unless disguised as pointers)
            \end{itemize}
          }
  \end{itemize}
\end{frame}

\begin{frame}{Apparent contradiction?}
  Above we said, that in \Pascal:
  \begin{itemize}
    \item You can create arrays of anything!
    \item You cannot create arrays of functions!
  \end{itemize}
  Resolution:
  \begin{itemize}
    \item We like to think of the array constructor as being non-discriminatory
    \item We like to think of functions as second-class
    \item We like to allow ‟non-discriminatory” constructors to discriminate against second-class
    \item As we shall see, the ‟fault” lies with functions, not with arrays.
  \end{itemize}
\end{frame}

\begin{frame}{Value manipulation}
  \begin{itemize}
    \item Operations on values
          \begin{itemize}
            \item Passing them to procedures as arguments
            \item Returning them through an argument of a procedure
            \item Returning them as the result of a function
            \item Assigning them into a variable
            \item Using them to create a composite value
            \item Creating/computing them by evaluating an expression
            \item …
          \end{itemize}
    \item A value for which \emph{all} these operations are allowed is called a \emph{first-class value}
    \item We are used to integer or character values, but function values are also possible!
  \end{itemize}
\end{frame}

\subunit[strong-weak]{Strong \vs weak typing}

\begin{frame}{Strong \vs weak typing}
  \squeezeSpace
  \setbeamercovered{transparent}
  Two kinds of PLs⚓
  \begin{description}[<+-| hl@+>]
    \item [Strongly typed] e.g., \ML, \Eiffel, \Modula, \Java
    \begin{itemize}
      \item it is impossible to break the association of a value with a type from within the framework of the language.
      \item it is impossible to subject a value to an operation which is not acceptable for its type.
    \end{itemize}
    \item [Weakly typed] e.g., assembly,~\CPL, \CC, some variants of \Pascal
    \begin{itemize}
      \item values have associated types, but it is possible for the programmer to break or ignore this association.
      \item type punning, which we discussed earlier (\cref{\labelPrefix Section:type-errors}),
            is one technique for breaking this association.
    \end{itemize}
  \end{description}
  \uncover<+->{\emph{in truth there is a spectrum of strength…}}
\end{frame}

\begin{frame}{Spectrum of strength}
  \begin{Block}{}
    Some languages are more strongly typed than others
  \end{Block}
  \begin{itemize}
    \item \Pascal is more strongly typed than~\CPL; can still break type rules with:
          \begin{itemize}
            \item Variant records
            \item Through files†{See below, discussion of ‟structural typing”}
          \end{itemize}
    \item \Java is more strong typed than \CSharp:
          \begin{itemize}
            \item \Java's JVM guarantees (dynamically) strong typing
            \item in \CSharp, there are several ways of type punning
          \end{itemize}
  \end{itemize}
\end{frame}

\subunit[static-dynamic]{Statics \vs dynamic typing}

\begin{frame}[<+-| hl@+>]{Type checking}
  \squeezeSpace[6]
  \setbeamercovered{transparent}
  \begin{Definition}{Type checking}
    Language implementation applies \Red{type checking} to ensure that
    no type errors occur.
  \end{Definition}
  \medskip
  \begin{description}[Multiplication]
    \item [Multiplication] check that both operands are numeric.
    \item [Logical and] check that both operands are of \alert{Boolean} type.
    \item [Field access] check that the operand is a \alert{Record} containing the given field name.
    \item [Tuple access] check that the operand is a \alert{Tuple} (array value) and that
    the index is valid.
  \end{description}
  \onslide<+->{}
  \begin{block}{Safety}
    \begin{itemize}
      \item If a PL is both strongly typed and statically typed we say that it is \Red{safe}
      \item Recall (\cref{\labelPrefix Section:type-errors}) that in \CSharp, type punning must be annotated with the \kk{unsafe} keyword
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Time of enforcement}
  \squeezeSpace
  \setbeamercovered{transparent}
  \begin{itemize}
    \item Type checking must precede the operation
    \item could be done either at compile-time or at run-time:
  \end{itemize}
  ⚓
  \begin{description}[<+-| hl@+>]
    \item [Statically typed PLs]▄⏎
    \begin{uncoverenv}<+-| alert@+>
      \begin{itemize}
        \item type rules are enforced at \Red{compile time}.
        \item every variable and every formal parameter have an associated type.
      \end{itemize}
      \Q{\RED{\CPL, \Pascal, \Eiffel, \ML, …}}
    \end{uncoverenv}
    \item [Dynamically typed PLs]▄⏎
    \begin{uncoverenv}<+-| alert@+>
      \begin{itemize}
        \item type rules are enforced at \Red{run-time}.
        \item variables, and more generally– expressions, have no associated type.
        \item only values have fixed types.
      \end{itemize}
      \Q{\RED{\Smalltalk, \Prolog, \Snobol, \APL, \AWK, \Rexx,…}}
    \end{uncoverenv}
  \end{description}
\end{frame}

\begin{frame}[<+-| @hl+>]{Dynamic typing ＆ type tags}
  \setbeamercovered{transparent}
  \begin{itemize}
    \item Identifiers have no type associated with them.
    \item Types are associated with the values generated in runtime
    \item Each value carries a \Red{type tag}, identifying its type.
  \end{itemize}
  {\large \BLUE{Pros}}
  \begin{description}[Run partially correct programs]
    \item [Flexibility] Arrays don’t have to be of a homogeneous type
    \item [Run partial programs] An identifier needs to be ‟type-correct” only if accessed
    \item [Quick turnaround] Faster development time
  \end{description}
\end{frame}

\begin{frame}[<+-| hl@+>]{Cons of dynamic typing}\setbeamercovered{transparent}
  \onslide<+->{Conversely, ‟\emph{pros of static typing}”}
  \begin{description}[Space overhead]
    \item [Space overhead] Each value is tagged with type information
    \item [Time overhead] Tag must be examined at runtime
    \item [Unsafety] Many type errors could have been detected by static compile-time checks
    \item [Obfuscation] Entities annotated with type information are easier to understand.
  \end{description}
  ⚓\Red{\textbf{\emph{yet, it seems as if the world
  is moving toward dynamically typed languages}}}
\end{frame}

\begin{frame}[<+-| hl@+>]{Characteristics of static typing}
  \setbeamercovered{transparent}
  \begin{description}[Invariant of Operations]
    \item [Type] annotation for each variable, parameter, function and procedure.
    \item [Prior-Declaration] usually means that all identifiers should be declared before used.
    \item [Invariant of Values] no value will ever be subject to operations it does not recognize.
    \item [Invariant of Variables] a variable may contain only values of its associated type.
    \item [Invariant of Operations] no operation, including user defined functions and procedures,
    will ever be applied to values of a type they do not expect
  \end{description}
\end{frame}

\begin{frame}{Why static typing?}
  \setbeamercovered{transparent}
  \begin{block}{Theorem (H. G. Rice)}
    \begin{itemize}[<+-| hl@+>]
      \item Let~$f$ be \emph{any} feature or property of the execution of computer programs
      \item Suppose that~$f$ is not-trivial, i.e., that~$f$
            holds for at least one program~$p_t$, and does not hold
            for at least some other program~$p_f$.
      \item \Red{Then, there is \textbf{no} general algorithm that, given a program~$p$,
            decides whether~$p$ exhibits feature~$f$ or not.}
    \end{itemize}
  \end{block} ⚓Examples:
  \begin{itemize}\footnotesize
    \item Cannot (systematically) decide if the program stops.
    \item Cannot (systematically) decide if the program is correct.
    \item Cannot (systematically) decide almost any other interesting run time property of a program.
  \end{itemize}
\end{frame}

\begin{frame}[<+-| hl@+>]{Escaping the ‟evil” Dr.~Rice }
  \setbeamercovered{transparent}
  \begin{itemize}
    \item We still need every little help in fighting the horrors of software development
    \item Types manage to escape this ‟evil” theorem;
  \end{itemize}
  ⚓Several other automatic aids are:
  \begin{description}[Design by contract]
    \item [Garbage collection] automatic memory management (run time)
    \item [Const correctness] no modification of \kk{const} parameters (compile time)
    \item [Design by contract] assertions, invariants, preconditions and post-conditions: partial specification of a function (run time)
    \item [Void safety] to prevent null-pointer access
    \item [Other] \Java makes every effort to ensure that an initialized variable is never used; compiler warnings, find bug heuristics,…
  \end{description}
  \onslide<+->{}
\end{frame}

\begin{frame}{Benefits of static typing}
  Prevent run time crashes:
  \begin{itemize}
    \item Mismatch in ＃ of parameters
    \item Mismatch in types of parameters
    \item Sending an object an inappropriate message
    \item Early error detection (supposedly) reduces development time
  \end{itemize}
  Enforce design decisions:
  \begin{itemize}
    \item Cost
    \item Effort
    \item More efficient and more compact object code
          \begin{itemize}
            \item Values do not carry along the type tag
            \item No need to conduct checks before each operation
          \end{itemize}
  \end{itemize}
  Nevertheless, static typing cannot protect against pseudo-type errors.
\end{frame}

\subunit[other]{Other kinds of typing}

\begin{frame}{Mixed typing}
  \squeezeSpace[5]
  \begin{Definition}{Mixed typing}
    We say that a PL has \Red{mixed typing} if the PL exercises
    \textbf{both} static typing and dynamic typing (usually for different purposes).
  \end{Definition}
  \begin{table}[H]
    \coloredTable
    \begin{tabular}{R{0.22} *3{C{0.18}}z}
      \toprule
      \centerline{\textbf{Error}} & \textbf{Compile time} & \textbf{Load time} & \textbf{Runtime} & ⏎
      \midrule
      $V = E$                     & ✓                   & ✓                &                  & ⏎
      $f(E)$                      & ✓                   & ✓                &                  & ⏎
      \emph{null pointer access}  &                       &                    & ✓              & ⏎
      \emph{array overflow}       &                       &                    & ✓              & ⏎
      \bottomrule
    \end{tabular}
    \par
    \Caption[Mixed typing in \Java]
  \end{table}
  Note that null pointer access and array indexing errors are pseudo-type errors.
\end{frame}

\begin{frame}{Gradual typing}
  Some modern additions allow gradual introduction of types into your program
  \begin{itemize}
    \item Write your program as usual in a dynamically typed language.
    \item As the program matures, gradually add type annotation to:
          \begin{itemize}
            \item Variables
            \item Arguments to functions
            \item Function return type
          \end{itemize}
    \item The PL will cooperate behind the scenes:
          \begin{itemize}
            \item Mark obvious type violations
            \item Mark contradicting annotations
            \item Reduce runtime overhead
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[<+-| hl@+>]{Duck typing}
  \Q{\Red{Duck typing} is a variant of ‟dynamic typing”}
  {\bfseries Given an operation \cc{op} and a value~$v$} ⚓
  \begin{description}[Dynamic typing]
    \item [Dynamic typing] \Red{at run time}
    \begin{enumerate}
      \item Determine type~$T$ for which \cc{op} is defined.
      \item Determine type~$T'$ the type of~$v$
      \item If~$T'≤T$, execute \cc{op} on~$v$
    \end{enumerate}
    \item [Duck typing] \Red{at run time}
    \begin{enumerate}
      \item Determine~$O(v)$, the set of operations recognized by~$v$, by either
            \begin{itemize}
              \item determining~$T$, the type of~$v$
              \item reading the list~$O(v)$ as attached to~$v$
            \end{itemize}
      \item If~$\cc{op}∈O(v)$, execute \cc{op} on~$v$
    \end{enumerate}
  \end{description}
  \onslide<+->{}
\end{frame}

\begin{frame}{Notion of ‟type” with duck typing}
  \squeezeSpace
  \begin{description}[Type of Values]
    \item [Type of Values] duck typing allows each value to have its own ‟type”.
    \item [Runtime type] e.g.,
    \begin{itemize}
      \item each function
      \item each parameter
      \item each invocation
    \end{itemize}
    defines a set of operations that are being applied by
    \begin{itemize}
      \item this function
      \item to this parameter
      \item in this particular invocation.
    \end{itemize}
  \end{description}
  \squeezeSpace[2]
  \begin{Definition}{Duck typing error}
    A ‟duck” typing error occurs if a
    value's type does not match the ‟runtime type” during the program runtime
  \end{Definition}
\end{frame}

\subunit[Responsibility]{Type information responsibility}

\begin{frame}[fragile]{The 3 alternatives for declaration of types}
  \squeezeSpace
  ⚓
  \begin{description}[\textbf{III.} Semiimplicit typing]
    \item [\textbf{I.} Manifest typing] \begin{uncoverenv}<+-| alert@+>\small
    as in~\CPL, \Pascal
    \end{uncoverenv}
    \item [\textbf{II.} Inferred typing] \begin{uncoverenv}<+-| alert@+>
    \small as in \ML; \emph{AKA \Red{Implicit typing}}
    \Q{most PLs can infer, at least partially, types. In \ML it is \emph{particularly astonishing} since it involves recursive functions and
    type parameters.}
    \end{uncoverenv}
    \item [\textbf{III.} Semiimplicit typing] \begin{uncoverenv}<+-| alert@+>
    \begin{context}\small
      \begin{itemize}
        \item \Fortran:
              \begin{context}\footnotesize\itshape
                variables which begin with one of the six letters
                \Q{\emph{‟\texttt{i}”}, \emph{‟\texttt{j}”}, \emph{‟\texttt{k}”}, \emph{‟\texttt{l}”}, \emph{‟\texttt{m}”}, and \emph{‟\texttt{n}”}}
                are integers; all others are real.
                †{The risk of inadvertent creation of variables can be precluded⏎by the declaration \kk{implicit none}}
              \end{context}
        \item \Basic (older versions):
              \begin{context}\footnotesize\itshape
                Suffixes such as \cc{％} and \cc{\$} determine the variable’s type.
                \begin{code}[watermark text=\Basic]{CPLUSPLUS}
10 A$ = "HELLO"
20 PRINT A$
                \end{code}
              \end{context}
        \item \Perl:
              \begin{context}\footnotesize\itshape
                Essentially the same as \Basic.
              \end{context}
      \end{itemize}
    \end{context}
    \end{uncoverenv}
  \end{description}
\end{frame}

\begin{frame}[fragile]{Manifest typing, aka explicit typing}
  Programmer is in charge for \Red{annotating} identifiers of
  \begin{itemize}
    \item values
    \item variables
    \item functions
    \item procedures
    \item parameters
    \item …
  \end{itemize}
  with \Red{type information}.
  \Q{found, e.g., in \Pascal, \Ada,~\CPL, \Java, …}
  Type annotation is also a documentation aid
  \begin{code}[watermark text={}]{PASCAL}
X: speed; (* Good: Manifest typing, rich type systems *)
Y: real; (* Bad: Manifest typing, relying on primitive types *)
Z = 3; (* Worse: implicit typing, no declarations *)
  \end{code}
\end{frame}

\begin{frame}{Responsibility for type annotation}
  \squeezeSpace
  \begin{Definition}[0.73]{Type inference}
    Compilers have the ability to apply type inference rules to
    e.g., to determine the type of expressions.
  \end{Definition}
  \squeezeSpace[-2]
  Why not apply this also to variables and other entities?
  \begin{Definition}{Implicit typing}
    A programming language feature by which the programmer does not have to provide type information;
    the compiler infers the type of an entity from the way it is defined.
  \end{Definition}
\end{frame}

\begin{frame}[fragile]{☡The risk of implicit typing}
  \setbeamercovered{transparent}
  The compiler infers the type of any defined value, including that of functions.
  \begin{description}[\ML answer II]
    \item [Risk] Inadvertent creation of variables due to typos and spelling errors:
    \item [\ML Answer] no variables
  \end{description}
  ⚓\begin{centering}
  \large Value declaration
  \end{centering}
  ⚓\emph{just like \kk{CONST} declaration in \Pascal}
  \begin{columns}[t]\begin{column}{0.35\columnwidth}
    \begin{block}{\Pascal}
      \begin{code}{PASCAL}
CONST
  a = 100
      \end{code}
    \end{block}\end{column}
    \begin{column}{0.40\columnwidth}
      \begin{block}{\ML}
        \begin{code}{EMEL}
val a = 100
        \end{code}
      \end{block}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}[<+-| hl@+>]{☡More risks of implicit typing}
  \setbeamercovered{transparent}
  \textbf{Formal parameters:} declared (without type) in the header of a function.
  \begin{description}[\ML answer II]
    \item [Risk] Confusing error messages, and confusing type errors
    \item [\ML Answer] programmer is allowed to add type constraints
  \end{description}
  \Red{\emph{but, there is more}}
  \begin{description}[\ML answer II]
    \item [Risk] Some complex (recursive and generic) type inference problems are undecideable
    \item [\ML answer I] careful analysis of the type system to detect when this problem may occur
    \item [\ML answer II] type constraints
  \end{description}
\end{frame}

\subunit{Summary}
\begin{frame}{Main concepts mentioned}
  \begin{itemize}
    \item \textbf{Sophistication level:} typed \vs untyped PLs, degenerate type
          system, non-standard types
    \item \textbf{Orthogonality:} discriminatory type constructor, first class
      type, second class type
    \item \textbf{Strength of type system:} strong typing, weak typing
    \item \textbf{Time of enforcement:} static typing, dynamic typing, attaching type tag to values, 
        Rice theorem, mixed typing, gradual typing, duck typing, safe PLs.
    \item \textbf{Time of enforcement:} static typing, dynamic typing, attaching type tag to values,
          Rice theorem, mixed typing, gradual typing, duck typing, safe PLs.
    \item \textbf{Responsibility for type annotation:} type annotation, manifest typing,
          implicit typing, type inference, semiimplicit typing,
  \end{itemize}
\end{frame}

\begin{frame}{What's best?}
  \textbf{Depends on purpose} …
  \begin{description}
    \item [Light-headed] a scripting language, designed for small, not-to-be
    maintained, quick and dirty programs which are supposed to be run only a
    small number of times with little concern about efficiency:
    \item No typing or degenerate typing,
    \begin{itemize}
      \item Weak typing to remove hassles
      \item Dynamic typing to achieve flexibility
      \item Semi-implicit typing to reduce programmer time
    \end{itemize}
    \item [Software engineering oriented] programs which are developed by
    several programmers, maintained and changed, run numerous times, and with
    efficiency concerns
    \begin{itemize}
      \item Type system must exist to document and protect the program
      \item Strong typing to reduce errors
      \item Static typing to enhance efficiency, clarity and robustness
      \item No semi-implicit typing to prevent inadvertent creation of
            variables
      \item Flexible type system to allow the programmer to concentrate
            on the important stuff.
    \end{itemize}
  \end{description}
\end{frame}

\afterLastFrame
\references
\begin{itemize}\D{Duck typing}{http://en.wikipedia.org/wiki/Duck\_Typing}
  \D{First class citizens}{http://en.wikipedia.org/wiki/First-class\_citizen}
  \D{First class functions}{http://en.wikipedia.org/wiki/First-class\_function}
  \D{Gradual typing}{http://en.wikipedia.org/wiki/Gradual\_typing}
  \D{Manifest typing}{http://en.wikipedia.org/wiki/Manifest\_typing}
  \D{Type inference}{http://en.wikipedia.org/wiki/Type\_inference}
  \D{Type safety}{http://en.wikipedia.org/wiki/Type\_safety}
  \D{Type system}{http://en.wikipedia.org/wiki/Type\_system}
\end{itemize}
\exercises
\begin{enumerate}
  \item Follow these steps:
        \begin{itemize}
          \item Write a program which never makes type errors, yet,
                would be rejected by a compiler implementing static typing.
          \item Did you know that
                \Q{Your program explains how static typing escapes Rice theorem.}
          \item How is that?
        \end{itemize}
  \item ‟\textit{In PLs which exercise only static typing, values do not need to carry a type tag}”.
        \qq{True or false? Explain.}
  \item Explain why the parallels between tuple and record values make
        us consider ‟index overflow” and ‟index underflow” as type errors?
  \item Explain why ‟division by zero” is not usually regarded as type error.
  \item What is the difference between ‟\emph{mixed typing}” and ‟\emph{gradual typing}”?
  \item Explain why violating ‟void safety” is regarded as type error.
  \item Why shouldn't \[\arcsin (√2)\] be marked as type error?
  \item Explain the statement ‟reference types are closer to be first-class citizens,
        in \CC than in \Pascal” (Hint: where are the
        reference types in \Pascal?).
  \item In the above we saw an example in which the XOR function of two
        pointers.
        \begin{itemize}
          \item Explain how this trick can be used for a compact representation of
                a bi-linked list, storing only one pointer in each node.
          \item What limitations does this compact representation place on its users?
        \end{itemize}
  \item Explain why the combination of static typing and weak typing,
        as in the~\CPL PL, to never attach type tags to values.
\end{enumerate}

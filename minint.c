#include <stdio.h>

int minint(int candidate = -1) {
  if (candidate - 1 >= 0)
    return candidate;
  for (int stride = -1, stride2 = 2*stride; ; stride = stride2, stride2 += stride2)
    if (stride2 >= 0 || candidate + stride2 >= 0)
      return minint(candidate + stride);
}

int maxint(int candidate = 1) {
  if (candidate + 1 <= 0)
    return candidate;
  for (int stride = 1, stride2 = 2*stride; ; stride = stride2, stride2 += stride2)
    if (stride2 <= 0 || candidate + stride2 <= 0)
      return maxint(candidate + stride);
}

main() {
  printf("Max int is %d\n", maxint());
  printf("Min int is %d\n", minint());
}

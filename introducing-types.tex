\subunit[types-as-sets]{Types as sets}

\begin{frame}[fragile]{Visualizing the type system}
	\squeezeSpace[2]
	\begin{columns}[c]
		\column[c]{0.42\columnwidth}
		\footnotesize
		\setlength\belowdisplayskip{2pt plus 1pt minus 1pt}%
		\setlength\abovedisplayskip{2pt plus 1pt minus 1pt}%
		\uncover<2->{A PL~$𝓛$ has a set of values,~$𝕍_𝓛$}
		\uncover<3->{with many values in it.}
		\uncover<4->{A type is a \emph{set} of these values,}
		\uncover<5->{\alert{e.g., \[|T₁|=2.\]}}
		\uncover<6->{Types may be \emph{disjoint},}
		\uncover<7->{\alert{e.g., \[|T₁|∩|T₂|=∅,\]}}
		\uncover<8->{or \emph{contained},}
		\uncover<9->{\alert{e.g., \[T₁⊂T₃.\]}}
		\uncover<10->{and even \emph{intersecting},}
		\uncover<11->{\alert{e.g., \[|T₃|∩|T₄|≠ ∅.\]}}
		\uncover<12->{A type may be \emph{empty},}
		\uncover<13->{\alert{e.g., \[|T₅|=0,\]}}
		\uncover<14->{a \emph{singleton}, }
		\uncover<15->{\alert{e.g., \[|T₆|=1,\]}}
		\uncover<16->{and even contain \emph{the entire set of values}}
		\uncover<17->{\alert{e.g., \[T₇= 𝕍_𝓛\]}}
		\column[c]{0.6\columnwidth}
		\begin{Figure}
			\begin{adjustbox}{,clip,H=0.8,trim=2.6ex 1.5ex 0ex 0.5ex}
				\begin{tikzpicture}
					\tikzset{dot/.style={label=#1,inner sep=1pt,fill=black,shape=circle,radius=2pt}}
					\tikzset{set/.style 2 args={%
						shape=circle,radius=1,thick,
						opacity=0.5,fill={#1!80},
						minimum width=70pt,
						draw=#1,
						label=above:{\textcolor{#1!80!black}{\bf #2}}
					}}
					\uncover<2->{%
						\node[draw,
							circle,
							rotate=30,
							scale=1.82,
							xscale=1.8,
							ultra thick,
							fill=olive!10,
							minimum width=14ex,
							label=above:$𝕍_𝓛$
						] at (0,0.2) {}; }
					\uncover<3->{%
						\node[dot=$v₁$] at (-0.2,0.1) {};
						\node[dot=$v₂$] at (1,0) {};
						\node[dot=$v₃$] at (0.5,1.5) {};
						\node[dot=$v₄$] at (-1.1,-1.1) {};
						\node[dot=$v₅$] at (1.7,1.3) {};
						\node[dot=$v₆$] at (-2.1,-1.4) {};
						\node[dot=$v₇$] at (-1.5,-1.3) {};
						\node[dot=$v₈$] at (-1.4,0.8) {};
						\node[dot=$v₉$] at (1.5,0.3) {};
						\node[dot=$v_{10}$] at (-0.3,0.9) {};
						\node[dot=$v_{11}$] at (0.2,-0.3) {}; }
					\uncover<4->{\node[set={green} {$T₁$}] at (0,0) [rotate=-30, scale=2, xscale=0.3, yscale=0.2] {};}
					\uncover<6->{\node[set={blue} {$T₂$}] at (-1.53,-1.24) [rotate=17, scale=2.4,xscale=0.35,yscale=0.2] {};}
					\uncover<8->{\node[set={red} {$T₃$}] at (0.8,0.7) [rotate=25, scale=4.2,xscale=0.4, yscale=0.25] {};}
					\uncover<10->{\node[set={magenta}{$T₄$}] at (-0.7,1.02) [rotate=5, scale=1.2,xscale=0.8, yscale=0.25] {};}
					\uncover<12->{\node[set={black} {$T₅$}] at (-0.2,-1.42) [rotate=-5, scale=0.1] {};}
					\uncover<14->{\node[set={teal} {$T₆$}] at (1.5,1.4) [rotate=-95, scale=0.4] {};}
					\uncover<16->{\node[set={orange} {}] at (0,0.2) [rotate=30, scale=1.77,xscale=1.8,minimum width=14ex,
						label=below:{\textcolor{orange!80!black}{$T₇$}}]
						{} ; }
				\end{tikzpicture}
			\end{adjustbox}
		\end{Figure}
		\setlength\abovedisplayskip{2pt plus 1pt minus 1pt}%
		\setlength\belowdisplayskip{2pt plus 1pt minus 1pt}%
		\uncover<17->{%
			The \emph{type system} is the \emph{set of all types}; in our example,
			\alert{\[
				𝕍= \left❴T₁,T₂,T₃,T₄,T₅,T₆,T₇\right❵
			\]}}
	\end{columns}
\end{frame}

\begin{frame}{What's in a type system?}
	For a language~$𝓛$, let~$𝕋_𝓛$ denote its type system.
	Then,
	⚓
	\begin{itemize}[<+->]
		\item Each type is a subset of the values' universe.
		      \begin{equation}
		      	∀τ∈𝕋_𝓛:τ⊆𝕍_𝓛.
		      	\synopsis{A type of PL ‟is” a set of values}
		      \end{equation}
		      \item~$𝕋_𝓛$ is a set of subsets of the values' universe
		      \begin{equation}
		      	𝕋_𝓛⊂𝒫𝕍_𝓛.
		      	\synopsis{The set of types of a PL}
		      \end{equation}
		\item Types do not make a \emph{partition} of~$𝕍_𝓛$
		      \begin{itemize}
		      	\item One type may be contained in another
		      	\item The intersection of two types is not necessarily empty           
		      \end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}{Value--type association}
	For~$v∈𝕍$, let
	\begin{equation}
		\textsf{types}(v) = ❴T∈𝕍_𝓛|v∈T❵
		\synopsis{The set of types of a value}
	\end{equation}
	\noindent Then,
	⚓
	\begin{itemize}[<+->]
    \item \alert{Every} value has a type
		      \begin{equation}
		      	∀v: ＃\textsf{types}(v)>0
		      	\synopsis{Some values have more than one type: }
		      \end{equation}
        \item \alert{Some} values have more than one type,
		      \begin{equation}
		      	∃v:＃\textsf{types}(v)>1
		      	\synopsis{Some values have more than one type: }
		      \end{equation}
        \item \alert{Often}
		      \begin{equation}
		      	＃\textsf{types}(v)>1\stackrel{\text{\scriptsize\itshape often}}{⇒}＃\textsf{types}(v)=∞
		      	\synopsis{Typically, values with more than one type, have infinitely many types:}
		      \end{equation}
		      ⚓e.g., in~\CPL,
		      ‟\cc{0}” belongs to all pointer types
		      \begin{equation}
		      	T∈𝕋_{\CPL}⇒\text{‟\cc{0}”}∈T\cc{*}
		      	\synopsis{Infinitely many types of \cc{0} in~\CPL}
		      \end{equation}   
	\end{itemize}
\end{frame}

\begin{frame}{Types look like sets of values}
	\squeezeSpace
	\begin{itemize}
		\item Each type~$T$, defines a~$S_T$, the \emph{set of values} of~$T$.
		\item Types describe values and \emph{expressions}
		\item With every type there is ‟set predicate”:
		      \begin{description}[Predicate on expressions]
		      	\item [Predicate on values] A value~$v$ is of type~$T$, \emph{iff}~$v∈S_T$
		      	\item [Predicate on expressions] An expression~$E$ belongs in type~$T$
		      	\emph{iff} every value~$v$ that~$E$ may evaluate to, satisfies~$v∈S_T$           
		      \end{description}
	\end{itemize}
	Shorthand for~$S_T$?~$T=S_T$?
	\begin{itemize}
		\item a common \Red{‟abuse of notation”} by which~$T$ \textbf{sometimes} means~$S_T$
		\item get ready, everyone uses this abuse   
	\end{itemize}
\end{frame}

\begin{frame}{Not all sets of values make a type}
	Every type ‟is” a set of values, but not every set of values ‟is” a type.
	\begin{table}[H]
		\coloredTable
		\begin{tabular}{cc}
			\toprule
			\textbf{Set of values}                                            & \textbf{Is a type?}⏎      
			\midrule
			$❴4.20, \text{‟\textit{Cannabis}”}, \kk{true}, \cc{'?'}❵$ & \Red{No} (in most PLs)⏎   
			$❴\kk{true}, \kk{false}❵$                                     & \Blue{Yes} (in most PLs)⏎ 
			\bottomrule
		\end{tabular}
		\Caption[A set of values which does not makes a type and a set that does]
	\end{table}
	\begin{itemize}
		\item Only sets which are atomic types, or constructed
		      by the type constructors are types.
		\item Type~$T$ also defines a set of allowed operations
		\item Conversely, all~$v∈T$ must
		      \begin{description}[Respond similarly]
		      	\item [Recognize] the same operations
		      	\item [Respond similarly] to each recognized operation           
		      \end{description}
	\end{itemize}
\end{frame}

\begin{frame}{Type equality \emph{vs.} set equality}
	\uncover<+->{\textbf{Type equality:} When is~$T₁=T₂$?}
	\begin{itemize}[<+->]
		\item If it holds for the corresponding \emph{sets} that~$T₁=T₂$?
		\item Not always!
		\item Actually, the answer is most often negative!   
	\end{itemize}
	\uncover<+->{\textbf{Type containment:} When is~$T₁≤T₂$?}
	\begin{itemize}[<+->]
		\item Important, e.g., for assuring compatibility of actual to formal parameter.
		\item If it holds for the corresponding \emph{sets} that~$T₁⊆T₂$?
		\item Not always!
		\item Actually, the answer is most often negative!   
	\end{itemize}
\end{frame}

\begin{frame}{Type systems are defined recursively}
	\begin{description}[Type constructors]
		\item [Atomic types]\mbox{}
		\begin{itemize}
			\item Make the recursion's base.
			\item Mostly predefined by the PL.
			\item Programmer defined atomic types also happen.
			\item AKA: \emph{basic types} or \emph{primitive types}.     
		\end{itemize}
		\item [Type constructors]\mbox{}
		\begin{itemize}
			\item Defined by the PL.
			\item Typically modeled after ‟theoretical type constructors” (\cref{\labelPrefix Section:mock-type-constructors})
			\item May take~$0$,~$1$,~$2$, or any number of type arguments
			\item May take other arguments (e.g., integers or labels)     
		\end{itemize}
	\end{description}
\end{frame}

\begin{frame}[fragile]{Significance of type}
	\squeezeSpace
	An expression using the~$·\cc[·\cc]$ binary operator:\\
	\begin{quote} 
		\begin{tabular}{ccc}
      \pause
			\begin{lcode}{CEEPL}
X[i]
			\end{lcode}
			  & \pause \Huge $\Rightarrow$ 
			  &       \pause              
			\begin{minipage}{0.6\columnwidth}
			\begin{itemize}
			\item Is it legal?
			\item What does it mean?
			\item What type is the result?     
			\end{itemize}
			\end{minipage}
		\end{tabular}
	\end{quote}
  \pause
	\alert{{\Large In~\CPL:}}
	⚓\begin{TWOCOLUMNS}
	\begin{description}[]
		\item <+->[\Blue{Legal}] if \cc{i} is a pointer and \cc{X} is an integer, e.g.,
		\begin{lcode}{CEEPL}
void f() {¢¢
  int X = 12;
  int *i = &X;
  X[i] = X ** i; // ¢✓¢
}
		\end{lcode}
		\qq{\scriptsize (there is no typo in the above code; it is just a bit obfuscated)}
	\end{description}
	\MIDPOINT
	\begin{description}[]
		\item <+->[\Red{Illegal}] if \cc{i} is a floating point number and \cc{X} is a \kk{char},
		\begin{lcode}[output={error: subscripted value is neither array nor pointer nor vector}]{CEEPL}
void f() {¢¢
  float i = 4.20;
  char X = ¢\Z¢'c';
  X[i]; // ¢✗¢
}
		\end{lcode}   
	\end{description}
	\end{TWOCOLUMNS}
\end{frame}

\begin{frame}{Purpose of type?}
	We have values;
	⚓\alert{why do we need types?}
	⚓
	\begin{description}[Representation]
		\item <+-> [Taxonomy] of values; describe data effectively
		\item <+-> [Legality] determine set of legal operations on values
		(prevent type errors, e.g., multiply a pointer by a set)
		\item <+-> [Semantics] determine semantics of operations on values
		\item <+-> [Representation] define program-machine interface:
		\begin{description}[XX]
			\item <+-> [$\text{Program}⇒\text{Machine}$] how to \emph{code} values on different machines
			\item <+-> [$\text{Machine}⇒\text{Program}$] how to \emph{decode} sequences of bits as actual values     
		\end{description}
	\end{description}
\end{frame}

\begin{frame}{How does the PL/compiler/runtime uses types?}
	Given~$v∈𝕍$ and an operation \cc{op}
	\begin{description}
		\item [Legality] use~$\textsf{types}(v)$ to determine whether \cc{op} is applicable to~$v$.
		\qq{(\cref{Section:type-errors} just ahead)}
		\item [Semantics] use~$\textsf{types}(v)$ to determine the semantics of~$\cc{op(v)}$
		\qq{(\cref{Section:semantics-with-overloading} just ahead)}
		\item [Inference] determine the type of~$\cc{op(v)}$
		\item [Representation] (\cref{\labelPrefix Section:representing-types})
		\begin{itemize}
			\item [Encode] How to store~$v$ in memory
			\item [Decode] How to interpret the bits representing~$v$     
		\end{itemize}
	\end{description}
\end{frame}

\subunit[type-errors]{Type errors}

\begin{frame}[fragile]{Type error}
	Example:
	\begin{Code}[minipage,width=0.85\columnwidth,output={invalid operands to binary / (have ‘int’ and ‘char **’)}]{CEEPL}{Type error in~\CPL}
int main(int argc, char *argv[]) {¢¢
  return argc / argv; // ¢✗¢
}
	\end{Code}
	We can define: 
	\WD{Type error {[on a single value]}}{\vspace{-1ex}A program commits a \Red{type error} on a value~$v∈T$
	if it makes an attempt to manipulate~$v$ in a way which is inconsistent with~$T$.}
	But, type errors can be committed on several values.
\end{frame}

\begin{frame}[fragile]{Type error on multiple values}
	\squeezeSpace
	\WD{Type error {[on multiple values]}}{A program commits a \Red{type error} on
		values~\[v₁∈T₁,…, vₙ∈Tₙ,~n≥1\] if it makes an attempt to 
    manipulate~$v₁,…,vₙ$ in a way which is
	inconsistent with~$T₁,…, Tₙ$.}
	In our  example, the type error was committed on two values,~$v₁=\cc{argc}$
	and~$v₂=\cc{argv}$.
	\begin{Code}[minipage,width=0.85\columnwidth,output={invalid operands to binary / (have ‘int’ and ‘char **’)}]{CEEPL}{Type error in~\CPL}
int main(int argc, char *argv[]) {¢¢
  return argc / argv; // ¢✗¢
}
	\end{Code}
\end{frame}

\begin{frame}{Dealing with type errors}
	PLs report type errors
	\begin{description}
		\item [Dynamically] When the program tries to commit them at runtime
		\item [Statically] When it cannot be proved that the program will not commit them.   
	\end{description}
	In the above case, the type error \cc{argc / argv} was detected statically.
\end{frame}

\begin{frame}{Pseudo type error}
	\squeezeSpace
	\WD[0.8]{Pseudo type error}
	{A program commits a \Red{pseudo type error} on a value~$v∈T$ if it makes an attempt to
		manipulate~$v$ in a way
		\begin{itemize}
			\item which is, in general, legal for~$T$.
			\item yet, is illegal for the particular~$v∈T$.
		\end{itemize}
	}
	Examples;
	\begin{itemize}
		\item Division by zero
		\item Array index overflow
		\item Add 100 to \cc{MAXINT} in \Pascal
		\item Null pointer access
	\end{itemize}
\end{frame}

\begin{frame}[fragile]{Dealing with pseudo type errors}
	\squeezeSpace
	\begin{description}
		\item [Statically] Impossible in general
		\begin{itemize}
			\item Think of \cc{read(i); a[i]:=0}
			\item Smart compilers can make early detection in some cases
		\end{itemize}
		\item [Dynamically] When the program tries to commit these at runtime
		\item [Silently] No errors are generated if
		\begin{itemize}
			\item a \Pascal program adds \cc{100} to \kk{MAXINT}.
			\item a~\CPL program tries to access the \nth{101} cell in an array with \cc{100} cells.
		\end{itemize}
	\end{description}
\end{frame}

\begin{frame}[fragile]{Case study: Gnu-C-Compiler handling of division by zero}
	\squeezeSpace[2]
	Gnu-C-Compiler often detects cases of division by zero
	\pause
	\begin{lcode}[output={warning: division by zero [-Wdiv-by-zero]},minipage,width=60ex]{CEEPL}
int main() {¢¢
  return 0/(main-main); ¢✗¢
}
	\end{lcode}
	\pause
	But, not all of them
	\begin{lcode}[minipage,width=30ex]{CEEPL}
int f() {¢¢ return 0 - 0; }
int main() {¢¢
  return 0/f();// ¢✓¢
}
	\end{lcode}
	\pause
	Nevertheless, all division by zero errors are detected at runtime
	\begin{lcode}[output={Floating point exception (core dumped)},minipage,width=60ex]{BASH}
% gcc -Wno-div-by-zero divide-by-zero.c
% ./a.out
	\end{lcode}
\end{frame}

\begin{frame}{Types ＆ underlying machine}
	\begin{description}[Tagged architectures]
		\item [Machine language] all values are untyped bit patterns
		\item [Tagged architectures] add type information to values
		\begin{itemize}
			\item very rare
			\item no support for compound types
		\end{itemize}
		\item [Assembly language] minimal support for types, e.g., addresses vs.\ data
		\item [High-level PLs] attach types to
		\begin{itemize}
			\item values
			\item expressions
			\item memory cells
		\end{itemize}
	\end{description}
\end{frame}

\begin{frame}{Opacity of machine representation?}
	\squeezeSpace
	\begin{itemize}
		\item All values are represented as a \Red{sequence of bits}
		\item The type system often \Red{tries} to hide this fact.
		      \begin{description}[Semi-transparent]
		      	\item [Opaque] references in \Java and \ML are
		      	\qq{Programmer have no way of telling how they are represented}
		      	\item [Transparent] e.g., \kk{int} in \Java is \alert{transparent}
		      	\begin{itemize}
		      		\item it must be 32 bits wide
		      		\item it must use two's complement
		      	\end{itemize}
		      	\item [Semi-transparent] Type \kk{char} in~\CPL is \alert{semi-opaque}:
		      	\begin{itemize}
		      		\item Must be at least 8 bits wide
		      		\item Cannot use more bits than \kk{short}
		      		\item Cannot be \kk{signed} or \kk{unsigned}
		      		\item Can use one's complement or two's complement
		      	\end{itemize}
		      	the language tells you much, but not all, about the representation
		      \end{description}
	\end{itemize}
\end{frame}

\begin{frame}[fragile]{Type punning}
	\squeezeSpace
	Revealing the mascaraed of bit sequences as values:
	\squeezeSpace
	\WD<+->[0.55]{Type punning}{%
		\Red{type punning} is the power to interpret machine representation of~$v$ in a way which is
		inconsistent with~$v$
	}
	\medskip
	\centerline{{\itshape\Large\alert{Type punning has the power to}}}
	\medskip
	\begin{TWOCOLUMNS}
		⚓\Red{Peep} into the bit sequence implementation of a type
		\begin{lCode}{CEEPL}{Type casting}
long i, j;
int *p = &i, *q = &j;
long L1 = ((long)p);
long L2 = ((long)q);
long L = L1^L2;
		\end{lCode}
		\MIDPOINT
		⚓\Red{Mutilate} a value, by subjecting it to operations not allowed for its type
		\begin{lCode}{CEEPL}{Union type}
union {¢¢ double foo;
  long bar;
} baz;
baz.foo = 3.7;
printf("%ld\n", baz.bar);
		\end{lCode}
	\end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile]{Type punning in \CSharp}
  \begin{centering}
    \large In \CSharp, type punning must be annotated with the \kk{unsafe} keyword
  \end{centering}
  \begin{code}{CSHARP}
class Foo {¢¢
 public static void Main() {¢¢
   unsafe {¢¢ // could also annotate ¢\cc{class Foo}¢
              // or function ¢\cc{Main}¢
     int i = 14;
     int *p = &i;
     Console.WriteLine("I is " + i);
     Console.WriteLine("I is also " +
        p->ToString() );
     Console.WriteLine("Its address is " +
        (int)p);
   } // ¢\kk{unsafe}¢ block
 } // function ¢\cc{Main}¢
} // ¢\kk{class} \cc{Foo}¢
  \end{code}
\end{frame}

\subunit[semantics-with-overloading]{Type determines semantics: the overloading case study}

\begin{frame}{Riddle}
	\squeezeSpace
	Can you figure out this?
	\begin{Block}{Groucho Marx (1890--1977):}\large\itshape
		\begin{tabular}{cc}
			\vincludegraphics[width=0.15\columnwidth]{Graphics/groucho-marx.jpg}
			  & \parbox{\widthof{Fruit flies like a banana!}} 
			{%
			Time flies like an arrow.
			Fruit flies like a banana!
			}
		\end{tabular}
	\end{Block}
	\centering
	\begin{adjustbox}{}
		\begin{tikzpicture}
			\node[scale=15,overlay,text=red,opacity=0.3] {?};
		\end{tikzpicture}
	\end{adjustbox}
\end{frame}

\begin{frame}{Unraveling the Marx riddle}
	\squeezeSpace
	\begin{table}[H]
		\centering
		\begin{adjustbox}{}
			\coloredTable
			\begin{tabular}{*3l}
				\toprule
				\bfseries Term & \bfseries Meaning I & \bfseries Meaning II⏎ 
				\midrule
				\itshape flies & flows               & winged insects⏎       
				\itshape like  & similar to          & favor, prefer⏎        
				\bottomrule
			\end{tabular}
		\end{adjustbox}
		\Caption[Overloaded meanings of terms in the Groucho Marx riddle]
	\end{table}
	\begin{Figure}
		\begin{tabular}{T{0.45}T{0.45}}
			\adjincludegraphics[H=0.25, W=1]{Graphics/time-flies-like-an-arrow.png} &   
			\multicolumn1c{\adjincludegraphics[H=0.25,W=0.45]{Graphics/fruit-flies-like-a-banana.png}}⏎
			Time flies like an arrow.                                               &   
			\parbox[t]{\linewidth}{Fruit flies (\alert{the disgusting insects}) like (\alert{favor}) a banana!}
		\end{tabular}
	\end{Figure}
\end{frame}

\begin{frame}{Context dependent resolution of overloading}
	\begin{TWOCOLUMNSc}[-10ex]
		\begin{Figure}[Archie Bunker explains to Edith Bunker how context is used to
				resolve the ambiguity of the three overloaded meanings of the word
			‟Shalom” in Hebrew]
			\alert{Archie Is Branded}\medskip⏎
			\includegraphics[width=\columnwidth]{Graphics/all-in-the-family-archie-branded-by-skin-heads.png}\medskip⏎
			\urlfit{https://www.youtube.com/watch?v=RDcIEycwpSI\hashchar t=8m56}
			\raggedleft
			\scriptsize\slshape episode 57 (episode 20/season III)
			\textbf{All In The Family} 1973⏎
		\end{Figure}
		\begin{tabular}{c}
			\qrcode{https://www.youtube.com/watch?v=RDcIEycwpSI\hashchar t=8m56}\medskip⏎ 
			\urlfit{http://tinyurl.com/noverloading}                                        
		\end{tabular}
		\MIDPOINT
		\begin{adjustbox}{}
			\begin{Block}{Script}
				\newbox\tempBox
				\sbox\tempBox{\textbf{Archie Bunker}}
				\begin{tabular}
					{%
					@{\hspace{1pt}}
					>{\bfseries\hfill}
					p{\dimexpr(\wd\tempBox + 1ex)}
					@{\hspace{3pt}}
					>{\itshape\raggedright\arraybackslash}
					p{\dimexpr(\columnwidth - \wd\tempBox)}
					@{\hspace{1pt}}
					}
					Paul:          & Shalom.⏎                                                                                                  
					Edith Bunker:  & Shalom? What does that mean?⏎                                         
					Mike Stivic:   & Believe it or not, Ma, it means ‟peace”.⏎                                                              
					Gloria Stivic: & Jewish people also use it to say ‟hello” and ‟good-bye”.⏎                                                     
					Edith Bunker:  & How do you tell if they mean ‟hello” or ‟good-bye”?⏎                                                          
					Archie Bunker: & Simple, Edith, If a Jew is walking towards you, it means ‟hello”. If he's walkin' away, it means ‟good-bye”.⏎ 
					Edith Bunker:  & When does it mean ‟peace”?⏎                                                                                  
					Archie Bunker: & In between ‟hello” and ‟good-bye”.⏎                                                                           
				\end{tabular}
			\end{Block}
		\end{adjustbox}
	\end{TWOCOLUMNSc}
\end{frame}

\begin{frame}[fragile]{Keyword overloading in \Pascal}
	\squeezeSpace
	Keyword ‟\kk{of}” serves several similar meanings in different contexts
	\squeezeSpace[1.5]
	\newcommand\OF{\colorbox{Yellow}{\kk{of}}}
	\begin{TWOCOLUMNS}
		\begin{lCode}[width=0.9\columnwidth,minipage]{PASCAL}{Arrays}
CONST
  N = 100;
TYPE
  Range = 1..N;
  Matrix = Array[Range] ¢\OF¢
			Array[Range]¢\OF¢ Real;
		\end{lCode}
		\begin{lCode}[width=0.9\columnwidth,minipage]{PASCAL}{Sets}
VAR vowels: Set ¢\OF¢ Char;
		\end{lCode}
		\begin{lCode}[width=0.9\columnwidth,minipage]{PASCAL}{Multiway conditional}
Case month¢\OF¢
  January: ¢\textrm{…}¢
  February: ¢\textrm{…}¢
  ¢$⋮$¢
end
		\end{lCode}
		\MIDPOINT
		\begin{lCode}[width=0.9\columnwidth,minipage]{PASCAL}{Variant Records}
  TYPE
    String = ¢\textrm{…}¢
    Kind = (NIL, CONS, ATOM);
    Link = ^Child
    Child_= Record ¢\OF¢
      Case tag: Kind ¢\OF¢
        NIL: (* Nothing *)
        CONS: car, cdr: Link;
        ATOM: data: String
      end
    end;
\end{lCode}
		But, all these cases of overloading are \alert{unrelated} to type.
	\end{TWOCOLUMNS}
\end{frame}

\begin{frame}{Overloading}
	\squeezeSpace
	\WD{Overloading}{%
		An \emph{overloaded term} is a term that has multiple meanings,
		which \emph{may}, but also \emph{may not} be related.
	}
	\medskip
	How did the Marx trick work?
	\begin{itemize}
		\item Overloading
		\item Unrelated meanings
		\item Misleading context:
		      \qq{(On its \textbf{own}, the phrase ‟\alert{Fruit flies like a banana}” is not so confusing)}
	\end{itemize}
\end{frame}

\begin{frame}{Overloading in English}
	Unrelated meanings:
	\begin{description}
		\item [lie] \emph{to present false information with the intention of deceiving}
		\qq{‟I did not lie in the deposition”}
		\item [lie] \emph{to place oneself at rest in a horizontal position}
		\qq{‟I did not lie in this position”}
	\end{description}
	Close (more or less) meanings:
	\begin{description}
		\item [fly] to move through the air
		\item [fly] to travel by an airplane
		\item [fly] a two winged insect, such as insect
	\end{description}
	\squeezeSpace
	\WL[0.75]{The fundamental rule of overloading}
	{The intended meaning is figured out by \emph{context}}
\end{frame}

\begin{frame}{‟Plain” overloading \vs identifier/operator overloading}
  \begin{itemize}
		\item Overloading of \kk{of} in \Pascal is not of an \alert{identifier}.
    \item Keyword \kk{of} \alert{does not} name a nameable entity such as variable, a function, a procedure, etc.
  \end{itemize}
  \squeezeSpace
  \WD[0.8]{Identifier overloading}
  {\normalsize An \emph{identifier} or \emph{operator} is said to be
    \emph{overloaded} if it simultaneously denotes two or more distinct nameables }
  Operator ‟\cc{+}” in \Pascal and~\CPL denotes two
  distinct functions:
  \begin{itemize}
    \item Integer addition
    \item Floating point addition
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]{Builtin procedure overloading in \Pascal}
	\begin{code}{PASCAL}
Program Write;
  TYPE
    Days = (Sunday, Monday);
  Begin
    WriteLn(0);
    WriteLn(0.0);
    WriteLn(false);
    WriteLn(Sunday)
  end.
	\end{code}
	Output is
	\begin{session}
0
0.000000000E+00
FALSE
Sunday
	\end{session}
	The identifier \cc{WriteLn} in \Pascal denotes many distinct functions.
\end{frame}

\begin{frame}{Programmer defined overloading}
	\begin{itemize}
		\item~\CPL does not allow operator overloading by programmer.
		\q{but, its younger, fatter, and uglier, ‟daughter”, \CC, does}
		\item \Pascal does not allow operator overloading by programmer.
		      \q{but, its younger, fatter, and uglier, ‟sister”, \Ada, does}
	\end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]{Function overloading in \CC}
	\CPL forbids function overloading, but its young, fat, and
	ugly sister, \CC, welcomes it:
	\begin{Code}{CPLUSPLUS}{Function overloading in \CC}
double max(double d1, double d2) {¢¢
  return d1 > d2 ¢¢? d1 : d2;
}
char max(char c1, char c2) {¢¢
  return c1 > c2 ¢¢? c1 : c2;
}
char* max(char* s1, char* s2) {¢¢
  return strcmp(s1, s2) > 0 : s1 : s2;
}
const char* max(const char* s1, const char* s2){¢¢
  return strcmp(s1,s2)>0 : s1 : s2;
}
	\end{Code}
	Neither~\CPL, nor \CC have ‟builtin” functions.
	Hence, they have no builtin function overloading.
\end{frame}

\begin{frame}[fragile]{Overloading the division operator in \Ada}
	\begin{itemize}
		\item \Pascal forbids operator overloading by programmer, but, its younger, fatter, and uglier,
		      ‟daughter”, \Ada, allows it:
		\item Builtin semantics of ‟\cc{/}”:
		      \begin{description}[Integer division]
		      	\item [Integer division] \(\cc{Integer} ⨉ \cc{Integer} → \cc{Integer}\)
		      	\item [Real division] \(\cc{Real} ⨉ \cc{Real} → \cc{Real}\)
		      \end{description}
	\end{itemize}
	Programmer defined overloading:
	\begin{Code}{ADA}{User Overloading operator \ccn{/} in \Ada}
function "/" (m, n : Integer) return Float is
begin
  return Float(m) / Float(n);
end;
	\end{Code}
	Adds another meaning to division of integers:
	\alert{it can now \textbf{also} return a real number}.
\end{frame}

\begin{frame}[fragile=singleslide]{Resolving ambiguity of overloading}
	The actual meaning is determined by context:
	\begin{description}
		\item [(i)]~which parameters are passed to operator ‟\cc{/}” upon invocation
		\item [(ii)]~how its result is used.
	\end{description}
\end{frame}

\begin{frame}[fragile=singleslide]{Ambiguity resolution}
	Consider the call \cc{Id(E)} where \cc{Id} denotes both:
	\begin{itemize}
		\item a function~$f₁$ of type~$S₁→T₁$
		\item a function~$f₂$ of type~$S₂→T₂$
	\end{itemize}
	\small
	\begin{columns}[t]
		\column{0.38\columnwidth}
		\structure{Context Independent (\CC)}
		\begin{itemize}
			\item Either~$f₁$ or~$f₂$ is selected depending \emph{solely} on the type of \cc{E}
			\item We must have~$S₁≠S₂$
			\item May lead to ambiguities in certain cases, e.g.,~$＃\textsf{types}>1$
		\end{itemize}
		\column{0.6\columnwidth}
		\structure{Context Dependent (\Ada)}
		\begin{itemize}
			\item Either~$f₁$ or~$f₂$ is selected depending \emph{on both}
			      on the type of \cc{E} or how \cc{Id(E)} is used.
			\item Either~$S₁ ≠ S₂$ or~$T₁ ≠ T₂$ (or both).
			\item Ambiguity is not always resolved:
			      \begin{code}{ADA}
x : Float:=(7/2)/(5/2);
			      \end{code}
			      Has two ambiguous interpretations:
			      \begin{TWOCOLUMNS}
			      	\scriptsize
			      	\begin{itemize}
			      		\item~$3/2=1.5$
			      	\end{itemize}
			      	\MIDPOINT
			      	\begin{itemize}
			      		\item~$3.5/2.5=1.4$
			      	\end{itemize}
			      \end{TWOCOLUMNS}
		\end{itemize}
	\end{columns}
\end{frame}

\subunit{Summary}

\begin{frame}{Summary: main concepts}
  \squeezeSpace
  \begin{itemize}[<+->]
    \item Type system
    \item Recursively defined type systems
    \item Types define sets of values, but not all sets of values are types
    \item Purpose of type: Taxonomy, Legality, Semantics, Representation
    \item Type errors \vs pseudo type errors.
    \item Representation
          \begin{description}[Semi-transparent]
            \item [Opaque] 
            \item [Transparent] 
            \item [Semi-transparent]
          \end{description}
    \item General overloading of terms \vs overloading of functions (identifiers) and operators.
    \item Resolution of overloading ambiguity
          \begin{description}
            \item [\Ada] context dependent
            \item [\CC] context independent
          \end{description}
  \end{itemize}
\end{frame}

\afterLastFrame
\references
\begin{itemize}\D{Data type}{http://en.wikipedia.org/wiki/Data\_type}
	\D{Lisp machine}{http://en.wikipedia.org/wiki/Lisp\_machine}
	\D{Tagged architecture}{http://en.wikipedia.org/wiki/Tagged\_architecture}
	\D{Type punning}{http://en.wikipedia.org/wiki/Type\_punning}
	\D{Type safety}{http://en.wikipedia.org/wiki/Type\_safety}
	\D{Type system}{http://en.wikipedia.org/wiki/Type\_system}
\end{itemize}
\exercises
\begin{enumerate}
	\item What is the difference between value and type?
	\item Is it true that~$v∈𝕍_𝓛⇒∃T∈𝕋_𝓛:v∈T$? Explain.
	\item What's the difference between a ‟value” and a ‟variable”?
	\item Does it follow from the fact that cardinality of the set of all \Pascal programs in~$ℵ₀$,
	      that~$＃𝕍_{\Pascal}=ℵ₀$?
	\item What are the three purposes of a type system?
	\item Compare the terms ‟type error” and ‟type punning”.
	\item For which~$v$ it holds that~$\textsf{types}(v) =∅$? explain.
	\item What's the difference between a type and a variable?
	\item ‟\emph{describe data effectively}”, explain using examples if necessary.
	\item Provide an example of a value~$v$ for which \[1 <＃\textsf{types}(v)<∞.\]
	\item Enumerate all qualities that distinguish between a type and a ‟set of values”.
	\item This~\CPL function does not produce type errors. Explain why not.
	      \begin{code}{CEEPL}
short poem() {¢¢
  float home;
  return home;
  static dear;
  long ****time, no, c;
  "Do you love me" ¢¢? "Yes" : "or no";
  "I love you";
  "I love you", 2;
  volatile short question = * "How much?";
  1000 <"times"> dear;
  return question;
}
	      \end{code}
	      (Give one or two examples how the program uses overloading to make function \cc{poem} read like a poem; is this type overloading or something else?)
\end{enumerate}

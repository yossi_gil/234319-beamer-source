\subunit[objects-with-closures]{Objects with closures}

\begin{frame}[fragile]{Motivation: objects in \ML using closures ＆ references}
  \newcommand\equal{=}
  \newcommand\comma{,}
  \textbf{Accumulator}: \emph{adds}, \emph{counts}, and \emph{computes the mean}
  \begin{code}[output={%
val makeAccumulator \equal fn⏎
\qquad : unit -> ❴⏎
\qquad\qquad\qquad add:real -> unit\comma{}
\qquad\qquad\qquad count:unit -> int\comma{}⏎
\qquad\qquad\qquad mean:unit -> real\comma⏎
\qquad\qquad\qquad total:unit -> real
❵
}]{EMEL}
fun makeAccumulator() = let
  val n = ref 0
  val sum = ref 0.0
  in {¢¢
    count = fn() => ¢¢!n,
    total = fn() => ¢¢!sum,
    add = fn r:real =>
      (n := ¢¢!n + 1; sum := ¢¢!sum + r),
    mean = fn() =>(!sum / real(!n))
  } end;
  \end{code}
\end{frame}

\begin{frame}[fragile]{Computing your average grade}
  \newcommand\equal{=}
  \squeezeSpace
  ⚓\begin{TWOCOLUMNS}
  \emph{create a new accumulator}
  \begin{lcode}[output={%
          val grades \equal{} ❴add\equal fn,count\equal fn,mean\equal fn,total\equal fn❵:
\qquad: ❴add:real -> unit, count:unit -> int, mean:unit -> real,⏎
\qquad\qquad total:unit -> real❵
}
      ]{EMEL}
val grades = makeAccumulator();
  \end{lcode}
  \emph{record grade in CS101}
  \begin{lcode}[output = {val it \equal{} () : unit}]{EMEL}
(#add grades)(82.0);
  \end{lcode}
  \emph{record grade in PL (\emph{note that the parenthesis are optional})}
  \begin{lcode}[output = {val it \equal{} () : unit}]{EMEL}
#add grades (100.0);
  \end{lcode}
  \MIDPOINT
  \emph{record grade in calculus}
  \begin{lcode}[output = {val it \equal{} () : unit}]{EMEL}
#add grades (37.0);
  \end{lcode}
  \emph{how many grades so far?}
  \begin{lcode}[output = {val it \equal{} 3 : int}]{EMEL}
#count grades ();
  \end{lcode}
  \emph{what's their total?}
  \begin{lcode}[output = {val it \equal{} 219.0 : real}]{EMEL}
#total grades ();
  \end{lcode}
  \emph{what's my GPA?}
  \begin{lcode}[output = {val it \equal{} 73.0 : real}]{EMEL}
#mean grades ();
  \end{lcode}
  \end{TWOCOLUMNS}
\end{frame}

\subunit{Function objects of \protect\CC}

\begin{frame}[fragile,label=current]{Emulation closures with \CC function objects}
  Class of ‟adding something” function objects:
  \begin{lcode}{CPLUSPLUS}
class FunctionObjectForAdding {¢¢
  public:
    FunctionObjectForAdding(int _b): b(_b) {}
    int operator() (int a) {¢¢ return a + b; }
  private:
      int b; // Saved environment
};
  \end{lcode}
  \begin{itemize}
    \item A function object stores a copy of relevant pieces of the environment as data members of the class.
    \item Environment copy is managed by the function object, which, just like any other \CC variable, can be
          \begin{itemize}
            \item present on the stack
            \item allocated from the heap
            \item global
          \end{itemize}
    \item Memory management is the programmer's responsibility.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Using \CC's function objects}
  \begin{TWOCOLUMNS}[15ex]
    \begin{lCode}{CPLUSPLUS}{Usage:}
¢¢
#include <iostream>
¢¢
int main() {¢¢
  FunctionObjectForAdding add3(3);
  std::cout << add3(7);
  std::cout << add3(12);
  FunctionObjectForAdding add8(8);
  std::cout << add8(12);
  return 0;
}
    \end{lCode}
    \MIDPOINT
    \textbf{Output:}⏎
    \begin{session}
10
15
20
    \end{session}
  \end{TWOCOLUMNS}
\end{frame}

\subunit{Function objects in \protect\Java}

\begin{frame}[fragile,label=current]{Closures in \Java?}
  \begin{itemize}
    \item In \Java the only first class values are objects.
    \item Hence, in \Java functions are second class citizens.
    \item \Java does not have real closures.
    \item Still, you can imitate closures with objects.
  \end{itemize}
  Imitation is just like \CC's function objects
\end{frame}

\begin{frame}[fragile,label=current]{Function objects with \Java}
  \squeezeSpace[2]
  \begin{lCode}{JAVA}{Class of function object}
class Log {¢¢
  final double logBase; // Captured environment
  Log(final double base) {¢¢
    logBase = Math.log(base);
  }
  public double apply(double v) {¢¢
    return Math.log(v) / logBase;
  }
}
  \end{lCode}
  Using the function object:
  \begin{lcode}[output=Log base 2 of 1024 is 10.0]{JAVA}
public class L {¢¢
  public static void main(String[] args) {¢¢
    final Log log2 = new Log(2);
    System.out.println("Log base 2 of 1024 is " + log2.apply(1024));
  }
}
  \end{lcode}
\end{frame}

\begin{frame}[fragile]{An interface for function objects}
  \squeezeSpace[3]
  \begin{lCode}{JAVA}{Abstract function type}
interface Function<T, R> {¢¢ R apply(T t); }
  \end{lCode}
  \medskip
  \begin{TWOCOLUMNS}
    \begin{lcode}{JAVA}
class LOG implements Function<
  Double,
  Double //
> {¢¢
  // captured environment:
  final Double logBase;
  LOG(final Double base) {¢¢
    logBase = Math.log(base);
  }
  public Double apply(Double t) {¢¢
    return Math.log(t) / logBase;
  }
}
    \end{lcode}
    \MIDPOINT
    Why \cc{Double}? Why not \kk{double}?
    \begin{itemize}
      \item \Java primitive types follow \alert{value} semantics
      \item all other types follow \alert{reference} semantics
      \item \Java generics \alert{require} reference semantics
      \item hence, type \kk{double} is not first class
      \item use reference type \cc{Double} instead:
    \end{itemize}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile]{Function objects in \protect\Java}
  \squeezeSpace[4]
  \begin{lcode}{JAVA}
class LOG implements Function<Double, Double> {¢¢
  // captured environment:
  final Double logBase;
  LOG(final Double base) {¢¢
    logBase = Math.log(base);
  }
  public Double apply(Double t) {¢¢
    return Math.log(t) / logBase;
  }
}
  \end{lcode}
  Using the function object:
  \begin{lcode}[output=Log base 2 of 1024 is 10.0]{JAVA}
public class M {¢¢
  public static void main(String[] args) {¢¢
    Function<Double,Double> log2 = new LOG(2.0);
    System.out.println("Log base 2 of 1024 is " + log2.apply(1024.));
  }
}
  \end{lcode}
\end{frame}

\begin{frame}[fragile]{Another use of the interface for function objects}
  \squeezeSpace
  \begin{itemize}
    \item \kk{interface} \cc{Function} is handy in other situations
    \item It is part of the standard library of \Java 8
  \end{itemize}
  \begin{code}{JAVA}
class IntMap
  implements Function <Integer, String> {¢¢
    private final String[] map;
    public IntMap(String[] map) {¢¢
      this.map = map;
    }
    public String apply(Integer v) {¢¢
      return v >= 0 && v < map.length
            ? map[v]
            : "" + v;
    }
}
  \end{code}
  Note minor problem:
  \begin{itemize}
    \item Generics do \alert{not} take as parameters atomic types such as \kk{int} and \kk{double}
    \item Use instead equivalent reference types: \cc{Integer}, \cc{Double},…
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Using objects of type \texttt{Function<T,R>}}
  \squeezeSpace
  Storing function objects in variables:
  \begin{TWOCOLUMNS}
    \begin{lcode}{JAVA}
Function<Integer,String> inRoman =
  new IntMap(
    new String[] {¢¢
      "i", "ii", "iii", "iv", "v"
    }
  );
Function<Integer,String> inEnglish =
  new IntMap(
    new String[] {¢¢
      "one", "two", "three",
    }
  );
    \end{lcode}
    \MIDPOINT
    Applying function objects:
    \begin{lcode}{JAVA}
for (int i = 0; i < 10; ++i)
  System.out.println(i +
    "\t" + inRoman.apply(i) +
    "\t" + inEnglish.apply(i)
  );
    \end{lcode}
    \begin{session}
0 i one
1 ii two
2 iii three
3 iv 3
4 v 4
5 5 5
6 6 6
7 7 7
8 8 8
9 9 9
    \end{session}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile]{Functions returning function objects}
  Instead of calling the constructor, one may use
  \alert{factory functions}:
  \begin{Code}{JAVA}{Class with factory method}
class IntMap implements Function<Integer, String> {¢¢
  ¢…¢
    // Constructor is ¢\kk{private}¢!
    private IntMap(String[] map) {¢¢ ¢…¢ }
    // Factory function:
    public static IntMap mapping(String[] map) {¢¢
        return new IntMap(map);
      }
}
  \end{Code}
  Now, write
  \cc{mapping}\cc(…\cc) instead of
  \kk{new }\cc{IntMap}\cc(…\cc):
  \begin{itemize}
    \item Slightly shorter syntax
    \item Conceal the fact that a new object is created
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Using factory functions}
  \squeezeSpace
  \begin{TWOCOLUMNS}
    Using \emph{factory function} \kk{mapping} to create two function objects:
    \begin{code}{JAVA}
IntMap inLatin = mapping(
  new String[] {¢¢
    "i", "ii", "iii", "iv", "v"
  }
);
IntMap inEnglish = mapping(
  new String[] {¢¢
    "one", "two", "three",
  }
);
    \end{code}
    \MIDPOINT
    Using the returned function objects:
    \medskip
    \begin{lcode}{JAVA}
for (int i = 0; i < 10; ++i)
  System.out.println(i +
    "\t" + inRoman.apply(i) +
    "\t" + inEnglish.apply(i)
  );
    \end{lcode}
    \begin{session}
0 i one
1 ii two
2 iii three
3 iv 3
4 v 4
5 5 5
6 6 6
7 7 7
8 8 8
9 9 9
    \end{session}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile]{Smarter function objects with inner classes}
  \squeezeSpace[3]
  In the above, the programmer must \Red{manually} store the ‟environment”:
  \begin{TWOCOLUMNS}[-5ex]
    \begin{lcode}{JAVA}
class Log {¢¢
  final double logBase;
  Log(final double base) {¢¢
    logBase = Math.log(base);
  }
  ¢…¢
}
    \end{lcode}
    \MIDPOINT
    \begin{lcode}{JAVA}
class LOG implements Function<Double,
                              Double> {¢¢
  final Double logBase;
  LOG(final Double base) {¢¢
    logBase = Math.log(base);
  }
  ¢…¢
}
    \end{lcode}
  \end{TWOCOLUMNS}
  \textbf{Idea:}
  \begin{itemize}
    \item In (all but most ancient versions of) \Java
          you can define local classes: classes inside the scope of a method.
    \item Bindings made in a class~$C$ are available to
          \begin{itemize}
            \item are available to a function~$f$ defined in~$C$,
            \item are available to a class~$C'$ defined in~$f$,
            \item are available to a function~$f”$ defined in~$C'$,
            \item are available to a class~$C”$ defined in~$f”$,
            \item …
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Function objects with inner classes}
  \squeezeSpace
  \textbf{The trick:} Use inner classes; inner classes do
  \Red{close} on the environment:
  \begin{TWOCOLUMNS}[4ex]
    \begin{Code}[output=42]{JAVA}{A \Java Local Class}
public class C {¢¢
  static Object foo(final¢\Y<2->[]{crucial!}¢ int a) {¢¢
    class Inner {¢¢
      public int hashCode() {¢¢
        return (a-1)*a*(a+1)*(2*a + 3);
      }
    }
    return new Inner();
  }
  public static void main(String[] args) {¢¢
    Object o = foo(6);
    ¢…¢
    System.out.println(o.hashCode());
  }
}
    \end{Code}
    Output is \cc{42}!
    \MIDPOINT
    \small
    \begin{itemize}
      \item \textbf{Q:} How does \Java save the value of \cc{a}?
      \item \textbf{A1:} Activation records are allocated by the GC.
      \item \textbf{A2:} All accessible variables in the environment are \Red{copied} to the activation record.
            \footnotesize
            \begin{itemize}
              \item No BP!
              \item Simplified implementation of environment.
              \item Variables of the environment must be \kk{final}
            \end{itemize}
    \end{itemize}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile]{Inner class of factory function}
  \squeezeSpace[3]
  More power is drawn from the combination of:
  \begin{itemize}
    \item factory functions
    \item inner classes
  \end{itemize}
  \begin{TWOCOLUMNSc}[-20ex]
    If the factory function returns an instance of an inner class, function
    objects become even simpler:
    \MIDPOINT
    \begin{code}{JAVA}
// Should be ¢\kk{public}¢ to be useful:
public
  // Factory methods tend to be ¢\kk{static}¢
  // (why require an existing object before creating
  // a new object?)
  static
    // Abstract function type:
    Function<Integer,String>
      mapping(String[] map) {¢¢
        // Local class of function ¢\cc{mapping}¢:
        class IntMap
          implements Function<Integer, String> {¢¢
            public String apply(Integer v) {¢¢
              ¢…¢ // as before
    }
  }
  // Function ¢\cc{mapping}¢ returns a newly
  // created instance of its local class:
  return new IntMap();
}
    \end{code}
  \end{TWOCOLUMNSc}
  Look mom! No hands!
  \Q{Inner class \cc{IntMap} does \Red{not} have a \cc{map} data member!}
\end{frame}
\afterLastFrame

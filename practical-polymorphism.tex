\subunit[actual-overloading]{Overloading in \protect\Pascal, \protect\CPL/\protect\CC, and \protect\Java}

\begin{frame}[fragile]{Keyword overloading in \protect\CC}
  \squeezeSpace
  Meanings of \CC's ‟\kk{static}” keyword are only vaguely related:
  \begin{table}[H]
    \begin{adjustbox}{H=0.59}
      \coloredTable
      \begin{tabular}{T{0.15}T{0.35}T{0.54}}
        \toprule
        \textbf{Meaning} & \textbf{Example} & \textbf{Comments}⏎
        \midrule
        \emph{Scope} &
        \vspace{-2ex}
        \begin{code}[width=\linewidth]{CEEPL}
        ¢\vspace{-6ex}¢
static char buff[1000];
        \end{code}
        &
        When applied to definitions made at the outer most level of a file†
        {in Legalese ‟compilation unit” is a sophisticated and only slightly more general
        word for ‟file”}
        Antonym of \kk{extern}; global in file, but inaccessible from other files⏎
        \hline
        \emph{Storage class} &
        \vspace{-2ex}
        \begin{code}[width=\linewidth]{CEEPL}
        ¢\vspace{-6ex}¢
int counter(void) {¢¢
  static int val = 0;
  return val++;
}
        \end{code}
        & Do not place on the stack. Shared by all invocations of the function.
        Antonym of \kk{auto}; value persists between different invocations.⏎
        \hline
        \emph{Not an instance member} &
        \vspace{-2ex}
        \begin{code}[width=\linewidth]{CEEPL}
        ¢\vspace{-6ex}¢
class Book {¢¢
    static int n;
  public:
     Book() {¢¢ ++n; }
    ¢¢~Book() {¢¢ --n; }
};
        \end{code}
        & Shared by all instances of a \protect\kk{struct} or a \protect\kk{class}.⏎
        \bottomrule
      \end{tabular}
    \end{adjustbox}
    \Caption[Overloading of keyword \protect\kk{static} in~\protect\CPL]
  \end{table}
\end{frame}

\begin{frame}[fragile=singleslide]{Builtin operator overloading in~\protect\CPL}
  \begin{itemize}
    \item Keyword overloading does \Red{not} make entities with more than one type.
    \item Keyword overloading is \Red{not} type polymorphism
  \end{itemize}
  Many builtin operators offer overloaded semantics
  \begin{code}{CEEPL}
foo(int a, int b, double x, double y)
{¢¢
  a + b; /* Integer addition */
  x + y; /* Floating point addition */
}
  \end{code}
\end{frame}

\begin{frame}{Builtin overloading of operator ‟\protect\cc{*}” in \protect\CPL}
  \begin{description}[Floating point multiplication]
    \item [Integer multiplication] \( \kk{int} ⨉ \kk{int} → \kk{int} \)
    \item [Long integer multiplication] \( \kk{long} ⨉ \kk{long} → \kk{long} \)
    \item [Floating point multiplication] \( \kk{double} ⨉ \kk{double} → \kk{double} \)
    \item [Pointer dereferencing] \( \text{\emph{Pointer}}(σ) → σ \) for any type~$σ$
  \end{description}
  ‟\cc{*}” has another overloading in type definitions,
  but this overloading is not considered polymorphism.
\end{frame}

\begin{frame}[fragile]{Builtin operator overloading in \protect\Pascal}
  Operator ‟\cc{-}” in \Pascal serves for
  \begin{description}[Integer subtraction]
    \item [Integer negation] \( \cc{Integer} → \cc{Integer} \)
    \item [Real negation] \(\cc{Real} → \cc{Real}\)
    \item [Integer subtraction] \(\cc{Integer} ⨉ \cc{Integer} → \cc{Integer}\)
    \item [Real subtraction] \(\cc{Real} ⨉ \cc{Real} → \cc{Real}\)
    \item [Set difference] \(\cc{Set}(σ) ⨉ \cc{Set}(σ) → \cc{Set}(σ) \),
    where~$σ$ is
    any of the types for which \Pascal's \kk{set}s can be created
  \end{description}
  \begin{Block}[minipage,width=0.86\columnwidth]{Parametric polymorphism \vs overloading}
    One of the overloaded meanings of ‟\cc{-}” follows parametric 
      polymorphism
    \begin{equation}
      ∀σ∈𝕋_{\Pascal}∙ \Big((𝒫σ ⨉𝒫σ)→𝒫σ\Big) ∈\textsf{types}(\text{‟\cc{-}”})
      \synopsis{One of the overloaded meanings of operator ‟\cc{-}” follows parametric polymorphism}
    \end{equation}
  \end{Block}
\end{frame}

\begin{frame}[fragile=singleslide]{User defined operator overloading in \CC}
  \begin{Code}{CPLUSPLUS}{Overloading operator \ccn{+=}}
class Rational {¢¢
  public:
    Rational(double);
    const Rational& operator += (const Rational& other);
    ¢\textrm{…}¢
};
  \end{Code}
\end{frame}

\begin{frame}[fragile=singleslide]{More operator overloading opportunities in \CC}
  \alert{In \CC you can overload even stuff you did not know was an operator}
  \begin{itemize}
    \item Including ‟\cc{()}”, the ‟function call” operator
    \item Including the ‟type casting” operator
    \item Including ‟\cc{,}”, the comma operator
    \item Including ‟\cc{[]}”, the array access operator
    \item Including ‟\cc{*}”, the dereferencing operator
    \item Including ‟\cc{->*}”, the field access operator
    \item …
  \end{itemize}
  not so easy to learn and use
\end{frame}

\begin{frame}[fragile=singleslide]{Overloading in \protect\Java}
  \emph{Even if you do not know \Java, you should be able understand and
  apply the following:}
  \begin{description}[Programmer defined function overloading:]
    \item [Builtin operator overloading:] Similar to \CC
    \q{‟\cc{+}” serves also for string concatenation.}
    \item [Programmer defined operator overloading:] None.
    \q{Language designer did not wish to replicate the \CC nightmare.}
    \item [Builtin function overloading:] None.
    \q{\Java just like many other languages has no ‟builtin” functions.}
    \item [Programmer defined function overloading:] Similar to \CC.
  \end{description}
\end{frame}

\subunit[tournament]{Coercion and the \CC overloading tournament}

\begin{frame}[fragile=singleslide]{Coercion in \ML}
  \squeezeSpace
  No mixed type arithmetic in \ML:
  \begin{code}{EMEL}
- 1+1;
val it = 2 : int
- 1.0+1.0;
val it = 2.0 : real
- 1+1.0;
stdIn:7.1-7.6 Error: operator and operand
                                  don't agree [literal]
  operator domain: int * int
  operand: int * real
  in expression:
    1 + 1.0
-
  \end{code}
  No implicit coercion from \kk{int} to \kk{real}; must use function \kk{real}
  \begin{session}
- real;
val it = fn : int -> real
- (real 1) + 1.0;
val it = 2.0 : real
-
  \end{session}
\end{frame}

\begin{frame}[fragile=singleslide]{Programmer defined coercion in \protect\CC}
  Can be done by
  \begin{itemize}
    \item Defining a (non-\kk{explicit}%
          †{%
            In \CC, an \kk{explicit} constructor, i.e.,
            a constructor whose definition is adorned with the
            \kk{explicit} keyword is a constructor which will not be employed
            for implicit coercion;
            it can only be used if invoked explicitly.
          })
          constructor with a single argument
    \item Overloading the type cast operator
  \end{itemize}
  \begin{TWOCOLUMNS}
    \begin{code}{CPLUSPLUS}
class Rational {¢¢
   public:
     Rational(double);
     explicit Rational(
       const char *s
     );
     operator double(void);
     ¢\textrm{…}¢
};
    \end{code}
    \MIDPOINT
    \begin{code}{CPLUSPLUS}
Rational r = 2; // Builtin
// coercion from ¢\kk{int}¢ to ¢\/\kk{double}¢ and
// then programmer-defined coercion
// from ¢\kk{double}¢ to ¢\/\cc{Rational}¢
double d = sqrt(r);
// Programmer-defined coercion
// from ¢\cc{Rational}¢ to ¢\kk{double}¢
Rational h = "half"; // Error
Rational h =
  Rational("half"); // OK
    \end{code}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile=singleslide]{The overloading tournament in \CC}
  In every function call site \cc{foo(a1,a2, …, an)}, there could
  be many applicable overloaded versions of \cc{foo}. \CC applies context
  independent, compile-time \emph{tournament} to select the most appropriate
  overload.
  \begin{present}[0.74]{Ranking of coercion operations (short version)}
    \scriptsize
    \begin{description}[Programmer defined]
      \item [None or unavoidable] array~$→$ pointer,~$T→ \kk{const } T$, …
      \item [Size promotion]~$\kk{short}→\kk{int}$,~$\kk{int}→\kk{long}$,~$\kk{float}→\kk{double}$, …
      \item [Standard conversion]~$\kk{int}→\kk{double}$,~$\kk{double}→\kk{int}$,~$\kk{Derived*}→\kk{Base*}$,
      \item [Programmer defined] by constructor or operator overloading
      \item [Ellipsis] e.g., \kk{int} \cc{printf(\kk{const char} *fmt, …)}
    \end{description}
  \end{present}
  \small
  Winner must be:
  \begin{itemize}
    \item Better match in at least one argument
    \item At least \emph{as good} for every other argument
  \end{itemize}
  An error message if no single winner is found
\end{frame}

\begin{frame}[fragile=singleslide]{A tournament example}
  Resolve ambiguity of the function call
  \[
    \cc{max(a,b)}
  \] where,
  \begin{itemize}
    \item \cc{a} is of type \kk{float}
    \item \cc{b} is of type \cc{Rational}
  \end{itemize}
  and with two candidates:
  \begin{description}
    \item [I] \cc{\kk{double} max(\kk{double}, \kk{double})}
    \item [II] \cc{Rational max(\kk{long} \kk{double}, Rational)}
  \end{description}
  \small
  \begin{TWOCOLUMNS}
    \structure{Signature~$⟨\kk{double}, \kk{double}⟩$}
    \begin{description}
      \item [\nth{1} argument]~$\kk{float}→\kk{double}$
      \item [\nth{2} argument]~$\cc{Rational}→\kk{double}$
    \end{description}
    \MIDPOINT
    \structure{Signature~$⟨\kk{long double}, \cc{Rational}⟩$}
    \begin{description}
      \item [\nth{1} argument]~$\kk{float}→\kk{long double}$
      \item [\nth{2} argument] none
    \end{description}
  \end{TWOCOLUMNS}
  \justifying
  \begin{description}
    \item [First argument] equally good (size promotion)
    \item [Second argument] second contestant wins (‟none" is better than ‟programmer defined")
  \end{description}
  \alert{second contestant wins}.
\end{frame}

\begin{frame}[fragile=singleslide]{More tournament examples}
  With the declarations made previously, which version of \cc{max} would the
  following invoke? \[
  \cc{max}(\cc{Rational}(3),'\setminus')
\]
Given
\begin{code}{CPLUSPLUS}
void foo(int) {¢¢ cout << "int"; }
void foo(char) {¢¢ cout << "char"; }
void foo(char *) {¢¢ cout << "char *"; }
void foo(const char *) {¢¢ cout << "const char *"; }
\end{code}
What will be printed?
\begin{code}{CPLUSPLUS}
void bar() {¢¢ foo(0); }
\end{code}
⚓\cc{int}
\end{frame}

\begin{frame}[fragile=singleslide]{Overloading + coercion + parametric + inclusion = \protect \CC style headache!}
  \begin{itemize}
    \item Parametric polymorphism may contribute to ambiguity 
      \begin{code}{CPLUSPLUS}
template <typename T>
  const T & max(const T &a, const T &b) {
    return a > b ? a : b;
  }
    \end{code}
    \item Inheritance may contribute to ambiguity
    \item The ‟overloading” tournament is not limited to overloading
    \item Certain PLs languages forbid overloading and coercion
          and restrict parametric polymorphism for precisely this reason.
  \end{itemize}
\end{frame}

\subunit[polymorphic-functions]{Polymorphic functions}

\begin{frame}[fragile=singleslide]{What are polymorphic functions?}
  \squeezeSpace
  \furnish{%
    Monomorphic typing with hidden,
    Ad hoc tree with hidden,
    Inclusion tree with hidden,
    Polytypes with hidden,
    Parametric with current
  }
  \input{four-polymorphisms.tikz}
  \par
  \WD[0.9]{Polymorphic functions}
  {Functions that can work on a variety of types; a kind of
    \emph{parametric polymorphism}
    i.e., polymorphism occurring for unboundedly many \emph{related} types.
    The type variety \emph{may} or \emph{may not} show up as an
    explicit parameter.
  }
\end{frame}

\begin{frame}[fragile=singleslide]{\protect\cc{write} \vs~\protect\cc{eof} in \Pascal}
  \begin{TWOCOLUMNS}
    \structure{\cc{write($E$)}}
    \small
    \begin{itemize}
      \item Effect depends on the type of \cc{$E$}: type \cc{Char},
            type \cc{String}, type \cc{Integer},…
      \item The identifier \cc{write} simultaneously denotes several
            distinct procedures, each having its own type
      \item \emph{Overloading}
      \item (We ignore in this course the ‟magic” of \cc{Write}
            taking multiple parameters, where each can be of a different type.)
    \end{itemize}
    \MIDPOINT
    \normalsize
    \structure{$\cc{eof}(F)$}
    \small
    \begin{itemize}
      \item Type is:~$\cc{File}(σ)→ \cc{Boolean}$, where~$σ$ is \emph{any} type
      \item Function is \emph{polymorphic} ('many-shaped').
      \item Argument types:
            \kk{File of Char}, \kk{File of Integer}, etc.
      \item operates \emph{uniformly} on all of argument types
    \end{itemize}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile=singleslide]{Polymorphic functions with \CC's templates}
  \squeezeSpace[6]
  \begin{TWOCOLUMNS}[5ex]
    \begin{Code}[minipage,width=0.96\columnwidth]{CPLUSPLUS} {Definition of a function template}
template<typename Type>
  Type max(Type a, Type b) {¢¢
    return a > b ¢¢? a : b;
  }
    \end{Code}
    \MIDPOINT
    \begin{Code}[minipage,width=0.9\columnwidth]{CPLUSPLUS}{Using template functions}
int x,y,z;
double r,s,t;
z = max(x,y);
t = max(r,s);
    \end{Code}
  \end{TWOCOLUMNS}
  \medskip
  \note{\kk{typename}: a \CC keyword denoting type parameters.}
  \structure{Type Parameters}
  \small
  \begin{itemize}
    \item Explicitly declared
    \item Inferred upon use
    \item Can even make this inference
          \begin{Code}[minipage,width=0.8\columnwidth]{CPLUSPLUS}{Implicit instantiation of \CC function template}
unsigned long // return type
   (*pf) // variable name
(unsigned long, unsigned long) // argument types
= max; // assignment
    \end{Code}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]{If \Pascal allowed polymorphic functions…}
  \begin{code}{PASCAL}
function disjoint(s1, s2: set of ¢$σ$¢) :Boolean;
  begin
    disjoint := (s1 * s2 = [])
  end
VAR chars : set of Char;
    ints1, ints2 : set of 0..99;
¢\textrm{…}¢
if disjoint(chars, ['a','e','i','o','u']) then ¢\textrm{…}¢
if disjoint(ints1, ints2) then ¢\textrm{…}¢
  \end{code}
  \WD{Type variables/type parameters}{%
    Type expressions like~$σ$ in the definition of
    \cc{disjoint} are called \emph{type variables}
    or \emph{type parameters}.
  }
\end{frame}

\begin{frame}[fragile=singleslide]{Polymorphic functions in \ML}
  \squeezeSpace
  \emph{Type variables} are used in \ML to define parametric polymorphism:
  \begin{TWOCOLUMNS}
    \structure{Definition}
    \begin{code}[minipage, width=20ex]{EMEL}
fun second(x:¢$τ$¢, y:¢$τ$¢) = y
    \end{code}
    or
    \begin{code}[minipage, width=20ex]{EMEL}
fun second(x,y) = y
    \end{code}
    \emph{Type is~$τ⨉τ →τ$, where~$τ$ is arbitrary.}
    \MIDPOINT
    \begin{description}[Illegal Use]
      \item [Use]\mode<article>{▄}
      \begin{itemize}
        \item \cc{second(13,true)}
        \item \cc{second(name)}⏎
              where \cc{name} is the pair \cc{(1984,"Orwell")}
      \end{itemize}
      \item [Illegal Use]\mode<article>{▄}
      \begin{itemize}
        \item \cc{second(13)}
        \item \cc{second(1983,2,23)}
      \end{itemize}
    \end{description}
  \end{TWOCOLUMNS}
  \begin{session}
Standard ML of New Jersey v110.75 [built: Thu May 9 05:41:01 2013]
- fun second(x, y) = y;
val second = fn : 'a * 'b -> 'b
- fun second(x:'t, y:'t) = y;
val second = fn : 'a * 'a -> 'a
-
  \end{session}
\end{frame}

\begin{frame}[fragile=singleslide]{Polymorphic functions taking function parameters}
  \squeezeSpace
  Function \cc{twice} takes as a parameter function \cc{f} and returns
  a function \cc{g} such that \cc{g(x)=f(f(x))}:
  \begin{lcode}[width=45ex,minipage]{EMEL}
fun twice(f: ¢$σ → σ$¢) = fn (x: ¢$σ$¢) => f( f(x) )
  \end{lcode}
  e.g.,
  \begin{lcode}{EMEL}
val fourth = twice(sqr)
  \end{lcode}
  Function \kk{o} takes two arguments, functions \cc{f} and \cc{g} and
  returns a function which is their composition:
  \begin{lcode}[minipage,width=50ex]{EMEL}
fun op o (f: ¢$β → γ$¢, g: ¢$α → β$¢) = fn (x:¢$α$¢) => f(g(x))
  \end{lcode}
  e.g.,
  \begin{lcode}{EMEL}
val even = not o odd
  \end{lcode}
  or,
  \begin{lcode}[width=30ex,minipage]{EMEL}
fun twice(f: ¢$σ → σ$¢) = f o f
  \end{lcode}
\end{frame}

\begin{frame}[fragile=singleslide]{Polymorphic identity function in \ML}
  \squeezeSpace
  Identity function~$σ → σ$.
  \begin{code}{EMEL}
fun id(x: ¢$σ$¢) = x
  \end{code}
  represents
  \small
  \begin{itemize}
    \item Identity mapping on booleans
  \end{itemize}
  \begin{equation}
    ❴ \cc{false} → \cc{false}, \cc{true} → \cc{true} ❵
    \synopsis{Identity mapping on booleans}
  \end{equation}
  \begin{itemize}
    \item Identity mapping on integers
  \end{itemize}
  \begin{equation}
    ❴ …, -2→-2, -1→-1, 0→0, 1→1, 2→2, … ❵
    \synopsis{Identity mapping on integers}
  \end{equation}
  \begin{itemize}
    \item Identity mapping on strings
  \end{itemize}
  \begin{equation}
    \left❴
    \begin{array}{l}
      ε → ε,⏎
      \cc{"a"} → \cc{"a"}, \cc{"b"} → \cc{"b"}, …,⏎
      \cc{"aa"} → \cc{"aa"}, \cc{"ab"} → \cc{"ab"}, …,⏎
      ⋮⏎
    \end{array}
    \right❵
    \synopsis{Identity mapping on strings}
  \end{equation}
\end{frame}

\subunit[type-inference]{Type inference}

\begin{frame}[fragile=singleslide]{Type inference}
  \squeezeSpace
  \alert{The type of an entity is inferred, rather than explicitly stated.}
  \small
  \begin{description}
    \item [\Pascal] Constant definition:
    \begin{code}{PASCAL}
CONST pi=3.14159264590;
    \end{code}
    {\scriptsize
      \begin{enumerate}
        \item \cc{3.14159264590} is of type \cc{Real}.
        \item Therefore, \cc{pi} is of type \cc{Real}.
      \end{enumerate}
    }
    \item [\ML†{\ML allows to voluntarily state types of a declared
            entity. Explicitly stating types, even if redundant, is usually a good
    programming practice.}] Function definition
    \begin{EMEL}
  fun even(n) = (n mod 2 = 0)
    \end{EMEL}
    {\scriptsize
      \begin{enumerate}
        \item \cc{mod} is of type~$\cc{int}⨉\cc{int}→ \cc{int}$;
        \item Since \cc{n} occurs in \cc{n mod 2}, \cc{n} is of type \cc{int}.
        \item The type of operator \cc{=} is~$σ⨉σ → \cc{bool}$
              for all~$σ$;
        \item \cc{n} occurs in \cc{n mod 2}, so \cc{n} is of type \cc{int}.
        \item Therefore, the type of \cc{n mod 2 = 0} is \cc{bool}
        \item It follows that the type of \cc{even} is \[
              \cc{int}→ \cc{bool}
      \]
      \end{enumerate}
    }
  \end{description}
\end{frame}

\begin{frame}[fragile=singleslide]{Type inference does not always produce the desired result}
  Define a \cc{max} function in \ML:
  \begin{Code}{EMEL}{A max Function in \ML}
- fun max(x,y) =
    if x > y then x else y;
val max = fn : int * int -> int
  \end{Code}
  \tikz[remember picture] \node (a) {\vphantom{X}};
  But we want \cc{max}
  to operate on reals:
  \begin{Figure}[The ancient, obsolete, annoying, yet relatively easy to explain memory model of the 8086]
    \begin{adjustbox}{}
      \begin{tikzpicture}[remember picture,overlay]
        \path<2> (a.east) ++(0,1) node[%
          anchor=west,%
          rectangle callout,%
          fill=red!20,%
          font=\scriptsize,
          text width=40ex,%
          opacity=.7, %
          align=justify,%
          callout absolute pointer={(a.mid)}%
        ] {%
          Since \ML does not allow programmer
          defined overloading, we can only
          have one version of function \cc{max}⏎
        }
        ;
      \end{tikzpicture}
    \end{adjustbox}
  \end{Figure}
  \begin{Code}{EMEL}{Argument type declaration}
- fun max(x:real,y:real) =
    if x > y then x else y;
val max = fn : real * real -> real
  \end{Code}
\end{frame}

\begin{frame}{Polymorphic type inference}
  Type inference might yield a monotype
  \begin{itemize}
    \item As for the function \cc{even}
  \end{itemize}
  Type inference might yield a polytype
  \begin{itemize}
    \item \cc{fun id(x) = x}
          \begin{itemize}
            \item The type of \cc{id} is~$σ → σ$
          \end{itemize}
    \item \cc{fun op o (f, g) = fn (x) => f (g (x))}
          \begin{itemize}
            \item We can see from the way they are used that
                  \cc{f} and \cc{g} are functions.
            \item The result of \cc{g} must be the same
                  as the argument type of \cc{f}.
            \item Thus, type of \kk{o} can be inferred:
                  \begin{equation}
                    \kk{o}∈(β→γ)⨉(α→β)→(α→γ)
                    \synopsis{Type of operator \protect\kk{ok} in \ML}
                  \end{equation}
          \end{itemize}
  \end{itemize}
\end{frame}

\subunit[parameter-checking]{Checking parameters with parametric polymorphism}

\begin{frame}[fragile=singleslide]{Checking type parameters: in \CC}
  \begin{columns}
    \column[t]{0.25\columnwidth}
    \small
    Templates are checked when they are instantiated, not when they are defined:
    \column[t]{0.73\columnwidth}
    \begin{Code}{CPLUSPLUS}{Instantiation of a \CC function template}
template <typename T> // a ‟function template”
  const T& max(const T &a, const T &b) {¢¢
    return a > b ¢¢? a : b;
  }
int a = max(2/3,3/2); // a ‟template function'
double d = max(2.3,3.2); // another template function
// And, a third template function
struct S {¢…¢} s1, s2, s3 = max(s1,s2);
    \end{Code}
  \end{columns}
  \begin{session}
gcc max.C
max.C: In instantiation of
  ‘const T& max(const T&, const T&) [with T = S]’:
max.C:7:25: required from here
max.C:3:14: error: no match for ‘operator>’
  (operand types are ‘const S’ and ‘const S’)
     return a > b ? a : b;
              ^
  \end{session}
\end{frame}

\begin{frame}[fragile=singleslide]{Checking type parameters in \ML}
  Polymorphic functions are checked when they are defined, not when they are used.
  \scriptsize
  \begin{session}
Standard ML of New Jersey v110.75 [built: Thu May 9 05:41:01 2013]
- fun max(a:'T, b:'T): 'T = if a > b then a else b;
stdIn:1.28-1.50 Error: operator and operand don't agree [UBOUND match]
  operator domain: 'Z * 'Z
  operand: 'T * 'T
  in expression:
    a > b
  \end{session}
  \normalsize
  Cannot define a polymorphic \cc{max} function, since most
  types do not have a ‟greater than” operator, and the language
  does not offer overloading.
\end{frame}

\begin{frame}{Polymorphic functions: \CC \vs \ML}
  \small
  \begin{table}[H]
    \begin{adjustbox}{}
      \coloredTable
      \begin{tabular}{R{0.33}L{0.33}L{0.33}}
        \toprule
                                              & \textbf{\ML}   & \textbf{\CC}⏎
        \midrule
        \emph{Declaration of Type Parameters} & Optional       & Obligatory⏎
        \emph{Passing Type Arguments}         & Optional       & No⏎
        \emph{Checking}                       & On Declaration & On Instantiation⏎
        \bottomrule
      \end{tabular}
    \end{adjustbox}
    \Caption
  \end{table}
  \structure{Notes:}
  \small
  \begin{itemize}
    \item \ML can make a more sophisticated type inference than \CC.
    \item In fact, \ML can make deductions based on functions return type.
    \item Overloading complicates type inference
    \item For that reason, \ML does not allow programmer defined overloading
    \item And, for that reason, \ML ignores its own builtin overloading when
          conducting type inference.
  \end{itemize}
\end{frame}

\begin{frame}{Parametric polymorphism: \ML \vs \Ada \vs \CC}
  \footnotesize
  \begin{THREECOLUMNS}
    \structure{\normalsize \ML}
    \begin{itemize}
      \item Elegant syntax
      \item Type inference
      \item Checking at definition time
      \item Implicit instantiation
      \item Limited power, since no restrictions on type parameter\note{except for equality}
    \end{itemize}
    \MIDPOINT
    \structure{\normalsize \Ada}
    \begin{itemize}
      \item Verbose, and readable, but heavy syntax.
      \item No type inference
      \item Checking at definition time
      \item Explicit instantiation
      \item Explicit restrictions on type parameter
    \end{itemize}
    \MIDPOINT
    \structure{\normalsize \CC}
    \begin{itemize}
      \item Ugly, kludge and unreadable syntax
      \item Type inference on invocation
      \item Checking upon instantiation
      \item Implicit instantiation%
            †{of function templates, explicit function template instantiation is possible}
      \item Implicit restrictions on type parameter†{Recent versions of \CC allow an explicit list of constraints on type parameters}
    \end{itemize}
  \end{THREECOLUMNS}
\end{frame}

\begin{frame}[fragile=singleslide]{Const exercises}
  \begin{itemize}
    \item Given are the following definitions.
          \begin{code}{CEEPL}
typedef char* t1;
typedef char* const t2;
typedef const char* t3;
typedef const char* const t4;
¢¢
t1 c1;
t2 c2;
t3 c3;
t4 c4;
    \end{code}
    \item Determine for all~$i$,~$j$,~$k$ which of the following commands
          will legally compile?
          \begin{itemize}
            \item~$\cc{c}ᵢ = \cc{c}ⱼ$;
            \item~$\cc{c}ᵢ = \cc{const\_cast<t}ⱼ\cc{>(} \cc{c}ₖ\cc{)}$;
            \item~$\cc{*c}ᵢ = \cc{*c}ⱼ$;
            \item~$\cc{*const\_cast<t}ᵢ\cc{>(c}ⱼ\cc{)} = \cc{*c}ₖ$;
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]{Polytypes in \Ada: generics}
  \begin{code}{ADA}
generic(type ElementType) module Stack;
export Push,Pop,Empty,StackType,MaxStackSize;
constant MaxStackSize = 10;
type private StackType =
 record
  Size: 0..MaxStackSize := 0;
  Data: array 1..MaxStackSize of ElementType;
 end;
procedure Push(
          reference ThisStack: StackType;
          readonly What: ElementType);
procedure Pop(reference ThisStack):
          ElementType;
procedure Empty(readonly ThisStack):Boolean;
end; -- Stack
module IntegerStack = Stack(integer);
  \end{code}
\end{frame}

\subunit[case-studies]{Case studies}

\begin{frame}[fragile=singleslide]{Case study: universal pointer in~\CPL}
  \par{\large \bfseries Universal pointer type.}
  In~\CPL, a \kk{void*} pointer could be assigned to
  any pointer, and any pointer can be assigned to \kk{void*}.
  \begin{code}{CEEPL}
extern void* malloc(size_t);
extern void free(void*);
void foo(size_t n) {¢¢
  long *buff = malloc(n * sizeof(long));
  ¢\textrm{…}¢
  free(buff);
}
  \end{code}
  \par{\large\bfseries Parametric Polymorphism} In~\CPL the coercion from
  \kk{long*} to \kk{void*} and vice-versa is
  not ad-hoc
  \begin{itemize}
    \item It universally exists for all pointer types
    \item The actions performed are the same for all pointer types
  \end{itemize}
\end{frame}

\begin{frame}{Case study: casting in \CC}
  \CC deprecates~\CPL-style casts; instead there are four cast operations
  \begin{description}[\kk{reinterpret\_cast<$σ$>}]
    \item [\kk{const\_cast<$σ$>}] takes a type~$σ$ and returns a cast
    operator from any type~$σ$ to~$σ$ provided only that~$σ$
    can be obtained from~$σ$ just by adding \cc{const}
    \item [\kk{reinterpret\_cast<$σ$>}]takes a type~$σ$ and returns
    a cast operator from any type~$σ$ to~$σ$ (useful for peeping
    into bit representations)
    \item [\kk{static\_cast<$σ$>}] takes a type~$σ$ and returns a
    cast operator from any type~$σ$, provided this is a standard
    casting (e.g. \kk{double} to \kk{int})
    \item [\kk{dynamic\_cast<$σ$>}] takes a type~$σ$ of a derived
    class and returns a cast operator from any type~$σ$ of its base
    classes into~$σ$.
  \end{description}
\end{frame}

\begin{frame}[fragile=singleslide]{Const exercises}
  \begin{itemize}
    \item Given are the following definitions.
          \begin{code}{CEEPL}
typedef char* t1;
typedef char* const t2;
typedef const char* t3;
typedef const char* const t4;
¢¢
t1 c1;
t2 c2;
t3 c3;
t4 c4;
    \end{code}
    \item Determine for all~$i$,~$j$,~$k$ which of the following commands
          will legally compile?
          \begin{itemize}
            \item~$\cc{c}ᵢ = \cc{c}ⱼ$;
            \item~$\cc{c}ᵢ = \cc{const\_cast<t}ⱼ\cc{>(} \cc{c}ₖ\cc{)}$;
            \item~$\cc{*c}ᵢ = \cc{*c}ⱼ$;
            \item~$\cc{*const\_cast<t}ᵢ\cc{>(c}ⱼ\cc{)} = \cc{*c}ₖ$;
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]{Parametric polymorphism on enumerated types in \Pascal}
  \begin{Code}{PASCAL}{Nonsense code to demonstrate \Pascal's builtin parametric polymorphism}
for m := January to December do
  for d := Saturday downto Sunday do
    case suit of
      Club, Heart:
        suit := succ(suit);
      Diamond, Spade:
        if suit < Heart then
          if ord(m) < ord(d) then
            suit := pred(suit);
    end;
  \end{Code}
  \begin{itemize}
    \item control structure (up and down \cc{for} loops and \cc{case}),
    \item relational operators
    \item \cc{ord}, \cc{succ} and \cc{pred} functions.
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]{Responses to inflexibility in \Java}
  \structure{\Java = \CC minus all ‟complexities”}
  \begin{description}
    \item [Originally] \emph{dynamic typing}
    \begin{code}{JAVA}
Comparator.compare(Object, Object)
    \end{code}
    \item [Now] polymorphic types
    \begin{code}{JAVA}
comparator<T>.compare(T, T)
    \end{code}
  \end{description}
\end{frame}
\afterLastFrame

\exercises
\begin{enumerate}
\item 
  Consider the differences between operators and functions, 
    (consult \cref{\labelPrefix Section:value-systems}).
    Is any of these responsible for \Java's decision against
    operator overloading?
    and explain how 
\end{enumerate}

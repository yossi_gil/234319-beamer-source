\subunit{Motivation}
\newcommand\TREE[1]{%
  \tikz[scale=0.7]
  \tikzstyle{hot} = [rectangle,fill=red!30,draw=red!50!black,thick]
  \graph [
    tree layout,
    level distance=5ex,
    nodes={%
      ellipse,
      font=\scriptsize,
      fill=teal!20,
      draw=teal!50!black,
      inner sep=2pt,
      minimum height=2.2ex,
    }
  ] {#1};
}

\begin{frame}[fragile]{Thinking of expressions as trees}
  \begin{adjustbox}{}
    \begin{tabular}{cc}
      %* \end{tabular}
      \begin{lcode}{EMEL}
fun min(a, b) =
  if a < b then a else b;
      \end{lcode}
      &
      \begin{lcode}{EMEL}
fun max(a, b) =
  a + b - min(a, b);
      \end{lcode}
      ⏎
      \TREE {%
        /$\kk{if}·\kk{then}·\kk{else}·$[hot]-- {%
          /"$\cc<$"[hot] -- {%
            /\cc{a},
            /\cc{b}
          },
          /\cc{a},
          /\cc{b}
        }
      }
      &
      \TREE {%
        /\cc{-}[hot] -- {%
          /\cc{+}[hot] -- {/\cc{a}, /\cc{b}},
          /\cc{min}[hot] -- {/\cc{a}, /\cc{b}}
        }
      }
      ⏎
      \begin{lcode}{EMEL}
fun min3(a, b, c)
  = min(a, min(b, c));
      \end{lcode}
      &
      \begin{lcode}{EMEL}
fun max3(a, b, c)
  = max(a, max(b, c));
      \end{lcode}
      ⏎
      \TREE {/\cc{min}[hot]-- {/\cc{a}, /\cc{min}[hot]-- {/\cc{b}, /\cc{c}}}}&
      \TREE {/\cc{max}[hot]-- {/\cc{a}, /\cc{max}[hot]-- {/\cc{b}, /\cc{c}}}}⏎
      %* \begin{tabular}
    \end{tabular}
  \end{adjustbox}
  In the professional jargon, these trees are called~$λ$-expressions.
\end{frame}

\begin{frame}[fragile]{More on the tree perspective}
  \centering
  \begin{lcode}{EMEL}
fun median(a, b, c) =
  (a + (b + c)) -
    (min3(a, b, c) + max3(a, b, c));
  \end{lcode}
  \begin{Figure}
    [{%
        Evaluation tree for \protect\ML function
        \protect \ccn{median}, defined by
        \protect\kkn{fun}
        \protect\ccn{median(a, b, c) = }
        \protect\ccn{(a + }
        \protect\ccn{(b + c)) }
        \protect\ccn{- }
        \protect\ccn{(min3(a, b, c) +}
        \protect\ccn{max3(a, b, c))}
    }]
    ¶\squeezeSpace[-2]
    \centering
    \TREE {%
      /\cc{-}[hot] -- {%
        /\cc{+}[hot] -- {%
          /\cc{a},
          /\cc{+}[hot] -- {/\cc{b}, /\cc{c}},
        },
        /\cc{+} -- {%
          /\cc{min3}[hot] -- {/\cc{a}, /\cc{b}, /\cc{c}},
          /\cc{max3}[hot] -- {/\cc{a}, /\cc{b}, /\cc{c}}
        }
      }
    }
    ¶\squeezeSpace[-2]
  \end{Figure}
  \begin{description}[Inner nodes]
    \item [Leaves] Arguments, literals, references,…
    \item [Inner nodes] Locations of evaluation
    \begin{itemize}
      \item 1 / 2 / more children
      \item functions / operators
      \item builtin / defined-by-programmer
    \end{itemize}
  \end{description}
\end{frame}

\begin{frame}[fragile]{Applying a function to arguments}
  \begin{TWOCOLUMNSc}[-3ex]
    \begin{tabular}{c}
      \textbf{Function:}
      ⏎
      \TREE {%
      /\cc{-}[hot] -- {%
      /\cc{+}[hot] -- {%
      /\cc{a},
      /\cc{+}[hot] -- {/\cc{b}, /\cc{c}},
      },
      /\cc{+} -- {%
      /\cc{min3}[hot] -- {/\cc{a}, /\cc{b}, /\cc{c}},
      /\cc{max3}[hot] -- {/\cc{a}, /\cc{b}, /\cc{c}}
      }
      }
      }
    \end{tabular}
    \MIDPOINT
    \begin{Block}{Application}
      \cc{median( 0xBeef, 0xCafe, 0xBed )}
    \end{Block}
  \end{TWOCOLUMNSc}
  Evaluation:
  \begin{TWOCOLUMNS}
    \begin{itemize}
      \item using each argument 3 times.
      \item invoking 4 more functions: \cc{min}, \cc{max}, \cc{min3}, \cc{max3}
            \begin{itemize}
              \item some more than once.
            \end{itemize}
    \end{itemize}
    \MIDPOINT
    \begin{itemize}
      \item invoking 4 builtin operators: \mbox{}$\kk{if}·\kk{then}·\kk{else}·$, \cc<, \cc+ and \cc-.
            \begin{itemize}
              \item some more than once
            \end{itemize}
      \item using and passing the original arguments many more times
    \end{itemize}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}{What do we mean by ‟evaluation strategies”?}
  Issues of evaluation, e.g.,
  \begin{itemize}[<+(1)->]
    \item Order of operations
    \item Argument passing
    \item Caching
  \end{itemize}
  \onslide<5->{Two approaches:}
  \begin{description}
    \item <6-> [$λ$-calculus] \indentInArticle
    \begin{itemize}
      \item formal and precise
      \item but,
            \begin{itemize}
              \item \Red{not so intuitive}
              \item \Red{heavy on notation}
            \end{itemize}
    \end{itemize}
    \onslide<8->{\Red{\textbf{some other time…}}}
    \item <7->[Algorithmic] \indentInArticle
    \begin{itemize}
      \item intuitive
      \item minimal notation
      \item but, \Red{not so formal and precise}
    \end{itemize}
    \onslide<9->{\Red{\textbf{our approach!}}}
  \end{description}
\end{frame}

\subunit{Collateral evaluation}
\begin{frame}[fragile]{Evaluation order (revisiting expression ＆ commands)}
  Is this program ‟correct”?
  \begin{code}{CEEPL}
#include <stdio.h>
¢¢
int main() {¢¢
  return printf("Hello, ") - printf("World!\n");
}
  \end{code}
  \begin{itemize}[<+->]
    \item Function \cc{printf} returns the number of characters it prints.\hfill\medskip⏎
          \begin{tabular}[<+->]{cc}
            \begin{code}{CEEPL}sizeof "Hello, " == 7\end{code}
              &
            \begin{code}{CEEPL}sizeof "World!\n" == 7\end{code}
          \end{tabular}
    \item So, \cc{main} returns 0, interpreted as normal termination
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Expressions + side-effects = evaluation order dilemma}
  In evaluating, e.g.,~$X+Y$, the PL
  \Q{\emph{can} decide which of~$X$ and~$Y$ is evaluated first,}
  but,
  \Q{often PLs prefer to \textbf{refrain} from making a decision.}
  \WD[0.85]{Collateral evaluation}
  {Let~$X$ and~$Y$ be two code fragments (expressions or commands).
    Then, all of the following are correct implementations of \Red{collateral execution of~$X$ and~$Y$}
    \squeezeSpace
    \begin{TWOCOLUMNS} ⚓
      \begin{itemize}[<+->]
        \item~$X$ is executed before~$Y$
        \item~$X$ is executed after~$Y$
      \end{itemize}
      \MIDPOINT
      \begin{itemize}[<+->]
        \item ‟interleaved” execution
        \item simultaneous execution
      \end{itemize}
    \end{TWOCOLUMNS}
  }
\end{frame}

\begin{frame}[fragile]{What will be printed?}
  So, in compiling and executing
  \begin{code}{CEEPL}
#include <stdio.h>
¢¢
int main() {¢¢
  return printf("Hello, ") - printf("World!\n");
}
  \end{code}
  there is no telling what will be printed!
\end{frame}

\begin{frame}{Consequence of collateral semantics}
  \squeezeSpace
  Consider an application of function:
  \begin{quote}
    \TREE {%
      /\cc{-}[hot] -- {%
        /\cc{+}[hot] -- {%
          /\cc{a},
          /\cc{+}[hot] -- {/\cc{b}, /\cc{c}},
        },
        /\cc{+} -- {%
          /\cc{min3}[hot] -- {/\cc{a}, /\cc{b}, /\cc{c}},
          /\cc{max3}[hot] -- {/\cc{a}, /\cc{b}, /\cc{c}}
        }
      }
    }
  \end{quote}
  to arguments, e.g., \cc{median( 0xBeef, 0xCafe, 0xBed )}, and using auxiliary functions:
  \begin{quote}
    \begin{adjustbox}{}
      \begin{tabular}{*4c}
        \TREE{/$\kk{if}·\kk{then}·\kk{else}·$[hot]--{/"$\cc<$"[hot]--{/\cc{a},/\cc{b}},/\cc{a},/\cc{b}}} &
        \TREE{/\cc{-}[hot]--{/\cc{+}[hot]--{/\cc{a},/\cc{b}},/\cc{min}[hot]--{/\cc{a},/\cc{b}}}}            &
        \TREE{/\cc{min}[hot]--{/\cc{a},/\cc{min}[hot]--{/\cc{b},/\cc{c}}}}                                  &
        \TREE{/\cc{max}[hot]--{/\cc{a},/\cc{max}[hot]--{/\cc{b},/\cc{c}}}}⏎
      \end{tabular}
    \end{adjustbox}
    \begin{Block}[minipage,width=0.9\columnwidth]{Collateral semantics}
      The PL does not place its own restrictions on the evaluation order.
    \end{Block}
  \end{quote}
\end{frame}

\begin{frame}[fragile]{Intentionally undefined behavior in PLs}
  \squeezeSpace[3]
  \alert{PL designers do not specify everything}
  \begin{itemize}
    \item \emph{If certain patterns are ‟bad” programming practice}…
    \item \emph{If many ‟legitimate” implementation make sense}…
    \item \emph{If different compilers / different architectures may take a performance toll from over-specification}…
  \end{itemize}
  Then, the PL designer will tend to consider specifying ‟undefined behavior”.
  \begin{TWOCOLUMNS}[-0.04\columnwidth]
    \begin{code}{CEEPL}
messy() {¢¢
  int i, n, a[100];
  for (i = 0; i < n; i++)
    printf("a[%d]=%d\n", i, a[i]);
}
    \end{code}
    \MIDPOINT
    \begin{description}[\Java]
      \item [\CPL] Specifying that \kk{auto} variables are zero-initialized may
      cause an unnecessary performance overhead.
      \item [\Java] Advances in compiler technology make it possible for
      the compiler to produce an error message if an uninitialized variable is used.
    \end{description}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile]{Side-effects~$\stackrel{?}{\Longleftrightarrow}$ evaluation order question }
  \squeezeSpace
  \begin{TWOCOLUMNS}
    \begin{itemize}
      \item If expressions have side-effects then there is clearly an evaluation order question
            \begin{code}{CEEPL}
printf("Hello, ") -
    printf("World!\n");
      \end{code}
      \item Question may pop up even if there are no side effects, e.g., the evaluation of the following
            pure-mathematical expression over~$ℝ$ \Red{\[ \arcsin 2 + \frac{√{-1}}{0} ⨉ \lg₂ 0 \]} whose evaluation tree is
    \end{itemize}
    \MIDPOINT
    \begin{Figure}[Evaluation tree for an expression with many faulty subexpressions]
      \centering
      \begin{adjustbox}{}
        \begin{tikzpicture}[every node/.style={draw,circle,inner sep=1pt}]
          \scriptsize
          \node[fill=olive!20] {$+$}
          [level distance=8ex]
          child{%
            node[fill=red!20] {$\arcsin$} child{node[rectangle,thick,minimum width=3ex,
              ,minimum height=3ex,fill=green!20] {$2$}}}
          child{node[fill=olive!20] {$⨉$}
            child{node[fill=olive!20] {$/$}
              child{node[fill=red!20] {$√{}$}
                child{node[fill=blue!20] {$-$}
                  child{node[rectangle,thick,minimum width=3ex,minimum height=3ex,fill=green!20] {$1$}}
              }}
              child{node[rectangle,thick,minimum width=3ex,minimum height=3ex,fill=green!20] {$0$}}}
            child{%
              node[fill=red!20] {$\lg₂$}
              child{node[rectangle,thick,minimum width=3ex,
                ,minimum height=3ex,fill=green!20] {$0$}}}};
          \node[fill=olive!20,label=right:\itshape never evaluated] at (2.5,-1) {*};
          \node[fill=red!20,label=right:\itshape runtime error ] at (2.5,-2) {*};
          \node[fill=blue!20,label=right:\itshape can be evaluated] at (2.5,-3) {*};
          \node[rectangle,fill=green!20,label=right:\itshape literal] at (2.5,-4) {*};
        \end{tikzpicture}
      \end{adjustbox}
    \end{Figure}
    \alert{\small\emph{Depending on the evaluation order, each of the red nodes may trigger a runtime error first}}
  \end{TWOCOLUMNS}
\end{frame}

\subunit{Short-circuit evaluation}

\begin{frame}{More on expressions' evaluation order}
  \squeezeSpace
  \begin{Block}{Example: Rotate 13 Algorithm}
    \centerline{Star Wars Episode V: The Empire Strikes Back (1980)}
  \end{Block}
  \squeezeSpace[6]
  \begin{TWOCOLUMNS}
    \W<+->[0.9]{Spoiler}{Qnegu Inqre vf Yhxr Fxljnyxre'f sngure.}
    \MIDPOINT
    \W<+->[0.9]{Spoiler… Revealed!}{Darth Vader is Luke Skywalker's father.}
  \end{TWOCOLUMNS}
  \uncover<+->{\par\par‟ROT13” encryption: add 13 to-, or subtract 13 from- all letters.\par\par}
  \begin{Block}[minipage,width=0.8\columnwidth]{Algorithm for a ‟rot13” filter}
    \begin{enumerate}\small
      \item Define a conversion table, indexed by characters.
      \item Fill the conversion table for lower and upper case letters.
      \item Read the input: replacing each letter with its conversion table entry.
    \end{enumerate}
  \end{Block}
\end{frame}

\begin{frame}[fragile]{Rotate 13 implementation}
  Is there a bug here?
  \begin{lcode}{PASCAL}
PROGRAM Rotate13(Input, Output);
VAR
(* Define a translation table, indexed by characters. *)
    rot13: Array['A'..'z'] of Char;
    c: char;
(* Fill the translation table for lower and upper case letters: *)
Procedure fill; Begin
  For c := ¢¢'A' to ¢¢'z' do rot13[c] := chr(0);
  For c := ¢¢'A' to ¢¢'M' do rot13[c] := chr(ord(c)+13);
  For c := ¢¢'N' to ¢¢'Z' do rot13[c] := chr(ord(c)-13);
  For c := ¢¢'a' to ¢¢'m' do rot13[c] := chr(ord(c)+13);
  For c := ¢¢'n' to ¢¢'z' do rot13[c] := chr(ord(c)-13);
end;
  \end{lcode}
  \squeezeSpace[-2]
  \begin{Figure}[A \protect\Pascal array with character indices to implement the rotate-13 conversion]
    \begin{adjustbox}{W=1.1}
      \input{rot13.tikz}
    \end{adjustbox}
  \end{Figure}
\end{frame}

\begin{frame}[fragile]{Rotate 13 implementation (cont.)}
  Or maybe here?
  \begin{lCode}{PASCAL}{Encoding/decoding}
(* Replacing letters with their translation table entry: *)
Procedure Convert(Var c: Char); Begin
  If c >= ¢¢'A'
    and
      c <= ¢¢'z'
    and
      rot13[c] <> chr(0)
  then
    c := rot13[c]
end;
  \end{lCode}
  \squeezeSpace[-3]
  \begin{Figure}[Array \ccn{rot13}: full range and forbidden zone]
    \begin{adjustbox}{W=1.1}
      \input{rot13-boundaries.tikz}
    \end{adjustbox}
  \end{Figure}
\end{frame}

\begin{frame}[fragile]{Rotate 13 implementation (main program)}
  Or maybe a bug in here?
  \begin{lCode}{PASCAL}{Main program}
PROGRAM Rotate13(Input, Output);
¢…¢
Begin
  Fill;
  While not eof do begin
    Read(c);
    Convert(c);
    Write(c)
  end
end.
  \end{lCode}
\end{frame}

\begin{frame}[fragile]{Here is the bug!}
  In \Pascal, operator \kk{and} evaluates \Red{all} of its arguments (even if it becomes apparent that these are immaterial for the results)
  \begin{Code}{PASCAL}{Array bounds violation!}
Procedure Convert(Var c: Char); Begin
  If c >= ¢¢'A'
    and
      c <= ¢¢'Z'
    and
      rot13[c] <> chr(0) (* ¢✗¢ *)
  then
    c := rot13[c]
end;
  \end{Code}
  Another annoying (and typical) case…\par
  \begin{code}{PASCAL}
If p <> null and p^.next <> null then ¢…¢
  \end{code}
\end{frame}

\begin{frame}{Eager/strict/applicative evaluation}
  \squeezeSpace
  \WD{Eager (strict) evaluation order}{%
    An
    \Red{eager evaluation order}
    also called
    \Red{strict evaluation order}
    specifies that all arguments to functions are evaluated
    \alert{before} the procedure is applied.
  }
  \begin{itemize}
    \item Also called \Red{applicative order}
    \item Eager order does not specify in which order the arguments are computed; it can be
          \begin{itemize}
            \item unspecified (collateral)
            \item left to right
            \item right to left
            \item …
          \end{itemize}
    \item Most PLs use eager evaluation for
          \begin{itemize}
            \item all functions,
            \item the majority of operators
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Eager \vs short-circuit logical operators}
  \squeezeSpace[5]
  \WD[0.9]{Short-circuit evaluation}{Short-circuit evaluation~$∧$ and~$∨$ prescribes that
    the \nth{2} argument is evaluated only if the \nth{1} argument does not determine the result
    \begin{description}[X Logical ‟and”]
      \item [$∧$\quad \textbf{Logical ‟and”}] Evaluate the second argument only if the first argument is \kk{true}
      \item [$∨$\quad \textbf{Logical ‟or”}]Evaluate the second argument only if the first argument is \kk{false}
    \end{description}
  }
  \begin{table}[H]
    \coloredTable
    \begin{adjustbox}{}
      \begin{tabular}{l*2c}
        \toprule
        \textbf{PL}   & \textbf{Eager version} & \textbf{Short-circuit version}⏎
        \midrule
        \Pascal       & \kk{and}, \kk{or}      & ⏎
        \CPL          &                        & \cc{＆＆}, \cc{||}⏎
        \ML           &                        & \kk{andalso}, \kk{orelse}⏎
        \Eiffel, \Ada & \kk{and}, \kk{or}      & \kk{and then}, \kk{or else}⏎
        \bottomrule
      \end{tabular}
    \end{adjustbox}
    \Caption
  \end{table}
\end{frame}

\begin{frame}[fragile]{Comparing eager and short-circuit semantics}
  \begin{itemize}
    \item Same result, \uncover<+->{\Red{if there are no errors}}
    \item Same computation, \uncover<+->{\Red{if there are no side-effects}}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Programming idioms with short-circuit semantics}
  \begin{uncoverenv}<+->
    Beautiful programming idiom (originated by \Perl, but applicable e.g., in~\CPL)
    \begin{lcode}{CEEPL}
f = fopen(fileName,"r")
      || die("Cannot open file %s\n", fileName);
    \end{lcode}
    And another one:
    \begin{lcode}{CEEPL}
f == (FILE *)0 || ((void)fclose(f), f = (FILE *)0);
    \end{lcode}
  \end{uncoverenv}
  ⚓More tools of the trade
  \begin{description}[\Java/\CPL/\CC]
    \item [\Java/\CPL/\CC] Operator ‟\cc{?}”…‟\cc{:}” (just like \kk{if}†{evaluate only the branch that is required})
    \item [\CPL/\CC] Operator ‟\cc ,”
  \end{description}
  Similar to \Prolog, except that unlike \Prolog, each expression here yields only one result.
\end{frame}

\begin{frame}[fragile]{Clever short circuit evaluation in \Bash}
  \squeezeSpace
  A \Bash program to remove contents of current directory.
  \begin{TWOCOLUMNS}[0ex]
    \begin{code}{BASH}
for f in *; do
  echo -n "¢\X$¢$f: "
  [ -e ¢\X$¢$f ] || echo "already removed"
  [ -d ¢\X$¢$f ] &&
    echo -n "removing directory" &&
      rmdir ¢\X$¢$f && (
        [ -e ¢\X$¢$f ] && echo " ¢¢..¢¢.failed"
          || echo ""
      )
  [ -e ¢\X$¢$f ] &&
      echo -n "moving to /tmp" &&
      mv -f ¢\X$¢$f /tmp && (
        [ -e ¢\X$¢$f ] && echo " ¢¢..¢¢.failed"
          || echo ""
      )
done
    \end{code}
    \MIDPOINT
    \begin{itemize}\small
      \item \Bash commands may \Blue{succeed} or \Red{fail}:
            \begin{itemize}
              \item Success returns \kk{true} (\Blue{integer~$0$})
              \item Failure returns \kk{false} (\Red{error code~$≠0$})
            \end{itemize}
      \item ‟\cc{[}\match]” is a command; it takes arguments; it may
            succeed or fail:
            \begin{description}\footnotesize
              \item [‟\cc{[ -e }~$f$ \cc{]}”]
              3 arguments to command~‟\cc{[}\match]”;
              succeeds if file~$f$ exists
              \item [‟\cc{[ -d }~$f$ \cc{]}”]
              3 arguments to command~‟\cc{[}\match]”;
              succeeds if directory~$f$ exists
            \end{description}
    \end{itemize}
  \end{TWOCOLUMNS}
\end{frame}

\begin{frame}[fragile]{Emulating short-circuit operators with conditional commands}
  \Q<+->{\Red{poor substitute, operators occur in expressions, and expressions are not commands}}
  \begin{uncoverenv}<+->
    \alert{Logical ‟And”}
    \begin{code}{PASCAL}
If p^ <> null then
  If p^.next <> null
    (* some command *)
    \end{code}
    \alert{Logical ‟Or”}
    \begin{code}{PASCAL}
If p^ = null then
  (* command, possibly SKIP *)
else if p^.next <> null then
  (* some command *)
    \end{code}
  \end{uncoverenv}
\end{frame}

\subunit{Normal evaluation order}
\begin{frame}[t]{Normal evaluation order}
  \squeezeSpace[5]
  \WD{Normal evaluation order}{%
    In \Red{normal evaluation order} arguments to functions are evaluated \alert{whenever}
    they are used by the function.
  }
  \par
  \begin{itemize}
    \item Is a generalization of ‟short circuit evaluation”
    \item The terms
          \begin{itemize}
            \item ‟normal evaluation order” and \item ‟normal order evaluation”
          \end{itemize}
          are synonymous.
    \item Can be used to encapsulate any of the following~\CPL operators in functions:
          \begin{itemize}
            \item \cc{＆＆}
            \item \cc{||}
            \item \cc{,}
            \item \cc{? :}
          \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Normal \vs applicative order:}
  \begin{description}[Applicative order]
    \item [Normal order] a ‟symbolic expression”†{‟symbolic expression” is an imprecise name for \emph{$λ$}-expression} is passed;
    this expression is evaluated by the callee when it needs it.
    \item [Applicative order] a ‟symbolic expression” is evaluated by the caller, the result is passed to the callee.
  \end{description}
\end{frame}

\begin{frame}[fragile]{Capitalizing on normal order evaluation}
  A variant of the ‟\cc{die}” programming idiom:
  \begin{lCode}[watermark text=pseudo-\Java,minipage,width=40ex]{JAVA}{Clever ‟\ccn{unless}” function}
static <T> // exists for each type ¢T¢
  T unless(
    boolean b, // ¢\cc{!(}¢precondition¢\cc{)}¢
    ¢\kk{normal}¢ T t, // value
    ¢\kk{normal}¢ Exception e // in case precondition fails
  ) {¢¢
      if (b)
        throw e; // Do not evaluate ¢\cc t¢
      return t; // Do not evaluate ¢\cc e¢
    }
  \end{lCode}
  \begin{itemize}
    \item Parameters marked \kk{normal} are evaluated (only) when used.
    \item If precondition \emph{fails}, then exception \cc{e} is evaluated and then thrown.
    \item If precondition \emph{holds}, then argument \cc{t} is evaluated and then returned.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Function \protect\cc{unless} in pseudo-\Java}
  \squeezeSpace
  Defining:
  \begin{lcode}[watermark text=pseudo-\Java,minipage, width=40ex]{JAVA}
static <T> T unless(boolean b,
  ¢\kk{normal}¢ T t, ¢\kk{normal}¢ Exception e) {¢¢
          if (b) throw e;
            /* else */ return t;
}
  \end{lcode}
  Using:
  \begin{lcode}[watermark text=pseudo-\Java]{JAVA}
// Obtain an arbitrary integer:
final Integer a = readInteger();
// Obtain another arbitrary integer:
final Integer b = readInteger();
// Divide the first by the second (if possible):
final Integer c = unless(
                    b == 0, // always evaluated
                    a / b, // evaluated only if ¢b ¢¢!= 0¢
                    new ArithmeticException(
                       // evaluated only if ¢b == 0¢
                       "Dividing value '" + a + "' by 0!"
                    )
                 );
  \end{lcode}
\end{frame}

\begin{frame}[t,fragile]{New control structures with normal order evaluation}
  \squeezeSpace
  Defining:
  \begin{Code}[watermark text=pseudo-\CPL,minipage, width=56ex]{CEEPL}{Return the ‟last” value of an expression}
int lastValue(
    ¢\kk{normal}¢ int e, // Expression to evaluate
    ¢\kk{normal}¢ bool c // until this condition holds
  ) {¢¢
  // Evaluate expression once:
  const int ¢\V$¢$ = e;
  // If condition fails, recurse:
  return c ¢¢? ¢\V$¢$ : lastValue(e, c);
}
  \end{Code}
  Using:
  \begin{Code}[watermark text=pseudo-\CPL,minipage,width=60ex]{CEEPL}{GCD by repeated execution of expression with side effects}
int gcd(int m, int n) {¢¢
  int s;
  return lastValue(
    (s=m+n,m=min(n,m),n=s-m,n%=m,m), n==0
  );
}
  \end{Code}
\end{frame}

\subunit{Argument passing modes}
\begin{frame}{Call by value \vs call by reference}
  \squeezeSpace[2]
  \begin{Block}[width=0.6\columnwidth,minipage]{Apply a function to a variable}
    \centerline{$f(v)$}
  \end{Block}
  \begin{itemize}
    \item In applicative order,~$v$ is evaluated and passed to~$f$
    \item But, what if~$v$ is a variable?†{If~$v$ is an expression that has no L-Value, then the call~$f(v)$ must be ‟by value”}
          \begin{description}[call by reference is]
            \item [Call by reference]⏏
            \begin{itemize}
              \item Pass the \Red{L-Value} of~$v$.
              \item Follow reference semantics (\cref{\labelPrefix Section:value-reference})
              \item Function~$f$:
                    \begin{itemize}
                      \item \Red{can} assign to~$v$
                      \item can use the value of~$v$
                    \end{itemize}
            \end{itemize}
            \item [Call by value]⏏
            \begin{itemize}
              \item Pass the \Red{R-Value} of~$v$, i.e., a copy of~$v$.
              \item Follow value semantics (\cref{\labelPrefix Section:value-reference}.
              \item Function~$f$:
                    \begin{itemize}
                      \item can \Red{not} assign to~$v$
                      \item can use the value of~$v$
                    \end{itemize}
            \end{itemize}
          \end{description}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Visualizing call by value}
  \squeezeSpace[5]
  \begin{TWOCOLUMNSc}[9ex]
    \begin{TWOCOLUMNS}[-5ex]
      \begin{lCode}{JAVA}{Main}
void main() {¢¢
  int m = 54;
  int n = 24;
  gcd(m,n);
}
      \end{lCode}
      \MIDPOINT
      \begin{lCode}{JAVA}{Greatest common divisor}
int gcd(int m, int n) {¢¢
  if (m == 0) return n;
  if (m > n) return gcd(n, m);
  n %= m;
  return gcd(n, m);
}
      \end{lCode}
    \end{TWOCOLUMNS}
    \squeezeSpace[-2]
    Call chain:
    \[
      \begin{split}
        &\cc{main:} →⏎
        &\qquad \cc{gcd}₁(54,24) →⏎
        &\qquad\qquad\cc{gcd}₂(24,54)→⏎
        &\qquad\qquad\qquad\cc{gcd}₃(6,24)→⏎
        &\qquad\qquad\qquad\qquad\cc{gcd}₄(0,6)=6
      \end{split}
    \]
    \MIDPOINT
    \begin{Figure}
      \begin{adjustbox}{H=0.95}
        \input{visualizing-call-by-value.tikz}
      \end{adjustbox}
    \end{Figure}
  \end{TWOCOLUMNSc}
  \squeezeSpace[-2]
  \Red{Each activation record of \cc{gcd} has its own copy of~\cc{m} and~\cc{n}}
\end{frame}

\begin{frame}[fragile]{Visualizing call by reference}
  \squeezeSpace[5]
  \begin{TWOCOLUMNSc}[9ex]
    \begin{TWOCOLUMNS}[-6ex]
      \begin{lCode}[watermark text={Pseudo \Java}]{JAVA}{As before}
void main() {¢¢
  int m = 54;
  int n = 24;
  gcd(m,n);
}
      \end{lCode}
      \MIDPOINT
      \begin{lCode}[watermark text={Pseudo \Java}]{JAVA}{(Almost) as before}
int gcd(¢\Red{var}¢ int m, ¢\Red{var}¢ int n) {¢¢
  if (m == 0) return n;
  if (m > n) return gcd(n, m);
  n %= m;
  return gcd(n, m);
}
      \end{lCode}
    \end{TWOCOLUMNS}
    Call chain:
    \[
      \begin{split}
        & \cc{main:} →⏎
        &\qquad \cc{gcd}₁(54,24) →⏎
        &\qquad\qquad\cc{gcd}₂(24,54)→⏎
        &\qquad\qquad\qquad\cc{gcd}₃(6,24)→⏎
        &\qquad\qquad\qquad\qquad\cc{gcd}₄(0,6)=6
      \end{split}
    \]
    \MIDPOINT
    \begin{Figure}
      \begin{adjustbox}{H=0.95}
        \input visualizing-call-by-reference.tikz
      \end{adjustbox}
    \end{Figure}
  \end{TWOCOLUMNSc}
  \Red{Each activation record of \kk{gcd} is equipped with references to the original variables~\cc{m} and~\cc{n}}
\end{frame}

\begin{frame}[fragile]{Call by reference in a recursive greatest common divisor}
  \begin{TWOCOLUMNS}[-6ex]
    \begin{lCode}[watermark text={Pseudo \Java}]{JAVA}{}
void main() {¢¢
  int m = 54;
  int n = 24;
  gcd(m,n);
}
    \end{lCode}
    \MIDPOINT
    \begin{lCode}[watermark text={Pseudo \Java}]{JAVA}{}
int gcd(¢\Red{var}¢ int m, ¢\Red{var}¢ int n) {¢¢
  if (m == 0) return n;
  if (m > n) return gcd(n, m);
  n %= m;
  return gcd(n, m);
}
    \end{lCode}
  \end{TWOCOLUMNS}
  \onslide<+->{}
  \begin{table}[H]
    \begin{adjustbox}{}
      \rowcolors{5}{YellowOrange!14}{ProcessBlue!14}
      \begin{tabular}{llrrcc}
        \toprule
        & & \multicolumn2c{\textbf{Main}} & \multicolumn2c{\textbf{Local}}⏎
        \cmidrule(lr){3-4} \cmidrule(lr){5-6}
        \textbf{Call}                                            & \textbf{Action}                                                                   & \cc{m}         & \cc{n}         & \cc{m}               & \cc{n}⏎
        \midrule
        \onslide<+->$\cc{main}:→$                              & \begin{minipage}{12ex}\cc{\kk{int} m = 54;}⏎\cc{\kk{int} n = 24;}\end{minipage} & 54             & 24             & $\cc{m}_{\cc{main}}$ & $\cc{n}_{\cc{main}}$⏎
        \onslide<+->\qquad$\cc{gcd}₁(54,24)→$                &                                                                                   & 54             & 24             & $\cc{m}_{\cc{main}}$ & $\cc{n}_{\cc{main}}$⏎
        \onslide<+->\qquad\qquad$\cc{gcd}₂(24,54)→$          & \cc{n ％= m;}                                                                    & \xcancel{54} 6 & 24             & $\cc{n}_{\cc{main}}$ & $\cc{m}_{\cc{main}}$⏎
        \onslide<+->\qquad\qquad\qquad$\cc{gcd}₃(6,24)→$     & \cc{n ％= m;}                                                                    & 6              & \xcancel{24} 0 & $\cc{m}_{\cc{main}}$ & $\cc{n}_{\cc{main}}$⏎
        \onslide<+->\qquad\qquad\qquad\qquad$\cc{gcd}₄(0,6)=6$ &                                                                                   & 6              & 0              & $\cc{n}_{\cc{main}}$ & $\cc{m}_{\cc{main}}$⏎
        \bottomrule
      \end{tabular}
    \end{adjustbox}
    \Caption
  \end{table}
\end{frame}

\begin{frame}{Other argument passing modes:}
  Modes of applicative order:
  \begin{description}[call by value-result]
    \item [Call by value] a copy of the evaluated argument is passed.
    \item [Call by reference] a reference to the evaluated argument is passed.
    \item [Call by result] no value is passed to callee, on return, value is copied back.
    \item [Call by value-result] call by value plus call by result.
  \end{description}
  We also talk about ‟call by name”, which is a nothing but ‟normal order”
  \begin{description}[call by value-result]
    \item [Call by name] the unevaluated expression is passed as argument, and is computed
    by the callee.
  \end{description}
\end{frame}

\begin{frame}{Implementation of argument passing modes}
  \squeezeSpace
  Implementation of call by …
  \begin{description}
    \item [value] before call, push value onto stack
    \item [reference] before call, push address onto stack
    \item [result] after call, pop value from stack
    \item [value-result] \indentInArticle
    \begin{itemize}
      \item before call, push value onto stack
      \item after call, pop value from stack
    \end{itemize}
    \item [name] with ‟thunks”…
  \end{description}
\end{frame}

\begin{frame}[fragile]{Implementation of ‟call by name”}
  \squeezeSpace[3]
  Think of automatic conversion of
  \begin{code}[watermark text=pseudo-\Java,minipage,width=0.95\columnwidth]{JAVA}
Integer f(Integer a, b) {¢¢
  return unless( // Recall that ¢\cc{unless}¢ is ¢\kk{normal}¢-order in its¢~\nth{2}¢ and¢~\nth{3}¢ arguments.
      b == 0,
      // Convert into a thunk:
      a / b,
      // Convert into a thunk:
      new ArithmeticException("Dividing value '" + a + "' by 0!")
    );
}
  \end{code}
  into
  \begin{code}[watermark text=pseudo-\Java,minipage,width=0.9\columnwidth]{JAVA}
Integer f(Integer a, b) {¢¢
  Integer ¢\N$_$¢1() {¢¢ return a / b; } // ¢\nth{1}¢ thunk implementing ¢\nth{1}¢ argument
  Exception ¢\N$_$¢2() {¢¢ // ¢\nth{2}¢ thunk, implementing ¢\nth{2}¢ argument
    return new ArithmeticException(
      "Dividing value '" + a + "' by 0!");
  }
  return unless(b == 0, ¢\N$_$¢1, ¢\N$_$¢2);
}
  \end{code}
\end{frame}

\begin{frame}{Normal order arguments can be replaced by nested functions}
  \squeezeSpace
  \WD{Thunk}{A \Red{thunk} is a compiler generated nested function that implements a ‟normal” order argument}
  \begin{itemize}
    \item Thunks are only passed as arguments.
    \item Thunks are never returned.
    \item Therefore, inner functions as in \Pascal suffice.
    \item Below we shall see how nested functions are implemented.
    \item To implement ‟\emph{call by reference by name}”, the thunk simulating \kk{normal}
          order arguments, must be able to return references to variables.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Thunks in the \protect\cc{lastValue} example}
  \squeezeSpace
  \begin{Code}[watermark text=pseudo-\CPL,minipage,width=60ex]{CEEPL}{Before conversion to thunks}
int gcd(int m, int n) {¢¢
  int s;
  return lastValue(
    (s=m+n,m=min(n,m),n=s-m,n%=m,m), n==0
  );
}
  \end{Code}
  \begin{Code}[watermark text=pseudo-\CPL,minipage,width=60ex]{CEEPL}{After conversion to thunks}
int gcd(int m, int n) {¢¢
  int s;
  int ¢\N$_$¢1() {¢¢ // ¢\nth{1}¢ thunk implementing ¢\nth{1}¢ argument
    s = m + n; m=min(n,m); n=s-m; n %= m;
    return m;
  }
  int ¢\N$_$¢2() {¢¢ // ¢\nth{2}¢ thunk implementing ¢\nth{2}¢ argument
    return n == 0;
  }
  return lastValue(¢\N$_$¢1, ¢\N$_$¢2);
}
  \end{Code}
\end{frame}

\subunit{Call by need}
\begin{frame}[t,fragile]{Lazy evaluation order = memoization + call-by-name}
  \squeezeSpace[7]
  \WD[0.95]{Lazy evaluation order}{%
    In \Red{lazy evaluation order} arguments to functions are evaluated at most once, \alert{at the first time}
    they are used by the function.
  }
  \WD[0.95]{Call by need}{%
    Call by need is an argument passing mode, in which the arguments are evaluated lazily.
  }
  \begin{itemize}
    \item The term ‟memoization” means ‟caching function results”.
    \item Used in \Haskell (the main feature which distinguishes it from \ML)
    \item Only makes sense in PLs in which there is no ‟program state”
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Memoization}
  \squeezeSpace[6]
  \WD{Memoization}{%
    \Red{memoization} is
    \begin{itemize}
      \item a compiler optimization technique
      \item part of the semantics of certain PLs
    \end{itemize}
    by which results of function applications for certain combination of the arguments are cached.
  }
  \begin{table}[H]
    \coloredTable
    \begin{adjustbox}{}
      \begin{tabular}{lcp{36ex}}
        \toprule
        \textbf{Call}          &                     & \textbf{Note}⏎
        \midrule
        \cc{printf("Hello")}   & \onslide<+(1)-> ✗ & \small \Red{side effects}⏎
        \cc{time()}            & \onslide<+(1)-> ✗ & \small \Red{access global state}⏎
        \cc{eof()}             & \onslide<+(1)-> ✗ & \small \Red{access global state}⏎
        \cc{sin(12)}           & \onslide<+(1)-> ✓ & \small No side effects, no access to global state⏎
        \cc{strlen("Hello, ")} & \onslide<+(1)-> ✓ & \small No side effects, no access to global state⏎
        \cc{strcpy(s,t)}       & \onslide<+(1)-> ✗ & \small \Red{Has side effects.}⏎
        \bottomrule
      \end{tabular}
    \end{adjustbox}
    \Caption[Candidates for memoization]
  \end{table}
\end{frame}

\begin{frame}[fragile]{Memoization in \protect\Haskell}
  Memoization may significantly boost speed.
  \begin{Code}{HASKELL}{Find the~$n$\nthscript{th} Fibonacci number in~$O(n)$ time.}
fib :: Integer -> Integer
fib 0 = 1
fib 1 = 1
fib n = fib (n-1) + fib (n-2)
  \end{Code}
\end{frame}

\subunit{Summary}
\begin{frame}{Concepts of evaluation order}
  \begin{Figure}
    \begin{adjustbox}{}
      \input{concepts-of-order.tikz}
    \end{adjustbox}
  \end{Figure}
\end{frame}

\begin{frame}{Summary: argument passing modes}
  \begin{enumerate}
    \item Call by value
    \item Call by reference
    \item Call by result
    \item Call by value-result
    \item Call by name
    \item Call by need
  \end{enumerate}
\end{frame}

\begin{frame}{Summary: other concepts}
  \begin{itemize}
    \item Side effects
    \item Intentionally undefined behavior
    \item {}$λ$-expressions
    \item {}$λ$-calculus
          \begin{Block}[minipage, width=0.7\columnwidth]{$λ$-calculus}
            A mathematical formalism in which the concepts of this unit can be stated precisely.
          \end{Block}
    \item Memoization
    \item Thunks
  \end{itemize}
\end{frame}

\afterLastFrame

\exercises
\begin{enumerate}
  \item Would it be true to describe ‟\cc{,}”, the comma operator of~\CPL, as following a collateral evaluation order semantics.
  \item Can collateral evaluation order by defined for non-commutative operators? Explain.
  \item Explain why it is impossible to write a \Java function \cc{If} that behaves just like the ternary operator,
        i.e.,
        \begin{equation}
          \cc{If}(＃₁,＃₂,＃₃) \mathrel{\ooalign{\hss?\hss\cr≡}} ＃₁\cc?＃₂\cc:＃₃
          \synopsis{The problem of writing a function that emulates the ternary operator}
        \end{equation}
  \item Adding keywords \kk{normal} and \kk{var} to \Java, implement function \cc{defaultsTo}, such that
        \begin{equation}
          \cc{defaultsTo}(＃₁,＃₂) ≡ \cc{$＃₁$ \cc{=}~$＃₁$ == \kk{null}~?~$＃₂$ \cc{:}$＃₁$}
          \synopsis{A function to set a variable to default value.}
        \end{equation}
  \item Write a program which prints ‟CBVR” if the underlying PL employs
        call by value-result semantics, and ‟CBR” if it employs call by reference semantics.
  \item Write a function that swaps the contents of two integer variables in this PL, or explain why such a function
        cannot be written, assuming that your PL offers only
        \begin{itemize}
          \item call by value,
          \item call by reference,
          \item call by result
          \item call by value-result
          \item call by name
          \item call by need
        \end{itemize}
  \item Make the \cc{rot13} filter of this unit correct by replacing four tokens in it in a single, one character token.
  \item The following is found in the manual of the \CSharp programming language.
        \begin{Block}[minipage,width=0.7\columnwidth]{?? Operator (\CSharp Reference)}
          The \cc{??} operator is called the \emph{null-coalescing operator}.
          It returns the left-hand operand if
          the operand is not null; otherwise it returns the right hand operand.
        \end{Block}
        How is the \cc{??} operator useful? How is it related to the ‟Elvis” operator? Write a function employing call bey name to emulate it.
  \item Write a program that prints the parameter passing mode of the underlying language.
  \item What's the etymology of the name ‟\emph{lambda}” in the
        term~$λ$-functions?
  \item Find this bit of language specification that says~\CPL uses call by value.
  \item Which argument passing mode would you need to implement a function \cc{ForEach} that does
        the same as keyword \kk{foreach} in PLs which support it.
  \item It is said that \Java uses call by reference for object types. Explain why this is not entirely correct.
\end{enumerate}

\references
\begin{itemize}\D{Eager evaluation}{http://en.wikipedia.org/wiki/Eager\_evaluation}
  \D{Evaluation strategy}{http://en.wikipedia.org/wiki/Evaluation\_strategy}
  \D{Lazy evaluation}{http://en.wikipedia.org/wiki/Lazy\_evaluation}
  \D{Memoization}{http://en.wikipedia.org/wiki/Memoization}
  \D{Parameter}{http://en.wikipedia.org/wiki/Parameter\_(computer\_programming)}
  \D{Partial evaluation}{http://en.wikipedia.org/wiki/Partial\_evaluation}
  \D{Referential transparency}{https://en.wikipedia.org/wiki/Referential\_transparency\_(computer\_science)}
  \D{Short-circuit evaluation}{http://en.wikipedia.org/wiki/Short-circuit\_evaluation}
  \D{Thunks}{http://en.wikipedia.org/wiki/Thunk}
\end{itemize}

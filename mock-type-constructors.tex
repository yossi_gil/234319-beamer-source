\begin{frame}{The \protect\Mock type constructors}
  \Mock is not a real PL, so we can go wild:
  \begin{itemize}
    \item idealization.
    \item the ‟abstract” notion behind each type constructor
    \item loop holes in many of the definitions
    \item model for achievement
  \end{itemize}
  In many ways, \ML is a practical version of \Mock.
\end{frame}

\subunit{Power sets}

\begin{frame}{Power sets}
  \squeezeSpace[6]
  \begin{Definition}{Power set type constructor}
    If~$T∈𝕋$ is a type, then so is~$𝒫T$, its \Red{power set}
    comprising all subsets of~$T$, i.e.,
    \begin{equation}
      𝒫 T = ❴ T' \,|\, T' ⊆ T ❵.
      \synopsis{Power set type constructor}
    \end{equation}
  \end{Definition}
  An alternative notation
  \begin{equation}
    𝒫 T = 2^T
    \synopsis{Alternative notation for power sets}
  \end{equation}
  We have,
  \begin{equation}
    ＃(𝒫 T) = 2^{＃T}
    \synopsis{Cardinality of type constructed with the power set type constructor}
  \end{equation}
  where~\Red{$＃T$} denotes the \Red{cardinality} of~$T$.
  \par
  \qq{\Pascal is probably the only language which natively supports this type constructor.}
\end{frame}

\begin{frame}{Definition of type constructors is not enough}
  Types worth nothing without operators/functions
  \begin{itemize}
    \item for \alert{creating} values
    \item for \alert{manipulating} values
  \end{itemize}
\end{frame}

\begin{frame}{Value constructors for power sets}
  Value constructors are operators which take values of~$T$ and return values of type~$𝒫T$
  \begin{itemize}
    \item \alert{Nullary value constructor.} The empty set,~$∅$, is a value of all power sets:
          \begin{equation}
            ∀T:∅∈𝒫 T
            \synopsis{The empty set is a value of all power sets}
          \end{equation}
          in fact,~$∅$ is a literal.
    \item \alert{Unary value constructor.} Let's use the curly brackets:
          \begin{equation}
            ∀v∈T:❴v❵∈𝒫 T
            \synopsis{A unary value constructor for power sets}
          \end{equation}
    \item \alert{$n$-ary value constructor.} Generalizes the unary value constructor:
          \begin{equation}
            ∀v₁,v₂,…,vₙ∈T, n≥1:❴v₁, v₂,…, vₙ❵∈𝒫 T
            \synopsis{An~$n$-ary value constructor for power sets}
          \end{equation}
  \end{itemize}
\end{frame}

\begin{frame}{Defining operators on values of power sets}
  \squeezeSpace
  Let~$v∈T$, and and~$u, u₁, u₂∈𝒫T$ be arbitrary. Then,
  \begin{itemize}
    \item \alert{Testing for membership:}~$v∈u$
    \item \alert{Testing for set equality:}~$u₁=u₂$
    \item \alert{Set union operator:}~$u₁∪u₂∈𝒫T$
    \item \alert{Set intersection operator:}~$u₁∩u₂∈𝒫T$
    \item \alert{Set difference operator:}~$u₁ \setminus u₂∈𝒫T$
    \item \alert{Set complement operator:}~$\overline{u}∈𝒫T$
  \end{itemize}
\end{frame}

\begin{frame}{\Mock does not bother with practical issues}
  \squeezeSpace
  \begin{Block}[minipage,width=0.85 \columnwidth]{The language definition hardship}
    It is easy to define things mathematically, but it takes great language design effort
    to put these in a programming language.
  \end{Block}
  \begin{itemize}
    \item Are power sets really useful?
    \item Can they be implemented efficiently?
    \item How should programmers use their plain keyboards to key in code phrases such as
          ‟$∅$”, ‟$v∈u$”, and ‟$\overline{u}$”?
  \end{itemize}
  Real PLs must deal with these issues.
\end{frame}

\subunit[product-product]{Cartesian product}

\begin{frame}{Cartesian product type constructor}
  \begin{Definition}
    [0.73]
    {Cartesian product type constructor}
    If~$T₁$ and~$T₂$ are types, their \Red{Cartesian product} is
    a type denoted by~$T₁⨉T₂$; values of~$T₁⨉T₂$ are
    \begin{equation}
      T₁⨉T₂ = ❴⟨v₁, v₂⟩\,|\, v₁∈T₁; v₂∈T₂❵.
      \synopsis{Cartesian product type constructor}
    \end{equation}
  \end{Definition}
  Note that
  \begin{equation}
    ＃(T₁⨉T₂) = (＃T₁)⨉(＃T₂)
    \synopsis{Cardinality of type created by Cartesian product type constructor}
  \end{equation}
\end{frame}

\begin{frame}{Operators for product type}
  \squeezeSpace
  \begin{description}[Decomposing]
    \item [Composing] Given~$v₁∈T₁$,~$v₂∈T₂$ the \emph{binary} composition⏎
    operator~\Red{$⟨·,·⟩$} creates a value~$⟨v₁,v₂⟩∈T₁⨉T₂$, i.e.,
    \begin{equation}
      v₁∈T₁, v₂∈T₂⇒⟨v₁,v₂⟩∈T₁⨉T₂
      \synopsis{Operator for composing a value of a product type}
    \end{equation}
    \item [Decomposing] two \emph{unary} decomposition operators
    \begin{itemize}
      \item \Red{$(·)＃1$}
      \item \Red{$(·)＃2$}
    \end{itemize}
    If~$v=⟨v₁,v₂⟩∈T₁⨉T₂$, then
    \begin{itemize}
      \item~$v＃1=v₁$
      \item~$v＃2=v₂$
    \end{itemize}
    Thus, we have
    \begin{equation}
      v∈T₁⨉T₂⇒v＃1∈T₁∧v＃2∈T₂
      \synopsis{Operators for decomposing a value of a product type}
    \end{equation}
  \end{description}
\end{frame}

\begin{frame}{Cartesian products of three or more types}
  The Cartesian product type constructor is easily generalized to more than two types.
  \begin{description}[Commutativity?]
    \item [Commutativity?] Never!
    \item [Associativity?] Depending on the PL semantics:
    \begin{description}[Structural]
      \item [Structural]~$R⨉S⨉T = R⨉(S⨉T) = (R⨉S)⨉T$
      \item [Nominal]~$R⨉S⨉T≠R⨉(S⨉T)≠(R⨉S)⨉T≠R⨉S⨉T$
    \end{description}
  \end{description}
  Nominal semantics is more common.
\end{frame}

\begin{frame}{Equality of types?}
  \begin{Block}[minipage,width=0.7 \columnwidth]{The mathematical equality hardship}
    Even if~$x$ and~$y$ are ‟essentially” the same,
    a fully formal definition may force the claim~$x≠y$.
  \end{Block}
  From a practical point of view, the following two types are equivalent:
  \begin{itemize}
    \item~$T₁⨉T₂⨉T₃$
    \item~$(T₁⨉T₂)⨉T₃$
  \end{itemize}
  Can we write~$T₁⨉T₂⨉T₃=(T₁⨉T₂)⨉T₃$?
  \begin{itemize}
    \item PL tend to be idiosyncratic in their definition of equality.
    \item It takes non-trivial language design effort to make the two types equal.
    \item Many languages don't bother.
  \end{itemize}
\end{frame}

\subunit[integral-exponentiation]{Integral exponentiation}

\begin{frame}{Integral exponentiation}
  Integral exponentiation makes homogeneous tuples:
  a Cartesian product where all the tuple components are chosen
  from the same type.
  \begin{Definition}[0.79]{Integral exponentiation type constructor}
    For a type~$T∈𝕋$ and~$n∈ℕ$,
    the \Red{integral exponentiation} of~$T$ to the power of~$n$,~$Tⁿ$, is defined by
    \begin{equation}
      Tⁿ= \overbrace{T⨉⋯⨉T}^{n \text{\mbox{} times}}
      \synopsis{Integral exponentiation type constructor}
    \end{equation}
  \end{Definition}

  Observe that
  \begin{equation}
    ＃(Tⁿ) = (＃T)ⁿ.
    \synopsis{Cardinality of type created by integral exponentiation type constructor}
  \end{equation}
\end{frame}

\begin{frame}{Operators for integral exponentiation}
  \begin{description}[Decomposition]
    \item [Composition] Given values~$v₁,v₂,…,vₙ∈T$, the
    composition operator~\Red{$[·,…,·]$}
    evaluates to a value~$[v₁,v₂,…,vₙ]∈Tⁿ$
    \item [Decomposition] Given a value~$v=[v₁,…,vₙ]∈Tⁿ$ and an expression~$e$
    of type integer, the~\Red{$v＃e$}, is~$vᵢ$, where~$i$ is the value to
    which~$e$ evaluates.
  \end{description}
  \textbf{Issues:} How should \Mock deal with
  \begin{description}[Required operations]
    \item [Type error]~$e$ is not an integer
    \item [Pseudo type error]~$i<1$ or~$i>n$ (array index underflow/overflow)
    \item [Missing operations] programmers need insertion, appending, merging,
    and many other operations to generate values of type~$Tⁿ$
  \end{description}
\end{frame}

\subunit[unit]{\protect\Unit type}

\begin{frame}{\protect\Unit type}
  \squeezeSpace
  What does one mean by~$n∈ℕ$ in the definition of integral exponentiation?
  For a type~$T∈𝕋$ and
  \begin{itemize}
    \item How should \Mock define~$T¹$? Is~$T¹=T$?
          \begin{description}
            \item [Oops!] Not a very interesting case; PLs make arbitrary decisions.
          \end{description}
    \item What does~$T⁰$ mean? Is it the same for all~$T$?
          \begin{description}
            \item [Yes!] It is a useful and interesting type.
          \end{description}
  \end{itemize}
  \squeezeSpace
  \begin{Definition}[0.72]{The \protect\Unit type}
    The \Red{\Unit type} is~$T⁰$, where~$T$ is some type; alternatively,
    \Unit is a Cartesian product of zero types
  \end{Definition}
  \Unit can be thought of as
  \begin{description}[Composite type]
    \item [Composite type]⏏
    \begin{itemize}
      \item Cartesian product of~$n$ types where~$n=0$
      \item Exponential to the \nth{0} power (where the ‟component” is arbitrary)
    \end{itemize}
    \item [Atomic type]
  \end{description}
\end{frame}

\begin{frame}{Properties of \protect\Unit}
  \begin{itemize}\setlength\abovedisplayskip{2pt plus 1pt minus 1pt}%
    \setlength\belowdisplayskip{2pt plus 1pt minus 1pt}%
    \item \Unit is the \emph{neutral element} of Cartesian product
    \item \Unit is not the empty set, \alert{$\Unit≠∅$.}
          \begin{equation}
            ＃\Unit = 1
            \synopsis{Cardinality of type \Unit}
          \end{equation}
    \item Unit has exactly one value: the \emph{0-tuple}:
          \begin{equation}
            \Unit = ❴ \Red{()} ❵.
            \synopsis{Type \Unit as a singleton set}
          \end{equation}
    \item A \Unit ‟variable” is not really a \emph{variable}, since it can
          store only one value, which can never
          be changed.
    \item ＃bits required to store a vale of type \Unit:
          \begin{equation}
            \lg₂|\Unit|=\lg₂1=0.
            \synopsis{Bits required to represent a value of type \Unit}
          \end{equation}
  \end{itemize}
\end{frame}

\subunit[branding]{Branding}

\begin{frame}{Motivation for branding}
  It is often the case that we want to make a new type of an existing
  type, \emph{without} adding anything new to the type definition:
  \begin{present}[0.8]{The MKSC system of Units}
    \begin{description}[Coulomb]
      \item [\Red{Length}] A real number, designating \emph{meters}
      \item [\Red{Mass}] A real number, designating \emph{kilograms}
      \item [\Red{Time}] A real number, designating \emph{seconds}
      \item [\Red{Coulomb}] A real number, designating \emph{electrical charge}
    \end{description}
  \end{present}
\end{frame}

\begin{frame}{Branding type constructor}
  Let~$𝕀$ be an infinite set of identifiers (often called \emph{labels})
  \begin{Definition}{Branding}
    If~$T∈𝕋$ is a type and~$ℓ∈𝕀$ is label
    then~$ℓ(T)$ is \Red{the~$ℓ$ brand of~$T$} where,
    \begin{equation}
      ℓ(T)=❴⟨ℓ,v⟩\,|\, v∈T❵.
      \synopsis{Branding type constructor}
    \end{equation}
  \end{Definition}
  Characteristics:
  \begin{equation}
    ∀ℓ∈𝕀:T≠ℓ(T)
    \synopsis{The first rule of branding}
  \end{equation}
  \begin{equation}
    ∀ℓ₁,ℓ₂∈𝕀 : ℓ₁≠ℓ₂ ⇔ ℓ₁(T)≠ℓ₂(T)
    \synopsis{The second rule of branding}
  \end{equation}
\end{frame}

\begin{frame}{Operators for branding}
  \begin{description}
    \item [Creation] A value of type~$ℓ(T)$ is created from a value~$v∈T$
    \begin{equation}
      v∈T⇒ℓ(v)∈ℓ(T)
      \synopsis{Operator to create a value of~$ℓ(T)$ from~$v∈T$.}
    \end{equation}
    \item [Extraction] A value~$v∈T$ can be extracted from type~$ℓ(T)$
    \begin{equation}
      ℓ(v)∈T⇒ℓ(v)＃ℓ∈T
      \synopsis{Operator to extract the~$T$ value from a value of~$ℓ(T)$.}
    \end{equation}
  \end{description}
\end{frame}

\subunit{Records}
\begin{frame}{Labeled Cartesian products: records}
  \squeezeSpace
  \begin{description}[Cartesian products]
    \item [Cartesian products] positional access to components:
    \nth{1} component,
    \nth{2} component,
    \nth{3} component,
    \nth{4} component,
    etc.
    \item [Records] Access component by (hopefully, meaningful) name
  \end{description}
  \squeezeSpace
  \begin{Definition}[0.8]{Record type constructor}
    Let \[ ℓ₁,…,ℓₙ∈𝕀, \] for~$n≥0$ be unique labels.
    Let \[ T₁,…,Tₙ \] be types.
    Then,
    \begin{equation*}
      ❴ℓ₁:T₁,…,ℓₙ:Tₙ❵
    \end{equation*}
    is the \Red{record type} induced by the
    labels~$ℓ₁,…,ℓₙ$ and types~$T₁,…,Tₙ$
  \end{Definition}
  Record types can be thought of as product of brands:
  \begin{equation}
    ❴ℓ₁:T₁,…,ℓₙ:Tₙ❵ =ℓ₁(T₁)⨉⋯⨉ℓₙ(Tₙ)
    \synopsis{Record types as product of branded types}
  \end{equation}
\end{frame}

\begin{frame}{Operators for record type}
  Composition and decomposition operators are easy to define…
  \begin{description}[decomposition]
    \item [Composition] values of the record type
    \begin{equation*}
      ❴ℓ₁:T₁,…,ℓₙ:Tₙ❵
    \end{equation*} are created by
    by the~$n$-ary operator \Red{$ ❴ℓ₁:(·), …,ℓₙ:(·)❵$}:
    \begin{equation}
      \begin{split}
        ∀v₁&∈T₁,…,vₙ∈Tₙ:⏎
        & ❴ℓ₁:v₁, …,ℓₙ:vₙ❵∈❴ℓ₁:T₁,…,ℓₙ:Tₙ❵
      \end{split}
      \synopsis{Composition operator to create a value of record type}
    \end{equation}
    \item [Decomposition] Given a value,~$❴ℓ₁:v₁, …,ℓₙ:vₙ❵$,
    the \emph{unary} decomposition operator~\Red{$(·)＃ℓᵢ$}
    (for all~$1≤i≤n$) evaluates to~$vᵢ$:
    \begin{equation}
      ∀i, 1≤i≤n: ❴ℓ₁:v₁, …,ℓₙ:vₙ❵＃ℓᵢ = vᵢ
      \synopsis{Decomposition operator to elicit a field from a record type}
    \end{equation}
  \end{description}
\end{frame}

\subunit{Disjoint union}

\begin{frame}{Union type constructor?}
  \squeezeSpace[5]
  \begin{Definition}[0.72]{Union type constructor???}
    If~$T₁,T₂∈𝕋$ are types, then so is their union,~$T₁∪T₂$.
  \end{Definition}
  \begin{itemize}
    \item \alert{Problem: what if~$T₁$ and~$T₂$ are not disjoint?}
    \item In particular, they may even be equal…
    \item Suppose that value~$v∈T₁$, then~$v$ also belongs to~$T₁ ∪ T₂$, but how
          do we know whether it belongs to~$T₁$ or to~$T₂$?
  \end{itemize}
  The union must be \emph{disjoint}!
  \begin{Block}{}
    \Mock does not have a union type constructor
    †{Since~\CPL has \kk{union} type constructor,~\CPL programmers must be ready to
    deal with the cases~$T∪T$.}
  \end{Block}
\end{frame}

\begin{frame}{Choice types: disjoint union type constructor}
  \squeezeSpace[5]
  \begin{Definition}[0.65]{Choice type}
    If~$T₁,T₂∈𝕋$ are types, then so is
    their \Red{disjoint union},~$T₁+T₂$, defined by the set
    \begin{equation}
      T₁+T₂=ℓ₁(T₁)∪ℓ₂(T₂)
      \synopsis{Disjoint union type constructor}
    \end{equation}
    and where~$ℓ₁,ℓ₂∈𝕀$,~$ℓ₁≠ℓ₂$ are some labels.
  \end{Definition}
  The notation follows from the fact that
  \begin{equation}
    ＃(T₁+T₂)=＃T₁+＃T₂.
    \synopsis{Cardinality of type created by disjoint union}
  \end{equation}
  Labels;
  \begin{itemize}
    \item often called \alert{tags} in the context of disjoint unit
    \item used for telling whether~$v∈T₁ + T₂$ came from~$T₁$ or~$T₂$.
    \item are arbitrary; any~$ℓ₁,ℓ₂$ will do as long as~$ℓ₁≠ℓ₂$
  \end{itemize}
\end{frame}

\begin{frame}{Type equality hardship, again}
  \squeezeSpace
  \begin{itemize}
    \item Branding is similar to \Red{disjoint union of one type}.
    \item However, the support of branding in PLs is usually \Red{distinct}
          than that of disjoint union.
  \end{itemize}
  \alert{Enumerated types} are similar to disjoint unions:
  \squeezeSpace
  \begin{Definition}{Enumerated type constructor}
    If~$ℓ₁,ℓ₂,…,ℓₙ∈𝕀$,~$n≥1$ are labels, then \[ ❴ℓ₁,ℓ₂,…,ℓₙ❵ \] is an
    \Red{enumerated type}, whose values are \[ℓ₁,ℓ₂,…,lₙ\]
  \end{Definition}
  An enumerated type can be thought of as a disjoint union of branded \Unit types:
  \begin{equation}
    ❴ℓ₁,ℓ₂,…,lₙ❵ =ℓ₁(\Unit) +ℓ₂(\Unit) +⋯+ℓₙ(\Unit)
    \synopsis{Enumerated type as a disjoint union of branded \Unit{}s}
  \end{equation}
\end{frame}

\begin{frame}{Operators for choice type}
  \squeezeSpace
  Let~$T₁,T₂$ be types
  \begin{description}[Decomposing]
    \item [Creation] Use the~\Red{$ℓ₁⟨·⟩$} and \Red{$ℓ₂⟨·⟩$} operators
    \begin{equation}
      \begin{split}
        v₁∈T₁ &⇒ℓ₁⟨v₁⟩∈T₁+T₂⏎
        v₂∈T₂ &⇒ℓ₂⟨v₂⟩∈T₁+T₂
      \end{split}
      \synopsis{Making values of a choice type}
    \end{equation}
    \item [Checking] Use the~\Red{$?ℓ₁$} and~\Red{$?ℓ₂$} unary operators
    \begin{equation}
      v∈T₁ + T₂⇒
      v?ℓ₁ =
      \begin{cases}
        \kk{true}  & v=ℓ₁⟨v₁⟩, v₁∈T₁⏎
        \kk{false} & v=ℓ₂⟨v₂⟩, v₂∈T₂
      \end{cases}
      \synopsis{Testing choice types}.
    \end{equation}
    \q{\hfill Operator \Red{$?ℓ₂$} is defined similarly}
    \item [Decomposing] Given~$v=ℓ₁⟨v₁⟩∈T₁+T₂$, the unary decomposition operator~\Red{$＃ℓ₁$}
    evaluates to~$v₁∈T₁$.
    \q{\hfill Operator \Red{$＃ℓ₂$} is defined similarly}
  \end{description}
  \squeezeSpace[4.5]
  \W[0.55]{Type error}{when evaluating \Red{$⟨v⟩＃ℓ₁$} in the case that~$v=ℓ₂(v₂)$,~$v₂∈T₂$}
\end{frame}

\subunit{Type \protect\None and type \protect\Any}

\begin{frame}{The \protect\None type}
  \squeezeSpace[6]
  \begin{Definition}{The \protect\Bottom type}
    Type \None, also known as
    \Bottom, and often denoted~$⊥$, is the empty set,
    i.e.,
    \begin{equation}
      \None≡\Bottom≡⊥≡∅.
      \synopsis{The empty type}
    \end{equation}
  \end{Definition}
  Obviously,
  \begin{equation}
    ＃⊥=0
    \synopsis{Size of the empty type}
  \end{equation}
  \alert{Type~$⊥$ is\dots}
  \begin{itemize}
    \item derived from the choice constructor just as \Unit is derived from Cartesian product
          \Q{… the neutral element of the choice type constructor}
    \item Cardinality is zero!
          \Q{… no legal values}
  \end{itemize}
\end{frame}

\begin{frame}{The \protect\Any type}
  \squeezeSpace
  \begin{Definition}[0.82]{The \Any type}
    Type \Any, also known as \All, or \Top,
    and often denoted~$⊤$, is the universal set, i.e.,
    for a language~$𝓛$ with values' universe~$𝕍_{𝓛}$,
    \begin{equation}
      \Any≡⊤=𝕍_{𝓛}.
      \synopsis{The universal type}
    \end{equation}
  \end{Definition}
  Type~$⊤$: The type of any arbitrary value that~$𝓛$ may generate
  \begin{TWOCOLUMNS}
    \begin{equation}
      \begin{split}
        ∀T∈𝕋:\, ⊤⨉T & ⊆ & ⊤⏎
        ∀T∈𝕋:\, ⊥⨉T & = & ⊥
      \end{split}
      \synopsis{Cartesian product of arbitrary type and~$⊤$ and~$⊥$}
    \end{equation}
    \MIDPOINT
    \begin{equation}
      \begin{split}
        ∀T∈𝕋:\, ⊤+T & = & ⊤⏎
        ∀T∈𝕋:\, ⊥+T & = & T
      \end{split}
      \synopsis{Disjoint union of arbitrary type with~$⊤$ and~$⊥$}
    \end{equation}
  \end{TWOCOLUMNS}
\end{frame}

\subunit[mapping-types]{Mapping types}

\begin{frame}{Mappings \vs partial-mapping}
  \squeezeSpace[6]
  \begin{Definition}[0.9]{Mapping}
    A \emph{mapping} (also called \emph{function}) from
    a set~$S$ to a set~$T$ is a set \[ m ⊂S⨉T, \] that associates precisely one 
    value of~$T$ with each value of~$S$, i.e.,
  \begin{equation}
    ∀s∈S:\big|❴t \,|\, (s,t)∈m❵\big|=1
    \synopsis{Full mapping}
  \end{equation}
  \end{Definition}
  \squeezeSpace[1]
  \begin{Definition}[0.9]{Partial mapping}
    We say that the set \[ m⊂S⨉T \] is a \emph{partial mapping} (\emph{partial function}),
  from set~$S$ to set~$T$ if it never associates more than value
  of~$T$ with any value of~$S$, i.e.,
  \begin{equation}
    ∀s∈S:\big|❴t \,|\, (s,t)∈m❵\big|≤1
    \synopsis{Partial mapping}
  \end{equation}
  \end{Definition}
  If~$m$ is a partial mapping, there there may be members
  of~$S$ for which~$m$ associates no members of~$T$.
\end{frame}

\begin{frame}{Mapping ＆ partial mapping}
  \begin{Definition}[0.8]{Mapping (partial mapping) type constructor}
    Let~$T$ and~$S$ be types. 
    Then, \Red{(partial) mapping from~$S$ to~$T$}, 
      denoted~$S→T$ (sometimes also~$T^S$) is
    \begin{equation}
      S→T=❴m\,|\,\text{$m$ is a (partial) mapping from~$S$ to~$T$}❵.
      \synopsis{Mapping type constructor}
    \end{equation}
  \end{Definition}
  Ideally,
  \begin{equation}
    𝒫T = T→\kk{Boolean}
    \synopsis{Power set type constructor as a kind of mapping}
  \end{equation}
\end{frame}

\begin{frame}{Mapping is similar to exponentiation}
  We have,
  \begin{equation}
    ＃(S→ T) = ＃T^{＃S}.
    \synopsis{Cardinality of type created by mapping type constructor}
  \end{equation}
  Suppose that we write the
  \begin{equation}
    S→ T = T^S
    \synopsis{Mapping as exponentiation}
  \end{equation}
  Then, currying
  \begin{equation}
    (S₁ ⨉ S₂) → T = S₁ → (S₂ → T)
    \synopsis{Currying}
  \end{equation}
  looks very much like the exponential identity
  \begin{equation}
    T^{S₁·S₂} = \left(T^{S₂}\right)^{S₁}.
    \synopsis{Currying in algebra: power to product is power of power}
  \end{equation}
\end{frame}

\begin{frame}{Mapping and integral exponentiation}
  \squeezeSpace
  Let~$𝗻$ denote the type obtained by taking the~$n$ sized subrange of the integer type
  \begin{equation}
    𝗻= 1,…, n.
    \synopsis{Integer range type constructor}
  \end{equation}
  Then, the mapping~$𝗻→ ℝ$
  which is the type of a real array in \Fortran†{Unlike~\CPL, the first index
    of a \Fortran array is 1} is isomorphic to~$ℝⁿ$, i.e.,
  \begin{equation}
    𝗻→ ℝ = ℝⁿ
    \synopsis{Integral exponentiation type constructor}
  \end{equation}
  \Q{Ain't it fortunate that we allow ourselves to write the
  mapping~$𝗻→ ℝ$ also as~$ℝ^𝗻$?}
  Note that
  \begin{equation}
    \Unit =𝟭.
    \synopsis{Type \Unit as special case of integer range}
  \end{equation}
  and,
  \begin{equation}
    \None =𝟬.
    \synopsis{Type \None as special case of integer range}
  \end{equation}
\end{frame}

\begin{frame}{Unifying equation}
  Euler's equation involving the five most important constants of mathematics
  \begin{equation}
    e^{i π} + 1 = 0.
    \synopsis{Euler's identity, tying together the five fundamental mathematical constants}
  \end{equation}
  The type theory equivalent
  \begin{equation}
    ⊤^⊥ =𝟭.
    \synopsis{Identity tying together~$⊤$,~$⊥$, and \Unit}
  \end{equation}
  There is precisely one function that maps the \None type to the \All type.
  (This function is empty, but should we care?)
\end{frame}

\subunit[recursive-type-constructors]{Recursive type constructor}

\begin{frame}{Recursive type constructor}
  \squeezeSpace[6]
  \begin{Definition}[0.95]{Recursive type definition}
    \begin{itemize}
      \item  Let $T₁,…,Tₘ$ be some fixed types.
      \item Let $τ₁,…,τₙ$ be \emph{type unknowns}.
      \item Let $E₁, … Eₙ$  be type expressions involving types~$T₁,…,Tₘ$ and unknowns~$τ₁,…,τₙ$.
      \item Then, the system of equations
            \begin{equation}
              \begin{split}
                τ₁ & = & E₁(T₁,…,Tₘ,τ₁,…,τₙ)⏎
                & ⋮⏎
                τₙ & = & Eₙ(T₁,…,Tₘ,τ₁,…,τₙ)
              \end{split}
              \synopsis{Recursive definitions type constructor}
            \end{equation}
            defines new types~$τ₁,…,τₙ$ as the ‟minimal” solution of this system.
    \end{itemize}
  \end{Definition}
\end{frame}

\begin{frame}{Specifics of the definition of the ``recursive type constructor''}
  What is allowed in the ‟type expressions” \[ E₁, … Eₙ?  \]
  \squeezeSpace
  \begin{itemize}
    \item Cartesian product
    \item Disjoint union
    \item Mapping
          \item~$⋮$
    \item Not all expressions are allowed
    \item \alert{We will not deal with the specifics here.}
  \end{itemize}
  Kinds of recursive type constructor:
  \begin{description}
    \item[Simple] $n=1$, create a single type
    \item[General] $n\ge1$, create multiple types
  \end{description}
\end{frame}

\begin{frame}{Bottom up solution of recursive type equations}
  \squeezeSpace
  Consider the case~$n=1$, i.e., an equation with only one type variable,~$σ$
  \begin{equation}
    σ = E(T₁,…,Tₘ,σ)
    \synopsis{A recursive equation with only one type variable}
  \end{equation}
  then, we build the following approximations,
  \begin{equation}
    \begin{split}
      σ₀ & =∅= ⊥⏎
      σ₁ & = E(T₁,…,Tₘ,σ₀) = E(T₁,…,Tₘ,⊥)⏎
      σ₂ & = E(T₁,…,Tₘ,σ₁) = E(T₁,…,Tₘ,E(T₁,…,Tₘ,⊥))⏎
      & ⋮⏎
      σ_{n+1} & = E(T₁,…,Tₘ,σₙ) = E(T₁,…,Tₘ,E(T₁,…,Tₘ,σ_{n-1}))⏎
      & ⋮⏎
      ⏎
      σ & = \bigcup_{i=0}^∞σᵢ.
      \synopsis{Bottom up solution of recursive equation with only one type variable}
    \end{split}
  \end{equation}
\end{frame}

\begin{frame}[fragile=singleslide]{Solving the list recursive equation}
  \begin{code}{EMEL}
datatype intlist = nil | cons of int * intlist
  \end{code}
  For brevity,
  \begin{enumerate}
    \item let~$τ$ denote the unknown,
    \item write~$ℤ$ instead of \cc{int}
    \item write~$μ$ instead of \cc{nil}
    \item write~$\varsigma$ instead of \cc{cons}
  \end{enumerate}
  \begin{eqnarray*}
    τ& = &μ + \cc{\varsigma}(ℤ⨉τ)⏎
    & = &μ + \cc{\varsigma}\bigg(ℤ⨉ \big(μ + \cc{\varsigma}\big(ℤ⨉τ)\big)\bigg)⏎
    & = &μ + \cc{\varsigma}(ℤ⨉μ) + \cc{\varsigma}\big(ℤ⨉ \cc{\varsigma}(ℤ⨉τ)\big)⏎
    & = &μ + \cc{\varsigma}(ℤ⨉μ) + \cc{\varsigma}\bigg(ℤ⨉\cc{\varsigma}(ℤ⨉μ)\bigg) + ⋯⏎
  \end{eqnarray*}
\end{frame}

\begin{frame}[fragile=singleslide]{Does it converge?}
  Many (boring) theorems and (even more boring) proofs regarding
  \begin{itemize}
    \item ‟convergence”
    \item uniqueness of solution
    \item independence in the order of application
    \item proper structure of~$E₁,…, Eₙ$
  \end{itemize}
  Not discussed here
\end{frame}

\begin{frame}[fragile]{Taylor series ‟solution” of recursive type equations}
  \squeezeSpace[6.5]
  \begin{equation}
    \cc t = (1 + \cc T) = 1 + ℤ* \cc t * \cc t
    \synopsis{Recursive type equation for \cc{t}, the type of a pointer to a node in a doubly-linked-list}
  \end{equation}
  \squeezeSpace[2]
  \newcommand\codeWidth{14ex}
  \begin{TWOCOLUMNSc}[\codeWidth - 0.5\columnwidth]
    \begin{code}{CEEPL}
// Type alias:
typedef
// for an
// incomplete type:
  struct T *t;
¢¢
// completing the
// type definition
struct T {¢¢
  int data;
  t left;
  t right;
};
    \end{code}
    \MIDPOINT
    \squeezeSpace
    \begin{Figure}[{Using \protect\Mathematica to solve the quadratic equation~$τ=1+ℤτ²$ for~$τ$, the recursive binary tree type}]
      \begin{adjustbox}{H=0.79}
        \includegraphics{Graphics/taylor-binary-tree.png}
      \end{adjustbox}
    \end{Figure}
  \end{TWOCOLUMNSc}
\end{frame}

\begin{frame}[fragile]{Understanding the Taylor series expansion}
  \squeezeSpace
  \begin{equation}
    τ=1+ℤ+2ℤ²+5ℤ³+14ℤ⁴+42ℤ⁵+⋯
    \synopsis{Taylor expansion of the ‟negative” solution of the quadratic equation~$τ=1+ℤτ²$ defining~$τ$}
  \end{equation}
  \begin{Figure}[Distinct topologies of binary trees with with~$n$ nodes;~$n=0,1,2,3$; each node stores an integer~$i∈ℤ$]
    \begin{adjustbox}{W=0.95}
      \input{binary-trees.tikz}
    \end{adjustbox}
  \end{Figure}
\end{frame}

\subunit{Summary}

\begin{frame}{Taxonomy of the type constructors of \protect\Mock}
  \squeezeSpace[3]
  \begin{Figure}
    \begin{adjustbox}{max height=0.6\paperheight}
      \input{type-constructors-taxonomy.tikz}
    \end{adjustbox}
  \end{Figure}
  \squeezeSpace[2]
  \begin{Block}[width=0.7\columnwidth,minipage]{Theory \vs practice}
    Type constructors in real PLs are not so elegant; their shape is invariably a
    \alert{compromise} between many conflicting practical demands.
  \end{Block}
\end{frame}

\begin{frame}[fragile]{Operators for the type constructors of \protect\Mock}
  \squeezeSpace
  \newcommand\tr{\bfseries\centering}
  \newcommand\tc{\itshape}
  \renewcommand{\@}[2][l]{\begin{tabular}{@{}#1@{}}#2\end{tabular}}
  \begin{table}[H]
    \coloredTable
    \begin{adjustbox}{H=0.44}
      \begin{tabular}{l*3c}
                  \toprule
                  \tr \@[c]{Type         \\ constructor} &
                  \tr \@[c]{Notation} &
                  \tr \@[c]{Value         \\ composition} &
                  {\bfseries\@[c]{Value         \\ decomposition}}⏎
                  \midrule
                  \tc Power set                     & $𝒫T$                                                                                                    &
                    \@{$∅$         \\~$❴·,·,…,·❵$}          & ⏎
                  \tc Product                       & $T₁⨉T₂⨉⋯⨉Tₙ$                                                                                 & $⟨·,·,⋯,·⟩$                       & $·＃1,·＃2,…,·＃n$⏎
                  \tc \@{Integral         \\Exponentiation} & $Tⁿ$                                                                                                     & $[·,·,⋯,·]$                           & $·＃e$⏎
                  \tc Branding                      & $ℓ(T)$                                                                                                   & $ℓ(·)$                                  & $·＃ℓ$⏎
                  \tc Records                       & $❴ℓ₁:T₁,ℓ₂:T₂,…,ℓₙ:Tₙ❵$                                                            & $❴·:ℓ₁,·:ℓ₂, …,·:ℓₙ❵$ & $·＃ℓᵢ$⏎
                  \tc \@{Disjoint         \\Union}         & \@[c]{$T₁+T₂+⋯+Tₙ$\\~$ℓ₁(T₁)∪ℓ₂(T₂)∪ ⋯∪ℓₙ(Tₙ)$}                         & $ℓᵢ(·)$                               & \@{$·?ℓᵢ$\\~$·＃ℓᵢ$}⏎
                  \tc Mapping                       & \@{$S→T$         \\$T^S$}                                                                                        &                                            & ⏎
                  \tc \@{Subrange}                  & $𝗻$                                                                                                     &                                            & ⏎
                  \tc \@{Simple         \\Recursive}        & $τ=E(τ,T₁,…,Tₙ)$                                                                                   &                                            & ⏎
                  \tc \@{Multiple         \\Recursive}      & \@{$τ₁=E₁(T₁,…,Tₘ,τ₁,…,τₙ)$\\$⋮$\\$ τₙ = Eₙ(T₁,…,Tₘ,τ₁,…,τₙ)$} &                                            & ⏎
        \bottomrule
      \end{tabular}
    \end{adjustbox}
    \Caption[Taxonomy of the type constructors of \Mock]
  \end{table}
\end{frame}

\afterLastFrame

\references
\begin{itemize}
  %
  \D{Algebraic types}{https://en.wikipedia.org/wiki/Algebraic\_types}
  \D{Bottom type}{http://en.wikipedia.org/wiki/Product\_type}
  \D{Currying}{https://en.wikipedia.org/wiki/Currying}
  \D{Disjoint union}{https://en.wikipedia.org/wiki/Disjoint\_union}
  \D{Empty type}{https://en.wikipedia.org/wiki/Empty\_type}
  \D{Function type}{https://en.wikipedia.org/wiki/Function\_type}
  \D{Nested function}{http://en.wikipedia.org/wiki/Nested\_function}
  \D{Recursive data type}{https://en.wikipedia.org/wiki/Recursivedata\_type}
  \D{Set type constructor}{http://en.wikipedia.org/wiki/Set\_(abstract\_data\_type)}
  \D{Tagged union}{https://en.wikipedia.org/wiki/Tagged\_union}
  \D{Top type}{http://en.wikipedia.org/wiki/Top\_type}
  \D{Tuple types}{https://en.wikipedia.org/wiki/C％2B％2B11＃Tuple\_types}
  \D{Type system}{https://en.wikipedia.org/wiki/Type\_system}
  \D{Unit type}{https://en.wikipedia.org/wiki/Unit\_type}
  \D{Void safety}{http://en.wikipedia.org/wiki/Void\_safety}
  \D{Void type}{http://en.wikipedia.org/wiki/Void\_type}
\end{itemize}
\exercises
\begin{enumerate}
  \item What's the type of the composition operator for tuple type? and, for record type?
  \item Define a set of operators for decomposing power sets in \Mock.
  \item Define a set of operators for mappings in \Mock.
  \item Why is that the disjoint union type constructor does not have a positional equivalent,
        in the same manner that the record type constructor has a positional equivalent in the form
        of tuple type constructor?
  \item Explain why it makes sense that PLs for matrix, vector and other mathematical computation
        would not distinguish between a value of an array of
        of length 1 and a scalar value.
  \item Can you explain why arrays with a variety of index types (as in \Pascal and \Ada) seem to have vanished?
  \item Recall the notion of type errors: the expression~$v＃3$ is a type error when~$v∈ℓ(T)$.
        Which type errors can mappings produce?
  \item Why doesn't \Eiffel have a ‟Choice” type?
  \item Provide an example†{i.e., a specific type in a specific PL and perhaps even values}
        for a composite type whose values are atomic?
  \item Provide an example†{ditto} for a atomic type whose values are composite?
  \item Provide an example†{ditto} for a composite type whose values are composite? Can you tell whether the
        values are composed in the same way that the type is composed?
  \item Why \Java has no ‟Choice” type?
  \item Explain why languages designed for string processing
        would not distinguish between an array of length~$1$ of characters
        and a scalar value.
  \item Explain how it is possible to view functions with more than one parameter
        in, e.g.., \Pascal, as taking a tuple type argument. Refer both to
        the definition of the function and its invocation.
  \item What is the difference between type aliasing and type branding?
  \item Explain why many examples in this unit mention variables,
        despite the fact that this course did not discuss variables yet?
\end{enumerate}
